desc "import pharma data from csv files"

task :import_csv_data => :environment do
  Company.copy_from "public/data-files/companies.csv", :format => :utf8
  Brand.copy_from "public/data-files/brands.csv", :format => :utf8
  Drug.copy_from "public/data-files/drugs.csv", :format => :utf8
  BrandDrug.copy_from "public/data-files/brand_drugs.csv", :format => :utf8
  IcdCode.copy_from "public/data-files/icd10.csv", :format => :utf8
  p "data imported successfully"
end

task :import_csv_narowal_data => :environment do
  Company.copy_from "public/data-files/Narowal_companies.csv", :format => :utf8
  Brand.copy_from "public/data-files/Narowal_brands.csv", :format => :utf8
  Drug.copy_from "public/data-files/Narowal_drugs.csv", :format => :utf8
  BrandDrug.copy_from "public/data-files/Narowal_brand_drugs.csv", :format => :utf8
  p "n_data imported successfully"
end

task :import_csv_bahawalnagar_data => :environment do
  Company.copy_from "public/data-files/Bahawalnagar_companies.csv", :format => :utf8
  Brand.copy_from "public/data-files/Bahawalnagar_brands.csv", :format => :utf8
  Drug.copy_from "public/data-files/Bahawalnagar_drugs.csv", :format => :utf8
  BrandDrug.copy_from "public/data-files/Bahawalnagar_brand_drugs.csv", :format => :utf8
  p "bahawalnagar_data imported successfully"
end

task :set_category_medicine => :environment do 
  Brand.update_all(category: "medicine")
  p "Brands category updated successfully"
  Drug.update_all(category: "medicine")
  p "Drugs category updated successfully"
  BrandDrug.update_all(category: "medicine")
  p "BrandDrugs category updated successfully"
end

task :import_generic_item_csv_data => :environment do
  GenericItem.copy_from "public/data-files/generic_04_items.csv", :format => :utf8
  p "data imported successfully"
end

task :make_icd10_codes_txt_to_csv => :environment do
  begin
    p "** conversion started to csv **"
    File.open("public/data-files/icd10.txt") do |in_file|
      File.open("public/data-files/icd10.csv", 'w') do |out_file|
        in_file.each {|line| out_file << line.squeeze(' ').sub(' ', ',') }
      end
    end
    p "** icd10 codes successfully converted to csv **"   
  rescue Exception => e
    p "****** #{e.message} *******"
  end
end