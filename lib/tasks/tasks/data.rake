namespace :data do
  desc "TODO"
  task :generate, [:n]  => [:environment] do |t, args|
    medical_units = []
    medical_units << MedicalUnit.where(title: 'Mayo Hospital').first
    medical_units << MedicalUnit.where(title: 'Sir Ganga Ram Hospital').first
    medical_units << MedicalUnit.where(title: 'Services Hospital').first

    referred_to  = ['Emergency', 'Inpatient', 'OutPatient', 'Checkup']
    visitreasons = ['Injury', 'Diarrhea', 'Pregnancy', 'Congestion', 'Headache', 'Chest Pain', 'Ear Pain/Infection', 'Respiration', 'Dental']
    modeofconveyances = ['walk-in', 'Public Transport', 'Private Transport', '1122', 'Hospital ambulance', 'Private ambulance']
    
    fdo = Role.where(title: 'fdo').first
    ms = Role.where(title: 'MS').first
    
    medical_units.each do |medical_unit|
      user = User.create!(username: Faker::Internet.user_name, 
                 email: Faker::Internet.email, 
                 role_id: fdo.id, 
                 password: Faker::Internet.password, 
                 first_name: Faker::Name.first_name, 
                 last_name: Faker::Name.last_name, 
                 phone: Faker::PhoneNumber.phone_number, 
                 nic: Faker::Number.between(1, 10), 
                 dob: Date.new)

      user2 = User.create!(username: Faker::Internet.user_name, 
                 email: Faker::Internet.email, 
                 role_id: ms.id, 
                 password: Faker::Internet.password, 
                 first_name: Faker::Name.first_name, 
                 last_name: Faker::Name.last_name, 
                 phone: Faker::PhoneNumber.phone_number, 
                 nic: Faker::Number.between(1, 10), 
                 dob: Date.new)
     MedicalUnitUser.create!(medical_unit_id: medical_unit.id , user_id: user.id, role_id: fdo.id) 
     MedicalUnitUser.create!(medical_unit_id: medical_unit.id , user_id: user2.id, role_id: ms.id) 
    end

    

    (-1..5).each do |date|
      puts date
      puts '----------------'
      (args[:n].to_i).times do
        medical_unit = medical_units[Faker::Number.between(0,2)]
        referr = referred_to[Faker::Number.between(0,2)]
        reason = visitreasons[Faker::Number.between(0,8)]
        mode   = modeofconveyances[Faker::Number.between(0,5)]

        patient = Patient.create!(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, gender: 'male',
                   patient_nic: Faker::Number.number(10), guardian_nic: Faker::Number.number(10),
                   patient_passport: Faker::Number.between(1, 10),
                   guardian_passport: Faker::Number.between(1, 10),
                   birth_day: Faker::Number.between(1, 30),
                   birth_month: Faker::Number.between(3, 12),
                   birth_year: Faker::Number.between(1970, 2015),
                   unidentify_patient: false,
                   guardian_relationship: Faker::Name.name,
                   guardian_first_name: Faker::Name.first_name,
                   guardian_last_name: Faker::Name.last_name,
                   unidentify_guardian: false,
                   marital_status: 'Married', state: Faker::Address.state,
                   city: Faker::Address.city, near_by_city: Faker::Address.city,
                   address1: Faker::Address.street_address,
                   address2: Faker::Address.secondary_address, phone1_type: 'mobile',
                   phone1: Faker::PhoneNumber.cell_phone, phone2_type: 'mobile',
                   phone2: Faker::PhoneNumber.cell_phone, phone3_type: 'phone',
                   phone3: Faker::PhoneNumber.phone_number,
                   registered_at: medical_unit.id,
                   registered_by: medical_unit.users.where(role_id: fdo.id).first.id,
                   created_at: date.days.ago,
                   updated_at: date.days.ago
                   )
        
        patient.visits.create(referred_to: referr, 
                              reason: reason, 
                              mode_of_conveyance: mode,
                              ref_department: 'ICU',
                              medical_unit_id: medical_unit.id, 
                              patient_id: patient.id, 
                              user_id: medical_unit.users.where(role_id: fdo.id).first.id,
                              created_at: date.days.ago,
                              updated_at: date.days.ago
                              )
      end
    end
  end
end
