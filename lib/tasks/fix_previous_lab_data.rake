namespace :fix_previous_lab_data do
  task :fix_investigation_id, [:file_name] => [:environment] do |t, args|
    require 'csv'
    file_path = Rails.root + args[:file_name]
    csv_file = File.read(file_path)
    csv = CSV.parse(csv_file, :headers => true)
    csv.each do |row|
      hospital = MedicalUnit.find_by(title: row["hospital"])
      dept = Department.find_by(title: hospital.title+" Lab", medical_unit_id: hospital.id, parent_id: nil)
      lab_section = dept.sub_departments.find_by(title: hospital.title+' '+row["lab_section"].downcase, medical_unit_id: hospital.id, department_code: row["lab_section_code"])
      lit = LabInvestigationType.find_by(title: row["lab_section"])
      next if lit.blank?
      if "years".include?(row["date_unit"].downcase)
        age_start = row["age_start"].to_i * 365
        age_end = row["age_end"].to_i * 365
      elsif "months".include?(row["date_unit"].downcase)
        age_start = row["age_start"].to_f * (365.0/12.0)
        age_end = row["age_end"].to_f * (365.0/12.0)
      else
        age_start = row["age_start"].to_i
        age_end = row["age_end"].to_i
      end
      p row
      if hospital.present? && lit.present? && lab_section.present?
        p "1111111111111111111"
        li = LabInvestigation.find_by(medical_unit_id: hospital.id, profile_name: row["profile_name"], lab_investigation_type_id: lit.id, profile_code: row["profile_code"], status: "true", department_id: lab_section.id,report_type: row["report_type"])
        p "22222222222222222"
        next if li.blank?
        la = li.lab_assessments.find_by(parameter_code: row["parameter_code"])
        p "333333333333333333333333333"
        next if la.blank?
        lar = la.lab_access_ranges.find_by(gender: row["gender"], age_start: age_start.round, age_end: age_end.round, date_unit: 'days', start_range: row["start_range"], end_range: row["end_range"], range_title: row["range_title"], status: "true", lab_investigation_id: nil)
        if lar.blank?
          lar = la.lab_access_ranges.new(gender: row["gender"], age_start: age_start.round, age_end: age_end.round, date_unit: 'days', start_range: row["start_range"], end_range: row["end_range"], range_title: row["range_title"], status: "true", lab_investigation_id: nil)
          lar.lab_investigation_id = li.id
          lar.medical_unit_id = hospital.id
          lar.save
        else
          lar.update_attributes(lab_investigation_id: li.id, medical_unit_id: hospital.id)
        end
      end
    end
  end

  task :remove_duplicate_ranges  => [:environment] do |t, args|
    sql = "SELECT DISTINCT on (gender, age_start, age_end, date_unit, start_range, end_range, range_title, status, heading, lab_assessment_id, medical_unit_id, lab_investigation_id) id FROM lab_access_ranges;"
    ranges = ActiveRecord::Base.connection.execute(sql)
    range_ids = ranges.values.flatten!
    LabAccessRange.all.each do |range|
      unless range_ids.include?(range.id.to_s)
        range.delete
      end
    end
  end
end