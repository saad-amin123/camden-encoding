#!/bin/bash
# This file is meant to be executed via systemd.
source /usr/local/rvm/scripts/rvm
source /etc/profile.d/rvm.sh
export ruby_ver=$(rvm list default string)

export CONFIGURED=yes
export TIMEOUT=50
export APP_ROOT=/home/rails/api-pitb
export RAILS_ENV="production"
#export GEM_HOME="/home/rails/api-pitb/vendor/bundle"
#export GEM_PATH="/home/rails/api-pitb/vendor/bundle:/usr/local/rvm/gems/${ruby_ver}:/usr/local/rvm/gems/${ruby_ver}@global"
#export PATH="/home/rails/api-pitb/vendor/bundle/bin:/usr/local/rvm/gems/${ruby_ver}/bin:${PATH}"

# Passwords
export SECRET_KEY_BASE=839d8d3861156216a65e9fba4b7a94a91b254f08c92ca34f0931a6470e717789eb5efea7879d8d58bd32cedccceaad00ab3ea410be4db00b96c8c878ac9a2f4c
export APP_DATABASE_PASSWORD=clu3t0x@123

# Execute the unicorn process
/home/rails/api-pitb/vendor/vendor/bundle/bin/unicorn \
        -c /etc/unicorn.conf -E production --debug
