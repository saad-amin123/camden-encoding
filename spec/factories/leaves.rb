# == Schema Information
#
# Table name: leaves
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  medical_unit_id :integer
#  department_id   :integer
#  attendance      :string
#  shift           :string
#  date            :date
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :leafe, class: 'Leave' do
    user nil
    medical_unit nil
    department nil
    attendance "MyString"
    shift "MyString"
    date "2016-10-25"
  end
end
