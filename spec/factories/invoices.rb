# == Schema Information
#
# Table name: invoices
#
#  id                  :integer          not null, primary key
#  user_id             :integer
#  medical_unit_id     :integer
#  patient_id          :integer
#  visit_id            :integer
#  assignable_id       :integer
#  assignable_type     :string
#  total_amount        :string
#  discount_percentage :string
#  discount_value      :string
#  total_discount      :string
#  payable             :string
#  paid                :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

FactoryGirl.define do
  factory :invoice do
    user nil
    medical_unit nil
    patient nil
    visit nil
    assignable nil
    total_amount "MyString"
    discount_percentage "MyString"
    discount_value "MyString"
    total_discount "MyString"
    payable "MyString"
    paid "MyString"
  end
end
