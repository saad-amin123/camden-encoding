# == Schema Information
#
# Table name: consultation_order_details
#
#  id          :integer          not null, primary key
#  order_id    :integer
#  department  :string
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  note_id     :integer
#

FactoryGirl.define do
  factory :consultation_order_detail do
    description 'We have invited you'
    department 'Surgery'
    order
  end
end
