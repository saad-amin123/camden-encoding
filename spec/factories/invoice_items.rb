# == Schema Information
#
# Table name: invoice_items
#
#  id              :integer          not null, primary key
#  invoice_id      :integer
#  assignable_id   :integer
#  assignable_type :string
#  invoice_type    :string
#  description     :string
#  amount          :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :invoice_item do
    invoice nil
    assignable nil
    invoice_type "MyString"
    description "MyString"
    amount "MyString"
  end
end
