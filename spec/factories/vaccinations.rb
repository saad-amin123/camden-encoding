# == Schema Information
#
# Table name: vaccinations
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  patient_id      :integer
#  visit_id        :integer
#  medical_unit_id :integer
#  name            :string
#  given_on        :date
#  follow_up       :date
#  instruction     :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :vaccination do
    user nil
    patient nil
    visit nil
    medical_unit nil
    name "MyString"
    given_on "2017-05-11"
    follow_up "2017-05-11"
    instruction "MyString"
  end
end
