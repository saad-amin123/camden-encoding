# == Schema Information
#
# Table name: preferences
#
#  id         :integer          not null, primary key
#  department :string
#  category   :string
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :preference do
    department "MyString"
    category "MyString"
    user
  end
end
