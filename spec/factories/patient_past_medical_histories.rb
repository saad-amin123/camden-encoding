# == Schema Information
#
# Table name: patient_past_medical_histories
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  patient_id      :integer
#  visit_id        :integer
#  medical_unit_id :integer
#  med_name        :string
#  note            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :patient_past_medical_history do
    user nil
    patient nil
    visit nil
    medical_unit nil
    med_name "MyString"
    note "MyString"
  end
end
