# == Schema Information
#
# Table name: admissions
#
#  id                     :integer          not null, primary key
#  medical_unit_id        :integer
#  visit_id               :integer
#  patient_id             :integer
#  referred_from          :integer
#  referred_to            :integer
#  ward_id                :integer
#  doctor_id              :integer
#  user_id                :integer
#  attendent_name         :string
#  attendent_mobile       :string
#  attendent_address      :string
#  attendent_nic          :string
#  attendent_relationship :string
#  status                 :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  admission_number       :string
#

FactoryGirl.define do
  factory :admission do
    medical_unit_id 1
    visit_id 1
    parent_id 1
    patient_id 1
    referred_from 1
    referred_to 1
    ward_id 1
    doctor_id 1
    user_id 1
    attendent_name "MyString"
    attendent_mobile "MyString"
    attendent_address "MyString"
    attendent_nic "MyString"
    attendent_relationship "MyString"
    status "MyString"
  end
end
