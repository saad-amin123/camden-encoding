# == Schema Information
#
# Table name: user_wards
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  ward_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :user_ward do
    user_id 1
    ward_id 1
  end
end
