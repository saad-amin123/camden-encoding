# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  role_id                :integer
#  username               :string           default(""), not null
#  first_name             :string
#  last_name              :string
#  title                  :string
#  phone                  :string
#  nic                    :string
#  dob                    :date
#  father_name            :string
#  father_nic             :string
#  mother_name            :string
#  mother_nic             :string
#  address1               :string
#  authentication_token   :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default("0"), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  failed_attempts        :integer          default("0"), not null
#  unlock_token           :string
#  locked_at              :datetime
#  address2               :string
#  department_id          :integer
#

# spec/factories/user.rb
FactoryGirl.define do
  factory :user do
    # sequence(:username) { |n| "username#{n}" }
    username do 
      loop do
        possible_name = Faker::Name.name
        break possible_name unless User.exists?(username: possible_name)
      end
    end
    first_name Faker::Name.first_name
    last_name { Faker::Name.last_name }
    phone Faker::PhoneNumber.phone_number
    nic Faker::Number.number(10)
    dob '1980-01-29'
    # sequence(:email) { |n| "test#{n}@example.com" }
    email do 
      loop do
        possible_email = Faker::Internet.email
        break possible_email unless User.exists?(email: possible_email)
      end
    end
    password Faker::Internet.password(8)
    association :role
  end
end
