# == Schema Information
#
# Table name: bed_allocations
#
#  id                      :integer          not null, primary key
#  medical_unit_id         :integer
#  patient_id              :integer
#  admission_id            :integer
#  ward_id                 :integer
#  bay_id                  :integer
#  bed_id                  :integer
#  status                  :string           default("Free")
#  patient_last_updated_by :integer
#  user_id                 :integer
#  visit_id                :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  comment                 :string
#

FactoryGirl.define do
  factory :bed_allocation do
    medical_unit_id 1
    patient_id 1
    addmission_id 1
    ward_id 1
    bay_id 1
    bed_id 1
    status false
    patient_last_updated_by "MyString"
    user_id "MyString"
  end
end
