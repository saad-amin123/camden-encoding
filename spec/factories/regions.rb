# == Schema Information
#
# Table name: regions
#
#  id         :integer          not null, primary key
#  category   :string
#  name       :string
#  parent_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# spec/factories/device.rb
FactoryGirl.define do
  factory :region do |f|
  	f.id 1
    f.category 'tehsil'
    f.name 'Gilgit'
    f.parent_id 4
  end
end
