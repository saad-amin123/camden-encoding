# == Schema Information
#
# Table name: beds
#
#  id              :integer          not null, primary key
#  bed_number      :string
#  bay_id          :integer
#  ward_id         :integer
#  medical_unit_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  facility_id     :integer
#  deleted_at      :datetime
#  status          :string           default("Free")
#  bed_code        :string
#

FactoryGirl.define do
  factory :bed do
    bed_number "MyString"
    bay_id 1
    ward_id 1
    medical_unit_id 1
  end
end
