# == Schema Information
#
# Table name: orders
#
#  id                        :integer          not null, primary key
#  patient_id                :integer
#  doctor_id                 :integer
#  status                    :string
#  medical_unit_id           :integer
#  device_id                 :integer
#  updated_by                :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  department                :string
#  category                  :string
#  order_type                :string           default("Medicine")
#  visit_id                  :integer
#  cancel_reason             :string
#  cancel_reason_details     :string
#  store_id                  :integer
#  department_id             :integer
#  case_number               :string
#  external_medicine         :string
#  barcode_url               :string
#  lab_order_sequence_number :string
#

FactoryGirl.define do
  factory :order do
    association :patient
    status "Open"
    order_type "Medicine"
    category "Inpatient"
    department "Cardiology"
    cancel_reason 'Test Not Possible'
    cancel_reason_details 'Test Not Possible'
    association :medical_unit
    association :device
    association :doctor, :factory => :user
    updated_by 1
    association :visit
  end
end
