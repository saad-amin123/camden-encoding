# == Schema Information
#
# Table name: lab_access_ranges
#
#  id                   :integer          not null, primary key
#  gender               :string
#  age_start            :integer
#  age_end              :integer
#  date_unit            :string
#  start_range          :float
#  end_range            :float
#  range_title          :string
#  status               :string
#  heading              :string
#  lab_assessment_id    :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  medical_unit_id      :integer
#  lab_investigation_id :integer
#

FactoryGirl.define do
  factory :lab_access_range do
    
  end
end
