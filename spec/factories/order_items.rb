# == Schema Information
#
# Table name: order_items
#
#  id                   :integer          not null, primary key
#  order_id             :integer
#  brand_drug_id        :integer
#  quantity_prescribed  :integer
#  frequency_unit       :string
#  quantity_calculated  :integer
#  issued_brand_drug_id :integer
#  notes                :text
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  duration_unit        :string
#  duration             :integer
#  type                 :string
#  quantity             :float
#  frequency            :string
#  prn                  :boolean
#  route                :string
#  note_id              :integer
#  dosage               :string
#  dosage_unit          :string
#  strength             :string
#

FactoryGirl.define do
  factory :order_item do
    order_id 1
    brand_drug_id 1
    quantity_prescribed 1
    frequency 1
    frequency_unit "MyString"
    duration "MyString"
    quantity_calculated 1
    issued_brand_drug_id 1
    notes "MyText"
  end
end
