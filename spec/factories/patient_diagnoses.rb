# == Schema Information
#
# Table name: patient_diagnoses
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  name         :string
#  code         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  note_id      :integer
#  type         :string
#  visit_id     :integer
#  patient_id   :integer
#  instructions :text
#  plan         :text
#

FactoryGirl.define do
  factory :patient_diagnosis do
    association :visit
    association :patient
    association :user
    name Faker::Name.name
    code do 
      loop do
        possible_code = Faker::Number.number(10)
        break possible_code unless PatientDiagnosis.exists?(code: possible_code)
      end
    end
  end
end
