# == Schema Information
#
# Table name: lookups
#
#  id         :integer          not null, primary key
#  category   :string
#  key        :string
#  value      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  parent_id  :integer
#

# spec/factories/lookup.rb
FactoryGirl.define do
  factory :lookup do |f|
    f.id 1
    f.category 'complaint_category'
    f.key 'Other'
    f.value 'Other'
    f.parent_id 1
  end
end
