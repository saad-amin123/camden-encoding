# == Schema Information
#
# Table name: data_files
#
#  id              :integer          not null, primary key
#  medical_unit_id :integer
#  user_id         :integer
#  file_url        :string
#  status          :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :data_file do
    medical_unit_id 1
    user_id 1
    file_url "MyString"
    status "MyString"
  end
end
