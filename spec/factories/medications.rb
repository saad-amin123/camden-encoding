# == Schema Information
#
# Table name: medications
#
#  id              :integer          not null, primary key
#  patient_id      :integer
#  user_id         :integer
#  medical_unit_id :integer
#  visit_id        :integer
#  brand_drug_id   :integer
#  dosage          :string
#  unit            :string
#  route           :string
#  frequency       :string
#  usage           :string
#  instruction     :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  type            :string
#

FactoryGirl.define do
  factory :medication do
    dosage Faker::Number.decimal(2)
    unit 'mg'
    route 'PO'
    frequency 'Once'
    usage 'Week'
    instruction Faker::Lorem.characters(150)
    brand_drug
  end
end
