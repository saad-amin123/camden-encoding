# == Schema Information
#
# Table name: bays
#
#  id              :integer          not null, primary key
#  medical_unit_id :integer
#  department_id   :integer
#  parent_id       :integer
#  title           :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  ward_id         :integer
#  beds_count      :integer          default("0")
#  deleted_at      :datetime
#  bay_code        :string
#

FactoryGirl.define do
  factory :bay do
    medical_unit_id 1
    department_id ""
    parent_id 1
    title "MyString"
  end
end
