# == Schema Information
#
# Table name: line_items
#
#  id               :integer          not null, primary key
#  order_id         :integer
#  brand_drug_id    :integer
#  quantity         :integer
#  category         :string
#  expiry           :date
#  medical_unit_id  :integer
#  device_id        :integer
#  inventory_id     :integer
#  order_type       :string
#  status           :string
#  description      :text
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  user_id          :integer
#  reason           :string
#  order_item_id    :integer
#  transaction_type :string
#  store_id         :integer
#  department_id    :integer
#  batch_id         :integer
#  visit_id         :integer
#

FactoryGirl.define do
  factory :line_item do
    order_id 1
    brand_drug_id 1
    quantity 1
    category 'MyString'
    user_id 1
    expiry '2016-02-26'
    medical_unit_id 1
    device_id 1
    name 'MyName'
    inventory_id 1
  end
end
