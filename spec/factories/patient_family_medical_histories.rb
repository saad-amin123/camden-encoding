# == Schema Information
#
# Table name: patient_family_medical_histories
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  patient_id      :integer
#  visit_id        :integer
#  medical_unit_id :integer
#  note            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :patient_family_medical_history do
    user nil
    patient nil
    visit nil
    medical_unit nil
    note "MyString"
  end
end
