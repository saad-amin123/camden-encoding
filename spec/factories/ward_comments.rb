# == Schema Information
#
# Table name: ward_comments
#
#  id                :integer          not null, primary key
#  medical_unit_id   :integer
#  user_id           :integer
#  patient_id        :integer
#  visit_id          :integer
#  admission_id      :integer
#  bed_allocation_id :integer
#  comments          :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

FactoryGirl.define do
  factory :ward_comment do
    patient_id 1
    admission_id 1
    bed_allocation_id 1
    comments "MyString"
  end
end
