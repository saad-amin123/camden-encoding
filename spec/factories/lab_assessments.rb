# == Schema Information
#
# Table name: lab_assessments
#
#  id                     :integer          not null, primary key
#  title                  :string
#  uom                    :string
#  specimen               :string
#  status                 :string
#  result_type1           :string
#  result_type2           :string
#  medical_unit_id        :integer
#  parameter_code         :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  machine_code           :string
#  machine_parameter_code :string
#  machine_name           :string
#

FactoryGirl.define do
  factory :lab_assessment do
    
  end
end
