# == Schema Information
#
# Table name: icd_codes
#
#  id         :integer          not null, primary key
#  code       :string
#  name       :string
#  created_at :datetime
#  updated_at :datetime
#

FactoryGirl.define do
  factory :icd_code do
    code "MyString"
    name "MyString"
  end
end
