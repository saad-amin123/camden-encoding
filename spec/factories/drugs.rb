# == Schema Information
#
# Table name: drugs
#
#  id             :integer          not null, primary key
#  name           :string
#  overview       :text
#  characterstics :text
#  indications    :text
#  contradictions :text
#  interactions   :text
#  interference   :text
#  effects        :text
#  risk           :text
#  warning        :text
#  storage        :text
#  category       :string
#  created_at     :datetime
#  updated_at     :datetime
#

FactoryGirl.define do
  factory :drug do
    name 'MyString'
    overview 'MyText'
    category 'Mycategory'
    characterstics 'MyText'
    indications 'MyText'
    contradictions 'MyText'
    interactions 'MyText'
    interference 'MyText'
    effects 'MyText'
    risk 'MyText'
    warning 'MyText'
    storage 'MyString'
  end
end
