# == Schema Information
#
# Table name: companies
#
#  id         :integer          not null, primary key
#  name       :string
#  address    :text
#  phone      :string
#  fax        :string
#  created_at :datetime
#  updated_at :datetime
#

FactoryGirl.define do
  factory :company do
    name 'MyString'
    address 'MyText'
    phone 'MyString'
    fax 'MyString'
  end
end
