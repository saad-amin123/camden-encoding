# == Schema Information
#
# Table name: visit_histories
#
#  id              :integer          not null, primary key
#  referred_to     :string
#  ref_department  :string
#  medical_unit_id :integer
#  referred_by     :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  visit_id        :integer
#

FactoryGirl.define do
  factory :visit_history do
    referred_to "MyString"
    ref_department "MyString"
    visit_id 1
    hospital_id 1
    referred_by 1
    patient_mrn "12345678"
  end
end
