require 'rails_helper'
require 'rspec/rails'

RSpec.describe Api::V1::FavoriteComplaintsController, type: :controller do
  let(:user) { FactoryGirl.create(:user) }
  let(:medical_unit) { FactoryGirl.create(:medical_unit) }
  let(:role) { FactoryGirl.create(:role) }
  let(:device) { FactoryGirl.create(:device) }
  let(:complaint) { FactoryGirl.create(:complaint) }
  describe 'ADD #fav_complaint' do
    context 'should add fav_complaint with valid data' do
      before do
        post 'add', { auth_token: user.authentication_token, device_id: device.uuid, complaint_id: complaint.id }
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not add fav_complaint  with invalid auth_token' do
      before do
        post 'add', { auth_token: '9746ggkfgfytu793', device_id: device.uuid, complaint_id: complaint.id }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not add fav_complaint  with invalid device' do
      before do
        post 'add', { auth_token: user.authentication_token, device_id: '3796397', complaint_id: complaint.id }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /unidentify device/ }
    end
    context 'should not add fav_complaint  with invalid complaint_id' do
      before do
        post 'add', { auth_token: user.authentication_token, device_id: device.uuid, complaint_id: '236936' }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid complaint id/ }
    end
  end
  describe 'REMOVE #fav_complaint' do
    before do
        post 'add', { auth_token: user.authentication_token, device_id: device.uuid, complaint_id: complaint.id }
    end
    context 'should remove fav_complaint with valid data' do
      before do
        delete 'remove', { auth_token: user.authentication_token, complaint_id: complaint.id }
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not remove fav_complaint  with invalid auth_token' do
      before do
        delete 'remove', { auth_token: '9746ggkfgfytu793', complaint_id: complaint.id }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not remove fav_complaint  with invalid complaint_id' do
      before do
        delete 'remove', { auth_token: user.authentication_token, complaint_id: '236936' }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid complaint id/ }
    end
  end
end
