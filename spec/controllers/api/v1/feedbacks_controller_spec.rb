require 'rails_helper'
require 'rspec/rails'

RSpec.describe Api::V1::FeedbacksController, type: :controller do
  let(:user) { FactoryGirl.create(:user) }
  let(:medical_unit) { FactoryGirl.create(:medical_unit) }
  let(:role) { FactoryGirl.create(:role) }
  let(:device) { FactoryGirl.create(:device) }
  let(:complaint) { FactoryGirl.create(:complaint) }

  describe 'CREATE #feedback' do
    context 'should create feedback with valid data' do
      before do
        post 'create', { feedback: FactoryGirl.attributes_for(:feedback, satisfaction: 'yes', status: 'open'), auth_token: user.authentication_token, device_id: device.uuid, complaint_id: complaint.id, medical_unit_id: medical_unit.id }
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not create feedback with invalid authentication_token' do
      before do
        post 'create', { feedback: FactoryGirl.attributes_for(:feedback, satisfaction: 'yes', status: 'open'), auth_token: '6946974vhkdgkhd', device_id: device.uuid, complaint_id: complaint.id }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match 'invalid authentication token' }
    end
    context 'should not create feedback with invalid device' do
      before do
        post 'create', { feedback: FactoryGirl.attributes_for(:feedback, satisfaction: 'yes', status: 'open'), auth_token: user.authentication_token, device_id: '8638963', complaint_id: complaint.id }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match 'unidentify device' }
    end
    context 'should not create feedback with invalid complaint_id' do
      before do
        post 'create', { feedback: FactoryGirl.attributes_for(:feedback, satisfaction: 'yes', status: 'open'), auth_token: '6946974vhkdgkhd', device_id: device.uuid, complaint_id: '2344222' }
      end
      it { expect(response.status).to eq(401) }
    end
  end

  describe 'Search #feedback' do
    before do
      post 'create', { feedback: FactoryGirl.attributes_for(:feedback, satisfaction: 'yes', status: 'open'), auth_token: user.authentication_token, device_id: device.uuid, complaint_id: complaint.id, medical_unit_id: medical_unit.id  }
    end
    context 'should Search feedback with valid data' do
      before do
        get 'index', { auth_token: user.authentication_token, complaint_id: complaint.id }
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not Search feedback with invalid authentication_token' do
      before do
        get 'index', { auth_token: 'hsfkhfhkfkw79', complaint_id: complaint.id }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match 'invalid authentication token' }
    end
    context 'should not Search feedback with invalid complaint_id' do
      before do
        get 'index', { auth_token: user.authentication_token, complaint_id: '258528' }
      end
      # it { expect(response.status).to eq(401) }
    end
  end
end
