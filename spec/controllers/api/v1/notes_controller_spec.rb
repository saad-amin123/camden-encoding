require 'rails_helper'
require 'rspec/rails'

RSpec.describe Api::V1::NotesController, type: :controller do
  let(:user) { FactoryGirl.create(:user)}
  let(:device) { FactoryGirl.create(:device)}
  let(:medical_unit) { FactoryGirl.create(:medical_unit)}
  let(:patient) { FactoryGirl.create(:patient)}
  let(:visit) { FactoryGirl.create(:visit, patient: patient, medical_unit: medical_unit, user_id: user.id)}
  describe 'CREATE #note' do
    context 'should create note with valid data' do
      before do
        post 'create', { note: FactoryGirl.attributes_for(:note, department: 'Cardio', type: "AdmissionNote",chief_complaint: "Pain in his chest",physical_exam:"high blood pressure",plan:"admission in cardio", patient_diagnoses: [FactoryGirl.attributes_for(:patient_diagnosis, code: "DD1234", name: "itchy-eyes")]), auth_token: user.authentication_token,device_id: device.uuid,medical_unit_id: medical_unit.id, patient_id: patient.mrn, visit_id: visit.id }
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not create note with invalid auth_token' do
      before do
        post 'create', { note: FactoryGirl.attributes_for(:note, department: 'Cardio'), auth_token: 'gkegkegkegkgekgekhe', visit_id: visit.id, patient_id: patient.mrn,device_id: device.uuid,medical_unit_id: medical_unit.id }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not create patients with invalid patient' do
      before do
        post 'create', { note: FactoryGirl.attributes_for(:note, department: 'Cardio'), auth_token: user.authentication_token, visit_id: visit.id, patient_id: '7529922752',device_id: device.uuid,medical_unit_id: medical_unit.id }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid patient mrn/ }
    end
    context 'should not create patients with invalid visit' do
      before do
        post 'create', { note: FactoryGirl.attributes_for(:note, department: 'Cardio'), auth_token: user.authentication_token, visit_id: "69769", patient_id: patient.mrn,device_id: device.uuid,medical_unit_id: medical_unit.id}
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid visit id/ }
    end
  end
  describe 'LOGS #note' do
    user = FactoryGirl.create(:user)
    device = FactoryGirl.create(:device)
    medical_unit = FactoryGirl.create(:medical_unit)
    patient = FactoryGirl.create(:patient)
    visit = FactoryGirl.create(:visit, patient_id: patient.id, medical_unit_id: medical_unit.id, user_id: user.id)
    note = FactoryGirl.create(:note, visit_id: visit.id, created_at: "2016-04-25", type: "AdmissionNote")
    context 'should return notes with valid data' do
      before do
        get 'logs', type: "AdmissionNote", auth_token: user.authentication_token,device_id: device.uuid,medical_unit_id: medical_unit.id, patient_id: patient.mrn, visit_id: visit.id, start_date: "2016-04-22", end_date: "2016-04-30"
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not return notes with invalid auth_token' do
      before do
        get 'logs', type: "AdmissionNote", auth_token: "jdnjsdv11111",device_id: device.uuid,medical_unit_id: medical_unit.id, patient_id: patient.mrn, visit_id: visit.id
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not return notes with invalid device' do
      before do
        get 'logs', type: "AdmissionNote", auth_token: user.authentication_token,device_id: "fvf2222",medical_unit_id: medical_unit.id, patient_id: patient.mrn, visit_id: visit.id
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /unidentify device/ }
    end
    context 'should return notes with invalid visit' do
      before do
        get 'logs', type: "AdmissionNote", auth_token: user.authentication_token,device_id: device.uuid,medical_unit_id: medical_unit.id, patient_id: patient.mrn, visit_id: "dddc1122"
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid visit id/ }
    end
  end
  describe 'PREVIOUS_NOTE #note' do
    context 'should return note with valid data' do
      before do
        FactoryGirl.create(:note, department: 'Cardio', visit_id: visit.id)
        get 'previous_note', department: 'Cardio', auth_token: user.authentication_token,device_id: device.uuid,medical_unit_id: medical_unit.id, patient_id: patient.mrn, visit_id: visit.id
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not return note with invalid auth_token' do
      before do
        get 'previous_note', department: 'Cardio', auth_token: "kfvkvmkvmfkvmfk",device_id: device.uuid,medical_unit_id: medical_unit.id, patient_id: patient.mrn, visit_id: visit.id
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not return note with invalid visit' do
      before do
        get 'previous_note', department: 'Cardio', auth_token: user.authentication_token,device_id: device.uuid,medical_unit_id: medical_unit.id, patient_id: patient.mrn, visit_id: "kmfk555"
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid visit id/ }
    end
  end
end
