require 'rails_helper'
require 'rspec/rails'

RSpec.describe Api::V1::RadiologyOrderDetailsController, type: :controller do
  let(:user) { FactoryGirl.create(:user) }
  let(:medical_unit) { FactoryGirl.create(:medical_unit) }
  let(:role) { FactoryGirl.create(:role) }
  let(:device) { FactoryGirl.create(:device) }
  let(:order) { FactoryGirl.create(:order) }
  describe 'UPDATE #radiology_order_detail' do
    let(:radiology_order_detail) { FactoryGirl.create(:radiology_order_detail,area: "square",imaging_type: "X-Ray",order_id: order.id) }
    context 'should update radiology_order_detail with valid data' do
      before do
        put :update, id: radiology_order_detail.id, radiology_order_detail: FactoryGirl.attributes_for(:radiology_order_detail, note: 'Testing radiology order detail', result: 'normal result'), auth_token: user.authentication_token, device_id: device.uuid, order_id: order.id
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not update radiology_order_detail with invalid authentication_token' do
      before do
        put :update, id: radiology_order_detail, radiology_order_detail: FactoryGirl.attributes_for(:radiology_order_detail, note: 'Testing radiology order detail', result: 'normal result'), auth_token: 'yeoyeouyoeu', device_id: device.uuid, order_id: order.id
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not update radiology_order_detail with invalid device id' do
      before do
        put :update, id: radiology_order_detail, radiology_order_detail: FactoryGirl.attributes_for(:radiology_order_detail, note: 'Testing radiology order detail', result: 'normal result'), auth_token: user.authentication_token, device_id: '39753973', order_id: order.id
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /unidentify device/ }
    end
    context 'should not update radiology_order_detail with valid order_id' do
      before do
        put :update, id: radiology_order_detail, radiology_order_detail: FactoryGirl.attributes_for(:radiology_order_detail, note: 'Testing radiology order detail', result: 'normal result'), auth_token: user.authentication_token, device_id: device.uuid, order_id: '93779359'
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid order id/ }
    end
  end
end
