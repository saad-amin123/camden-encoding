require 'rails_helper'
require 'rspec/rails'

RSpec.describe Api::V1::OrdersController, type: :controller do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:medical_unit) { FactoryGirl.create(:medical_unit) }
  let!(:department) { FactoryGirl.create(:department,medical_unit: medical_unit) }
  let!(:device) { FactoryGirl.create(:device) }
  let!(:patient) { FactoryGirl.create(:patient) }
  let!(:visit) { FactoryGirl.create(:visit, patient_id: patient.id, medical_unit: medical_unit, user_id: user.id) }
  let!(:order) { FactoryGirl.create(:order, visit: visit, doctor_id: user.id, medical_unit: medical_unit, department_id: department.id) }
  
  describe 'RADIOLOGY_ORDER #order' do
    context 'should create radiology orders with valid data' do
      before do
        post 'radiology_order', { order: FactoryGirl.attributes_for(:order, order_type: 'Radiology',status: 'Open',category: 'Inpatient',department: 'Cardiology', cancel_reason: 'Test Not Possible',cancel_reason_details: 'Test Not Possible'), auth_token: user.authentication_token, device_id: device.uuid, patient_id: patient.mrn, medical_unit_id: medical_unit.id, visit_id: visit.id, radiology_order_details: [FactoryGirl.attributes_for(:radiology_order_detail, area: "square", imaging_type: "X-Ray", result: 'normal result')] }
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not create orders with invalid auth_token' do
      before do
        post 'radiology_order', { order: FactoryGirl.attributes_for(:order, order_type: 'Radiology'), auth_token: 'gkegkegkegkgekgekhe', device_id: device.uuid, patient_id: patient.mrn, medical_unit_id: medical_unit.id  }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not create orders with invalid device' do
      before do
        post 'radiology_order', { order: FactoryGirl.attributes_for(:order, order_type: 'Radiology'), auth_token: user.authentication_token, device_id: 'gwkhkwh', patient_id: patient.mrn, medical_unit_id: medical_unit.id  }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /unidentify device/ }
    end
    context 'should not create orders with invalid patient' do
      before do
        post 'radiology_order', { order: FactoryGirl.attributes_for(:order, order_type: 'Radiology'), auth_token: user.authentication_token, device_id: device.uuid, patient_id: '7529922752', medical_unit_id: medical_unit.id  }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid patient mrn/ }
    end
  end
  describe 'CONSULTATION_ORDER #order' do
    context 'should create consultation orders with valid data' do
      before do
        post 'create', { order: FactoryGirl.attributes_for(:order, order_type: 'Consultation',status: 'Open',category: 'Inpatient',department: 'Cardiology', cancel_reason: 'Test Not Possible',cancel_reason_details: 'Test Not Possible'), auth_token: user.authentication_token, device_id: device.uuid, patient_id: patient.mrn, medical_unit_id: medical_unit.id, visit_id: visit.id, consultation_order_details: [FactoryGirl.attributes_for(:consultation_order_detail, description: 'nothing', department: "Cardiology")] }
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not create Consultation orders with invalid auth_token' do
      before do
        post 'create', { order: FactoryGirl.attributes_for(:order, order_type: 'Consultation'), auth_token: 'gkegkegkegkgekgekhe', device_id: device.uuid, patient_id: patient.mrn, medical_unit_id: medical_unit.id  }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not create Consultation orders with invalid device' do
      before do
        post 'create', { order: FactoryGirl.attributes_for(:order, order_type: 'Consultation'), auth_token: user.authentication_token, device_id: 'gwkhkwh', patient_id: patient.mrn, medical_unit_id: medical_unit.id  }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /unidentify device/ }
    end
    context 'should not create Consultation orders with invalid patient' do
      before do
        post 'create', { order: FactoryGirl.attributes_for(:order, order_type: 'Consultation'), auth_token: user.authentication_token, device_id: device.uuid, patient_id: '7529922752', medical_unit_id: medical_unit.id  }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid patient mrn/ }
    end
  end
  describe 'ORDER_LOGS #order' do
    context 'should return order logs with valid data' do
      before do
        get 'order_logs', auth_token: user.authentication_token, visit_id: visit.id, order_type: "Medicine"
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not return order logs with invalid auth_token' do
      before do
        visit = FactoryGirl.create(:visit,medical_unit_id: medical_unit.id, patient_id: patient.id, user_id: user.id)
        get 'order_logs', auth_token: 'gkegkegkegkgekgekhe', visit_id: visit.id
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not return order logs with invalid visit id' do
      before do
        get 'order_logs', auth_token: user.authentication_token, visit_id: "dfhjfdshdfsh"
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid visit id/ }
    end
  end
  describe 'PROCEDURE_ORDER #order' do
    context 'should create procedure orders with valid data' do
      before do
        post 'procedure_order', { order: FactoryGirl.attributes_for(:order, order_type: 'Procedure',status: 'Open',category: 'Inpatient',department: 'Cardiology', cancel_reason: 'Test Not Possible',cancel_reason_details: 'Test Not Possible'), auth_token: user.authentication_token, device_id: device.uuid, patient_id: patient.mrn, medical_unit_id: medical_unit.id, visit_id: visit.id, procedure_order_details: [FactoryGirl.attributes_for(:procedure_order_detail, procedure: "square", option: "X-Ray")] }
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not create orders with invalid auth_token' do
      before do
        post 'procedure_order', { order: FactoryGirl.attributes_for(:order, order_type: 'Procedure'), auth_token: 'gkegkegkegkgekgekhe', device_id: device.uuid, patient_id: patient.mrn, medical_unit_id: medical_unit.id  }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not create orders with invalid device' do
      before do
        post 'procedure_order', { order: FactoryGirl.attributes_for(:order, order_type: 'Procedure'), auth_token: user.authentication_token, device_id: 'gwkhkwh', patient_id: patient.mrn, medical_unit_id: medical_unit.id  }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /unidentify device/ }
    end
    context 'should not create orders with invalid patient' do
      before do
        post 'procedure_order', { order: FactoryGirl.attributes_for(:order, order_type: 'Procedure'), auth_token: user.authentication_token, device_id: device.uuid, patient_id: '7529922752', medical_unit_id: medical_unit.id  }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid patient mrn/ }
    end
  end
  describe 'LAB_ORDER #order' do
    context 'should create lab orders with valid data' do
      before do
        post 'lab_order', { order: FactoryGirl.attributes_for(:order, order_type: 'Lab',status: 'Open',category: 'Inpatient',department: 'Cardiology', cancel_reason: 'Test Not Possible',cancel_reason_details: 'Test Not Possible'), auth_token: user.authentication_token, device_id: device.uuid, patient_id: patient.mrn, medical_unit_id: medical_unit.id, visit_id: visit.id, lab_order_details: [FactoryGirl.attributes_for(:lab_order_detail, test: "square", detail: "X-Ray")] }
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not create orders with invalid auth_token' do
      before do
        post 'lab_order', { order: FactoryGirl.attributes_for(:order, order_type: 'Lab'), auth_token: 'gkegkegkegkgekgekhe', device_id: device.uuid, patient_id: patient.mrn, medical_unit_id: medical_unit.id  }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not create orders with invalid device' do
      before do
        post 'lab_order', { order: FactoryGirl.attributes_for(:order, order_type: 'Lab'), auth_token: user.authentication_token, device_id: 'gwkhkwh', patient_id: patient.mrn, medical_unit_id: medical_unit.id  }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /unidentify device/ }
    end
    context 'should not create orders with invalid patient' do
      before do
        post 'lab_order', { order: FactoryGirl.attributes_for(:order, order_type: 'Lab'), auth_token: user.authentication_token, device_id: device.uuid, patient_id: '7529922752', medical_unit_id: medical_unit.id  }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid patient mrn/ }
    end
  end
  describe 'ADMISSION_ORDER #order' do
    context 'should create admission orders with valid data' do
      before do
        post 'create', { order: FactoryGirl.attributes_for(:order, order_type: 'Admission',status: 'Open',category: 'Inpatient',department: 'Cardiology', cancel_reason: 'Test Not Possible',cancel_reason_details: 'Test Not Possible'), auth_token: user.authentication_token, device_id: device.uuid, patient_id: patient.mrn, medical_unit_id: medical_unit.id, visit_id: visit.id, admission_order_details: [FactoryGirl.attributes_for(:admission_order_detail, description: 'nothing', department: "Cardiology", category: "Inpatient")] }
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not create admission orders with invalid auth_token' do
      before do
        post 'create', { order: FactoryGirl.attributes_for(:order, order_type: 'Admission',status: 'Open',category: 'Inpatient',department: 'Cardiology'), auth_token: '793976796393
          ', device_id: device.uuid, patient_id: patient.mrn, medical_unit_id: medical_unit.id, visit_id: visit.id, admission_order_details: [FactoryGirl.attributes_for(:admission_order_detail, description: 'nothing', department: "Cardiology", category: "Inpatient")] }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not create admission orders with invalid device' do
      before do
        post 'create', { order: FactoryGirl.attributes_for(:order, order_type: 'Admission',status: 'Open',category: 'Inpatient',department: 'Cardiology'), auth_token: user.authentication_token, device_id: '75397593', patient_id: patient.mrn, medical_unit_id: medical_unit.id, visit_id: visit.id, admission_order_details: [FactoryGirl.attributes_for(:admission_order_detail, description: 'nothing', department: "Cardiology", category: "Inpatient")] }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /unidentify device/ }
    end
    context 'should not create admission orders with invalid patient' do
      before do
        post 'create', { order: FactoryGirl.attributes_for(:order, order_type: 'Admission',status: 'Open',category: 'Inpatient',department: 'Cardiology'), auth_token: user.authentication_token, device_id: device.uuid, patient_id: '333833', medical_unit_id: medical_unit.id, visit_id: visit.id, admission_order_details: [FactoryGirl.attributes_for(:admission_order_detail, description: 'nothing', department: "Cardiology", category: "Inpatient")] }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid patient mrn/ }
    end
  end
  describe 'Search #admission_orders' do
    before do
      post 'create', { order: FactoryGirl.attributes_for(:order, order_type: 'Admission',status: 'Open',category: 'Inpatient',department: 'Cardiology', cancel_reason: 'Test Not Possible',cancel_reason_details: 'Test Not Possible'), auth_token: user.authentication_token, device_id: device.uuid, patient_id: patient.mrn, medical_unit_id: medical_unit.id, visit_id: visit.id, admission_order_details: [FactoryGirl.attributes_for(:admission_order_detail, description: 'nothing', department: "Cardiology", category: "Inpatient")] }
    end
    context 'should Search admission orders with valid data' do
      before do
        get 'search_admission_patients', { auth_token: user.authentication_token, device_id: device.uuid, medical_unit_id: medical_unit.id, category: 'Inpatient',department: 'Cardiology' }
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not Search admission orders with invalid authentication_token' do
      before do
        get 'search_admission_patients', { auth_token: '46648680404', device_id: device.uuid, medical_unit_id: medical_unit.id, category: 'Inpatient',department: 'Cardiology' }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match 'invalid authentication token' }
    end
    context 'should not Search admission orders with invalid device_id' do
      before do
        get 'search_admission_patients', { auth_token: user.authentication_token, device_id: '49846', medical_unit_id: medical_unit.id, category: 'Inpatient',department: 'Cardiology' }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /unidentify device/ }
    end
    context 'should not Search admission orders without category' do
      before do
        get 'search_admission_patients', { auth_token: user.authentication_token, device_id: device.uuid, medical_unit_id: medical_unit.id, category: 'Inpatient' }
      end
      it { expect(response.status).to eq(404) }
    end
    context 'should not Search admission orders without department' do
      before do
        get 'search_admission_patients', { auth_token: user.authentication_token, device_id: device.uuid, medical_unit_id: medical_unit.id, department: 'Cardiology' }
      end
      it { expect(response.status).to eq(404) }

    end
  end
  describe 'Search #consultation_orders' do
    before do
      post 'create', { order: FactoryGirl.attributes_for(:order, order_type: 'Consultation',status: 'Open',category: 'Inpatient',department: 'Cardiology', cancel_reason: 'Test Not Possible',cancel_reason_details: 'Test Not Possible'), auth_token: user.authentication_token, device_id: device.uuid, patient_id: patient.mrn, medical_unit_id: medical_unit.id, visit_id: visit.id, consultation_order_details: [FactoryGirl.attributes_for(:consultation_order_detail, description: 'nothing', department: "Cardiology")] }
    end
    context 'should Search consultation orders with valid data' do
      before do
        get 'search_consultation_patients', { auth_token: user.authentication_token, device_id: device.uuid, medical_unit_id: medical_unit.id, department: 'Cardiology' }
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not Search consultation orders with invalid authentication_token' do
      before do
        get 'search_consultation_patients', { auth_token: '46648680404', device_id: device.uuid, medical_unit_id: medical_unit.id, department: 'Cardiology' }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match 'invalid authentication token' }
    end
    context 'should not Search consultation orders with invalid device_id' do
      before do
        get 'search_consultation_patients', { auth_token: user.authentication_token, device_id: '6497694', medical_unit_id: medical_unit.id, department: 'Cardiology' }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /unidentify device/ }
    end
    context 'should not Search consultation orders without department' do
      before do
        get 'search_consultation_patients', { auth_token: user.authentication_token, device_id: device.uuid, medical_unit_id: medical_unit.id, department: 'Cardiology' }
      end
      it { expect(response.status).to eq(404) }
    end
  end
  describe 'Lab_history #orders' do
    order =  FactoryGirl.create(:order, order_type: 'Lab',status: 'Open',category: 'Inpatient',department: 'Cardiology', cancel_reason: 'Test Not Possible',cancel_reason_details: 'Test Not Possible') 
    lab_order_detail = FactoryGirl.create(:lab_order_detail, test: "urine", detail: "X-Ray", order_id: order.id)
    context 'should Search Lab history with valid data' do
      before do
        get 'lab_history', { auth_token: user.authentication_token, device_id: device.uuid, type: 'urine', start_date: "2016-04-10", end_date: "2016-04-30" }
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not Search Lab history with invalid authentication_token' do
      before do
        get 'lab_history', { auth_token: '46648680404', device_id: device.uuid, type: 'urine', start_date: "2016-04-10", end_date: "2016-04-30" }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match 'invalid authentication token' }
    end
    context 'should not Search Lab history with invalid device_id' do
      before do
        get 'lab_history', { auth_token: user.authentication_token, device_id: '6497694', type: 'urine', start_date: "2016-04-10", end_date: "2016-04-30" }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /unidentify device/ }
    end
    context 'should not Search Lab history without type' do
      before do
        get 'lab_history', { auth_token: user.authentication_token, device_id: device.uuid, start_date: "2016-04-10", end_date: "2016-04-30" }
      end
      it { expect(response.status).to eq(404) }
    end
  end
  describe 'radiology_history #orders' do
    order =  FactoryGirl.create(:order, order_type: 'Radiology',status: 'Open',category: 'Inpatient',department: 'Cardiology',created_at: "2016-04-27", cancel_reason: 'Test Not Possible',cancel_reason_details: 'Test Not Possible') 
    radiology_order_detail = FactoryGirl.create(:radiology_order_detail, area: "square", imaging_type: "X-Ray", order_id: order.id)
    context 'should Search radiology history with valid data' do
      before do
        get 'radiology_history', { auth_token: user.authentication_token, device_id: device.uuid, type: 'X-Ray', area: 'square', start_date: "2016-04-10", end_date: "2016-04-30" }
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not Search radiology history with invalid authentication_token' do
      before do
        get 'radiology_history', { auth_token: '46648680404', device_id: device.uuid, type: 'X-Ray', area: 'square', start_date: "2016-04-10", end_date: "2016-04-30" }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match 'invalid authentication token' }
    end
    context 'should not Search radiology history with invalid device_id' do
      before do
        get 'radiology_history', { auth_token: user.authentication_token, device_id: '6497694', type: 'X-Ray', area: 'square', start_date: "2016-04-10", end_date: "2016-04-30" }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /unidentify device/ }
    end
    context 'should not Search radiology history without type' do
      before do
        get 'radiology_history', { auth_token: user.authentication_token, device_id: device.uuid, start_date: "2016-04-10", end_date: "2016-04-30" }
      end
      it { expect(response.status).to eq(404) }
    end
  end
  describe 'UPDATE #order' do
    let(:order) { FactoryGirl.create(:order) }
    context 'should update order with valid data' do
      before do
        put :update, id: order, order: FactoryGirl.attributes_for(:order, order_type: 'Radiology',status: 'Open',category: 'Inpatient',department: 'Cardiology', cancel_reason: 'Test Not Possible',cancel_reason_details: 'Test Not Possible'), auth_token: user.authentication_token, device_id: device.uuid, order_id: order.id
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not cupdate order with invalid auth_token' do
      before do
        put :update, id: order, order: FactoryGirl.attributes_for(:order, order_type: 'Radiology',status: 'Open',category: 'Inpatient',department: 'Cardiology'), auth_token: '97469746796497', device_id: device.uuid, order_id: order.id
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not update order with invalid device' do
      before do
        put :update, id: order, order: FactoryGirl.attributes_for(:order, order_type: 'Radiology',status: 'Open',category: 'Inpatient',department: 'Cardiology'), auth_token: user.authentication_token, device_id: '767946794', order_id: order.id
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /unidentify device/ }
    end
  end
  describe 'ORDERS_HISTORY #order' do
    context 'should return orders with valid data' do
      before do
        get 'orders_history', auth_token: user.authentication_token, device_id: device.uuid, medical_unit_id: medical_unit.id, department_id: department.id, start_date: "2016-04-01", end_date: Date.today
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not return orders with invalid auth_token' do
      before do
        get 'orders_history', auth_token: "fvfvfvfvfvf", device_id: device.uuid, medical_unit_id: medical_unit.id, department_id: department.id, start_date: "2016-04-01", end_date: Date.today
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not return orders with invalid medical_unit' do
      before do
        get 'orders_history', auth_token: user.authentication_token, device_id: device.uuid, medical_unit_id: "ffvfvf444", department_id: department.id, start_date: "2016-04-01", end_date: Date.today
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid medical unit id/ }
    end
    context 'should not return orders without department_id' do
      before do
        get 'orders_history', auth_token: user.authentication_token, device_id: device.uuid, medical_unit_id: medical_unit.id, start_date: "2016-04-01", end_date: Date.today
      end
      it { expect(response.status).to eq(404) }
    end
  end
end
