require 'rails_helper'
require 'rspec/rails'

RSpec.describe Api::V1::RegionsController, type: :controller do
  describe 'Regions API' do
    before do
      @region = FactoryGirl.create(:region)
    end

    describe 'GET #region' do
      context 'should return searched regions' do
        before do
          get 'index', category: @region.category
        end

        it { expect(response.status).to eq(200) }
      end
      context 'should return all regions' do
        before do
          get 'index', category: 'all'
        end

        it { expect(response.status).to eq(200) }
      end
      context 'should return no region' do
        before do
          get 'index', category: 'area'
        end

        it { expect(response.status).to eq(404) }
      end
      context 'should return no region' do
        before do
          get 'index', category: nil
        end

        it { expect(response.status).to eq(404) }
      end
      context 'should return parent regions' do
        before do
          get 'index', region_id: @region.parent_id
        end

        it { expect(response.status).to eq(200) }
      end
      context 'should return all regions' do
        before do
          get 'index', region_id: 'all'
        end

        it { expect(response.status).to eq(200) }
      end
      context 'should return no region' do
        before do
          get 'index', region_id: 500
        end

        it { expect(response.status).to eq(404) }
      end
      context 'should return no region' do
        before do
          get 'index', region_id: nil
        end

        it { expect(response.status).to eq(404) }
      end
    end
  end
end
