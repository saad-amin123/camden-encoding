require 'rails_helper'
require 'rspec/rails'

RSpec.describe Api::V1::LabTestDetailsController, type: :controller do
  let(:user) { FactoryGirl.create(:user) }
  let(:medical_unit) { FactoryGirl.create(:medical_unit) }
  let(:role) { FactoryGirl.create(:role) }
  let(:device) { FactoryGirl.create(:device) }
  let(:order) { FactoryGirl.create(:order) }
  describe 'UPDATE #lab_test_detail' do
    let(:lab_test_detail) { FactoryGirl.create(:lab_test_detail) }
    context 'should update lab_test_detail with valid data' do
      before do
        put :update, id: lab_test_detail.id, lab_test_detail: FactoryGirl.attributes_for(:lab_test_detail, caption: 'Total Cholesterol', range: 'Below 200', unit: 'mg/dL', value: '160', comments: 'this is comments'), auth_token: user.authentication_token, device_id: device.uuid
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not update lab_test_detail with invalid authentication_token' do
      before do
        put :update, id: lab_test_detail, lab_test_detail: FactoryGirl.attributes_for(:lab_test_detail, caption: 'Total Cholesterol', range: 'Below 200', unit: 'mg/dL', value: '160', comments: 'this is comments'), auth_token: 'yeoyeouyoeu', device_id: device.uuid
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not update lab_test_detail with invalid device id' do
      before do
        put :update, id: lab_test_detail, lab_test_detail: FactoryGirl.attributes_for(:lab_test_detail, caption: 'Total Cholesterol', range: 'Below 200', unit: 'mg/dL', value: '160', comments: 'this is comments'), auth_token: user.authentication_token, device_id: '39753973'
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /unidentify device/ }
    end
  end
end
