require 'rails_helper'
require 'rspec/rails'

RSpec.describe Api::V1::PatientDiagnosesController, type: :controller do
  let(:user) { FactoryGirl.create(:user) }
  let(:role) { FactoryGirl.create(:role) }
  let(:device) { FactoryGirl.create(:device) }
  let(:patient) { FactoryGirl.create(:patient) }
  let(:visit) { FactoryGirl.create(:visit) }
  describe 'CREATE #patient_diagnosis' do
    context 'should create patient_diagnosis with valid data' do
      before do
        post 'create', { patient_diagnosis: FactoryGirl.attributes_for(:patient_diagnosis, name: 'cancer', code: '0011'), auth_token: user.authentication_token, device_id: device.uuid, patient_id: patient.mrn, visit_id: visit.id }
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not create patient_diagnosis with invalid auth_token' do
      before do
        post 'create', { patient_diagnosis: FactoryGirl.attributes_for(:patient_diagnosis, name: 'cancer'), auth_token: 'gkegkegkegkgekgekhe', device_id: device.uuid, patient_id: patient.mrn, visit_id: visit.id }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not create patient_diagnosis with invalid device' do
      before do
        post 'create', { patient_diagnosis: FactoryGirl.attributes_for(:patient_diagnosis, code: '0019'), auth_token: user.authentication_token, device_id: 'gwkhkwh', patient_id: patient.mrn, visit_id: visit.id }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /unidentify device/ }
    end
    context 'should not create patient_diagnosis with invalid patient' do
      before do
        post 'create', { patient_diagnosis: FactoryGirl.attributes_for(:patient_diagnosis, code: '9099'), auth_token: user.authentication_token, device_id: device.uuid, patient_id: '7529922752', visit_id: visit.id }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid patient mrn/ }
    end
    context 'should not create patient_diagnosis with invalid visit' do
      before do
        post 'create', { patient_diagnosis: FactoryGirl.attributes_for(:patient_diagnosis, code: '9099'), auth_token: user.authentication_token, device_id: device.uuid, patient_id: patient.mrn, visit_id: '6795974' }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid visit id/ }
    end
  end
  describe 'DELETE #patient_diagnosis' do
    before do
        @patient_diagnosis = FactoryGirl.create(:patient_diagnosis,  patient_id: patient.id)
    end
    context 'should delete patient_diagnosis with valid data' do
      before do
        # @patient_diagnosis = FactoryGirl.create(:patient_diagnosis,  patient_id: patient.id)
        delete 'destroy', { id: @patient_diagnosis.id, auth_token: user.authentication_token, patient_id: patient.mrn, visit_id: visit.id }
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not delete patient_diagnosis with invalid auth_token' do
      before do
        delete 'destroy', { id: @patient_diagnosis.id, auth_token: 'gkegkegkegkgekgekhe', patient_id: patient.id, visit_id: visit.id }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not delete patient_diagnosis with invalid patient' do
      before do
        delete 'destroy', { id: @patient_diagnosis.id, auth_token: user.authentication_token, patient_id: '7529922752', visit_id: visit.id }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid patient mrn/ }
    end
    context 'should not delete patient_diagnosis with invalid visit' do
      before do
        delete 'destroy', { id: @patient_diagnosis.id, auth_token: user.authentication_token, patient_id: patient.mrn, visit_id: "dcsd222" }
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid visit id/ }
    end
  end
end
