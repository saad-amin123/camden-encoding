require 'rails_helper'
require 'rspec/rails'

RSpec.describe Api::V1::InventoriesController, type: :controller do
  let!(:user) { FactoryGirl.create(:user)}
  let!(:device) { FactoryGirl.create(:device)}
  let!(:medical_unit) { FactoryGirl.create(:medical_unit)}
  let!(:store) { FactoryGirl.create(:store, medical_unit_id: medical_unit.id)}
  let!(:brand_drug) { FactoryGirl.create(:brand_drug)}
  let!(:inventory) { FactoryGirl.create(:inventory, brand_drug: brand_drug, medical_unit: medical_unit, store: store)}
  describe 'Search #inventory' do
    context 'should return inventory with valid data' do
      before do
        get 'search', auth_token: user.authentication_token, device_id: device.uuid, medical_unit_id: medical_unit.id, brand_drug_id: brand_drug.id, store_id: store.id
      end
      it { expect(response.status).to eq(200) }
    end
    context 'should not return inventory with invalid auth_token' do
      before do
        get 'search', auth_token: "jvvnfjvnfdj", device_id: device.uuid, medical_unit_id: medical_unit.id, brand_drug_id: brand_drug.id, store_id: store.id
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid authentication token/ }
    end
    context 'should not return inventory with invalid medical_unit' do
      before do
        get 'search', auth_token: user.authentication_token, device_id: device.uuid, medical_unit_id: "2efece", brand_drug_id: brand_drug.id, store_id: store.id
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid medical unit id/ }
    end
    context 'should not return inventory with invalid store' do
      before do
        get 'search', auth_token: user.authentication_token, device_id: device.uuid, medical_unit_id: medical_unit.id, brand_drug_id: brand_drug.id, store_id: "dsdf4t4t"
      end
      it { expect(response.status).to eq(401) }
      it { expect(response.body).to match /invalid store/ }
    end
  end
end
