# == Schema Information
#
# Table name: devices
#
#  id               :integer          not null, primary key
#  uuid             :string
#  assignable_id    :integer
#  assignable_type  :string
#  role_id          :integer
#  operating_system :string
#  os_version       :string
#  status           :string
#  latitude         :float
#  longitude        :float
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

require 'rails_helper'

RSpec.describe Device, type: :model do
  it { should validate_presence_of(:uuid) }
  it { should validate_presence_of(:operating_system) }
  it { should validate_presence_of(:os_version) }
  it { should validate_presence_of(:status) }
  it { should validate_presence_of(:role) }
  it { should validate_presence_of(:assignable_type) }
  it { should validate_presence_of(:assignable_id) }
  context 'associations' do
    it { should belong_to :role }
    it { should belong_to :assignable }
  end
end
