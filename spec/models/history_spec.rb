# == Schema Information
#
# Table name: histories
#
#  id         :integer          not null, primary key
#  patient_id :integer
#  history    :text
#  type       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe History, :type => :model do
	it { should validate_presence_of(:history) }
  context 'associations' do
    it { should belong_to :patient }
  end
end
