# == Schema Information
#
# Table name: allergies
#
#  id              :integer          not null, primary key
#  patient_id      :integer
#  user_id         :integer
#  medical_unit_id :integer
#  visit_id        :integer
#  allergy_type    :string
#  value           :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

require 'rails_helper'

RSpec.describe Allergy, :type => :model do

  it { should validate_inclusion_of(:allergy_type).in_array(%w[Drug Non\ Drug]) }

  context 'associations' do
    it { should belong_to :medical_unit }
    it { should belong_to :user }
    it { should belong_to :visit }
    it { should belong_to :patient }
  end
end
