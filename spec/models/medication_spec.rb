# == Schema Information
#
# Table name: medications
#
#  id              :integer          not null, primary key
#  patient_id      :integer
#  user_id         :integer
#  medical_unit_id :integer
#  visit_id        :integer
#  brand_drug_id   :integer
#  dosage          :string
#  unit            :string
#  route           :string
#  frequency       :string
#  usage           :string
#  instruction     :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  type            :string
#

require 'rails_helper'

RSpec.describe Medication, :type => :model do

  it { should validate_inclusion_of(:unit).in_array(%w[mg mg/ml ml G]) }
  it { should validate_inclusion_of(:route).in_array(%w[PO IV Inhaler]) }
  it { should validate_inclusion_of(:frequency).in_array(%w[Once Q12 Q24 Q48]) }
  it { should validate_inclusion_of(:usage).in_array(%w[Hr Day Week Month]) }
  it { should validate_presence_of(:dosage) }
  it { should validate_presence_of(:unit) }
  it { should validate_presence_of(:brand_drug) }
  it { should validate_length_of(:instruction).is_at_most(150) }

  context 'associations' do
    it { should belong_to :medical_unit }
    it { should belong_to :user }
    it { should belong_to :visit }
    it { should belong_to :patient }
  end
end
