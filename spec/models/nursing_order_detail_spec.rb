# == Schema Information
#
# Table name: nursing_order_details
#
#  id           :integer          not null, primary key
#  order_id     :integer
#  department   :string
#  instructions :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'rails_helper'

RSpec.describe NursingOrderDetail, :type => :model do
  it { should validate_presence_of(:department) }
  context 'associations' do
    it { should belong_to :order }
  end
end
