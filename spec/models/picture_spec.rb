# == Schema Information
#
# Table name: pictures
#
#  id             :integer          not null, primary key
#  imageable_id   :integer
#  imageable_type :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :integer
#  file           :string
#  visit_id       :integer
#

require 'rails_helper'

RSpec.describe Picture, type: :model do
  it { should validate_presence_of(:file) }
  it { should validate_presence_of(:user) }

  context 'associations' do
    it { should belong_to :user }
  end
end
