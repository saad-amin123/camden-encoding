# == Schema Information
#
# Table name: consultation_order_details
#
#  id          :integer          not null, primary key
#  order_id    :integer
#  department  :string
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  note_id     :integer
#

require 'rails_helper'

RSpec.describe ConsultationOrderDetail, :type => :model do
  it { should validate_presence_of(:department) }
  context 'associations' do
    it { should belong_to :order }
  end
end
