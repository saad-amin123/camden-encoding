# == Schema Information
#
# Table name: visit_histories
#
#  id              :integer          not null, primary key
#  referred_to     :string
#  ref_department  :string
#  medical_unit_id :integer
#  referred_by     :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  visit_id        :integer
#

require 'rails_helper'

RSpec.describe VisitHistory, type: :model do
  it { should validate_presence_of(:referred_to) }
  it { should validate_presence_of(:ref_department) }
  
  context 'associations' do
    it { should belong_to :visit }
  end
end
