# == Schema Information
#
# Table name: vitals
#
#  id              :integer          not null, primary key
#  patient_id      :integer
#  user_id         :integer
#  medical_unit_id :integer
#  visit_id        :integer
#  weight          :float
#  height          :float
#  temperature     :float
#  bp_systolic     :float
#  bp_diastolic    :float
#  pulse           :float
#  resp_rate       :float
#  o2_saturation   :float
#  head_circ       :float
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

require 'rails_helper'

RSpec.describe Vital, :type => :model do
  
  it { should validate_inclusion_of(:weight).in_array([*0..999]) }
  it { should validate_inclusion_of(:height).in_array([*0..96]) }
  it { should validate_inclusion_of(:temperature).in_array([*95..107]) }
  it { should validate_inclusion_of(:bp_systolic).in_array([*50..210]) }
  it { should validate_inclusion_of(:bp_diastolic).in_array([*33..120]) }
  it { should validate_inclusion_of(:pulse).in_array([*40..140]) }
  it { should validate_inclusion_of(:resp_rate).in_array([*12..70]) }
  it { should validate_inclusion_of(:o2_saturation).in_array([*0..100]) }
  it { should validate_inclusion_of(:head_circ).in_array([*16..56]) }

  context 'associations' do
    it { should belong_to :medical_unit }
    it { should belong_to :user }
    it { should belong_to :visit }
    it { should belong_to :patient }
  end
end
