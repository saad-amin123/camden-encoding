# == Schema Information
#
# Table name: complaint_watchers
#
#  id           :integer          not null, primary key
#  complaint_id :integer
#  user_id      :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'rails_helper'

RSpec.describe ComplaintWatcher, type: :model do
  context 'associations' do
    it { should belong_to :user }
    it { should belong_to :complaint }
  end
end
