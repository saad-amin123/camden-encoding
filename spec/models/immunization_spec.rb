# == Schema Information
#
# Table name: immunizations
#
#  id              :integer          not null, primary key
#  patient_id      :integer
#  user_id         :integer
#  medical_unit_id :integer
#  visit_id        :integer
#  age             :string
#  vaccine         :string
#  dose            :string
#  due             :date
#  given_on        :date
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

require 'rails_helper'

RSpec.describe Immunization, :type => :model do
  context 'associations' do
    it { should belong_to :medical_unit }
    it { should belong_to :user }
    it { should belong_to :visit }
    it { should belong_to :patient }
  end
end
