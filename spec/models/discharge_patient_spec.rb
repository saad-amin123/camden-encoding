# == Schema Information
#
# Table name: discharge_patients
#
#  id              :integer          not null, primary key
#  medical_unit_id :integer
#  patient_id      :integer
#  bed_id          :integer
#  admission_id    :integer
#  treatment_plan  :string
#  medications     :string
#  follow_up       :string
#  discharge_type  :string
#  comments        :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  visit_id        :integer
#

require 'rails_helper'

RSpec.describe DischargePatient, :type => :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
