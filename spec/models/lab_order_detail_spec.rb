# == Schema Information
#
# Table name: lab_order_details
#
#  id                   :integer          not null, primary key
#  order_id             :integer
#  test                 :string
#  detail               :string
#  sample_collected     :boolean
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  instructions         :string
#  note                 :text
#  status               :string           default("Open")
#  lab_investigation_id :integer
#  department_id        :integer
#  order_generator_id   :integer
#  sample_collector_id  :integer
#  lab_technician_id    :integer
#  pathologist_id       :integer
#

require 'rails_helper'

RSpec.describe LabOrderDetail, :type => :model do
  it { should validate_presence_of(:test) }
  context 'associations' do
    it { should belong_to :order }
    it { should have_many(:lab_test_details)}
  end
end
