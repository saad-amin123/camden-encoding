# == Schema Information
#
# Table name: radiology_order_details
#
#  id              :integer          not null, primary key
#  order_id        :integer
#  imaging_type    :string
#  area            :string
#  contrast_option :string
#  other_option    :string
#  routine         :boolean          default("false")
#  urgent          :boolean          default("false")
#  left            :boolean          default("false")
#  right           :boolean          default("false")
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  instructions    :text
#  note            :text
#  result          :string
#

require 'rails_helper'

RSpec.describe RadiologyOrderDetail, :type => :model do
  it { should validate_presence_of(:imaging_type) }
  it { should validate_presence_of(:area) }
  it { should validate_presence_of(:order) }

  context 'associations' do
    it { should belong_to :order }
  end
end
