# == Schema Information
#
# Table name: stores
#
#  id              :integer          not null, primary key
#  name            :string
#  medical_unit_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  parent_id       :integer
#  department_id   :integer
#

require 'rails_helper'

RSpec.describe Store, :type => :model do
  it { should validate_presence_of(:name) }
  context 'associations' do
    it { should belong_to :medical_unit }
  end
end
