# == Schema Information
#
# Table name: add_admissions
#
#  id           :integer          not null, primary key
#  order_id     :integer
#  category     :string
#  department   :string
#  floor_number :string
#  bed_number   :string
#  bay_number   :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'rails_helper'

RSpec.describe AddAdmission, :type => :model do
  it { should validate_presence_of(:department) }
  it { should validate_presence_of(:category) }
  it { should validate_presence_of(:floor_number) }
  it { should validate_presence_of(:bed_number) }
  
  context 'associations' do
    it { should belong_to :order }
  end
end
