# == Schema Information
#
# Table name: lab_test_details
#
#  id                  :integer          not null, primary key
#  order_id            :integer
#  lab_order_detail_id :integer
#  caption             :string
#  range               :string
#  unit                :string
#  value               :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  comments            :string
#  test_label          :string
#

require 'rails_helper'

RSpec.describe LabTestDetail, :type => :model do
  it { should validate_presence_of(:caption) }
  context 'associations' do
    it { should belong_to :order }
    it { should belong_to :lab_order_detail }
  end
end
