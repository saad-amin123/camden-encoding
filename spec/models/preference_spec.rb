# == Schema Information
#
# Table name: preferences
#
#  id         :integer          not null, primary key
#  department :string
#  category   :string
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Preference, type: :model do
  it { should validate_presence_of(:department) }
  it { should validate_presence_of(:category) }
  
  context 'associations' do
    it { should belong_to :user }
  end
end
