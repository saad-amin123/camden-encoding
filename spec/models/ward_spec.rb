# == Schema Information
#
# Table name: wards
#
#  id              :integer          not null, primary key
#  medical_unit_id :integer
#  department_id   :integer
#  parent_id       :integer
#  title           :string
#  active          :boolean          default("true")
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  user_id         :integer
#  beds_count      :integer          default("0")
#  bays_count      :integer          default("0")
#  deleted_at      :datetime
#  ward_code       :string
#

require 'rails_helper'

RSpec.describe Ward, :type => :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
