# == Schema Information
#
# Table name: study_templates
#
#  id           :integer          not null, primary key
#  lab_study_id :integer
#  caption      :string
#  range        :string
#  unit         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  test_label   :string
#

require 'rails_helper'

RSpec.describe StudyTemplate, :type => :model do
  it { should validate_presence_of(:caption) }

  context 'associations' do
    it { should belong_to(:lab_study) }
  end
end
