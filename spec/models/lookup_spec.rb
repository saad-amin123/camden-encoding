# == Schema Information
#
# Table name: lookups
#
#  id         :integer          not null, primary key
#  category   :string
#  key        :string
#  value      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  parent_id  :integer
#

require 'rails_helper'

RSpec.describe Lookup, type: :model do
  it { should validate_presence_of(:category) }
  it { should validate_presence_of(:key) }
  it { should validate_presence_of(:value) }

  context 'associations' do
    it { should belong_to :parent }
  end
end
