# == Schema Information
#
# Table name: departments
#
#  id              :integer          not null, primary key
#  title           :string
#  medical_unit_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  parent_id       :integer
#  department_code :string
#  ipd             :boolean          default("false")
#  opd             :boolean          default("false")
#  emergency       :boolean          default("false")
#  wards_count     :integer          default("0")
#  lookup_id       :integer
#

require 'rails_helper'

RSpec.describe Department, type: :model do
end
