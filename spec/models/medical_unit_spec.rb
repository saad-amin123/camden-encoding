# == Schema Information
#
# Table name: medical_units
#
#  id                    :integer          not null, primary key
#  region_id             :integer
#  title                 :string
#  identification_number :string
#  location              :string
#  latitude              :float
#  longitude             :float
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  category              :string
#  bays_count            :integer          default("0")
#  parent_id             :integer
#  code                  :string
#

require 'rails_helper'
require 'rspec/rails'

RSpec.describe MedicalUnit, type: :model do
  it { should validate_presence_of(:title) }

  context 'associations' do
    it { should belong_to :region }
    it { should have_many :departments }
    it { should have_many :devices }
    it { should have_many :visits }
  end

  describe "public instance methods" do
    user = FactoryGirl.create(:user)
    medical_unit = FactoryGirl.create(:medical_unit)
    patient = FactoryGirl.create(:patient)    
    visit=FactoryGirl.create(:visit, patient_id: patient.id, user_id: user.id,medical_unit_id: medical_unit.id)
    ids = medical_unit.visited_patients("in-patient","cardialogy").pluck(:id)
    it 'should be an array' do
      expect(ids).to be_an Array
      expect(ids).to include(patient.id)
    end
  end

end
