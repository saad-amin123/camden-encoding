# == Schema Information
#
# Table name: complaints
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  medical_unit_id :integer
#  device_id       :integer
#  category        :string
#  area            :string
#  description     :string
#  severity_level  :string
#  show_identity   :boolean          default("false")
#  status          :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  created_by      :string
#  assign_to_role  :integer
#  critical        :boolean          default("false")
#  high_priority   :boolean          default("false")
#

require 'rails_helper'

RSpec.describe Complaint, type: :model do
  it { should validate_presence_of(:category) }
  it { should validate_presence_of(:area) }
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:severity_level) }
  it { should validate_presence_of(:assign_to_role) }
  it { should validate_presence_of(:user_id) }
  it { should validate_presence_of(:device_id) }
  it { should validate_presence_of(:medical_unit_id) }
  context 'associations' do
    it { should belong_to :user }
    it { should belong_to :device }
    it { should belong_to :medical_unit }
  end
end
