# == Schema Information
#
# Table name: inventories
#
#  id              :integer          not null, primary key
#  brand_drug_id   :integer
#  quantity        :integer          default("0")
#  medical_unit_id :integer
#  device_id       :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  store_id        :integer
#  department_id   :integer
#

require 'rails_helper'

RSpec.describe Inventory, type: :model do
  it { should validate_presence_of(:quantity) }
  
  context 'associations' do
    it { should belong_to :brand_drug }
    it { should belong_to :medical_unit }
    it { should belong_to :device }
    it { should have_many :line_items }
  end
end
