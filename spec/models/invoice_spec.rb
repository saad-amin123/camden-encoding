# == Schema Information
#
# Table name: invoices
#
#  id                  :integer          not null, primary key
#  user_id             :integer
#  medical_unit_id     :integer
#  patient_id          :integer
#  visit_id            :integer
#  assignable_id       :integer
#  assignable_type     :string
#  total_amount        :string
#  discount_percentage :string
#  discount_value      :string
#  total_discount      :string
#  payable             :string
#  paid                :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

require 'rails_helper'

RSpec.describe Invoice, :type => :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
