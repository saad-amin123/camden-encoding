# == Schema Information
#
# Table name: items
#
#  id              :integer          not null, primary key
#  generic_item_id :integer
#  medical_unit_id :integer
#  department_id   :integer
#  user_id         :integer
#  item_name       :string
#  item_code       :string
#  item_type       :string
#  main_group      :string
#  uom             :string
#  upc             :string
#  price           :string
#  sub_group       :string
#  strength        :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  status          :string           default("Open")
#  product_name    :string
#  pack            :float            default("1.0")
#  unit            :float            default("1.0")
#  pack_size       :float            default("1.0")
#

require 'rails_helper'

RSpec.describe Item, :type => :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
