# == Schema Information
#
# Table name: item_transactions
#
#  id                      :integer          not null, primary key
#  item_id                 :integer
#  medical_unit_id         :integer
#  department_id           :integer
#  store_id                :integer
#  user_id                 :integer
#  patient_id              :integer
#  visit_id                :integer
#  transaction_type        :string
#  issue_department_id     :integer
#  recieve_store_id        :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  quantity                :string
#  item_stock_id           :integer
#  receipt_number          :string
#  doctor_id               :integer
#  open_stock              :string
#  sister_incharge_name    :string
#  registrar_name          :string
#  pharmacist_name         :string
#  store_keeper_name       :string
#  pharmacist_dms_ams_name :string
#  doctor_nurse_name       :string
#  unit                    :float
#  comment                 :string
#

require 'rails_helper'

RSpec.describe ItemTransaction, :type => :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
