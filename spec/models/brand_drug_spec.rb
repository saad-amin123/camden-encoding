# == Schema Information
#
# Table name: brand_drugs
#
#  id           :integer          not null, primary key
#  name         :string
#  category     :string
#  form         :string
#  dumb         :integer
#  packing      :string
#  trade_price  :float
#  retail_price :float
#  mg           :string
#  drug_id      :integer
#  brand_id     :integer
#  created_at   :datetime
#  updated_at   :datetime
#

require 'rails_helper'

RSpec.describe BrandDrug, type: :model do
  it { should validate_presence_of(:category) }
  it { should validate_presence_of(:name) }
  context 'associations' do
    it { should belong_to :brand }
  end
end
