# == Schema Information
#
# Table name: patients
#
#  id                    :integer          not null, primary key
#  registered_by         :integer
#  registered_at         :integer
#  mrn                   :string
#  first_name            :string
#  last_name             :string
#  middle_name           :string
#  gender                :string
#  education             :string
#  birth_day             :integer
#  birth_month           :integer
#  birth_year            :integer
#  patient_nic           :string
#  patient_passport      :string
#  unidentify_patient    :boolean          default("false")
#  guardian_relationship :string
#  guardian_first_name   :string
#  guardian_last_name    :string
#  guardian_middle_name  :string
#  guardian_nic          :string
#  guardian_passport     :string
#  unidentify_guardian   :boolean          default("false")
#  state                 :string
#  city                  :string
#  near_by_city          :string
#  address1              :string
#  address2              :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  phone1                :string
#  phone1_type           :string
#  phone2                :string
#  phone2_type           :string
#  phone3                :string
#  phone3_type           :string
#  marital_status        :string
#

require 'rails_helper'

RSpec.describe PatientSerializer, type: :serializer do
  context 'Individual Resource Representation' do
    # let!(:current_user) { FactoryGirl.create(:user) }
    let(:resource) { FactoryGirl.create(:patient) }
    
    subject { described_class.new(resource) } 
    it 'has a first_name' do
      expect(subject.first_name).to eql(resource.first_name)
    end
    it 'has a middle_name' do
      expect(subject.middle_name).to eql(resource.middle_name)
    end
    it 'has a last_name' do
      expect(subject.last_name).to eql(resource.last_name)
    end
    it 'has a gender' do
      expect(subject.gender).to eql(resource.gender)
    end
    it 'has a education' do
      expect(subject.education).to eql(resource.education)
    end
    it 'has a birth_day' do
      expect(subject.birth_day).to eql(resource.birth_day)
    end
    it 'has a birth_month' do
      expect(subject.birth_month).to eql(resource.birth_month)
    end
    it 'has a birth_year' do
      expect(subject.birth_year).to eql(resource.birth_year)
    end
    it 'has a patient_nic' do
      expect(subject.patient_nic).to eql(resource.patient_nic)
    end
    it 'has a patient_passport' do
      expect(subject.patient_passport).to eql(resource.patient_passport)
    end
    it 'has a unidentify_patient' do
      expect(subject.unidentify_patient).to eql(resource.unidentify_patient)
    end
    it 'has a guardian_relationship' do
      expect(subject.guardian_relationship).to eql(resource.guardian_relationship)
    end
    it 'has a guardian_first_name' do
      expect(subject.guardian_first_name).to eql(resource.guardian_first_name)
    end
    it 'has a guardian_middle_name' do
      expect(subject.guardian_middle_name).to eql(resource.guardian_middle_name)
    end
    it 'has a guardian_last_name' do
      expect(subject.guardian_last_name).to eql(resource.guardian_last_name)
    end
    it 'has a guardian_nic' do
      expect(subject.guardian_nic).to eql(resource.guardian_nic)
    end
    it 'has a guardian_passport' do
      expect(subject.guardian_passport).to eql(resource.guardian_passport)
    end
    it 'has a unidentify_guardian' do
      expect(subject.unidentify_guardian).to eql(resource.unidentify_guardian)
    end
    it 'has a state' do
      expect(subject.state).to eql(resource.state)
    end
    it 'has a city' do
      expect(subject.city).to eql(resource.city)
    end
    it 'has a near_by_city' do
      expect(subject.near_by_city).to eql(resource.near_by_city)
    end
    it 'has a address1' do
      expect(subject.address1).to eql(resource.address1)
    end
    it 'has a address2' do
      expect(subject.address2).to eql(resource.address2)
    end
    it 'has a phone1_type' do
      expect(subject.phone1_type).to eql(resource.phone1_type)
    end
    it 'has a phone1' do
      expect(subject.phone1).to eql(resource.phone1)
    end
    it 'has a phone2_type' do
      expect(subject.phone2_type).to eql(resource.phone2_type)
    end
    it 'has a phon2' do
      expect(subject.phone2).to eql(resource.phone2)
    end
    it 'has a phone3_type' do
      expect(subject.phone3_type).to eql(resource.phone3_type)
    end
    it 'has a phone3' do
      expect(subject.phone3).to eql(resource.phone3)
    end
  end
end
