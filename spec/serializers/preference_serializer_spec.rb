require 'rails_helper'

RSpec.describe PreferenceSerializer, type: :serializer do
  context 'Individual Resource Representation' do
    let(:resource) { FactoryGirl.create(:preference) }

    let(:serialization) { PreferenceSerializer.new(resource) }

    subject do
      JSON.parse(serialization.to_json)['preference']
    end

    it 'has a department' do
      expect(subject['department']).to eql(resource.department)
    end
    it 'has a category' do
      expect(subject['category']).to eql(resource.category)
    end
  end
end
