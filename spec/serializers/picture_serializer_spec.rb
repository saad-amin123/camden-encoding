# == Schema Information
#
# Table name: pictures
#
#  id             :integer          not null, primary key
#  imageable_id   :integer
#  imageable_type :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :integer
#  file           :string
#

require 'rails_helper'

RSpec.describe PictureSerializer, type: :serializer do
  subject { described_class }
  it { should have_one(:user) }

  context 'Individual Resource Representation' do
    let(:resource) { FactoryGirl.create(:picture) }

    let(:serialization) { PictureSerializer.new(resource) }

    subject do
      JSON.parse(serialization.to_json)['picture']
    end

    it 'has a imageable_id' do
      expect(subject['imageable_id']).to eql(resource.imageable_id)
    end
    it 'has a imageable_type' do
      expect(subject['imageable_type']).to eql(resource.imageable_type)
    end
    # it 'has a file' do
    #   expect(subject['file']).to eql(resource.file)
    # end
  end
end
