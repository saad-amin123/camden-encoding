require 'rails_helper'

RSpec.describe PatientDiagnosisSerializer, type: :serializer do
  subject { described_class }
  it { should have_one(:user) }
  it { should have_one(:visit) }
  it { should have_one(:patient) }

  context 'Individual Resource Representation' do
    let(:resource) { FactoryGirl.create(:patient_diagnosis) }

    subject { described_class.new(resource) } 

    it 'has a patient_diagnosis_id' do
      expect(subject.id).to eql(resource.id)
    end
    it 'has a patient_diagnosis_name' do
      expect(subject.name).to eql(resource.name)
    end
    it 'has a patient_diagnosis_code' do
      expect(subject.code).to eql(resource.code)
    end
  end
end
