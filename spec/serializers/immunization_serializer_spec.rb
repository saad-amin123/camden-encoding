require 'rails_helper'

RSpec.describe ImmunizationSerializer, type: :serializer do
  subject { described_class }
  it { should have_one(:user) }
  it { should have_one(:visit) }
  it { should have_one(:medical_unit) }

  context 'Individual Resource Representation' do
    let(:resource) { FactoryGirl.create(:immunization) }

    let(:serialization) { ImmunizationSerializer.new(resource) }

    subject do
      JSON.parse(serialization.to_json)['immunization']
    end
    
    it 'has a patient_id' do
      expect(subject['patient_id']).to eql(resource.patient_id)
    end
    it 'has a age' do
      expect(subject['age']).to eql(resource.age)
    end
    it 'has a vaccine' do
      expect(subject['vaccine']).to eql(resource.vaccine)
    end
    it 'has a dose' do
      expect(subject['dose']).to eql(resource.dose)
    end
    it 'has a due' do
      expect(subject['due']).to eql(resource.due)
    end
    it 'has a given_on' do
      expect(subject['given_on']).to eql(resource.given_on)
    end
  end
end
