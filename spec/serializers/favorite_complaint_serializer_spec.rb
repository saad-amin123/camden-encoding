require 'rails_helper'

RSpec.describe FavoriteComplaintSerializer, type: :serializer do
  subject { described_class }
  it { should have_one(:user) }
  it { should have_one(:complaint) }
end
