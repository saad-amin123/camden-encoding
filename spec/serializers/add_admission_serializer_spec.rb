require 'rails_helper'

RSpec.describe AddAdmissionSerializer, type: :serializer do
  subject { described_class }
  it { should have_one(:order) }

  context 'Individual Resource Representation' do
    let(:resource) { FactoryGirl.create(:add_admission) }

    let(:serialization) { AddAdmissionSerializer.new(resource) }

    subject do
      JSON.parse(serialization.to_json)['add_admission']
    end

    it 'has a id' do
      expect(subject['id']).to eql(resource.id)
    end
    it 'has a category' do
      expect(subject['category']).to eql(resource.category)
    end
    it 'has a department' do
      expect(subject['department']).to eql(resource.department)
    end
    it 'has a floor_number' do
      expect(subject['floor_number']).to eql(resource.floor_number)
    end
    it 'has a bed_number' do
      expect(subject['bed_number']).to eql(resource.bed_number)
    end
    it 'has a bay_number' do
      expect(subject['bay_number']).to eql(resource.bay_number)
    end
  end
end