# == Schema Information
#
# Table name: lookups
#
#  id         :integer          not null, primary key
#  category   :string
#  key        :string
#  value      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  parent_id  :integer
#

require 'rails_helper'

RSpec.describe LookupSerializer, :type => :serializer do

  context 'Individual Resource Representation' do
    let(:resource) { FactoryGirl.create(:lookup) }

    let(:serialization) { LookupSerializer.new(resource) }

    subject do
      JSON.parse(serialization.to_json)['lookup']
    end

    it 'has a category' do
      expect(subject['category']).to eql(resource.category)
    end
    it 'has a key' do
      expect(subject['key']).to eql(resource.key)
    end
    it 'has a value' do
      expect(subject['value']).to eql(resource.value)
    end
    it 'has a parent_id' do
      expect(subject['parent_id']).to eql(resource.parent_id)
    end
  end
end

