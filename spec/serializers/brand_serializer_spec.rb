require 'rails_helper'

RSpec.describe BrandSerializer, type: :serializer do
	
  context 'Individual Resource Representation' do

    let(:resource) { FactoryGirl.create(:brand) }

    let(:serialization) { BrandSerializer.new(resource) }

    subject do
      JSON.parse(serialization.to_json)['brand']
    end

    it 'has a name' do
      expect(subject['name']).to eql(resource.name)
    end
    it 'has a category' do
      expect(subject['category']).to eql(resource.category)
    end
  end
end
