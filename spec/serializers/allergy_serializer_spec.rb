require 'rails_helper'

RSpec.describe AllergySerializer, type: :serializer do
  subject { described_class }
  it { should have_one(:user) }
  it { should have_one(:patient) }
  it { should have_one(:visit) }
  it { should have_one(:medical_unit) }

  context 'Individual Resource Representation' do
    let(:resource) { FactoryGirl.create(:allergy) }

    let(:serialization) { AllergySerializer.new(resource) }

    subject do
      JSON.parse(serialization.to_json)['allergy']
    end

    it 'has a allergy_type' do
      expect(subject['allergy_type']).to eql(resource.allergy_type)
    end
    it 'has a value' do
      expect(subject['value']).to eql(resource.value)
    end
  end
end
