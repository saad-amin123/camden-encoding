country = Region.create(name: 'Pakistan', category: 'country')
["Punjab", "Sindh", "KPK", "Balochistan", "Azad Jammu & Kashmir", "FATA", "Gilgit-baltistan", "Federal"].each do |province|
  Region.create(name: province, category: 'province', parent_id: country.id)
end

punjab = Region.where(category: 'province', name: 'Punjab').first
 ["Attock", "Bahawalnagar", "Bahawalpur", "Bhakkar", "Chakwal", "Chiniot", "Dera ghazi khan", "Faisalabad", "Gujranwala", "Gujrat", "Hafizabad", "Jhang", "Jhelum", "Kasur", "Khanewal", "Khushab", "Lahore", "Layyah", "Lodhran", "Mandi bahauddin", "Mianwali", "Multan", "Muzaffargarh", "Narowal", "Nankana sahib", "Okara", "Pakpattan", "Rahim yar khan", "Rajanpur", "Rawalpindi", "Sahiwal", "Sargodha", "Sheikhupura", "Sialkot", "Toba tek singh", "Vehari"].each do |district|
	Region.create(name: district, category: 'district', parent_id: punjab.id)
end

sindh = Region.where(category: 'province', name: 'Sindh').first
["Badin", "Dadu", "Ghotki", "Hyderabad", "Jacobabad", "Jamshoro", "Karachi", "Kashmore", "Khairpur", "Larkana", "Matiari", "Mirpurkhas", "Naushahro firoze", "Shaheed benazirabad", "Kambar shahdadkot", "Sanghar", "Shikarpur", "Sukkur", "Tando allahyar", "Tando muhammad khan", "Tharparkar", "Thatta", "Umerkot", "Sujawal"].each do |district|
	Region.create(name: district, category: 'district', parent_id: sindh.id)
end

kpk = Region.where(category: 'province', name: 'KPK').first
["Abbottabad", "Bannu", "Battagram", "Buner", "Charsadda", "Chitral", "Dera ismail khan", "Hangu", "Haripur", "Karak", "Kohat", "Upper kohistan", "Lakki marwat", "Lower dir", "Malakand", "Mansehra", "Mardan", "Nowshera", "Peshawar", "Shangla", "Swabi", "Swat", "Tank", "Upper dir", "Tor ghar", "Lower kohistan"] .each do |district|
	Region.create(name: district, category: 'district', parent_id: kpk.id)
end

balochistan = Region.where(category: 'province', name: 'Balochistan').first
["Awaran", "Barkhan", "Kachhi", "Chagai", "Dera bugti", "Gwadar", "Harnai", "Jafarabad", "Jhal magsi", "Kalat", "Kech", "Kharan", "Kohlu", "Khuzdar", "Killa abdullah", "Killa saifullah", "Lasbela", "Loralai", "Mastung", "Musakhel", "Nasirabad", "Nushki", "Panjgur", "Pishin", "Quetta", "Sherani", "Sibi", "Washuk", "Zhob", "Ziarat", "Lehri", "Sohbatpur "].each do |district|
	Region.create(name: district, category: 'district', parent_id: balochistan.id)
end

azad_kashmir = Region.where(category: 'province', name: 'Azad Jammu & Kashmir').first
["Muzaffarabad", "Hattian", "Neelum", "Mirpur", "Bhimber", "Kotli", "Poonch", "Bagh", "Haveli", "Sudhnati"].each do |district|
	Region.create(name: district, category: 'district', parent_id: azad_kashmir.id)
end

fata = Region.where(category: 'province', name: 'FATA').first
["Bajaur agency", "Khyber agency", "Kurram agency", "Mohmand agency", "North waziristan agency", "Orakzai agency", "South waziristan agency", "Fr bannu", "Fr dera ismail khan", "Fr kohat", "Fr lakki marwat", "Fr peshawar", "Fr tank"].each do |district|
	Region.create(name: district, category: 'district', parent_id: fata.id)
end

gilgit = Region.where(category: 'province', name: 'Gilgit-baltistan').first
["Ghanche", "Skardu", "Astore", "Diamer", "Ghizer", "Gilgit", "Hunza", "Kharmang", "Shigar", "Nagar"].each do |district|
	Region.create(name: district, category: 'district', parent_id: gilgit.id)
end

islamabad = Region.where(category: 'province', name: 'Federal').first
['Islamabad'].each do |district|
	Region.create(name: district, category: 'district', parent_id: islamabad.id)
end

attock = Region.where(category: 'district', name: 'Attock').first
['Attock', 'Jand', 'Fateh Jang', 'Pindi Gheb', 'Hsssan Abdal', 'Hazro'].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: attock.id)
end

bahawalnagar = Region.where(category: 'district', name: 'Bahawalnagar').first
["Bahawalnagar", "Minchan abad", "Fort abbas", "Haroon abad", "Chishtian"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: bahawalnagar.id)
end

bahawalpur = Region.where(category: 'district', name: 'Bahawalpur').first
["Bahawalpur", "Hasil pur", "Ahmed pur east", "Yazman", "Khairpur tamewali"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: bahawalpur.id)
end

bhakkar = Region.where(category: 'district', name: 'Bhakkar').first
["Bhakkar", "Mankera", "Kalur kot", "Darya khan"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: bhakkar.id)
end

chakwal = Region.where(category: 'district', name: 'Chakwal').first
["Chakwal", "Tala gang", "Choa saidan shah", "Kallar kahar"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: chakwal.id)
end

chiniot = Region.where(category: 'district', name: 'Chiniot').first
["Chiniot", "Lalian", "Bhawana"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: chiniot.id)
end

faisalabad = Region.where(category: 'district', name: 'Faisalabad').first
["Faisalabad", "Jaranwala", "Samundari", "Chak jhumra", "Tandlianwala"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: faisalabad.id)
end

gujranwala = Region.where(category: 'district', name: 'Gujranwala').first
["Gujranwala", "Wazirabad", "Noshehra virkan", "Kamoke"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: gujranwala.id)
end

gujrat = Region.where(category: 'district', name: 'Gujrat').first
["Gujrat", "Kharian", "Sarai alamgir"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: gujrat.id)
end

hafizabad = Region.where(category: 'district', name: 'Hafizabad').first
["Hafizabad", "Pindi bhattian"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: hafizabad.id)
end

jhang = Region.where(category: 'district', name: 'Jhang').first
["Jhang", "Shore kot", "Ahmedpur sial"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: jhang.id)
end

jhelum = Region.where(category: 'district', name: 'Jhelum').first
["Jhelum", "Pind dadan khan", "Sohawa", "Dina"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: jhelum.id)
end

kasur = Region.where(category: 'district', name: 'Kasur').first
["Kasur", "Chunian", "Pattoki", "Kot radha kishan"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: kasur.id)
end

khanewal = Region.where(category: 'district', name: 'Khanewal').first
["Khanewal", "Chunian", "Pattoki", "Kot radha kishan"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: khanewal.id)
end

khushab = Region.where(category: 'district', name: 'Khushab').first
["Khushab", "Noor pur", "Quaidabad"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: khushab.id)
end

lahore = Region.where(category: 'district', name: 'Lahore').first
['Lahore'].map(&:capitalize).each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: lahore.id)
end

layyah = Region.where(category: 'district', name: 'Layyah').first
["Layyah", "Chaubara", "Karor lal esan"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: layyah.id)
end

lodhran = Region.where(category: 'district', name: 'Lodhran').first
["Lodhran", "Dunya pur", "Kahror pakka"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: lodhran.id)
end
mandi_bahauddin = Region.where(category: 'district', name: 'Mandi bahauddin').first
["Mandi bahauddin", "Phalia", "Malakwal"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: mandi_bahauddin.id)
end

mianwali = Region.where(category: 'district', name: 'Mianwali').first
["Mianwali", "Essa khel", "Piplan"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: mianwali.id)
end

multan = Region.where(category: 'district', name: 'Multan').first
["Multan", "Shujaabad", "Jalalpur pir wala"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: multan.id)
end

muzaffargarh = Region.where(category: 'district', name: 'Muzaffargarh').first
["Muzaffargarh", "Alipur", "Kot addu", "Jatoi"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: muzaffargarh.id)
end

narowal = Region.where(category: 'district', name: 'Narowal').first

["Narowal", "Shakar garh", "Zafarwal"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: narowal.id)
end

nankana_sahib = Region.where(category: 'district', name: 'Nankana sahib').first
["Nankana sahib", "Chunian", "Pattoki", "Kot radha kishan"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: nankana_sahib.id)
end

okara = Region.where(category: 'district', name: 'Okara').first
["Okara", "Depalpur", "Renala khurd"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: okara.id)
end

pakpattan = Region.where(category: 'district', name: 'Pakpattan').first
["Pakpattan", "Arifwala"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: pakpattan.id)
end

rahim_yar_khan = Region.where(category: 'district', name: 'Rahim yar khan').first
["Rahim yar khan", "Liaquat pur"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: rahim_yar_khan.id)
end

rajanpur = Region.where(category: 'district', name: 'Rajanpur').first
["Rajanpur", "Jampur", "Rojhan"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: rajanpur.id)
end

rawalpindi = Region.where(category: 'district', name: 'Rawalpindi').first
["Rawalpindi", "Kahuta", "Murree", "Taxila", "Wah cantt", "Gujar khan", "Kotli sattian", "Kallar sayyedan"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: rawalpindi.id)
end

sahiwal = Region.where(category: 'district', name: 'Sahiwal').first
["Sahiwal", "Chicha watni"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: sahiwal.id)
end

sargodha = Region.where(category: 'district', name: 'Sargodha').first
["Sargodha", "Bhalwal", "Shah pur", "Silan wali", "Sahiwal", "Kot momin"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: sargodha.id)
end

sheikhupura = Region.where(category: 'district', name: 'Sheikhupura').first
["Sheikhupura", "Feroze wala", "Muridkay", "Sharqpur"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: sheikhupura.id)
end

sialkot = Region.where(category: 'district', name: 'Sialkot').first
["Sialkot", "Daska", "Pasroor", "Sambrial"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: sialkot.id)
end

toba_tek_singh = Region.where(category: 'district', name: 'Toba tek singh').first
["Toba tek singh", "Kamalia", "Gojra"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: toba_tek_singh.id)
end

vehari = Region.where(category: 'district', name: 'Vehari').first
["Vehari", "Burewala", "Melsi"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: vehari.id)
end

badin = Region.where(category: 'district', name: 'Badin').first
["Badin", "Matli", "Tando bhagu", "Golarchi", "Talhar", "Shaheed fazal rahu"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: badin.id)
end

dadu = Region.where(category: 'district', name: 'Dadu').first
["Dadu", "Khairpur nathan shah ", "Mehr", "Johi"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: dadu.id)
end

hyderabad = Region.where(category: 'district', name: 'Hyderabad').first
["Hyderabad", "Latifabad ", "Qasimabad"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: hyderabad.id)
end


ghotki = Region.where(category: 'district', name: 'Ghotki').first
["Ghotki", "Khan garh", "Ubaoro", "Daharki", "Mirpur mathelo"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: ghotki.id)
end

jacobabad = Region.where(category: 'district', name: 'Jacobabad').first
["Jacobabad", "Garhi khairo", "Thul"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: jacobabad.id)
end

kashmore = Region.where(category: 'district', name: 'Kashmore').first
["Kashmore", "Kandkot", "Tangwani"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: kashmore.id)
end

khairpur = Region.where(category: 'district', name: 'Khairpur').first
["Khairpur", "Gambat", "Kot digi", "Mir wah", "Faiz ganj", "Sobhodero", "Kingri", "Nara"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: khairpur.id)
end

larkana = Region.where(category: 'district', name: 'Larkana').first
["Larkana", "Rato dero", "Dokri", "Bakrani"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: larkana.id)
end

matiari = Region.where(category: 'district', name: 'Matiari').first
["Matiari", "Hala", "Saeedabad"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: matiari.id)
end

mirpurkhas = Region.where(category: 'district', name: 'Mirpurkhas').first
["Mirpurkhas", "Digri", "Kot ghulam muhammad", "Jhuddo", "Sindhri", "Hussain bux marri"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: mirpurkhas.id)
end

naushahro = Region.where(category: 'district', name: 'Naushahro firoze').first
["Naushahro firoze", "Kandyaro", "Moro", "Bheria", "Mehrab pur"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: naushahro.id)
end

shahdadkot = Region.where(category: 'district', name: 'Kambar shahdadkot').first
["Kambar shahdadkot", "Miro khan", "Kambar ali khan", "Warah", "Sujawal junejo", "Kubo saeed khan", "Nasirabad"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: shahdadkot.id)
end

sanghar = Region.where(category: 'district', name: 'Sanghar').first
["Sanghar", "Sinjhoro", "Khipro", "Shahdadpur", "Tando adam", "Jam nawaz ali"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: sanghar.id)
end

shikarpur = Region.where(category: 'district', name: 'Shikarpur').first
["Shikarpur", "Khanpur", "Garhi yasin", "Lakhi"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: shikarpur.id)
end

sukkur = Region.where(category: 'district', name: 'Sukkur').first
["Sukkur", "Rohri", "Panuakil", "Salehpat", "New sukkur"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: sukkur.id)
end

tando_allahyar = Region.where(category: 'district', name: 'Tando allahyar').first
["Tando allahyar", "Chamber", "Jhando mari"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: tando_allahyar.id)
end

tando_khan = Region.where(category: 'district', name: 'Tando muhammad khan').first
["Tando muhammad khan", "Bulri shah karim", "Tando ghulam hyder"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: tando_khan.id)
end

tharparkar = Region.where(category: 'district', name: 'Tharparkar').first
["Tharparkar", "Chachro", "Nagar parkar", "Diplo"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: tharparkar.id)
end

thatta = Region.where(category: 'district', name: 'Thatta').first
["Thatta", "Mirpur sakro", "Keti bunder", "Ghorabari", "Sujawal", "Mirpur bathoro", "Jati", "Shah bander", "Kharo chan"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: thatta.id)
end

umerkot = Region.where(category: 'district', name: 'Umerkot').first
["Umerkot", "Samaro", "Kunri", "Pithoro"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: umerkot.id)
end

 bannu = Region.where(category: 'district', name: 'Bannu').first
["Bannu", "Domel"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: bannu.id)
end

charsadda = Region.where(category: 'district', name: 'Charsadda').first
["Charsadda", "Shabqadar", "Tangi"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: charsadda.id)
end

chitral = Region.where(category: 'district', name: 'Chitral').first
["Chitral", "Drosh", "Lutkhu", "Mastuj", "Turkoh", "Mulkoh"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: chitral.id)
end

dera_khan = Region.where(category: 'district', name: 'Dera ismail khan').first
["Dera ismail khan", "Kulachi"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: dera_khan.id)
end

hangu = Region.where(category: 'district', name: 'Hangu').first
["Hangu", "Tall"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: hangu.id)
end

haripur = Region.where(category: 'district', name: 'Haripur').first
["Haripur", "Ghazi"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: haripur.id)
end

karak = Region.where(category: 'district', name: 'Karak').first
["Karak", "Banda daud shah", "Takht e nasrati"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: karak.id)
end

lakki_marwat = Region.where(category: 'district', name: 'Lakki marwat').first
["Lakki marwat", "Naurang"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: lakki_marwat.id)
end

lower_dir = Region.where(category: 'district', name: 'Lower dir').first
["Lower dir", "Balambat", "Lal qilla", "Munda", "Adenzai", "Samar bagh", "Temergara"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: lower_dir.id)
end

malakand = Region.where(category: 'district', name: 'Malakand').first
["Malakand", "Swat ranizai", "Sam ranizai"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: malakand.id)
end

mansehra = Region.where(category: 'district', name: 'Mansehra').first
["Mansehra", "Bala kot", "Oghi", "Kala dhaka"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: mansehra.id)
end

mardan = Region.where(category: 'district', name: 'Mardan').first
["Mardan", "Takht bhai"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: mardan.id)
end

shangla = Region.where(category: 'district', name: 'Shangla').first
["Shangla", "Bisham", "Chakesar", "Martung", "Puran", "Alpurai"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: shangla.id)
end

swabi = Region.where(category: 'district', name: 'Swabi').first
["Swabi", "Lahor", "Topi", "Razar"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: swabi.id)
end

swat = Region.where(category: 'district', name: 'Swat').first
["Swat", "Matta", "Khwaza khela"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: swat.id)
end

upper_dir = Region.where(category: 'district', name: 'Upper dir').first
["Dir", "Barawal", "Kohistan", "Wari", "Khal"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: upper_dir.id)
end

awaran = Region.where(category: 'district', name: 'Awaran').first
["Awaran", "Jhaljao", "Mashkai", "Korak jahoo", "Gishkore"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: awaran.id)
end

kachhi = Region.where(category: 'district', name: 'Kachhi').first
["Kachhi", "Bolan", "Mach", "Bhag"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: kachhi.id)
end

chagai = Region.where(category: 'district', name: 'Chagai').first
["Chagai", "Dalbandin", "Nokundi", "Taftan"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: chagai.id)
end

dera_bugti = Region.where(category: 'district', name: 'Dera bugti').first
["Dera bugti", "Phellawagh", "Sui", "Loti"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: dera_bugti.id)
end

gwadar = Region.where(category: 'district', name: 'Gwadar').first
["Gwadar", "Jiwani", "Pasni", "Ormara"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: gwadar.id)
end

harnai = Region.where(category: 'district', name: 'Harnai').first
["Harnai", "Sharigh"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: harnai.id)
end

jafarabad = Region.where(category: 'district', name: 'Jafarabad').first
["Jafarabad", "Usta muhammad", "Jhet pat", "Sohbat pur", "Gandaka"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: jafarabad.id)
end

jhal_magsi = Region.where(category: 'district', name: 'Jhal magsi').first
["Jhal magsi", "Ghandawa"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: jhal_magsi.id)
end

kalat = Region.where(category: 'district', name: 'Kalat').first
["Kalat", "Surab", "Mangochar"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: kalat.id)
end

kech = Region.where(category: 'district', name: 'Kech').first
["Kech", "Turbat", "Tump", "Mund"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: kech.id)
end

kharan = Region.where(category: 'district', name: 'Kharan').first
["Kharan", "Sar kharan"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: kharan.id)
end

kohlu = Region.where(category: 'district', name: 'Kohlu').first
["Kohlu", "Kahan", "Mawand"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: kohlu.id)
end

khuzdar = Region.where(category: 'district', name: 'Khuzdar').first
["Khuzdar", "Zehri", "Wadh"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: khuzdar.id)
end

killa_abdullah = Region.where(category: 'district', name: 'Killa abdullah').first
["Killa abdullah", "Chaman", "Gulistan"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: killa_abdullah.id)
end

killa_saifullah = Region.where(category: 'district', name: 'Killa saifullah').first
["Killa saifullah", "Muslim bagh", "Loiband"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: killa_saifullah.id)
end

lasbela = Region.where(category: 'district', name: 'Lasbela').first
["Lasbela", "Uthal", "Hab", "Dureji"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: lasbela.id)
end

loralai = Region.where(category: 'district', name: 'Loralai').first
["Loralai", "Duki", "Mekhtar"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: loralai.id)
end

mastung = Region.where(category: 'district', name: 'Mastung').first
["Mastung", "Dasht"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: mastung.id)
end

musakhel = Region.where(category: 'district', name: 'Musakhel').first
["Musakhel", "Drug"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: musakhel.id)
end

nasirabad = Region.where(category: 'district', name: 'Nasirabad').first
["Nasirabad", "Dera murad jamali", "Chattar", "Tamboo", "Baba kot"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: nasirabad.id)
end

pishin = Region.where(category: 'district', name: 'Pishin').first
["Pishin", "Hurramzai", "Karezat"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: pishin.id)
end

sibi = Region.where(category: 'district', name: 'Sibi').first
['Sibi', 'Lehri'].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: sibi.id)
end

washuk = Region.where(category: 'district', name: 'Washuk').first
["Washuk", "Besima", "Mashkhel"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: washuk.id)
end

zhob = Region.where(category: 'district', name: 'Zhob').first
["Zhob", "Qamar din karez"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: zhob.id)
end

muzaffarabad = Region.where(category: 'district', name: 'Muzaffarabad').first
["Muzaffarabad", "Alipur", "Kot addu", "Jatoi"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: muzaffarabad.id)
end

hattian = Region.where(category: 'district', name: 'Hattian').first
["Hattian", "Lipa", "Chikar"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: hattian.id)
end

neelum = Region.where(category: 'district', name: 'Neelum').first
["Neelum", "Atth muqam", "Sharda"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: neelum.id)
end

mirpur = Region.where(category: 'district', name: 'Mirpur').first
["Mirpur", "Digri", "Kot ghulam muhammad", "Jhuddo", "Sindhri", "Hussain bux marri"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: mirpur.id)
end

bhimber = Region.where(category: 'district', name: 'Bhimber').first
["Bhimber", "Barnala", "Samahni"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: bhimber.id)
end

kotli = Region.where(category: 'district', name: 'Kotli').first
["Kotli", "Nakyal", "Sehnsa", "Charoai"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: kotli.id)
end

poonch = Region.where(category: 'district', name: 'Poonch').first
["Poonch", "Rawala kot", "Hajeera", "Abbaspur"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: poonch.id)
end

bagh = Region.where(category: 'district', name: 'Bagh').first
["Bagh", "Dhir kot"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: bagh.id)
end

sudhnati = Region.where(category: 'district', name: 'Sudhnati').first
["Sudhnati", "Palandari", "Mang", "Tarar khal", "Baloch"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: sudhnati.id)
end

ghanche = Region.where(category: 'district', name: 'Ghanche').first
["Mashabrum", "Khaplu"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: ghanche.id)
end

skardu = Region.where(category: 'district', name: 'Skardu').first
["Skardu", "Kharmang", "Shigar", "Gultari", "Rondu"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: skardu.id)
end

astore = Region.where(category: 'district', name: 'Astore').first
["Astore", "Shounter"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: astore.id)
end

diamer = Region.where(category: 'district', name: 'Diamer').first
["Diamer", "Chilas", "Darail", "Tangir"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: diamer.id)
end

ghizer = Region.where(category: 'district', name: 'Ghizer').first
["Ghizer", "Gupis", "Yasin", "Punial", "Ishkoman"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: ghizer.id)
end

gilgit = Region.where(category: 'district', name: 'Astore').first
["Gilgit", "Ali abad", "Gojal", "Nagir no. 1", "Nagir no. 2"].each do |tehsil|
	Region.create(name: tehsil, category: 'tehsil', parent_id: gilgit.id)
end
