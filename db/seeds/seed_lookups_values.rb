Lookup.where(category: 'Radiology_Imaging_type').destroy_all
Lookup.where(category: 'Radiology_Area').destroy_all
Lookup.where(category: 'Radiology_contrast_options').destroy_all
Lookup.where(category: 'Radiology_other_options').destroy_all
Lookup.where(category: 'Lab').destroy_all
Lookup.where(category: 'Historical Notes').destroy_all

['Complete Blood Count - CBC', 'WBC Differential', 'Lipid profile',
 'Renal Function Test - RFT', 'Liver Function Test - LFT',
 'Erythrocyte Sedimentation Rate (ESR)', 'Blood Glucose Test',
 'Thyroid function test'].map(&:capitalize).each do |value|
  Lookup.create(key: value, category: 'Lab', value: value)
end

['Clinic Note', 'Admission Note', 'ER Note', 'History & Physical',
 'Consultation Note', 'Progress Note', 'Operative Note',
 'DC Summary'].each do |value|
  Lookup.create(key: value, category: 'Historical Notes', value: value)
end

['X-Ray', 'MRI', 'CT Scan', 'Ultrasound', 'Mammogram', 'Nuclear Medicine',
 'Bone Density', 'Flouroscopy', 'Doppler', 'Misc.'].each do |value|
  Lookup.create(key: value, category: 'Radiology_Imaging_type', value: value)
end

x_ray = Lookup.where(category: 'Radiology_Imaging_type', value: 'X-Ray').first
['Head/Neck', 'Musculoskeletal', 'Extremities', 'Chest',
 'Abdomen'].each do |value|
  Lookup.create(key: value, category: 'Radiology_Area', value: value,
                parent_id: x_ray.id)
end

mri = Lookup.where(category: 'Radiology_Imaging_type', value: 'MRI').first
['Head/Brain', 'Pelvis/Spine', 'Extremities', 'Chest',
 'Breast', 'Abdomen', 'MRA'].each do |value|
  Lookup.create(key: value, category: 'Radiology_Area', value: value,
                parent_id: mri.id)
end

ct_scan = Lookup.where(category: 'Radiology_Imaging_type',
                       value: 'CT Scan').first
['Head/Face/Neck', 'Musculoskeletal', 'Thorax/Cardiac/Pulmonery',
 'Abdomen/Pelvis Extremeties',
 'Specials protocols w IV Contrast'].each do |value|
  Lookup.create(key: value, category: 'Radiology_Area', value: value,
                parent_id: ct_scan.id)
end

ultrasound = Lookup.where(category: 'Radiology_Imaging_type',
                          value: 'Ultrasound').first
['Soft tissue abdomen & lower back', 'Abdomen complete', 'Liver', 'Kidneys',
 'Arterial inflow & venous outflow', 'Guidance for needle biopsy',
 'Renal biopsy', 'Renal  transplant', 'Aorta diagnostic', 'Screening for AAA',
 'Scrotum', 'Thyroid', 'Pelvis/Transabdominal', 'Pelvis / Transvaginal',
 'Biophysical Profile (BPP)', 'Mark for paracentesis', 'Mark for thoracentesis',
 'Mark for radiation tx', 'Soft tissue head & neck',
 'Soft tissue chest & upper back (no breast)',
 'Breast', 'Soft tissue pelvic wall & lower back',
 'Soft tissue pelvic wall/buttock/penis/perineum',
 'Soft tissue upper/lower extremities  Axilla Groin'].each do |value|
  Lookup.create(key: value, category: 'Radiology_Area', value: value,
                parent_id: ultrasound.id)
end

nuclear_medicine = Lookup.where(category: 'Radiology_Imaging_type',
                                value: 'Nuclear Medicine').first
['Myocardial perfusion imaging', 'Nvg (MUGA) (Nuclear Ventriculogram)',
 'PET Scan', 'Thyroid Scan', 'Parathyroid Scan', 'Adrenal imaging',
 'MIBG Scan', 'Hida scan', 'Gi-Bleed Scan', 'WBC Scan', 'Nuclear bone scan',
 'Ventillation/Perfusion Scan (V/Q)'].each do |value|
  Lookup.create(key: value, category: 'Radiology_Area', value: value,
                parent_id: nuclear_medicine.id)
end

bone_density = Lookup.where(category: 'Radiology_Imaging_type',
                            value: 'Bone Density').first
['Bone Density'].each do |value|
  Lookup.create(key: value, category: 'Radiology_Area', value: value,
                parent_id: bone_density.id)
end

flouroscopy = Lookup.where(category: 'Radiology_Imaging_type',
                           value: 'Flouroscopy').first
['Esophagogram', 'Upper GI series', 'Small bowell series', 'Barium enema',
 'Loopogram'].each do |value|
  Lookup.create(key: value, category: 'Radiology_Area', value: value,
                parent_id: flouroscopy.id)
end

doppler = Lookup.where(category: 'Radiology_Imaging_type',
                       value: 'Doppler').first
['Carotid', 'Neck veins', 'Arm', 'Leg', 'Arterio venous', 'Venous incompetence',
 'DVT', 'Dialysis fistula'].each do |value|
  Lookup.create(key: value, category: 'Radiology_Area', value: value,
                parent_id: doppler.id)
end

mri = Lookup.where(category: 'Radiology_Imaging_type', value: 'MRI').first
['With Gadolinium', 'Without Gadolinium',
 'With & without Gadolinium'].each do |value|
  Lookup.create(key: value, category: 'Radiology_contrast_options',
                value: value, parent_id: mri.id)
end

ct_scan = Lookup.where(category: 'Radiology_Imaging_type',
                       value: 'CT Scan').first
['With oral contrast only', 'Without contrast', 'with IV contrast',
 'With & without IV contrast', 'With Oral & IV Contrast'].each do |value|
  Lookup.create(key: value, category: 'Radiology_contrast_options',
                value: value, parent_id: ct_scan.id)
end

head_neck = Lookup.where(category: 'Radiology_Area', value: 'Head/Neck').first
['M&ible', 'Sinus Series', 'Skill series (single view)',
 'Skull series (4 or more views)', 'Para Nasal Sinus (PNS)',
 'Soft Tissue neck (STN)', 'Orthopantomogram (OPG)',
 'Lateral Cephalometric Radiograph'].each do |value|
  Lookup.create(key: value, category: 'Radiology_other_options', value: value,
                parent_id: head_neck.id)
end

musculoskeletal = Lookup.where(category: 'Radiology_Area',
                               value: 'Musculoskeletal').first
['M&Cervical spine (3 views or less)', 'Thoracic (2 views)',
 'Lumbar spine (min 2 views)', 'Lumber spine (min 4 views)',
 'Lumbar spine (min 6 views)', 'Thoracolumbar spine (2 or more views)',
 'Sternum (2 or more views)', 'Pelvis (single view - non weight bearing)',
 'Hips bilateral w/ Pelvis (2 Views)',
 'Hips bilateral w/ Pelvis (3 or 4 Views)'].each do |value|
  Lookup.create(key: value, category: 'Radiology_other_options', value: value,
                parent_id: musculoskeletal.id)
end

extremities = Lookup.where(category: 'Radiology_Area',
                           value: 'Extremities').first
['Fingers', 'H&', 'Wrist', 'Forearm', 'Elbow', 'Humerus', 'Clavicle',
 'Shoulder', 'Scapula', 'Ribs', 'Hip', 'Hip w/ Pelvis', 'Femur (min 2 views)',
 'Knee (3 View - weight bearing)', 'Knee', 'Tribia/Fibula', 'Ankle', 'Foot',
 'Heel'].each do |value|
  Lookup.create(key: value, category: 'Radiology_other_options', value: value,
                parent_id: extremities.id)
end

chest = Lookup.where(category: 'Radiology_Area', value: 'Chest').first
['AP View', 'Lateral DECUB', 'PA & Lateral', 'Ribs'].each do |value|
  Lookup.create(key: value, category: 'Radiology_other_options', value: value,
                parent_id: chest.id)
end

abdomen = Lookup.where(category: 'Radiology_Area', value: 'Abdomen').first
['Flat & Upright', 'Lateral decubitus', 'KUB'].each do |value|
  Lookup.create(key: value, category: 'Radiology_other_options', value: value,
                parent_id: abdomen.id)
end
