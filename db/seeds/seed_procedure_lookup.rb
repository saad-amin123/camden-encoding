['Blood Bank', 'Cardiac Procedures', 'Pulmonary Procedures',
 'GI', 'Infectious diesease', 'Oncology', 'Nephrology',
 'General Surgery', 'Sub Specialiaty Surgery', 'Dermatology',
 'Neurology', 'Biopsy', 'Critical Care Procedures', 'OB/GYN',
 'Vascular', 'Urology', 'Transplant', 'Misc'].each do |value|
  Lookup.create(key: value, category: 'Procedure', value: value)
end

blood_bank = Lookup.where(category: 'Procedure', value: 'Blood Bank').first
['Packed RBC', 'Platelets', 'Fresh frozen plasma', 'Cryoprecipitate',
 'Factor VII', 'Factor VIII', 'Factor IX', 'Plamapheresis',
 'Leukopheresis'].each do |value|
  Lookup.create(key: value, category: 'Procedure', value: value,
                parent_id: blood_bank.id)
end

cardiac_procedures = Lookup.where(category: 'Procedure',
                                  value: 'Cardiac Procedures').first
['ECG',	'Transthoracic Echocardiogram',	'Transesophageal Echocardiogram',
 'Exercise Stress echo',	'Dobutamine stress echo',	'Exercise tradmil test',
 'Cardiac cath/coronoary angiography', 'Peripheral angiogram', 'CABG',
 'Valve replacement', 'Pacemaker Implantation', 'Pericardio sentesis',
 'Nuclear Stress test'].each do |value|
  Lookup.create(key: value, category: 'Procedure', value: value,
                parent_id: cardiac_procedures.id)
end

pulmonary_procedures = Lookup.where(category: 'Procedure',
                                    value: 'Pulmonary Procedures').first
['PFT', 'Bronchoscopy', 'Endobronchial US (EBUS)', 'Endotracheal intubation',
 'Thoracentesis', 'Chest tube placement'].each do |value|
  Lookup.create(key: value, category: 'Procedure', value: value,
                parent_id: pulmonary_procedures.id)
end

gi = Lookup.where(category: 'Procedure', value: 'GI').first
['Endoscopy', 'Sigmoidoscopy', 'Colonoscopy',	'Endoscopic US (EUS)', 'ERCP',
 'Paracentesis'].each do |value|
  Lookup.create(key: value, category: 'Procedure', value: value,
                parent_id: gi.id)
end

oncology = Lookup.where(category: 'Procedure', value: 'Oncology').first
['Port placement', 'Bone marrow aspirate & biopsy',
 'Intrathecal chemotherapy'].each do |value|
  Lookup.create(key: value, category: 'Procedure', value: value,
                parent_id: oncology.id)
end

nephrology = Lookup.where(category: 'Procedure', value: 'Nephrology').first
['Dialysis Catheter Placement', 'Hemodialysis',
 'Peritoneal dialysis'].each do |value|
  Lookup.create(key: value, category: 'Procedure', value: value,
                parent_id: nephrology.id)
end

dermatology = Lookup.where(category: 'Procedure', value: 'Dermatology').first
['Shave biopsy', 'Punch biopsy', 'UVB phototherapy', 'PUVA', 'Electro D&C',
 'Intralesional injections', 'Cryotherapy'].each do |value|
  Lookup.create(key: value, category: 'Procedure', value: value,
                parent_id: dermatology.id)
end

neurology = Lookup.where(category: 'Procedure', value: 'Neurology').first
['EEG', 'Lumbar Puncture', 'EMG'].each do |value|
  Lookup.create(key: value, category: 'Procedure', value: value,
                parent_id: neurology.id)
end

biopsy = Lookup.where(category: 'Procedure', value: 'Biopsy').first
['Brain', 'Head & Neck', 'Skin', 'Lung', 'Heart', 'Colon', 'Gastric',
 'Spleen', 'Liver', 'Kidney', 'Lymph nodes', 'Bone', 'Bone marrow',
 'Muslce', 'Breast', 'Endometrial',	'Cervical', 'Vaginal', 'FNAB',
 'Core biopsy', 'Abscess drainage', 'Cytology', 'Misc'].each do |value|
  Lookup.create(key: value, category: 'Procedure', value: value,
                parent_id: biopsy.id)
end

critical_care_procedures = Lookup.where(category: 'Procedure',
                                        value: 'Critical Care Procedures').first
['Endoscopy'].each do |value|
  Lookup.create(key: value, category: 'Procedure', value: value,
                parent_id: critical_care_procedures.id)
end

ob_gyn = Lookup.where(category: 'Procedure', value: 'OB/GYN').first
['C-Section', 'Pap smear', 'Survical cone biopsy', 'Colposcopy', 'D&C',
 'Endometrial ablation', 'Adhesiolysis', 'Colporrhaphy', 'Endometrial biopsy',
 'Hysterectomy', 'BSO', 'Hysterocscopy', 'Hysterosalpingography',
 'Tuboligation', 'Vulvectomy', 'Trachelectomy', 'Myomectomy'].each do |value|
  Lookup.create(key: value, category: 'Procedure', value: value,
                parent_id: ob_gyn.id)
end

vascular = Lookup.where(category: 'Procedure', value: 'Vascular').first
['Access', 'Caroitid endarterectomy'].each do |value|
  Lookup.create(key: value, category: 'Procedure', value: value,
                parent_id: vascular.id)
end

urology = Lookup.where(category: 'Procedure', value: 'Urology').first
['Cystoscopy', 'Urinary Catherter insertion', 'TURBT', 'Cystectomy',
 'Prostrate biopsy', 'Prostatectomy', 'Orchiectomy', 'Nephrectomy',
 'Lithotripsy', 'Circumcision', 'Adrenalectomy', 'Pyelogram', 'RPLND',
 'TURP', 'Ureteroscopy', 'Vasectomy'].each do |value|
  Lookup.create(key: value, category: 'Procedure', value: value,
                parent_id: urology.id)
end

transplant = Lookup.where(category: 'Procedure', value: 'Transplant').first
['Cardiac', 'Liver', 'Lung', 'Bone marrow', 'Stem cell',
 'Kidney'].each do |value|
  Lookup.create(key: value, category: 'Procedure', value: value,
                parent_id: transplant.id)
end
