Lookup.where(category: 'visiter-referred-to').destroy_all

visiterreferredto = ['Emergency', 'Inpatient', 'Outpatient', 'Nurse', 'MO', 'Dr'].each do |value|
  Lookup.create(key: value, category: 'visiter-referred-to', value: value)
end

emergency = Lookup.where(category: 'visiter-referred-to', value: 'Emergency').first
['General', 'Pediatrics', 'Surgery', 'Medicine', 'Triage', 'Other'].each do |value|
  Lookup.create(key: value, category: 'visiter-referred-to', value: value, parent_id: emergency.id)
end

inpatient = Lookup.where(category: 'visiter-referred-to', value: 'Inpatient').first
['Surgery', 'Surgery I', 'Surgery II', 'Surgery III', 'Surgery IV', 'Surgery V',
 'Medicine', 'Medicine I', 'Medicine II', 'Medicine III', 'Medicine IV',
 'Medicine V', 'Pediatrics', 'ENT', 'ICU', 'CCU', 'Cardiology', 'Orthopedics',
 'Dermatology', 'Plastic Surgery', 'TB', 'Urology', 'Gynae & Obs', 'Psychiatry',
 'Ophthalmology', 'Nephrology', 'Hematology/Oncology', 'Gastroenterology',
 'Endocrinology', 'Infectious Disease', 'Neurology', 'Pulmonology', 'Radiation Oncology',
 'Wound care', 'Physical Therapy', 'Other'].each do |value|
  Lookup.create(key: value, category: 'visiter-referred-to', value: value, parent_id: inpatient.id)
end

inpatient = Lookup.where(category: 'visiter-referred-to', value: 'Inpatient').first
['Nursery','Cardiology + Angio.','Haematology/ Oncology','Gastroenterology','Medical Unit I',
'Medical Unit II','Medical Unit III','Medical Unit IV','Devel. Paeds','Nephrology',
'Neurology','MICU','CICU','SNICU','SICU','CSIUC + Step Down','Paediatric Surgery',
'Unit I','Paediatric Surgery Unit II','Urology','Eye','Plastic Surgery','Orthopaedics',
'Neuro-Surgery','E.N.T','Maxillofacial Surgery','Emergecny Medical ','Emergecny Surgical',
'Emergecny Neunetal (NNE)','Private Ward','Inpatient OTs (16)','Main OT','Cardiac OT',
'Ortho OT','ENT OT ','Endoscopy Unit','Dental Unit','Diarrehea Section','TPN Section','BMT'].each do |value|
  Lookup.create(key: value, category: 'visiter-referred-to', value: value, parent_id: inpatient.id)
end

outpatient = Lookup.where(category: 'visiter-referred-to', value: 'Outpatient').first
['Surgery', 'Medicine', 'Pediatrics', 'ENT', 'Cardiology', 'Orthopedics', 'Dermatology',
 'Plastic Surgery', 'TB', 'Urology', 'Gynae & Obs', 'Psychiatry', 'Ophthalmology',
 'Nephrology', 'Hematology/Oncology', 'Gastroenterology', 'Endocrinology', 'Infectious Disease',
 'Neurology', 'Pulmonology', 'Radiation Oncology', 'Wound care', 'Physical Therapy',
 'Other'].each do |value|
  Lookup.create(key: value, category: 'visiter-referred-to', value: value, parent_id: outpatient.id)
end

nurse = Lookup.where(category: 'visiter-referred-to', value: 'Nurse').first
['Nurse Clinic','Other'].each do |value|
  Lookup.create(key: value, category: 'visiter-referred-to', value: value, parent_id: nurse.id)
end

mo = Lookup.where(category: 'visiter-referred-to', value: 'MO').first
['Clinic-01','Other'].each do |value|
  Lookup.create(key: value, category: 'visiter-referred-to', value: value, parent_id: mo.id)
end

dr = Lookup.where(category: 'visiter-referred-to', value: 'Dr').first
['Clinic-10','Other'].each do |value|
  Lookup.create(key: value, category: 'visiter-referred-to', value: value, parent_id: dr.id)
end
