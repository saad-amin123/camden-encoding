visitreason = ['Shortness of breath', 'Abdominal Pain', 'Back pain', 'Ear related', 'Eye related', 'Road Accident', 'Fracture', 'Poisoning', 'Burns (Acid/Fire)', 'Fever', 'Cough', 'Vomiting', 'Fainting', 'Pale Skin/Eyes', 'Skin Related', 'Sore/Cyst', 'Lump'].each do |value|
  Lookup.create(key: value, category: 'visit-reason', value: value)
end