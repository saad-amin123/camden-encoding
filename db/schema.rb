# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190618130432) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "MedicalUnits_Users", id: false, force: :cascade do |t|
    t.integer "user_id",         null: false
    t.integer "medical_unit_id", null: false
  end

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "activities", force: :cascade do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type", using: :btree
  add_index "activities", ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type", using: :btree
  add_index "activities", ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type", using: :btree

  create_table "add_admissions", force: :cascade do |t|
    t.integer  "order_id"
    t.string   "category"
    t.string   "department"
    t.string   "floor_number"
    t.string   "bed_number"
    t.string   "bay_number"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "addendums", force: :cascade do |t|
    t.text     "historical_note"
    t.integer  "note_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "addendums", ["note_id"], name: "index_addendums_on_note_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "username",               default: "", null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "admission_order_details", force: :cascade do |t|
    t.integer  "order_id"
    t.string   "category"
    t.string   "department"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "admission_type"
    t.string   "instructions"
  end

  create_table "admissions", force: :cascade do |t|
    t.integer  "medical_unit_id"
    t.integer  "visit_id"
    t.integer  "patient_id"
    t.integer  "referred_from"
    t.integer  "referred_to"
    t.integer  "ward_id"
    t.integer  "doctor_id"
    t.integer  "user_id"
    t.string   "attendent_name"
    t.string   "attendent_mobile"
    t.string   "attendent_address"
    t.string   "attendent_nic"
    t.string   "attendent_relationship"
    t.string   "status"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "admission_number"
  end

  create_table "allergies", force: :cascade do |t|
    t.integer  "patient_id"
    t.integer  "user_id"
    t.integer  "medical_unit_id"
    t.integer  "visit_id"
    t.string   "allergy_type"
    t.string   "value"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "batches", force: :cascade do |t|
    t.string   "number"
    t.datetime "expiry_date"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "quantity",    default: 0
  end

  create_table "bays", force: :cascade do |t|
    t.integer  "medical_unit_id"
    t.integer  "department_id"
    t.integer  "parent_id"
    t.string   "title"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "ward_id"
    t.integer  "beds_count",      default: 0
    t.datetime "deleted_at"
    t.string   "bay_code"
  end

  add_index "bays", ["deleted_at"], name: "index_bays_on_deleted_at", using: :btree

  create_table "bed_allocations", force: :cascade do |t|
    t.integer  "medical_unit_id"
    t.integer  "patient_id"
    t.integer  "admission_id"
    t.integer  "ward_id"
    t.integer  "bay_id"
    t.integer  "bed_id"
    t.string   "status",                  default: "Free"
    t.integer  "patient_last_updated_by"
    t.integer  "user_id"
    t.integer  "visit_id"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "comment"
  end

  create_table "beds", force: :cascade do |t|
    t.string   "bed_number"
    t.integer  "bay_id"
    t.integer  "ward_id"
    t.integer  "medical_unit_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "facility_id"
    t.datetime "deleted_at"
    t.string   "status",          default: "Free"
    t.string   "bed_code"
  end

  add_index "beds", ["deleted_at"], name: "index_beds_on_deleted_at", using: :btree

  create_table "brand_drugs", force: :cascade do |t|
    t.string   "name"
    t.string   "category"
    t.string   "form"
    t.integer  "dumb"
    t.string   "packing"
    t.float    "trade_price"
    t.float    "retail_price"
    t.string   "mg"
    t.integer  "drug_id"
    t.integer  "brand_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "brands", force: :cascade do |t|
    t.integer  "company_id"
    t.string   "name"
    t.string   "category"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "brands", ["company_id"], name: "index_brands_on_company_id", using: :btree

  create_table "call_requests", force: :cascade do |t|
    t.integer  "request_from"
    t.integer  "request_to"
    t.string   "code"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.text     "address"
    t.string   "phone"
    t.string   "fax"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "complaint_watchers", force: :cascade do |t|
    t.integer  "complaint_id"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "complaints", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "medical_unit_id"
    t.integer  "device_id"
    t.string   "category"
    t.string   "area"
    t.string   "description"
    t.string   "severity_level"
    t.boolean  "show_identity",   default: false
    t.string   "status"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "created_by"
    t.integer  "assign_to_role"
    t.boolean  "critical",        default: false
    t.boolean  "high_priority",   default: false
  end

  create_table "consultation_order_details", force: :cascade do |t|
    t.integer  "order_id"
    t.string   "department"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "note_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.integer  "patient_id"
    t.string   "number"
    t.string   "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "data_files", force: :cascade do |t|
    t.integer  "medical_unit_id"
    t.integer  "user_id"
    t.string   "file_url"
    t.string   "status"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "departments", force: :cascade do |t|
    t.string   "title"
    t.integer  "medical_unit_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "parent_id"
    t.string   "department_code"
    t.boolean  "ipd",             default: false
    t.boolean  "opd",             default: false
    t.boolean  "emergency",       default: false
    t.integer  "wards_count",     default: 0
    t.integer  "lookup_id"
  end

  create_table "devices", force: :cascade do |t|
    t.string   "uuid"
    t.integer  "assignable_id"
    t.string   "assignable_type"
    t.integer  "role_id"
    t.string   "operating_system"
    t.string   "os_version"
    t.string   "status"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "discharge_patients", force: :cascade do |t|
    t.integer  "medical_unit_id"
    t.integer  "patient_id"
    t.integer  "bed_id"
    t.integer  "admission_id"
    t.string   "treatment_plan"
    t.string   "medications"
    t.string   "follow_up"
    t.string   "discharge_type"
    t.string   "comments"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "visit_id"
  end

  create_table "doctor_patients", force: :cascade do |t|
    t.integer  "doctor_id"
    t.boolean  "active",     default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "patient_id"
  end

  create_table "drugs", force: :cascade do |t|
    t.string   "name"
    t.text     "overview"
    t.text     "characterstics"
    t.text     "indications"
    t.text     "contradictions"
    t.text     "interactions"
    t.text     "interference"
    t.text     "effects"
    t.text     "risk"
    t.text     "warning"
    t.text     "storage"
    t.string   "category"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "facilities", force: :cascade do |t|
    t.string   "facility_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "favorite_complaints", force: :cascade do |t|
    t.integer  "complaint_id"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "feedbacks", force: :cascade do |t|
    t.integer  "complaint_id"
    t.integer  "medical_unit_id"
    t.integer  "user_id"
    t.integer  "device_id"
    t.string   "satisfaction"
    t.string   "message"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "status"
  end

  create_table "follow_ups", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "patient_id"
    t.integer  "visit_id"
    t.integer  "medical_unit_id"
    t.date     "follow_up"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "follow_ups", ["medical_unit_id"], name: "index_follow_ups_on_medical_unit_id", using: :btree
  add_index "follow_ups", ["patient_id"], name: "index_follow_ups_on_patient_id", using: :btree
  add_index "follow_ups", ["user_id"], name: "index_follow_ups_on_user_id", using: :btree
  add_index "follow_ups", ["visit_id"], name: "index_follow_ups_on_visit_id", using: :btree

  create_table "generic_items", force: :cascade do |t|
    t.string   "name"
    t.string   "category"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "histories", force: :cascade do |t|
    t.integer  "patient_id"
    t.text     "history"
    t.string   "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "histories", ["patient_id"], name: "index_histories_on_patient_id", using: :btree

  create_table "icd_codes", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "immunizations", force: :cascade do |t|
    t.integer  "patient_id"
    t.integer  "user_id"
    t.integer  "medical_unit_id"
    t.integer  "visit_id"
    t.string   "age"
    t.string   "vaccine"
    t.string   "dose"
    t.date     "due"
    t.date     "given_on"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "inventories", force: :cascade do |t|
    t.integer  "brand_drug_id"
    t.integer  "quantity",        default: 0
    t.integer  "medical_unit_id"
    t.integer  "device_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "store_id"
    t.integer  "department_id"
  end

  create_table "investigation_notes", force: :cascade do |t|
    t.integer  "medical_unit_id"
    t.integer  "patient_id"
    t.integer  "visit_id"
    t.integer  "admission_id"
    t.integer  "user_id"
    t.string   "note"
    t.string   "investigation_type"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "invoice_items", force: :cascade do |t|
    t.integer  "invoice_id"
    t.integer  "assignable_id"
    t.string   "assignable_type"
    t.string   "invoice_type"
    t.string   "description"
    t.string   "amount"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "invoice_items", ["invoice_id"], name: "index_invoice_items_on_invoice_id", using: :btree

  create_table "invoices", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "medical_unit_id"
    t.integer  "patient_id"
    t.integer  "visit_id"
    t.integer  "assignable_id"
    t.string   "assignable_type"
    t.string   "total_amount"
    t.string   "discount_percentage"
    t.string   "discount_value"
    t.string   "total_discount"
    t.string   "payable"
    t.string   "paid"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "invoices", ["medical_unit_id"], name: "index_invoices_on_medical_unit_id", using: :btree
  add_index "invoices", ["patient_id"], name: "index_invoices_on_patient_id", using: :btree
  add_index "invoices", ["user_id"], name: "index_invoices_on_user_id", using: :btree
  add_index "invoices", ["visit_id"], name: "index_invoices_on_visit_id", using: :btree

  create_table "item_stocks", force: :cascade do |t|
    t.integer  "medical_unit_id"
    t.integer  "department_id"
    t.integer  "store_id"
    t.integer  "user_id"
    t.integer  "item_id"
    t.string   "quantity_in_hand"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "item_stocks", ["department_id"], name: "index_item_stocks_on_department_id", using: :btree
  add_index "item_stocks", ["item_id"], name: "index_item_stocks_on_item_id", using: :btree
  add_index "item_stocks", ["medical_unit_id"], name: "index_item_stocks_on_medical_unit_id", using: :btree
  add_index "item_stocks", ["store_id"], name: "index_item_stocks_on_store_id", using: :btree
  add_index "item_stocks", ["user_id"], name: "index_item_stocks_on_user_id", using: :btree

  create_table "item_transactions", force: :cascade do |t|
    t.integer  "item_id"
    t.integer  "medical_unit_id"
    t.integer  "department_id"
    t.integer  "store_id"
    t.integer  "user_id"
    t.integer  "patient_id"
    t.integer  "visit_id"
    t.string   "transaction_type"
    t.integer  "issue_department_id"
    t.integer  "recieve_store_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "quantity"
    t.integer  "item_stock_id"
    t.string   "receipt_number"
    t.integer  "doctor_id"
    t.string   "open_stock"
    t.string   "sister_incharge_name"
    t.string   "registrar_name"
    t.string   "pharmacist_name"
    t.string   "store_keeper_name"
    t.string   "pharmacist_dms_ams_name"
    t.string   "doctor_nurse_name"
    t.float    "unit"
    t.string   "comment"
  end

  add_index "item_transactions", ["department_id"], name: "index_item_transactions_on_department_id", using: :btree
  add_index "item_transactions", ["item_id"], name: "index_item_transactions_on_item_id", using: :btree
  add_index "item_transactions", ["item_stock_id"], name: "index_item_transactions_on_item_stock_id", using: :btree
  add_index "item_transactions", ["medical_unit_id"], name: "index_item_transactions_on_medical_unit_id", using: :btree
  add_index "item_transactions", ["patient_id"], name: "index_item_transactions_on_patient_id", using: :btree
  add_index "item_transactions", ["store_id"], name: "index_item_transactions_on_store_id", using: :btree
  add_index "item_transactions", ["user_id"], name: "index_item_transactions_on_user_id", using: :btree
  add_index "item_transactions", ["visit_id"], name: "index_item_transactions_on_visit_id", using: :btree

  create_table "items", force: :cascade do |t|
    t.integer  "generic_item_id"
    t.integer  "medical_unit_id"
    t.integer  "department_id"
    t.integer  "user_id"
    t.string   "item_name"
    t.string   "item_code"
    t.string   "item_type"
    t.string   "main_group"
    t.string   "uom"
    t.string   "upc"
    t.string   "price"
    t.string   "sub_group"
    t.string   "strength"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "status",          default: "Open"
    t.string   "product_name"
    t.float    "pack",            default: 1.0
    t.float    "unit",            default: 1.0
    t.float    "pack_size",       default: 1.0
  end

  add_index "items", ["department_id"], name: "index_items_on_department_id", using: :btree
  add_index "items", ["generic_item_id"], name: "index_items_on_generic_item_id", using: :btree
  add_index "items", ["medical_unit_id"], name: "index_items_on_medical_unit_id", using: :btree
  add_index "items", ["user_id"], name: "index_items_on_user_id", using: :btree

  create_table "lab_access_ranges", force: :cascade do |t|
    t.string   "gender"
    t.integer  "age_start"
    t.integer  "age_end"
    t.string   "date_unit"
    t.float    "start_range"
    t.float    "end_range"
    t.string   "range_title"
    t.string   "status"
    t.string   "heading"
    t.integer  "lab_assessment_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "medical_unit_id"
    t.integer  "lab_investigation_id"
  end

  create_table "lab_assessments", force: :cascade do |t|
    t.string   "title"
    t.string   "uom"
    t.string   "specimen"
    t.string   "status"
    t.string   "result_type1"
    t.string   "result_type2"
    t.integer  "medical_unit_id"
    t.string   "parameter_code"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "machine_code"
    t.string   "machine_parameter_code"
    t.string   "machine_name"
  end

  create_table "lab_assessments_investigations", force: :cascade do |t|
    t.integer "lab_assessment_id"
    t.integer "lab_investigation_id"
    t.integer "sequence"
    t.string  "label"
  end

  create_table "lab_investigation_prices", force: :cascade do |t|
    t.integer  "medical_unit_id"
    t.integer  "lab_investigation_id"
    t.string   "department_name"
    t.integer  "price"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "lab_investigation_types", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lab_investigations", force: :cascade do |t|
    t.string   "profile_name"
    t.integer  "medical_unit_id"
    t.string   "status"
    t.integer  "lab_investigation_type_id"
    t.string   "profile_code"
    t.string   "report_type"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "department_id"
  end

  create_table "lab_order_details", force: :cascade do |t|
    t.integer  "order_id"
    t.string   "test"
    t.string   "detail"
    t.boolean  "sample_collected"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "instructions"
    t.text     "note"
    t.string   "status",               default: "Open"
    t.integer  "lab_investigation_id"
    t.integer  "department_id"
    t.integer  "order_generator_id"
    t.integer  "sample_collector_id"
    t.integer  "lab_technician_id"
    t.integer  "pathologist_id"
  end

  add_index "lab_order_details", ["lab_technician_id"], name: "index_lab_order_details_on_lab_technician_id", using: :btree
  add_index "lab_order_details", ["order_generator_id"], name: "index_lab_order_details_on_order_generator_id", using: :btree
  add_index "lab_order_details", ["order_id"], name: "index_lab_order_details_on_order_id", using: :btree
  add_index "lab_order_details", ["pathologist_id"], name: "index_lab_order_details_on_pathologist_id", using: :btree
  add_index "lab_order_details", ["sample_collector_id"], name: "index_lab_order_details_on_sample_collector_id", using: :btree

  create_table "lab_order_sequences", force: :cascade do |t|
    t.integer  "medical_unit_id"
    t.string   "lab_sequence"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "lab_result_parameters", force: :cascade do |t|
    t.string   "parameter_title"
    t.string   "result1"
    t.string   "result2"
    t.string   "uom"
    t.string   "range_title"
    t.integer  "lab_result_id"
    t.boolean  "out_of_range"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "lab_result_parameters", ["lab_result_id"], name: "index_lab_result_parameters_on_lab_result_id", using: :btree

  create_table "lab_results", force: :cascade do |t|
    t.integer  "patient_id"
    t.integer  "medical_unit_id"
    t.integer  "lab_investigation_id"
    t.integer  "lab_order_detail_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.text     "note"
  end

  create_table "lab_studies", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lab_test_details", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "lab_order_detail_id"
    t.string   "caption"
    t.string   "range"
    t.string   "unit"
    t.string   "value"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "comments"
    t.string   "test_label"
  end

  create_table "leaves", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "medical_unit_id"
    t.integer  "department_id"
    t.string   "attendance"
    t.string   "shift"
    t.date     "date"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "leaves", ["department_id"], name: "index_leaves_on_department_id", using: :btree
  add_index "leaves", ["medical_unit_id"], name: "index_leaves_on_medical_unit_id", using: :btree
  add_index "leaves", ["user_id"], name: "index_leaves_on_user_id", using: :btree

  create_table "line_items", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "brand_drug_id"
    t.integer  "quantity"
    t.string   "category"
    t.date     "expiry"
    t.integer  "medical_unit_id"
    t.integer  "device_id"
    t.integer  "inventory_id"
    t.string   "order_type"
    t.string   "status"
    t.text     "description"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "user_id"
    t.string   "reason"
    t.integer  "order_item_id"
    t.string   "transaction_type"
    t.integer  "store_id"
    t.integer  "department_id"
    t.integer  "batch_id"
    t.integer  "visit_id"
  end

  add_index "line_items", ["visit_id"], name: "index_line_items_on_visit_id", using: :btree

  create_table "lookups", force: :cascade do |t|
    t.string   "category"
    t.string   "key"
    t.string   "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "parent_id"
  end

  create_table "machines", force: :cascade do |t|
    t.string   "machine_code"
    t.string   "machine_name"
    t.integer  "medical_unit_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "lab_assessment_id"
    t.string   "machine_parameter_code"
  end

  add_index "machines", ["lab_assessment_id"], name: "index_machines_on_lab_assessment_id", using: :btree
  add_index "machines", ["medical_unit_id"], name: "index_machines_on_medical_unit_id", using: :btree

  create_table "medical_unit_users", force: :cascade do |t|
    t.integer  "medical_unit_id"
    t.integer  "user_id"
    t.integer  "role_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "medical_units", force: :cascade do |t|
    t.integer  "region_id"
    t.string   "title"
    t.string   "identification_number"
    t.string   "location"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "category"
    t.integer  "bays_count",            default: 0
    t.integer  "parent_id"
    t.string   "code"
  end

  add_index "medical_units", ["region_id"], name: "index_medical_units_on_region_id", using: :btree

  create_table "medications", force: :cascade do |t|
    t.integer  "patient_id"
    t.integer  "user_id"
    t.integer  "medical_unit_id"
    t.integer  "visit_id"
    t.integer  "brand_drug_id"
    t.string   "dosage"
    t.string   "unit"
    t.string   "route"
    t.string   "frequency"
    t.string   "usage"
    t.string   "instruction"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "type"
  end

  create_table "medicine_requests", force: :cascade do |t|
    t.integer  "medical_unit_id"
    t.integer  "patient_id"
    t.integer  "visit_id"
    t.integer  "admission_id"
    t.integer  "prescribed_by"
    t.integer  "requested_by"
    t.string   "status"
    t.integer  "item_id"
    t.string   "request_id"
    t.float    "prescribed_qty"
    t.string   "product_name"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "store_id"
  end

  add_index "medicine_requests", ["admission_id"], name: "index_medicine_requests_on_admission_id", using: :btree
  add_index "medicine_requests", ["item_id"], name: "index_medicine_requests_on_item_id", using: :btree
  add_index "medicine_requests", ["medical_unit_id"], name: "index_medicine_requests_on_medical_unit_id", using: :btree
  add_index "medicine_requests", ["patient_id"], name: "index_medicine_requests_on_patient_id", using: :btree
  add_index "medicine_requests", ["visit_id"], name: "index_medicine_requests_on_visit_id", using: :btree

  create_table "notes", force: :cascade do |t|
    t.integer  "visit_id"
    t.string   "department"
    t.text     "chief_complaint"
    t.text     "med_history"
    t.text     "surgical_history"
    t.text     "physical_exam"
    t.text     "instructions"
    t.text     "plan"
    t.string   "type"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "user_id"
    t.integer  "device_id"
    t.integer  "medical_unit_id"
    t.text     "subjective"
    t.date     "operation_date"
    t.text     "procedure_indications"
    t.text     "surgeon_assistant_note"
    t.text     "procedure_description"
    t.text     "anesthesia"
    t.text     "complications"
    t.text     "specimens"
    t.string   "blood_loss"
    t.string   "disposition"
    t.date     "admission_date"
    t.date     "discharge_date"
    t.text     "free_text"
    t.text     "hospital_course"
    t.text     "follow_up"
  end

  add_index "notes", ["visit_id"], name: "index_notes_on_visit_id", using: :btree

  create_table "nursing_order_details", force: :cascade do |t|
    t.integer  "order_id"
    t.string   "department"
    t.string   "instructions"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "order_items", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "brand_drug_id"
    t.integer  "quantity_prescribed"
    t.string   "frequency_unit"
    t.integer  "quantity_calculated"
    t.integer  "issued_brand_drug_id"
    t.text     "notes"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "duration_unit"
    t.integer  "duration"
    t.string   "type"
    t.float    "quantity"
    t.string   "frequency"
    t.boolean  "prn"
    t.string   "route"
    t.integer  "note_id"
    t.string   "dosage"
    t.string   "dosage_unit"
    t.string   "strength"
  end

  create_table "order_logs", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "lab_order_detail_id"
    t.string   "status"
    t.string   "updated_by"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "patient_id"
    t.integer  "doctor_id"
    t.string   "status"
    t.integer  "medical_unit_id"
    t.integer  "device_id"
    t.integer  "updated_by"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.string   "department"
    t.string   "category"
    t.string   "order_type",                default: "Medicine"
    t.integer  "visit_id"
    t.string   "cancel_reason"
    t.string   "cancel_reason_details"
    t.integer  "store_id"
    t.integer  "department_id"
    t.string   "case_number"
    t.string   "external_medicine"
    t.string   "barcode_url"
    t.string   "lab_order_sequence_number"
  end

  add_index "orders", ["medical_unit_id"], name: "index_orders_on_medical_unit_id", using: :btree

  create_table "organizations", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "medical_unit_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "organizations", ["medical_unit_id"], name: "index_organizations_on_medical_unit_id", using: :btree
  add_index "organizations", ["user_id"], name: "index_organizations_on_user_id", using: :btree

  create_table "patient_diagnoses", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "code"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "note_id"
    t.string   "type"
    t.integer  "visit_id"
    t.integer  "patient_id"
    t.text     "instructions"
    t.text     "plan"
  end

  create_table "patient_family_medical_histories", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "patient_id"
    t.integer  "visit_id"
    t.integer  "medical_unit_id"
    t.string   "note"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "patient_family_medical_histories", ["medical_unit_id"], name: "index_patient_family_medical_histories_on_medical_unit_id", using: :btree
  add_index "patient_family_medical_histories", ["patient_id"], name: "index_patient_family_medical_histories_on_patient_id", using: :btree
  add_index "patient_family_medical_histories", ["user_id"], name: "index_patient_family_medical_histories_on_user_id", using: :btree
  add_index "patient_family_medical_histories", ["visit_id"], name: "index_patient_family_medical_histories_on_visit_id", using: :btree

  create_table "patient_histories", force: :cascade do |t|
    t.integer  "visit_id"
    t.integer  "patient_id"
    t.integer  "medical_unit_id"
    t.string   "visit_reason"
    t.string   "chief_complaint"
    t.text     "diagnosis"
    t.string   "notes"
    t.text     "medication_order"
    t.text     "disposition"
    t.string   "allergies"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "doctor_name"
    t.integer  "user_id"
  end

  create_table "patient_past_medical_histories", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "patient_id"
    t.integer  "visit_id"
    t.integer  "medical_unit_id"
    t.string   "med_name"
    t.string   "note"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "patient_past_medical_histories", ["medical_unit_id"], name: "index_patient_past_medical_histories_on_medical_unit_id", using: :btree
  add_index "patient_past_medical_histories", ["patient_id"], name: "index_patient_past_medical_histories_on_patient_id", using: :btree
  add_index "patient_past_medical_histories", ["user_id"], name: "index_patient_past_medical_histories_on_user_id", using: :btree
  add_index "patient_past_medical_histories", ["visit_id"], name: "index_patient_past_medical_histories_on_visit_id", using: :btree

  create_table "patients", force: :cascade do |t|
    t.integer  "registered_by"
    t.integer  "registered_at"
    t.string   "mrn"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "middle_name"
    t.string   "gender"
    t.string   "education"
    t.integer  "birth_day"
    t.integer  "birth_month"
    t.integer  "birth_year"
    t.string   "patient_nic"
    t.string   "patient_passport"
    t.boolean  "unidentify_patient",    default: false
    t.string   "guardian_relationship"
    t.string   "guardian_first_name"
    t.string   "guardian_last_name"
    t.string   "guardian_middle_name"
    t.string   "guardian_nic"
    t.string   "guardian_passport"
    t.boolean  "unidentify_guardian",   default: false
    t.string   "state"
    t.string   "city"
    t.string   "near_by_city"
    t.string   "address1"
    t.string   "address2"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "phone1"
    t.string   "phone1_type"
    t.string   "phone2"
    t.string   "phone2_type"
    t.string   "phone3"
    t.string   "phone3_type"
    t.string   "marital_status"
    t.string   "blood_group"
    t.string   "hiv"
    t.string   "hepatitis_b_antigens"
    t.string   "hepatitis_c"
    t.string   "age"
    t.string   "father_name"
    t.boolean  "unidentify_gender",     default: false
    t.string   "guardian_state"
    t.string   "guardian_city"
    t.string   "guardian_near_by_city"
    t.string   "guardian_address"
    t.string   "guardian_phone_type"
    t.string   "guardian_phone"
    t.string   "barcode_url"
    t.string   "qrcode_url"
    t.boolean  "unidentify",            default: false
    t.string   "external_patient_id"
  end

  add_index "patients", ["guardian_nic"], name: "index_patients_on_guardian_nic", using: :btree
  add_index "patients", ["mrn"], name: "index_patients_on_mrn", unique: true, using: :btree
  add_index "patients", ["patient_nic"], name: "index_patients_on_patient_nic", using: :btree
  add_index "patients", ["phone1"], name: "index_patients_on_phone1", using: :btree

  create_table "permissions", force: :cascade do |t|
    t.integer  "role_id"
    t.string   "activity"
    t.boolean  "access",     default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "pictures", force: :cascade do |t|
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "user_id"
    t.string   "file"
    t.integer  "visit_id"
  end

  add_index "pictures", ["user_id"], name: "index_pictures_on_user_id", using: :btree
  add_index "pictures", ["visit_id"], name: "index_pictures_on_visit_id", using: :btree

  create_table "preferences", force: :cascade do |t|
    t.string   "department"
    t.string   "category"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "procedure_order_details", force: :cascade do |t|
    t.integer  "order_id"
    t.string   "procedure"
    t.string   "option"
    t.boolean  "left",         default: false
    t.boolean  "right",        default: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "instructions"
    t.integer  "note_id"
    t.string   "type"
  end

  create_table "radiology_order_details", force: :cascade do |t|
    t.integer  "order_id"
    t.string   "imaging_type"
    t.string   "area"
    t.string   "contrast_option"
    t.string   "other_option"
    t.boolean  "routine",         default: false
    t.boolean  "urgent",          default: false
    t.boolean  "left",            default: false
    t.boolean  "right",           default: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.text     "instructions"
    t.text     "note"
    t.string   "result"
  end

  create_table "regions", force: :cascade do |t|
    t.string   "category"
    t.string   "name"
    t.integer  "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "regions", ["category"], name: "index_regions_on_category", using: :btree
  add_index "regions", ["parent_id"], name: "index_regions_on_parent_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "title"
    t.string   "default_screen"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "rosters", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "medical_unit_id"
    t.integer  "department_id"
    t.string   "doctors"
    t.string   "shift"
    t.date     "date"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "rosters", ["department_id"], name: "index_rosters_on_department_id", using: :btree
  add_index "rosters", ["medical_unit_id"], name: "index_rosters_on_medical_unit_id", using: :btree
  add_index "rosters", ["user_id"], name: "index_rosters_on_user_id", using: :btree

  create_table "services", force: :cascade do |t|
    t.string   "category"
    t.string   "title"
    t.string   "charges"
    t.integer  "medical_unit_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "currency"
  end

  add_index "services", ["medical_unit_id"], name: "index_services_on_medical_unit_id", using: :btree

  create_table "stores", force: :cascade do |t|
    t.string   "name"
    t.integer  "medical_unit_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "parent_id"
    t.integer  "department_id"
  end

  add_index "stores", ["department_id"], name: "index_stores_on_department_id", using: :btree
  add_index "stores", ["medical_unit_id"], name: "index_stores_on_medical_unit_id", using: :btree

  create_table "stores_users", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "store_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "study_templates", force: :cascade do |t|
    t.integer  "lab_study_id"
    t.string   "caption"
    t.string   "range"
    t.string   "unit"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "test_label"
  end

  create_table "user_departments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "department_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "user_wards", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "ward_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.integer  "role_id"
    t.string   "username",               default: "", null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "title"
    t.string   "phone"
    t.string   "nic"
    t.date     "dob"
    t.string   "father_name"
    t.string   "father_nic"
    t.string   "mother_name"
    t.string   "mother_nic"
    t.string   "address1"
    t.string   "authentication_token"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "address2"
    t.integer  "department_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

  create_table "vaccinations", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "patient_id"
    t.integer  "visit_id"
    t.integer  "medical_unit_id"
    t.string   "name"
    t.date     "given_on"
    t.date     "follow_up"
    t.string   "instruction"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "vaccinations", ["medical_unit_id"], name: "index_vaccinations_on_medical_unit_id", using: :btree
  add_index "vaccinations", ["patient_id"], name: "index_vaccinations_on_patient_id", using: :btree
  add_index "vaccinations", ["user_id"], name: "index_vaccinations_on_user_id", using: :btree
  add_index "vaccinations", ["visit_id"], name: "index_vaccinations_on_visit_id", using: :btree

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  create_table "visit_histories", force: :cascade do |t|
    t.string   "referred_to"
    t.string   "ref_department"
    t.integer  "medical_unit_id"
    t.integer  "referred_by"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "visit_id"
  end

  create_table "visit_users", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "visit_id"
    t.boolean  "is_checkout", default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "visit_users", ["user_id"], name: "index_visit_users_on_user_id", using: :btree
  add_index "visit_users", ["visit_id"], name: "index_visit_users_on_visit_id", using: :btree

  create_table "visits", force: :cascade do |t|
    t.integer  "medical_unit_id"
    t.integer  "patient_id"
    t.integer  "user_id"
    t.string   "visit_number"
    t.string   "reason"
    t.string   "reason_note"
    t.string   "ref_department"
    t.string   "ref_department_note"
    t.string   "mode_of_conveyance"
    t.string   "mode_of_conveyance_note"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "referred_to"
    t.float    "amount_paid"
    t.string   "current_ref_department"
    t.string   "current_referred_to"
    t.boolean  "is_active",               default: true
    t.string   "ambulance_number"
    t.string   "driver_name"
    t.boolean  "mlc",                     default: false
    t.string   "policeman_name"
    t.string   "belt_number"
    t.string   "police_station_number"
    t.integer  "department_id"
    t.integer  "doctor_id"
    t.string   "external_visit_number"
  end

  add_index "visits", ["medical_unit_id"], name: "index_visits_on_medical_unit_id", using: :btree
  add_index "visits", ["patient_id"], name: "index_visits_on_patient_id", using: :btree

  create_table "vitals", force: :cascade do |t|
    t.integer  "patient_id"
    t.integer  "user_id"
    t.integer  "medical_unit_id"
    t.integer  "visit_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.float    "weight"
    t.float    "height"
    t.float    "temperature"
    t.float    "bp_systolic"
    t.float    "bp_diastolic"
    t.float    "pulse"
    t.float    "resp_rate"
    t.float    "o2_saturation"
    t.float    "head_circ"
  end

  create_table "ward_comments", force: :cascade do |t|
    t.integer  "medical_unit_id"
    t.integer  "user_id"
    t.integer  "patient_id"
    t.integer  "visit_id"
    t.integer  "admission_id"
    t.integer  "bed_allocation_id"
    t.string   "comments"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "wards", force: :cascade do |t|
    t.integer  "medical_unit_id"
    t.integer  "department_id"
    t.integer  "parent_id"
    t.string   "title"
    t.boolean  "active",          default: true
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "user_id"
    t.integer  "beds_count",      default: 0
    t.integer  "bays_count",      default: 0
    t.datetime "deleted_at"
    t.string   "ward_code"
  end

  add_index "wards", ["deleted_at"], name: "index_wards_on_deleted_at", using: :btree

  add_foreign_key "addendums", "notes"
  add_foreign_key "brands", "companies"
  add_foreign_key "follow_ups", "medical_units"
  add_foreign_key "follow_ups", "patients"
  add_foreign_key "follow_ups", "users"
  add_foreign_key "follow_ups", "visits"
  add_foreign_key "histories", "patients"
  add_foreign_key "invoice_items", "invoices"
  add_foreign_key "invoices", "medical_units"
  add_foreign_key "invoices", "patients"
  add_foreign_key "invoices", "users"
  add_foreign_key "invoices", "visits"
  add_foreign_key "item_stocks", "departments"
  add_foreign_key "item_stocks", "items"
  add_foreign_key "item_stocks", "medical_units"
  add_foreign_key "item_stocks", "stores"
  add_foreign_key "item_stocks", "users"
  add_foreign_key "item_transactions", "departments"
  add_foreign_key "item_transactions", "items"
  add_foreign_key "item_transactions", "medical_units"
  add_foreign_key "item_transactions", "patients"
  add_foreign_key "item_transactions", "stores"
  add_foreign_key "item_transactions", "users"
  add_foreign_key "item_transactions", "visits"
  add_foreign_key "items", "departments"
  add_foreign_key "items", "generic_items"
  add_foreign_key "items", "medical_units"
  add_foreign_key "items", "users"
  add_foreign_key "lab_order_details", "orders"
  add_foreign_key "leaves", "departments"
  add_foreign_key "leaves", "medical_units"
  add_foreign_key "leaves", "users"
  add_foreign_key "machines", "lab_assessments"
  add_foreign_key "machines", "medical_units"
  add_foreign_key "medicine_requests", "admissions"
  add_foreign_key "medicine_requests", "items"
  add_foreign_key "medicine_requests", "medical_units"
  add_foreign_key "medicine_requests", "patients"
  add_foreign_key "medicine_requests", "visits"
  add_foreign_key "notes", "visits"
  add_foreign_key "organizations", "medical_units"
  add_foreign_key "organizations", "users"
  add_foreign_key "patient_family_medical_histories", "medical_units"
  add_foreign_key "patient_family_medical_histories", "patients"
  add_foreign_key "patient_family_medical_histories", "users"
  add_foreign_key "patient_family_medical_histories", "visits"
  add_foreign_key "patient_past_medical_histories", "medical_units"
  add_foreign_key "patient_past_medical_histories", "patients"
  add_foreign_key "patient_past_medical_histories", "users"
  add_foreign_key "patient_past_medical_histories", "visits"
  add_foreign_key "rosters", "departments"
  add_foreign_key "rosters", "medical_units"
  add_foreign_key "rosters", "users"
  add_foreign_key "services", "medical_units"
  add_foreign_key "stores", "medical_units"
  add_foreign_key "vaccinations", "medical_units"
  add_foreign_key "vaccinations", "patients"
  add_foreign_key "vaccinations", "users"
  add_foreign_key "vaccinations", "visits"
  add_foreign_key "visit_users", "users"
  add_foreign_key "visit_users", "visits"
end
