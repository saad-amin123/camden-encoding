class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.references :role
      t.string :username,              null: false, default: ""
      t.string :first_name
      t.string :last_name
      t.string :title
      t.string :phone
      t.string :nic
      t.date :dob
      t.string :father_name
      t.string :father_nic
      t.string :mother_name
      t.string :mother_nic
      t.string :address
      t.string :authentication_token
      t.timestamps null: false
    end
  end
end
