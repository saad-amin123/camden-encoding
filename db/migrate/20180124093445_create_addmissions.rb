class CreateAddmissions < ActiveRecord::Migration
  def change
    create_table :admissions do |t|
      t.integer :medical_unit_id
      t.integer :visit_id
      t.integer :patient_id
      t.integer :referred_from
      t.integer :referred_to
      t.integer :ward_id
      t.integer :doctor_id
      t.integer :user_id
      t.string :attendent_name
      t.string :attendent_mobile
      t.string :attendent_address
      t.string :attendent_nic
      t.string :attendent_relationship
      t.string :status

      t.timestamps null: false
    end
  end
end
