class CreateStudyTemplates < ActiveRecord::Migration
  def change
    create_table :study_templates do |t|
      t.references :lab_study
      t.string :caption
      t.string :range
      t.string :unit
      t.timestamps null: false
    end
  end
end
