class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :category
      t.string :title
      t.string :charges
      t.references :medical_unit, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
