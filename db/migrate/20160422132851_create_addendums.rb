class CreateAddendums < ActiveRecord::Migration
  def change
    create_table :addendums do |t|
      t.text :historical_note
      t.references :note, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
