class AddStockItemRefInTransaction < ActiveRecord::Migration
  def change
  	add_reference :item_transactions, :item_stock, index: true
  end
end
