class ChangeTypeOfCritical < ActiveRecord::Migration
  def change
  	remove_column :complaints, :critical
  	add_column :complaints, :critical, :boolean, default: false
  end
end
