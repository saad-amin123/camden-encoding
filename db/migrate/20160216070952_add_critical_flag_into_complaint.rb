class AddCriticalFlagIntoComplaint < ActiveRecord::Migration
  def change
  	add_column :complaints, :critical, :boolean, default: false
  end
end
