class AddDeletedAtToBay < ActiveRecord::Migration
  def change
    add_column :bays, :deleted_at, :datetime
    add_index :bays, :deleted_at
  end
end
