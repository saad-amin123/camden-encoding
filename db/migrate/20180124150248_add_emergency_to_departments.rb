class AddEmergencyToDepartments < ActiveRecord::Migration
  def change
    add_column :departments, :emergency, :boolean,:default => false
  end
end
