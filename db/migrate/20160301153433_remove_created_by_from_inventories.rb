class RemoveCreatedByFromInventories < ActiveRecord::Migration
  def change
    remove_column :inventories, :created_by, :integer
    remove_column :inventories, :updated_by, :integer
  end
end
