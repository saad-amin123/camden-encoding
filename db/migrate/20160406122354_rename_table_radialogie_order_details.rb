class RenameTableRadialogieOrderDetails < ActiveRecord::Migration
  def change
  	rename_table :radiologie_order_details, :radiology_order_details
  end
end
