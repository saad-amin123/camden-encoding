class WardsCountToDepartment < ActiveRecord::Migration
  def change
  	add_column :departments, :wards_count, :integer, :default => 0
  end
end
