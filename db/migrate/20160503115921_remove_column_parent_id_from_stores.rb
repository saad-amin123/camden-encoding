class RemoveColumnParentIdFromStores < ActiveRecord::Migration
  def change
    remove_column :stores, :parent_id, :string
    add_column :stores, :parent_id, :integer
  end
end
