class CreateBatches < ActiveRecord::Migration
  def change
    create_table :batches do |t|
      t.string :number
      t.datetime :expiry_date
      t.timestamps null: false
    end
  end
end
