class AddColumnTypeToPatientDiagnoses < ActiveRecord::Migration
  def change
    add_column :patient_diagnoses, :type, :string
  end
end
