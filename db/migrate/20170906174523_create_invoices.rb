class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.references :user, index: true, foreign_key: true
      t.references :medical_unit, index: true, foreign_key: true
      t.references :patient, index: true, foreign_key: true
      t.references :visit, index: true, foreign_key: true
      t.references :assignable, :polymorphic => true
      t.string :total_amount
      t.string :discount_percentage
      t.string :discount_value
      t.string :total_discount
      t.string :payable
      t.string :paid

      t.timestamps null: false
    end
  end
end
