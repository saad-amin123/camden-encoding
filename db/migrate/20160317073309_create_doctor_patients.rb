class CreateDoctorPatients < ActiveRecord::Migration
  def change
    create_table :doctor_patients do |t|
      t.integer :doctor_id
      t.string :patient_mrn
      t.boolean :active, default: false

      t.timestamps null: false
    end
  end
end
