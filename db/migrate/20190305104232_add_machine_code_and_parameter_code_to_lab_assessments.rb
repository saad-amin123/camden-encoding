class AddMachineCodeAndParameterCodeToLabAssessments < ActiveRecord::Migration
  def change
    add_column :lab_assessments, :machine_code, :string
    add_column :lab_assessments, :machine_parameter_code, :string
    add_column :lab_assessments, :machine_name, :string
  end
end
