class AddResultInRadiology < ActiveRecord::Migration
  def change
    add_column :radiology_order_details, :result, :string
  end
end
