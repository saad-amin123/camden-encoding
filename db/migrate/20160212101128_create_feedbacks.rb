class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.references :complaint
      t.references :medical_unit
      t.references :user
      t.references :device
      t.string :satisfaction
      t.string :message

      t.timestamps null: false
    end
  end
end
