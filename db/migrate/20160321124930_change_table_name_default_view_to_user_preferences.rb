class ChangeTableNameDefaultViewToUserPreferences < ActiveRecord::Migration
  def change
  	rename_table :default_views, :preferences
  end
end
