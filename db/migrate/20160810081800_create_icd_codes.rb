class CreateIcdCodes < ActiveRecord::Migration
  def change
    create_table :icd_codes do |t|
      t.string :code
      t.string :name
      t.timestamps 
    end
  end
end
