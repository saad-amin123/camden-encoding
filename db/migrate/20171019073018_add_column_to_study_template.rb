class AddColumnToStudyTemplate < ActiveRecord::Migration
  def change
    add_column :study_templates, :test_label, :string
  end
end
