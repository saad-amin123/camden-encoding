class CreateHomeMedications < ActiveRecord::Migration
  def change
    create_table :home_medications do |t|
      t.references :patient
      t.references :user
      t.references :medical_unit
      t.references :visit
      t.references :brand_drug
      t.string :dosage
      t.string :unit
      t.string :route
      t.string :frequency
      t.string :usage
      t.string :instruction

      t.timestamps null: false
    end
  end
end
