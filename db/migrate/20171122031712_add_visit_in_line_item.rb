class AddVisitInLineItem < ActiveRecord::Migration
  def change
  	add_reference :line_items, :visit, index: true
  end
end
