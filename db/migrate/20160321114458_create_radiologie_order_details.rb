class CreateRadiologieOrderDetails < ActiveRecord::Migration
  def change
    create_table :radiologie_order_details do |t|
      t.references :order
      t.string :imaging_type
      t.string :area
      t.string :contrast_option
      t.string :other_option
      t.boolean :routine, default: false
      t.boolean :urgent, default: false
      t.boolean :left, default: false
      t.boolean :right, default: false

      t.timestamps null: false
    end
  end
end
