class RemoveIssueIdFromLineItems < ActiveRecord::Migration
  def change
    remove_column :line_items, :issue_id, :integer
    remove_column :line_items, :issue_type, :string
    remove_column :line_items, :added_by, :integer
    remove_column :line_items, :updated_by, :integer
    add_column :line_items, :user_id, :integer
  end
end
