class RemoveColumnFromOrders < ActiveRecord::Migration
  def change
    remove_column :orders, :prn, :boolean
    remove_column :orders, :route, :string
    add_column :order_items, :prn, :boolean
    add_column :order_items, :route, :string
  end
end
