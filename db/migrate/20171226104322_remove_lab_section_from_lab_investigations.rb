class RemoveLabSectionFromLabInvestigations < ActiveRecord::Migration
  def change
  	remove_column :lab_investigations, :lab_section, :string
  end
end
