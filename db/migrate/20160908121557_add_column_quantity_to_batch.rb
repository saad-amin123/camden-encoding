class AddColumnQuantityToBatch < ActiveRecord::Migration
  def change
  	add_column :batches ,:quantity ,:integer, :default => 0
  end
end
