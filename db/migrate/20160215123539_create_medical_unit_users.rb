class CreateMedicalUnitUsers < ActiveRecord::Migration
  def change
    create_table :medical_unit_users do |t|
      t.references :medical_unit
      t.references :user
      t.references :role
      t.timestamps null: false
    end
  end
end
