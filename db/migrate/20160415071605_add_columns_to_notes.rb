class AddColumnsToNotes < ActiveRecord::Migration
  def change
    add_column :notes, :user_id, :integer
    add_column :notes, :device_id, :integer
    add_column :notes, :medical_unit_id, :integer
  end
end
