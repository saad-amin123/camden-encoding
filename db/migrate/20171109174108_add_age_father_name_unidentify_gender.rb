class AddAgeFatherNameUnidentifyGender < ActiveRecord::Migration
  def change
  	add_column :patients, :age, :string
  	add_column :patients, :father_name, :string
  	add_column :patients, :unidentify_gender, :boolean, default: false
  end
end
