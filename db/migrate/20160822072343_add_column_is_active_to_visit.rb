class AddColumnIsActiveToVisit < ActiveRecord::Migration
  def change
  	add_column :visits, :is_active, :boolean , :default => true
  end
end
