class RemoveColumnPatientMrnFromPatientLocations < ActiveRecord::Migration
  def change
    remove_column :patient_locations, :patient_mrn, :string
  end
end
