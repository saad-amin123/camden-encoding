class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.references :company, index: true, foreign_key: true
      t.string :name
      t.string :category

      t.timestamps null: true
    end
  end
end
