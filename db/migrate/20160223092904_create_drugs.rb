class CreateDrugs < ActiveRecord::Migration
  def change
    create_table :drugs do |t|
      t.string :name
      t.text :overview
      t.text :characterstics
      t.text :indications
      t.text :contradictions
      t.text :interactions
      t.text :interference
      t.text :effects
      t.text :risk
      t.text :warning
      t.text :storage
      t.string :category

      t.timestamps null: true
    end
  end
end
