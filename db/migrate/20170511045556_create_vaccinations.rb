class CreateVaccinations < ActiveRecord::Migration
  def change
    create_table :vaccinations do |t|
      t.references :user, index: true, foreign_key: true
      t.references :patient, index: true, foreign_key: true
      t.references :visit, index: true, foreign_key: true
      t.references :medical_unit, index: true, foreign_key: true
      t.string :name
      t.date :given_on
      t.date :follow_up
      t.string :instruction

      t.timestamps null: false
    end
  end
end
