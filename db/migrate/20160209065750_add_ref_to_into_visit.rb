class AddRefToIntoVisit < ActiveRecord::Migration
  def change
  	add_column :visits, :referred_to, :string
  end
end
