class CreateMedicalUnits < ActiveRecord::Migration
  def change
    create_table :medical_units do |t|
      t.references :region
      t.string :title
      t.string :identification_number
      t.string :location
      t.float :latitude 
      t.float :longitude
      t.timestamps null: false
    end
  end
end
