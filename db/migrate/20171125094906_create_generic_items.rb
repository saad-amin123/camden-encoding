class CreateGenericItems < ActiveRecord::Migration
  def change
    create_table :generic_items do |t|
      t.string :name
      t.string :category

      t.timestamps null: true
    end
  end
end
