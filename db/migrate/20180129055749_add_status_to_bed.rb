class AddStatusToBed < ActiveRecord::Migration
  def change
    add_column :beds, :status, :string, :default => 'Free'
  end
end
