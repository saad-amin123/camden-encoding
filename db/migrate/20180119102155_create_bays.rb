class CreateBays < ActiveRecord::Migration
  def change
    create_table :bays do |t|
      t.integer :medical_unit_id
      t.integer :department_id
      t.integer :parent_id
      t.string :title

      t.timestamps null: false
    end
  end
end
