class AddDeptIdToLabInvestigations < ActiveRecord::Migration
  def change
  	add_column :lab_investigations, :department_id, :integer
  end
end
