class RemoveColumnPatientIdFromPatientDiagnoses < ActiveRecord::Migration
  def change
    remove_column :patient_diagnoses, :patient_id, :integer
    add_column :patient_diagnoses, :visit_id, :integer
  end
end
