class CreateItemStocks < ActiveRecord::Migration
  def change
    create_table :item_stocks do |t|
      t.references :medical_unit, index: true, foreign_key: true
      t.references :department, index: true, foreign_key: true
      t.references :store, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.references :item, index: true, foreign_key: true
      t.string :quantity_in_hand

      t.timestamps null: false
    end
  end
end
