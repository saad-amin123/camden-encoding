class AddColumnStoreIdToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :store_id, :integer
    add_column :orders, :department_id, :integer
  end
end
