class AddLabAssessmentToMachine < ActiveRecord::Migration
  def change
    add_reference :machines, :lab_assessment, index: true, foreign_key: true
    add_column :machines, :machine_parameter_code, :string
  end
end
