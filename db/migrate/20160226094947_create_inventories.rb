class CreateInventories < ActiveRecord::Migration
  def change
    create_table :inventories do |t|
      t.integer :brand_drug_id
      t.integer :quantity
      t.integer :medical_unit_id
      t.integer :created_by
      t.integer :updated_by
      t.integer :device_id

      t.timestamps null: false
    end
  end
end
