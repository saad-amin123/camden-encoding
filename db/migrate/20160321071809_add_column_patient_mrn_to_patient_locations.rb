class AddColumnPatientMrnToPatientLocations < ActiveRecord::Migration
  def change
    add_column :patient_locations, :patient_mrn, :string
  end
end
