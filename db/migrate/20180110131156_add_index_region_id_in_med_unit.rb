class AddIndexRegionIdInMedUnit < ActiveRecord::Migration
  def change
  	add_index :medical_units, :region_id
  end
end
