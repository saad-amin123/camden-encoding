class CreateBeds < ActiveRecord::Migration
  def change
    create_table :beds do |t|
      t.string :bed_number
      t.integer :bay_id
      t.integer :ward_id
      t.integer :medical_unit_id

      t.timestamps null: false
    end
  end
end
