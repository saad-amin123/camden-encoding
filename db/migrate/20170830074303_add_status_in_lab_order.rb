class AddStatusInLabOrder < ActiveRecord::Migration
  def change
    add_column :lab_order_details, :status, :string, :default => 'Open'
  end
end
