class AddIndexLabResultIdToLabResultParameter < ActiveRecord::Migration
  def change
    add_index :lab_result_parameters , :lab_result_id
  end
end
