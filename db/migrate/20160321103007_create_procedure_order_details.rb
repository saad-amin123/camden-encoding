class CreateProcedureOrderDetails < ActiveRecord::Migration
  def change
    create_table :procedure_order_details do |t|
      t.references :order
      t.string :procedure
      t.string :option
      t.boolean :left, default: false
      t.boolean :right, default: false

      t.timestamps null: false
    end
  end
end
