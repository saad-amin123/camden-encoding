class ChangeTypeOfIdentity < ActiveRecord::Migration
  def change
  	change_column :complaints, :show_identity, 'boolean USING CAST(show_identity AS boolean)', default: false
  end
end
