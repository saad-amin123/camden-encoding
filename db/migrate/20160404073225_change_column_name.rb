class ChangeColumnName < ActiveRecord::Migration
  def change
    remove_column :admission_order_details, :type, :string
    add_column :admission_order_details, :admission_type, :string
  end
end
