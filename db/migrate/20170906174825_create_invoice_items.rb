class CreateInvoiceItems < ActiveRecord::Migration
  def change
    create_table :invoice_items do |t|
      t.references :invoice, index: true, foreign_key: true
      t.references :assignable, :polymorphic => true
      t.string :invoice_type
      t.string :description
      t.string :amount

      t.timestamps null: false
    end
  end
end
