class AddColumnInstructionsToPatientDiagnoses < ActiveRecord::Migration
  def change
    add_column :patient_diagnoses, :instructions, :text
  end
end
