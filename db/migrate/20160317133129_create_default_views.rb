class CreateDefaultViews < ActiveRecord::Migration
  def change
    create_table :default_views do |t|
      t.string :department
      t.string :category
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
