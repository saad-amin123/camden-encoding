class CreateAdmissionOrderDetails < ActiveRecord::Migration
  def change
    create_table :admission_order_details do |t|
      t.references :order
      t.string :category
      t.string :department
      t.string :type

      t.timestamps null: false
    end
  end
end
