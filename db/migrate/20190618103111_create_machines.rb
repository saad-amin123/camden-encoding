class CreateMachines < ActiveRecord::Migration
  def change
    create_table :machines do |t|
      t.string :machine_code
      t.string :machine_name
      t.references :medical_unit, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
