class PatientIdIndexOnVisit < ActiveRecord::Migration
  def change
  	add_index :visits, :patient_id
  	add_index :visits, :medical_unit_id
  end
end
