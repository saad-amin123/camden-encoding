class RemoveQuantityAndAddProductNameInItem < ActiveRecord::Migration
  def change
  	remove_column :items, :quantity, :string
  	remove_column :items, :quantity_in_hand, :string
  	remove_column :items, :store_id
  	add_column :items, :product_name, :string
  end
end
