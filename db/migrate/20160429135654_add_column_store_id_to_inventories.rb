class AddColumnStoreIdToInventories < ActiveRecord::Migration
  def change
    add_column :inventories, :store_id, :integer
    add_column :inventories, :department_id, :integer
    add_column :line_items, :store_id, :integer
    add_column :line_items, :department_id, :integer
  end
end
