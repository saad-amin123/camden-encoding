class AddColumnTransactionTypeToLineItems < ActiveRecord::Migration
  def change
    add_column :line_items, :transaction_type, :string
  end
end
