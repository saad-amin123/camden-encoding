class ChangeColumnsIntoVitals < ActiveRecord::Migration
  def change
  	remove_column :vitals, :weight, :float
    remove_column :vitals, :height, :float
    remove_column :vitals, :temperature, :float
    remove_column :vitals, :bp_systolic, :float
    remove_column :vitals, :bp_diastolic, :float
    remove_column :vitals, :pulse, :float
    remove_column :vitals, :resp_rate, :float
    remove_column :vitals, :o2_saturation, :float
    remove_column :vitals, :head_circ, :float


    add_column :vitals, :weight, :float
    add_column :vitals, :height, :float
    add_column :vitals, :temperature, :float
    add_column :vitals, :bp_systolic, :float
    add_column :vitals, :bp_diastolic, :float
    add_column :vitals, :pulse, :float
    add_column :vitals, :resp_rate, :float
    add_column :vitals, :o2_saturation, :float
    add_column :vitals, :head_circ, :float
  end
end
