class AddColumnsInOrderTable < ActiveRecord::Migration
  def change
    add_column :orders, :department, :string
    add_column :orders, :category, :string
    add_column :orders, :order_type, :string, :default => 'Medicine'
    add_column :orders, :visit_id, :integer
  end
end
