class RenameTableHomeMedicationsToMedication < ActiveRecord::Migration
  def change
    rename_table :home_medications, :medications
  end
end
