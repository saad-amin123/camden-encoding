class AddHighPriorityInComplaint < ActiveRecord::Migration
  def change
  	add_column :complaints, :high_priority, :boolean, default: false
  end
end
