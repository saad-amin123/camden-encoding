class CreateVitals < ActiveRecord::Migration
  def change
    create_table :vitals do |t|
      t.references :patient
      t.references :user
      t.references :medical_unit
      t.references :visit
      t.float :weight
      t.float :height
      t.float :temperature
      t.float :bp_systolic
      t.float :bp_diastolic
      t.float :pulse
      t.float :resp_rate
      t.float :o2_saturation
      t.float :head_circ

      t.timestamps null: false
    end
  end
end
