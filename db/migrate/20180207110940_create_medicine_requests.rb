class CreateMedicineRequests < ActiveRecord::Migration
  def change
    create_table :medicine_requests do |t|
      t.references :medical_unit, index: true, foreign_key: true
      t.references :patient, index: true, foreign_key: true
      t.references :visit, index: true, foreign_key: true
      t.references :admission, index: true, foreign_key: true
      t.integer :prescribed_by
      t.integer :requested_by
      t.string :status
      t.references :item, index: true, foreign_key: true
      t.string :request_id
      t.float :prescribed_qty
      t.string :product_name
      t.timestamps null: false
    end
  end
end
