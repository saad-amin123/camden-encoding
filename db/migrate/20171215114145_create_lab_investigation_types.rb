class CreateLabInvestigationTypes < ActiveRecord::Migration
  def change
    create_table :lab_investigation_types do |t|
    	t.string :title

      t.timestamps null: false
    end
  end
end
