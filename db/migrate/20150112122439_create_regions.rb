class CreateRegions < ActiveRecord::Migration
  def change
    create_table :regions do |t|
      t.string :category
      t.string :name
      t.integer :parent_id	
      t.timestamps null: false
    end
  end
end
