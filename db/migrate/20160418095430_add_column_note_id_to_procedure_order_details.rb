class AddColumnNoteIdToProcedureOrderDetails < ActiveRecord::Migration
  def change
    add_column :procedure_order_details, :note_id, :integer
    add_column :order_items, :note_id, :integer
    add_column :notes, :disposition, :string
  end
end
