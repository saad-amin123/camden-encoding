class CreateLabInvestigationPrices < ActiveRecord::Migration
  def change
    create_table :lab_investigation_prices do |t|
    	t.integer :medical_unit_id
    	t.integer :lab_investigation_id
    	t.string  :department_name
    	t.integer :price
      t.timestamps null: false
    end
  end
end
