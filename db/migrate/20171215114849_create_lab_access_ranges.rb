class CreateLabAccessRanges < ActiveRecord::Migration
  def change
    create_table :lab_access_ranges do |t|
    	t.string  :gender
    	t.integer :age_start
    	t.integer :age_end
      t.string  :date_unit
    	t.float   :start_range
    	t.float   :end_range
    	t.string  :range_title
    	t.string  :status
      t.string  :heading
      t.integer :lab_assessment_id

      t.timestamps null: false
    end
  end
end
