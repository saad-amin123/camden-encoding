class DriverNameAndAmbulanceInVisitModel < ActiveRecord::Migration
  def change
  	add_column :visits, :ambulance_number, :string
  	add_column :visits, :driver_name, :string
  end
end
