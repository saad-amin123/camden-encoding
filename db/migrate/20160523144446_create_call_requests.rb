class CreateCallRequests < ActiveRecord::Migration
  def change
    create_table :call_requests do |t|
      t.integer :request_from
      t.integer :request_to
      t.string :code
      t.timestamps null: false
    end
  end
end
