class CreateVisits < ActiveRecord::Migration
  def change
    create_table :visits do |t|
      t.references :medical_unit
      t.references :patient
      t.references :user
      t.string :visit_number
      t.string :reason
      t.string :reason_note
      t.string :ref_department
      t.string :ref_department_note
      t.string :mode_of_conveyance
      t.string :mode_of_conveyance_note
      t.timestamps null: false
    end
  end
end
