class AddPoliceCaseInVisit < ActiveRecord::Migration
  def change
  	add_column :visits, :mlc, :boolean, default: false
  	add_column :visits, :policeman_name, :string
  	add_column :visits, :belt_number, :string
  	add_column :visits, :police_station_number, :string
  end
end
