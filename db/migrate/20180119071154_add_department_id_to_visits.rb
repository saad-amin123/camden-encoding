class AddDepartmentIdToVisits < ActiveRecord::Migration
  def change
    add_column :visits, :department_id, :integer
  end
end
