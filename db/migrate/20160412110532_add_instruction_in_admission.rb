class AddInstructionInAdmission < ActiveRecord::Migration
  def change
    add_column :admission_order_details, :instructions, :string
  end
end
