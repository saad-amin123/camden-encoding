class CreateJoinTableUserMedicalUnit < ActiveRecord::Migration
  def change
    create_join_table :Users, :MedicalUnits do |t|
      # t.index [:user_id, :medical_unit_id]
      # t.index [:medical_unit_id, :user_id]
    end
  end
end
