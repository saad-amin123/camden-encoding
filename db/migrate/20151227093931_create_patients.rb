class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.integer :registered_by
      t.integer :registered_at
      t.string :mrn
      t.string :first_name
      t.string :last_name
      t.string :middle_name
      t.string :gender
      t.string :education
      t.integer :birth_day
      t.integer :birth_month
      t.integer :birth_year
      t.string :patient_nic
      t.string :patient_passport
      t.boolean :unidentify_patient , default: false
      t.string :guardian_relationship
      t.string :guardian_first_name
      t.string :guardian_last_name
      t.string :guardian_middle_name
      t.string :guardian_nic
      t.string :guardian_passport
      t.boolean :unidentify_guardian , default: false
      
      t.string :state
      t.string :city
      t.string :near_by_city
      t.string :address1
      t.string :address2
      
      t.timestamps null: false
    end
  end
end
