class CreateWardComments < ActiveRecord::Migration
  def change
    create_table :ward_comments do |t|
      t.integer :medical_unit_id
      t.integer :user_id
      t.integer :patient_id
      t.integer :visit_id
      t.integer :admission_id
      t.integer :bed_allocation_id
      t.string :comments

      t.timestamps null: false
    end
  end
end
