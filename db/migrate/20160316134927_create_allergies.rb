class CreateAllergies < ActiveRecord::Migration
  def change
    create_table :allergies do |t|
      t.references :patient
      t.references :user
      t.references :medical_unit
      t.references :visit
      t.string :allergy_type
      t.string :value

      t.timestamps null: false
    end
  end
end
