class CreateLeaves < ActiveRecord::Migration
  def change
    create_table :leaves do |t|
      t.references :user, index: true, foreign_key: true
      t.references :medical_unit, index: true, foreign_key: true
      t.references :department, index: true, foreign_key: true
      t.string :attendance
      t.string :shift
      t.date :date

      t.timestamps null: false
    end
  end
end
