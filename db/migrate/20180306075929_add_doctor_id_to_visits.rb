class AddDoctorIdToVisits < ActiveRecord::Migration
  def change
    add_column :visits, :doctor_id, :integer
  end
end
