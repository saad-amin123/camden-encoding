class AddStoreIdToMedicineRequests < ActiveRecord::Migration
  def change
    add_column :medicine_requests, :store_id, :integer
  end
end
