class AddAddress2IntoUsers < ActiveRecord::Migration
  def change
  	add_column :users, :address2, :string
  	rename_column :users, :address, :address1
  end
end
