class AddInstructionsInProcedure < ActiveRecord::Migration
  def change
    add_column :procedure_order_details, :instructions, :string
  end
end
