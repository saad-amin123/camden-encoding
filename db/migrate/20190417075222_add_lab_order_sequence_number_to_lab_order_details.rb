class AddLabOrderSequenceNumberToLabOrderDetails < ActiveRecord::Migration
  def change
    add_column :orders, :lab_order_sequence_number, :string
  end
end
