# helper
module ApplicationHelper

  def create_lab_order_sequence(order_id)
    day_serial_no = order_id
    order_day = day_serial_no[0..1]
    current_day = Date.today.strftime("%d")
    current_month = Date.today.strftime("%m")
    current_year = Date.today.strftime("%y")
    current_date =  Date.today().strftime('%y%m%d')
    last_month = Date.today.ago(1.month).strftime("%m")
    previous_date = current_year + current_month + order_day

    if current_day != order_day
      if current_date > previous_date
        @date = previous_date
      else
        last_month_date = current_year + last_month  + order_day
        if last_month_date > current_date
          last_year = Date.today.ago(1.year).strftime("%y") + last_month + order_day
          @date = last_year
        else
          @date = last_month_date
        end
      end
    else
      @date = current_year + current_month + order_day
    end
    return @date
  end

end
