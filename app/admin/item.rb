ActiveAdmin.register Item do
  permit_params :id, :generic_item_id, :medical_unit_id, 
    :department_id, :user_id, :item_name, :item_code, 
    :item_type, :main_group, :uom, :upc, :price, :sub_group, 
    :strength, :created_at, :updated_at, :status, :product_name, 
    :pack, :unit, :pack_size
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  form do |f|
    f.inputs do
      f.input :generic_item
      f.input :medical_unit
      f.input :department
      f.input :user
      f.input :item_name
      f.input :item_code
      f.input :item_type
      f.input :main_group
      f.input :uom
      f.input :upc
      f.input :price
      f.input :sub_group
      f.input :strength
      f.input :status
      f.input :product_name
      f.input :pack
      f.input :unit
      f.input :pack_size
    end
    f.actions
  end


end
