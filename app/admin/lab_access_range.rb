ActiveAdmin.register LabAccessRange do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
	permit_params :lab_assessment_id, :medical_unit_id, 
	:lab_investigation_id, :gender, :age_start, :age_end, 
	:date_unit, :start_range, :end_range, :range_title, 
	:status, :heading
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
	form do |f|
    f.inputs do
      f.semantic_errors # shows errors on :base
      f.input :lab_assessment
      f.input :medical_unit, input_html: { id:'medicalUnitIDofLab'}
      f.input :lab_investigation, input_html: { id:'labInvestigationID' , disabled: 'disabled'}
      f.input :gender
      f.input :age_start
      f.input :age_end
      f.input :date_unit, collection: ["Year", "Month", "Day"]
      f.input :start_range
      f.input :end_range
      f.input :range_title
      f.input :status, collection: ["true", "false"]
      f.input :heading
    end
    f.actions
  end

end
