ActiveAdmin.register Complaint do

  scope :all, default: true
  scope :Open
  scope :Closed
  scope :In_Progress
  scope :Pending
  scope :Resolved

  actions :all, except: [:destroy]
  permit_params :user_id, :device_id, :medical_unit_id, :category, :area,
                :description, :severity_level, :show_identity, :status,
                :assign_to_role, :critical, :high_priority, :created_by

  index do
    selectable_column
    id_column
    column :user
    column :medical_unit
    column :category
    column :area
    column :status
    column :critical
    actions
  end

  form do |f|
    f.inputs do
      f.input :user, as: :select, collection: (User.all)
      f.input :device, as: :select, collection: (Device.all.map{|d| [d.uuid,d.id]})
      f.input :medical_unit, as: :select, collection: (MedicalUnit.all)
      f.input :category
      f.input :area
      f.input :description
      f.input :severity_level
      f.input :show_identity
      f.input :assign_to_role
      f.input :critical
      f.input :created_by
      f.input :high_priority
      f.input :status,
              as: :select,
              collection: Complaint::STATUS.collect { |status| [status, status] } # rubocop:disable Metrics/LineLength
    end
    f.actions # adds the 'Submit' and 'Cancel' buttons
  end

  show do
    attributes_table :user, :device, :medical_unit, :category, :area,
                     :description, :severity_level, :show_identity, :status,
                     :assign_to_role, :critical, :high_priority
  end
end
