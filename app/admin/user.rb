ActiveAdmin.register User do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end


  permit_params :role_id, :username, :first_name, :last_name, :phone,
                :nic, :dob, :father_name, :father_nic, :mother_name,
                :mother_nic, :address1, :address2, :authentication_token,
                :email, :password, :password_confirmation, department_ids: [], 
                store_ids: []

  index do
    selectable_column
    id_column
    column :username
    column :first_name
    column :last_name
    column :phone
    column :nic
    column :dob
    column :father_name
    column :father_nic
    column :mother_name
    column :mother_nic
    column :address1
    column :address2
    column :email
    actions
  end

  filter :username
  filter :first_name
  filter :nic
  filter :email

  form do |f|
    f.inputs do
      # f.input :role, as: :select, collection: (Role.all).order('title ASC')
      f.input :username
      f.input :first_name
      f.input :last_name
      f.input :phone
      f.input :nic
      f.input :dob, start_year: Time.now.year - 80, end_year: Time.now.year - 8
      f.input :father_name
      f.input :father_nic
      f.input :mother_name
      f.input :mother_nic
      f.input :address1
      f.input :address2
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :store_ids, as: :check_boxes, collection: Store.all, label: "Stores"
      f.input :departments , collection: (Department.where.not(department_code: nil)).order('title ASC')

    end
    f.actions
  end

  show do
    attributes_table :role, :username, :first_name, :last_name,
                     :phone, :nic, :dob,
                     :father_name, :father_nic, :mother_name, :mother_nic,
                     :address1, :address2, :email, :authentication_token
  end
end
