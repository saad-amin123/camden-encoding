ActiveAdmin.register MedicalUnitUser do
  menu parent: "Manage Medical Unit"

  filter :medical_unit, collection: proc { MedicalUnit.order(:title) }
  filter :user, collection: proc { User.order(:first_name) }
  filter :role, collection: proc { Role.order(:title) }


  index do
    selectable_column
    id_column
    column :user
    column :medical_unit

    actions
  end

  form do |f|
    f.inputs do
      f.input :user
      f.input :medical_unit
      f.input :role
    end
    f.actions # adds the 'Submit' and 'Cancel' buttons
  end

  show do
    attributes_table :id, :user, :medical_unit, :role
  end
end
