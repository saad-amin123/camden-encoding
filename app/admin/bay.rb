ActiveAdmin.register Bay do

	filter :medical_unit, collection: proc { MedicalUnit.order(:title) }
	filter :ward, collection: proc { Ward.order(:title) }
	index do
		column :medical_unit
		column :title
		column :ward
		column :created_at
		column :updated_at

		column "action" do |resource|
	      links = ''.html_safe
	      links += link_to I18n.t('active_admin.view'), resource_path(resource),:class => "member_link view_link"
	      links += link_to I18n.t('active_admin.edit'), edit_resource_path(resource), :class => "member_link edit_link"
	      links += link_to I18n.t('active_admin.delete'), resource_path(resource), :method => :delete, :confirm => I18n.t('active_admin.delete_confirmation'), :class => "member_link delete_link"
	      links
	    end
	end	

	show do
		attributes_table do
			row	:medical_unit
			row :title
			row :ward
			row :created_at
			row :updated_at
	    end
	end	

	form do |f|
      f.inputs do
      f.input :medical_unit_id,
              as: :select,
              collection: (MedicalUnit.all),
              label: 'Medical Unit', 
              input_html: { id:'medicalUnitId'}
              
      if f.object.new_record?
      	f.input :ward,input_html: { id:'wardID' , disabled: 'disabled'}
      else
      	f.input :ward
      end
      f.input :title
    end
    f.actions
  end

end