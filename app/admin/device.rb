ActiveAdmin.register Device do
  scope :all, default: true
  scope :active
  scope :inactive
  scope :lost

  form do |f|
    f.inputs do
      f.semantic_errors # shows errors on :base
      f.input :role
      f.input :uuid
      f.input :assignable_type,
              input_html: { class: 'polyselect' },
              collection: Device::ASSIGNABLE_TYPES
      f.input :assignable_id,
              as: :select,
              collection: (MedicalUnit.all),
              input_html: { class: 'medical-unit-select', disabled: 'disabled' }, # rubocop:disable Metrics/LineLength
              label: 'Medical Unit'
      f.input :assignable_id,
              as: :select,
              collection: (User.all),
              input_html: { class: 'user-select', disabled: 'disabled' },
              label: 'User'
      f.input :operating_system
      f.input :os_version
      f.input :status,
              as: :select,
              collection: Device::STATUS.collect { |s| [s, s] }
    end
    f.actions # adds the 'Submit' and 'Cancel' buttons
  end
end
