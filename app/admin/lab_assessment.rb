ActiveAdmin.register LabAssessment do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
	permit_params :title, :uom, :specimen, :status, :result_type1, 
	:result_type2, :parameter_code, :machine_parameter_code, :machine_code
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
	form do |f|
    f.inputs do
      f.semantic_errors # shows errors on :base
      f.input :title
      f.input :uom
      f.input :specimen
      f.input :status, collection: ["true", "false"]
      f.input :result_type1, collection: ["string", "decimal"]
      f.input :result_type2, collection: ["true", "false"]
      f.input :machine_parameter_code
      f.input :machine_code
      if f.object.new_record?
        f.input :parameter_code
      end
    end
    f.actions
  end

end
