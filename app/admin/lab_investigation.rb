ActiveAdmin.register LabInvestigation do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
	permit_params :lab_investigation_type_id, :medical_unit, :department_id, 
	:profile_name, :status, :profile_code, :report_type, lab_assessment_ids: []
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
  filter :medical_unit, collection: proc { MedicalUnit.order(:title) }
  filter :lab_assessments, collection: proc { LabAssessment.order(:title) }

	form do |f|
    f.inputs do
      f.semantic_errors # shows errors on :base
      f.input :lab_investigation_type
      f.input :medical_unit
      f.input :department
      f.input :price
      f.input :profile_name
      f.input :status, collection: ["true", "false"]
      f.input :profile_code
      f.input :report_type
      f.input :lab_assessments, :as => :select, :input_html => {:multiple => true}
    end
    f.actions
  end

  controller do
    def create
      li = LabInvestigation.create(profile_name: params[:lab_investigation][:profile_name],
        medical_unit_id: params[:lab_investigation][:medical_unit_id],
        status: params[:lab_investigation][:status],
        lab_investigation_type_id: params[:lab_investigation][:lab_investigation_type_id],
        profile_code: params[:lab_investigation][:profile_code],
        report_type: params[:lab_investigation][:report_type],
        department_id: params[:lab_investigation][:department_id],
        )
      
      li.lab_assessments << LabAssessment.where(id: params[:lab_investigation][:lab_assessment_ids])
      
      LabInvestigationPrice.create(price: params[:lab_investigation][:price],medical_unit_id: params[:lab_investigation][:medical_unit_id], lab_investigation_id: params[:lab_investigation][:lab_investigation_type_id])
      redirect_to admin_lab_investigations_url
    end
  end


end
