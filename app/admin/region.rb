ActiveAdmin.register Region do
  menu parent: 'Global Settings'

  scope :all, default: true
  scope :country
  scope :province
  scope :district
  scope :tehsil

  form do |f|
    f.inputs do
      f.semantic_errors # shows errors on :base
      f.input :parent
      f.input :category,
              as: :select,
              collection: Region::CATEGORY.collect { |category| [category, category] } # rubocop:disable Metrics/LineLength
      f.input :name
    end
    f.actions # adds the 'Submit' and 'Cancel' buttons
  end
end
