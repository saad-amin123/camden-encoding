ActiveAdmin.register Visit do

  menu parent: 'Manage Patient'

  permit_params :reason, :reason_note, :referred_to,
                :ref_department, :ref_department_note,
                :mode_of_conveyance, :mode_of_conveyance_note,
                :amount_paid

  index do
    selectable_column
    id_column
    column :visit_number
    column :reason
    column :reason_note
    column :referred_to
    column :ref_department
    column :ref_department_note
    column :mode_of_conveyance
    column :mode_of_conveyance_note
    column :amount_paid
    column :department_id
    actions
  end

  form do |f|
    f.inputs do
      f.input :reason
      f.input :reason_note
      f.input :referred_to
      f.input :ref_department
      f.input :ref_department_note
      f.input :mode_of_conveyance
      f.input :mode_of_conveyance_note
      f.input :amount_paid
    end
    f.actions # adds the 'Submit' and 'Cancel' buttons
  end

  show do
    attributes_table  :visit_number, :reason, :reason_note, :referred_to,
                      :ref_department, :ref_department_note,
                      :mode_of_conveyance, :mode_of_conveyance_note,
                      :amount_paid
  end
end
