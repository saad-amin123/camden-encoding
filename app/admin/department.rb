ActiveAdmin.register Department do
	filter :medical_unit, collection: proc { Department.order(:title) }
  filter :title


	index do
		column :title
		column :medical_unit
		column :parent
		column :department_code
		column :lookup
		column :ipd
		column :opd
		column :emergency
		column :wards_count
		column :created_at
		column :updated_at

		column "action" do |resource|
	      links = ''.html_safe
	      links += link_to I18n.t('active_admin.view'), resource_path(resource),:class => "member_link view_link"
	      links += link_to I18n.t('active_admin.edit'), edit_resource_path(resource), :class => "member_link edit_link"
	      links += link_to I18n.t('active_admin.delete'), resource_path(resource), :method => :delete, :confirm => I18n.t('active_admin.delete_confirmation'), :class => "member_link delete_link"
	      links
	    end
	end	

	show do
		attributes_table do
			row :title
			row :medical_unit
			row :parent
			row :department_code
			row :lookup
			row :ipd
			row :opd
			row :emergency
			row :wards_count
			row :created_at
			row :updated_at
	    end
	end	

	form do |f|
      f.inputs do
      	  f.input :title
	      f.input :medical_unit
	      f.input :parent
	      f.input :department_code
	      f.input :lookup
	      f.input :ipd
	      f.input :opd
	      f.input :emergency
    	end
    f.actions
  end


end
