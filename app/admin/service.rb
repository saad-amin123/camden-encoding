ActiveAdmin.register Service do
  filter :medical_unit, collection: proc { MedicalUnit.order(:title) }
end