ActiveAdmin.register Bed do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
# == Schema Information
#
# Table name: bed
#
# 	string   "bed_number"
# 	integer  "bay_id"
# 	integer  "ward_id"
# 	integer  "medical_unit_id"
# 	datetime "created_at",      null: false
# 	datetime "updated_at",      null: false
# 	integer  "facility_id"
#permit_params :medical_unit_id, :ward_id, :bay_id, :bed_number

	filter :bay, collection: proc { Bay.order(:title) }
	filter :ward, collection: proc { Ward.order(:title) }

index do
		column :bay_id
		column :bed_number
		column :facility_id
		column :ward_id
		column :medical_unit_id
		column :created_at
		column :updated_at

		column "action" do |resource|
	      links = ''.html_safe
	      links += link_to I18n.t('active_admin.view'), resource_path(resource),:class => "member_link view_link"
	      links += link_to I18n.t('active_admin.edit'), edit_resource_path(resource), :class => "member_link edit_link"
	      links += link_to I18n.t('active_admin.delete'), resource_path(resource), :method => :delete, :confirm => I18n.t('active_admin.delete_confirmation'), :class => "member_link delete_link"
	      links
	    end
	end	

	show do
		attributes_table do
			row :bay_id
			row :bed_number
			row :facility_id
			row :ward_id
			row :medical_unit_id
			row :created_at
			row :updated_at
	    end
	end	

	form do |f|
      f.inputs do
      f.input :medical_unit_id,
              as: :select,
              collection: (MedicalUnit.all),
              label: 'Medical Unit', 
              input_html: { id:'medicalUnitId'}
              
      if f.object.new_record?
      	f.input :ward,input_html: { id:'wardID' , disabled: 'disabled'}
      else
      	f.input :ward
      end
      if f.object.new_record?
      	f.input :bay,input_html: { id:'bayID' , disabled: 'disabled'}
      else
      f.input :bay
  	  end
      f.input :bed_number
    end
    f.actions
  end


end
