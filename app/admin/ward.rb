ActiveAdmin.register Ward do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
  
 	filter :department, collection: proc { Department.order(:title) }

	index do
		column :medical_unit_id
		column :department
		column :parent
		column :title
		column :active
		column :created_at
		column :updated_at

		column "action" do |resource|
	      links = ''.html_safe
	      links += link_to I18n.t('active_admin.view'), resource_path(resource),:class => "member_link view_link"
	      links += link_to I18n.t('active_admin.edit'), edit_resource_path(resource), :class => "member_link edit_link"
	      links += link_to I18n.t('active_admin.delete'), resource_path(resource), :method => :delete, :confirm => I18n.t('active_admin.delete_confirmation'), :class => "member_link delete_link"
	      links
	    end
	end	

	show do
		attributes_table do
			row	:medical_unit
			row :department
			row :parent
			row :title
			row :active
			row :created_at
			row :updated_at
	    end
	end	

	form do |f|
      f.inputs do
      f.input :medical_unit_id,
              as: :select,
              collection: (MedicalUnit.all),
              label: 'Medical Unit'
      f.input :parent
      f.input :title
      f.input :active
    end
    f.actions
  end

end
