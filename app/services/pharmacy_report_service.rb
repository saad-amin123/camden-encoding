class PharmacyReportService
  def initialize(user_id, store_id, hospital_id, start_date = '', end_date = '')
    @user_id = user_id
    @store_id = store_id
    @hospital_id = hospital_id
    @start_date = "#{start_date} 00:00:00"
    @end_date = "#{end_date} 23:59:59"
  end

  def current_stock
    q = "SELECT 
    items.product_name as medicine_name, 
    item_stocks.quantity_in_hand, 
    current_timestamp::date as date,
    current_timestamp::time as time 
    from items
    INNER JOIN item_stocks ON items.id=item_stocks.item_id
    where item_stocks.medical_unit_id = #{@hospital_id} AND 
    item_stocks.store_id = #{@store_id}
    order by item_stocks.created_at DESC;"
    csv_data = ActiveRecord::Base.connection.execute(q)
    generate_csv(current_stock_params, csv_data)
  end

  def medicine_list
    q = "SELECT product_name as Item_name, 
    item_code as Item_code, item_type as Type, 
    uom as UOM, upc as UPC, price as Price, 
    strength as Strength, status as Status from items 
    where medical_unit_id = #{@hospital_id} 
    order by created_at DESC;"
    csv_data = ActiveRecord::Base.connection.execute(q)
    generate_csv(medicine_list_params, csv_data)
  end

  def stock_balance
    q = "WITH ISSUE_ADD AS 
      (
        SELECT ITEM_ID,
        PRODUCT_NAME,
        PHARMACIST_NAME,
        SUM(CASE TRANSACTION_TYPE
          WHEN 'issue' THEN CAST(QUANTITY AS FLOAT)
            ELSE 0
        END) AS ISSUE,
        SUM(CASE TRANSACTION_TYPE
          WHEN 'add' THEN CAST(QUANTITY AS FLOAT)
          ELSE 0
        END) AS ADD,
        SUM(CASE TRANSACTION_TYPE
          WHEN 'adjust' THEN (CAST(QUANTITY AS FLOAT) - CAST(OPEN_STOCK AS FLOAT))
          ELSE 0
        END) AS ADJUST
        FROM (
          SELECT * FROM ITEM_TRANSACTIONS 
          WHERE(CREATED_AT >= '#{@start_date}' 
            AND CREATED_AT <= '#{@end_date}'
            AND STORE_ID = #{@store_id}
          )
        ) AS ITEM_TRANSACTIONS,
        ITEMS
        WHERE ITEMS.ID = ITEM_TRANSACTIONS.ITEM_ID
          AND QUANTITY NOT IN ('')
        GROUP BY ITEM_ID,
          PHARMACIST_NAME,
          PRODUCT_NAME
      ), OPENING AS
      (
        WITH OPENING_INNER AS 
        (
          SELECT ITEM_ID, OPEN_STOCK,
            ROW_NUMBER() OVER (PARTITION BY ITEM_ID ORDER BY CREATED_AT ASC) AS rn 
          FROM ITEM_TRANSACTIONS 
          WHERE(CREATED_AT >= '#{@start_date}' 
          AND CREATED_AT <= '#{@end_date}' 
          AND STORE_ID = #{@store_id})
        )
        SELECT * FROM OPENING_INNER WHERE rn = 1
      ), CLOSING AS
      (
        WITH CLOSING_INNER AS 
        (
          SELECT it.ITEM_ID, i.QUANTITY_IN_HAND,
            ROW_NUMBER() OVER (PARTITION BY it.ITEM_ID ORDER BY it.CREATED_AT DESC) AS rn 
          FROM ITEM_TRANSACTIONS AS it
          INNER JOIN ITEM_STOCKS AS i ON i.ITEM_ID = it.ITEM_ID
          WHERE(it.CREATED_AT >= '#{@start_date}' 
          AND it.CREATED_AT <= '#{@end_date}' 
          AND it.STORE_ID = #{@store_id})
        )
        SELECT * FROM CLOSING_INNER WHERE rn = 1
      )
      SELECT ISSUE_ADD.ITEM_ID,
      ISSUE_ADD.PHARMACIST_NAME as pharmacist_name,
      ISSUE_ADD.PRODUCT_NAME as medicine_name, 
      ISSUE_ADD.ISSUE as issue, 
      ISSUE_ADD.ADD as add,
      ISSUE_ADD.ADJUST as adjust, 
      OPENING.OPEN_STOCK as opening, 
      CLOSING.QUANTITY_IN_HAND as closing 
      FROM ISSUE_ADD
      INNER JOIN OPENING AS OPENING 
      ON ISSUE_ADD.ITEM_ID = OPENING.ITEM_ID
      INNER JOIN CLOSING AS CLOSING 
      ON ISSUE_ADD.ITEM_ID = CLOSING.ITEM_ID;"
    csv_data = ActiveRecord::Base.connection.execute(q)
    csv = CSV.generate( encoding: 'Windows-1251' ) do |csv|
      csv << ["Medicine_name", "Issue", "Add", "Opening","Adjustment", "Closing","Pharmacist Name"]
      csv_data.each do |record|
        csv << [
          record["medicine_name"].to_s.gsub(";",""),
          record["issue"].to_i,
          record["add"].to_i,
          record["opening"].to_i,
          record["adjust"].to_i,
          (((record["opening"].to_i + record["add"].to_i).to_i - record["issue"].to_i) + record["adjust"].to_i).to_f,
          record["pharmacist_name"].to_s
          # record["closing"].to_i 
          # record["opening"].to_f + record["add"].to_f - record["issue"].to_f + record["adjust"].to_f
          # (((record["opening"].to_i - record["issue"].to_i).abs + record["add"].to_i) + record["adjust"].to_i)
        ]
      end
    end
    return csv
  end

  def issue_stock
    q = "SELECT 
    users.first_name, 
    users.last_name, 
    items.product_name as medicine_name, 
    sum(CAST(t.quantity AS FLOAT)) as total_issued_quantity 
    FROM (select * from item_transactions 
          where quantity NOT IN ('') 
          AND created_at::timestamp >= '#{@start_date}' 
          AND created_at::timestamp <= '#{@end_date}'
          AND transaction_type = 'issue' 
          AND store_id = #{@store_id} 
          order by created_at DESC) as t 
    INNER JOIN items ON items.id=t.item_id 
    INNER JOIN users on users.id = t.user_id
    GROUP BY 
    items.product_name, 
    users.first_name, 
    users.last_name;"
    csv_data = ActiveRecord::Base.connection.execute(q)
    generate_csv(issue_stock_params, csv_data)
  end

  def prescriptions
    q = "select 
    users.first_name, 
    users.last_name, 
    patients.mrn, 
    items.product_name as medicine_name, 
    sum(CAST(t.quantity AS FLOAT)) as quantity, 
    t.created_at::date as issue_date,  
    t.created_at::time as issue_time 
    FROM (select * from item_transactions where quantity NOT IN ('') 
          AND created_at::timestamp >= '#{@start_date}' 
          AND created_at::timestamp <= '#{@end_date}' 
          AND store_id = #{@store_id} 
          AND transaction_type = 'issue') as t 
    INNER JOIN users ON t.user_id=users.id 
    INNER JOIN patients on t.patient_id = patients.id 
    INNER JOIN items on t.item_id = items.id 
    GROUP BY 
    users.first_name, 
    users.last_name,
    patients.mrn, 
    items.product_name, 
    t.created_at
    order by t.created_at DESC;"
    csv_data = ActiveRecord::Base.connection.execute(q)
    generate_csv(prescriptions_params, csv_data)
  end

  private

  def medicine_list_params
    ["Item_name", "Item_code", "Type", "UOM", "UPC", 
      "Price", "Strength", "Status"]
  end

  def prescriptions_params
    ["Operator_name", "MRN", "Medicine_name", "Quantity",
      "Issue_date", "Issue_time"]
  end

  def issue_stock_params
    ["Operator_name", "Medicine_name", "Total_issued_quantity"]
  end

  def current_stock_params
    ["Medicine_name", "Quantity_in_hand", "Date", "Time"]
  end

  def set_date_format(date)
    date.present? ? date.to_date.strftime("%d-%m-%Y") : date
  end

  def generate_csv(parameters = [], csv_data)
    csv = CSV.generate( encoding: 'Windows-1251' ) do |csv|
      csv << parameters
      csv_data.each do |record|
        parameters_array = []
        parameters.each do |parameter|
          parameter = parameter.downcase
          if parameter == "operator_name"
            parameters_array << record["first_name"].to_s.gsub(";","") + ' ' + record["last_name"].to_s.gsub(";","")
          elsif (parameter == "date" or parameter == "issue_date")
            parameters_array << set_date_format(record[parameter].to_s.gsub(";",""))
          elsif (parameter == "time" or parameter == "issue_time")
            parameters_array << record[parameter].split('.')[0]
          else
            parameters_array << record[parameter].to_s.gsub(";","")
          end
        end
        csv << parameters_array
      end
    end
    return csv
  end
end