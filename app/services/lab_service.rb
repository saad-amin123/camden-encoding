class LabService
  def initialize(hospital_id, patient)
    @hospital_id = hospital_id
    @patient = patient
    if patient.present?
      date_unit = @patient.age.split(' ')[1].downcase
      if "years".include?(date_unit)
        @patient_age = @patient.age.split(' ')[0].to_i * 365
      elsif "months".include?(date_unit)
        @patient_age = (@patient.age.split(' ')[0].to_f * (365.0/12.0)).round
      else
        @patient_age = @patient.age.split(' ')[0].to_i
      end
    end
  end

  def hospital_tests_list
    sql = "SELECT li.id, li.profile_name 
    from lab_investigations as li 
    where li.status='true' AND medical_unit_id = #{@hospital_id};"
    data = ActiveRecord::Base.connection.execute(sql)
    return data
  end

  def test_parameters_ranges(p_name, current_user)
    profile_name = p_name
    profile = LabInvestigation.find_by(profile_name: profile_name, medical_unit_id: @hospital_id)

    if profile.blank?
      profile_name = p_name + ' '
      profile = LabInvestigation.find_by(profile_name: profile_name, medical_unit_id: @hospital_id)
    end

    if profile.present?
      profile_id = "AND lar.lab_investigation_id = #{profile.id}"
      # profile_id = "AND lar.lab_investigation_id = #{profile.id};"
    else
      profile_id = ""
      # profile_id = ";"
    end
    sql = "SELECT '#{profile_name}' as profile_name, la.title as parameter_title, 
    la.uom, la.result_type1, la.result_type2, lar.gender, 
    lar.age_start, lar.age_end, lar.range_title, lar.heading 
    FROM lab_assessments as la 
    INNER JOIN lab_access_ranges as lar 
    ON lar.lab_assessment_id = la.id 
    INNER JOIN lab_assessments_investigations as lai 
    ON la.id = lai.lab_assessment_id 
    WHERE lai.lab_investigation_id IN (
      SELECT  li.id FROM lab_investigations as li 
      WHERE li.status = 'true' 
      AND li.medical_unit_id = #{@hospital_id} 
      AND li.profile_name = '#{profile_name}' 
      ORDER BY li.id ASC LIMIT 1
    ) AND (la.status = 'true' 
    AND lar.status = 'true' 
    AND lar.gender = '#{@patient.gender}' 
    AND lar.medical_unit_id = #{@hospital_id} 
    AND #{@patient_age} between lar.age_start and lar.age_end) 
    #{profile_id} ORDER by lai.sequence ASC;"
    # sql = "WITH li AS(
    #   SELECT * FROM lab_investigations 
    #   WHERE status = 'true' AND medical_unit_id = #{@hospital_id} 
    #   AND profile_name = '#{profile_name}'
    # ), la AS(
    #   SELECT * FROM lab_assessments 
    #   WHERE status = 'true' AND medical_unit_id = #{@hospital_id}
    # ), lar AS(
    #   SELECT * FROM lab_access_ranges 
    #   WHERE status = 'true' AND gender = '#{@patient.gender}' 
    #   AND #{@patient_age} between age_start and age_end 
    # )
    # SELECT la.title as parameter_title, la.uom, la.result_type1, 
    # la.result_type2, lar.gender, lar.age_start, lar.age_end, 
    # lar.range_title, lar.heading 
    # FROM li 
    # INNER JOIN lab_assessments_investigations as lai 
    # ON li.id = lai.lab_investigation_id 
    # INNER JOIN la 
    # ON lai.lab_assessment_id = la.id 
    # INNER JOIN lar 
    # ON la.id = lar.lab_assessment_id;"
    data = ActiveRecord::Base.connection.execute(sql)
    return data
  end
end
