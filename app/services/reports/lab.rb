class Reports::Lab
  def initialize(params, hospital)
    # @start_date = "2018-01-10 00:00:00"
    # @end_date = "2018-01-22 23:59:59"
    start_arr = params[:from].split(" ")
    date_split = start_arr[0].split("/")
    @start_date = date_split[2]+'-'+date_split[0]+'-'+date_split[1]+' '+start_arr[1]
    end_arr = params[:end_date].split(" ")
    date_split = end_arr[0].split("/")
    @end_date = date_split[2]+'-'+date_split[0]+'-'+date_split[1]+' '+end_arr[1]
    @dept = params[:department].present? && params[:department] == "undefined" ? "" : params[:department]
    @profile_name =  params[:test].present? && params[:test] == "undefined" ? '' : params[:test]
    @sample_collector_id = params[:collector].present? && params[:collector] == "undefined" ? '' : params[:collector]
    @lab_technician_id = params[:technician].present? && params[:technician] == "undefined" ? '' : params[:technician]
    @pathologist_id = params[:pathologist].present? && params[:pathologist] == "undefined" ? '' : params[:pathologist]
    @medical_unit_id = params[:medical_unit_id].present? ? params[:medical_unit_id] : hospital.try(:id)
    @medical_unit_id = Order.where(order_type: 'Lab').pluck(:medical_unit_id).uniq.join(', ') if params[:medical_unit_id] == 'All'

    # @mrn = "fdsafasf"
    # @status = "Completed"
  end
  
  def lab_report
    sql = "WITH order_generator AS(
      SELECT * FROM order_logs WHERE status = 'Open'
    ), sample_collector AS(
      SELECT * FROM order_logs WHERE status = 'Collected'
    ), lab_technician AS(
      SELECT * FROM order_logs WHERE status = 'Unverified'
    ), pathologist AS(
      SELECT * FROM order_logs WHERE status = 'Completed'
    )
    SELECT distinct d.title, p.first_name AS patient_first_name,
    p.gender AS patient_gender,p.age AS patient_age,  
    p.last_name AS patient_last_name, p.mrn, lod.test, 
    og.updated_by AS order_generator_name, o.case_number,  
    og.created_at+interval '5 hour' AS order_generation_time, 
    sc.updated_by AS sample_collector_name, 
    lt.updated_by AS lab_technician_name, lt.created_at+interval '5 hour' AS entry_result_time, 
    patho.updated_by AS pathologist_name, patho.created_at+interval '5 hour' AS verification_time 
    FROM lab_results as lr 
    INNER JOIN patients AS p ON lr.patient_id = p.id
    INNER JOIN visits AS v ON v.patient_id = p.id
    INNER JOIN lab_order_details AS lod ON lr.lab_order_detail_id = lod.id 
    INNER JOIN orders AS o ON o.id = lod.order_id 
    INNER JOIN departments AS d ON lod.department_id = d.id 
    LEFT JOIN order_generator AS og ON og.order_id = lod.order_id 
    LEFT JOIN sample_collector AS sc ON sc.order_id = lod.order_id 
    LEFT JOIN lab_technician AS lt ON lt.order_id = lod.order_id 
    LEFT JOIN pathologist AS patho ON patho.order_id = lod.order_id 
    WHERE lr.medical_unit_id IN (#{@medical_unit_id}) 
    AND lr.created_at >= '#{@start_date}' 
    AND lr.created_at <= '#{@end_date}' "
    sql += @dept.present? ? "AND v.referred_to = '#{@dept}'" : " "
    sql += @profile_name.present? ? "AND lod.test = '#{@profile_name}' " : " "
    sql += @sample_collector_id.present? ? "AND sc.id = #{@sample_collector_id} " : " "
    sql += @lab_technician_id.present? ? "AND lt.id = #{@lab_technician_id} " : " "
    sql += @pathologist_id.present? ? "AND patho.id = #{@pathologist_id} " : " "
    sql += @mrn.present? ? "AND p.mrn = #{@mrn} " : " "
    csv_data = ActiveRecord::Base.connection.execute(sql)
    return generate_lab_report_csv(csv_data)
  end

  private

  def generate_lab_report_csv(csv_data)
    csv = CSV.generate( encoding: 'Windows-1251' ) do |csv|
      csv << ["Patient Name", "MR Number","Gender","Age", "Case Number", "Department", "Profile Name", 
        "Order Generator", "Order Generation Time", "Sample Collector", 
        "Lab Technician", "Entry Result Time", "Pathologist"]
      csv_data.each_with_index do |record, index|
        csv << [record["patient_first_name"].to_s.gsub(";","")+record["patient_last_name"].to_s.gsub(";",""), 
          record["mrn"],record["patient_gender"],record["patient_age"],
          record["case_number"], record["title"], 
          record["test"].to_s.gsub(";",""), record["order_generator_name"], 
          set_datetime(record["order_generation_time"]), record["sample_collector_name"], 
          record["lab_technician_name"], set_datetime(record["entry_result_time"]), 
          record["pathologist_name"], set_datetime(record["verification_time"])]
      end
      csv << ["Sub Total", csv_data.field_values('mrn').uniq.length, 
        csv_data.field_values('case_number').uniq.length, "",
        csv_data.field_values('test').length]
    end
    return csv
  end

  def set_datetime(value)
    return '' if value.blank?
    value.to_datetime.strftime("%d/%m/%Y %I:%M:%S %p")
  end
end