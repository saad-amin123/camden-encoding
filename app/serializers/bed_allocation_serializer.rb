# classs Serializer
class BedAllocationSerializer < ActiveModel::Serializer
  attributes :id,
             :medical_unit_id,
             :patient,             
             :admission_id,            
             :ward,                 
             :bay,                 
             :bed,                 
             :status,                  
             :patient_last_updated_by, 
             :user_id,                 
             :visit_id,                
             :created_at,              
             :updated_at,
             :comment 
     
end
