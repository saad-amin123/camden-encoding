class OperativeNoteSerializer < NoteSerializer
  attributes :physical_exam,:instructions,:operation_date,:specimens,:disposition,:procedure_indications,:surgeon_assistant_note,:procedure_description,:anesthesia,:complications,:blood_loss
  has_many :medicines,:procedures
end