class InventorySerializer < ActiveModel::Serializer
  attributes :id, :brand_drug_id, :quantity, :medical_unit_id, :device_id, :updated_at, :store_id, :department_id
  has_one :brand_drug
end
