# classs Serializer
class WardCommentSerializer < ActiveModel::Serializer
  attributes :id,
             :medical_unit_id,
             :patient_id,             
             :admission_id,            
             :visit_id,                 
             :user,                 
             :comments,                 
             :bed_allocation_id,                
             :created_at,              
             :updated_at
     
end
