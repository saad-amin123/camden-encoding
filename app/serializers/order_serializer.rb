class OrderSerializer < ActiveModel::Serializer
  attributes :id, :patient_id, :lab_order_sequence_number, :doctor_id, :status, :device_id, :updated_by, :updated_at, :patient
             #:doctor_name, :patient_name, :patient_birth_day,
             #:patient_birth_month, :patient_birth_year, :patient_mrn,:phone1,:patient_nic, :patient_gender, :patient_age,
             #:order_type, :cancel_reason, :cancel_reason_details, :department_id, :store_id, :visit_id, :case_number, :external_medicine
  #has_many :order_items, :admission_order_details, :consultation_order_details, :lab_test_details, :order_logs, :lab_order_details, :line_items
  #has_one :radiology_order_detail, :procedure_order_detail, :nursing_order_detail
  # has_many :lab_order_details

  def doctor_name
    doc = User.find(doctor_id)
    return doc ? doc.full_name : nil
  end
  def lab_order_details
    if current_user.role.title == 'Lab' || current_user.role.title == 'Pathologist'
      dept_ids = current_user.department.sub_departments.pluck(:id)
      dept_ids << current_user.department.id
      return object.lab_order_details.where(department_id: dept_ids)
    else
      object.lab_order_details
    end
  end
  def current_patient
    patient = Patient.find(patient_id) if self.patient_id.present?
    return patient
  end
  def patient_name
    patient = Patient.find(patient_id) if self.patient_id.present?
    return patient ? patient.full_name : nil
  end
  def patient_mrn
    patient = Patient.find(patient_id) if self.patient_id.present?
    return patient ? patient.mrn : nil
  end
  def patient_gender
    patient = Patient.find(patient_id) if self.patient_id.present?
    return patient ? patient.gender : nil
  end
  def patient_age
    patient = Patient.find(patient_id) if self.patient_id.present?
    return patient ? patient.age : nil
  end
  def patient_nic
    patient = Patient.find(patient_id) if self.patient_id.present?
    return patient ? patient.patient_nic : nil
  end
  def phone1
    patient = Patient.find(patient_id) if self.patient_id.present?
    return patient ? patient.phone1 : nil
  end
  def patient_birth_day
    patient = Patient.find(patient_id) if self.patient_id.present?
    return patient ? patient.birth_day : nil
  end
  def patient_birth_month
    patient = Patient.find(patient_id) if self.patient_id.present?
    return patient ? patient.birth_month : nil
  end
  def patient_birth_year
    patient = Patient.find(patient_id) if self.patient_id.present?
    return patient ? patient.birth_year : nil
  end
  def medical_unit_title
    medical_unit = MedicalUnit.find(medical_unit_id) if self.medical_unit_id.present?
    return medical_unit ? medical_unit.title : nil
  end
  # def status
  #   if object.lab_order_details.present?
  #     # array1 = ["Unverified", "Expired", "Cancel", "Verified"]
  #     # array1 = ["Not_Verified"]
  #     array2 = ["Expired", "Cancel", "Verified"]
  #     if (object.lab_order_details.pluck(:status)- array2).length == 0
  #       object.update_attributes(status: 'Completed')
  #     # elsif object.lab_order_details.pluck(:status).any? {|i| array1.include?(i) }
  #     #   object.update_attributes(status: 'Not_Verified')
  #     elsif object.lab_order_details.pluck(:status).uniq.length==1
  #       object.update_attributes(status: object.lab_order_details.pluck(:status).uniq.first)
  #     else
  #       object.update_attributes(status: 'Incomplete')
  #     end
  #   end
  #   return object.status
  # end
end
