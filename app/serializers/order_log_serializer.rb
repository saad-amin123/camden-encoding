class OrderLogSerializer < ActiveModel::Serializer
  attributes :id, :updated_by, :status, :updated_at, :created_at

  def status
    if object.status == 'Open'
      return 'Ordered'
    elsif object.status == 'Pending'
      return 'Sample Collected'
    elsif object.status == 'Closed'
      return 'Result available'
    elsif object.status == 'Cancel'
      return 'Test not possible'
    elsif object.status == 'Expired'
      return 'Expired within 48 hours'   	
    end
  end
end