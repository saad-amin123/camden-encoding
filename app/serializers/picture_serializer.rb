# classs Serializer
class PictureSerializer < ActiveModel::Serializer
  attributes :id,
             :imageable_id,
             :imageable_type,
             :file,
             :created_at,
             :updated_at
  has_one :user
end
