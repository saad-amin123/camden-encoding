class IcdCodeSerializer < ActiveModel::Serializer
  attributes :id, :code, :name
end
