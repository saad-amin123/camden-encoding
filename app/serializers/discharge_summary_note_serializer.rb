class DischargeSummaryNoteSerializer < NoteSerializer
  attributes :instructions,:admission_date,:discharge_date,:free_text,:disposition,:hospital_course,:follow_up
  has_many :medicines,:procedures,:consultations
end