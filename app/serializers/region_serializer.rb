# classs Serializer
class RegionSerializer < ActiveModel::Serializer
  attributes :id,
             :category,
             :name,
             :parent_id,
             :created_at,
             :updated_at
end
