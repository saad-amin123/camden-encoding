class AddAdmissionSerializer < ActiveModel::Serializer
  attributes :id, :category, :department, :floor_number, :bed_number, :bay_number, :created_at, :updated_at
  has_one :order
end