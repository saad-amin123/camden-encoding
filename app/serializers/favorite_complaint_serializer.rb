# classs Serializer
class FavoriteComplaintSerializer < ActiveModel::Serializer
  attributes :id
  has_one :complaint, :user
end
