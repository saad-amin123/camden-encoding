class PreferenceSerializer < ActiveModel::Serializer
  attributes :department, :category
end
