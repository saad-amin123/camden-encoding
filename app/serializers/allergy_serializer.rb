class AllergySerializer < ActiveModel::Serializer
  attributes :id, :allergy_type, :value, :created_at, :updated_at
end