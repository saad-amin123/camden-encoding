class OrderItemSerializer < ActiveModel::Serializer
  attributes :id, :order_id, :brand_drug_id, :quantity_prescribed, :frequency, :frequency_unit, :duration, :quantity_calculated, :issued_brand_drug_id, :notes, :duration_unit, :type, :quantity, :route, :prn, :dosage, :dosage_unit,:strength,:created_at,:updated_at
  has_one :brand_drug
end
