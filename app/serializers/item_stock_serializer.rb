# classs Serializer
class ItemStockSerializer < ActiveModel::Serializer
  attributes :id,
             :medical_unit_id,  
             :department_id,
             :store_id,   
             :user_id,        
             :quantity_in_hand,
             :item_id,
             :product_name,   
             :created_at,     
             :updated_at

  def product_name
    product_name = object.item.product_name
    return product_name.present? ? product_name : nil
  end
end