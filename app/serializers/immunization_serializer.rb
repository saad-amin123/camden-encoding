class ImmunizationSerializer < ActiveModel::Serializer
  attributes :id, :patient_id, :age, :vaccine, :dose, :due, :given_on,
             :created_at, :updated_at
  has_one :user, :medical_unit, :visit
end