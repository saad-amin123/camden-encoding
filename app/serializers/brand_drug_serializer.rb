class BrandDrugSerializer < ActiveModel::Serializer
  attributes :id, :name, :category, :form, :mg, :drug_id, :brand_id, :quantity, :inventory_updated_at, :last_updated_by
  has_one :brand
  has_one :drug

  def quantity
    begin
      medical_unit_id = current_inventory[0]
      store_id = current_inventory[1]
      department_id = current_inventory[2]
      inventory = Inventory.where(brand_drug_id: object.id, medical_unit_id: medical_unit_id, store_id: store_id, department_id: department_id)
      return inventory.present? ? inventory.first.quantity : 0
    rescue
      return 0
    end
  end
  def inventory_updated_at
    begin
      medical_unit_id = current_inventory[0]
      store_id = current_inventory[1]
      department_id = current_inventory[2]
      updated = Inventory.where(brand_drug_id: object.id, medical_unit_id: medical_unit_id, store_id: store_id, department_id: department_id)
      return updated.present? ? updated.first.updated_at : nil
    rescue
      return nil
    end
  end
  def last_updated_by
    begin
      medical_unit_id = current_inventory[0]
      store_id = current_inventory[1]
      department_id = current_inventory[2]
      user_id = Inventory.where(brand_drug_id: object.id, medical_unit_id: medical_unit_id, store_id: store_id, department_id: department_id).last.line_items.last.user_id
      user = User.find(user_id)
      return user.full_name
    rescue
      return nil
    end
  end
end
