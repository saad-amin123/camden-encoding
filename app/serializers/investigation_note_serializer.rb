# classs Serializer
class InvestigationNoteSerializer < ActiveModel::Serializer
  attributes :id,
             :medical_unit_id,
             :patient_id,             
             :admission_id,
             :investigation_type, 
             :note,                
             :visit_id,                
             :created_at,              
             :updated_at,
             :user 
     
end