class PublicActivity::ActivitySerializer < ActiveModel::Serializer
  attributes    :trackable_id,
                :trackable_type,
                :owner_id,
                :owner_type,
                :parameters,
                :created_at

  has_one :owner                   
    
end