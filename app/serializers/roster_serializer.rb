# classs Serializer
class RosterSerializer < ActiveModel::Serializer
  attributes :id,
             :user_id,
             :medical_unit_id,
             :department_id,
             :doctors,
             :shift,
             :date,
             :created_at,
             :updated_at
  def doctors
  	doctors_list = []
  	if object.doctors.present?
	    object.doctors.split(',').each do |doctor_id|
	      doctor = User.find(doctor_id.to_i)
	      doctors_list << doctor
	    end
	  end
    return doctors_list.present? ? doctors_list : nil
  end
end
