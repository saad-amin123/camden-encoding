class ProgressNoteSerializer < NoteSerializer
  attributes :subjective,:disposition,:physical_exam,:instructions,:plan
  has_many :addendums
end