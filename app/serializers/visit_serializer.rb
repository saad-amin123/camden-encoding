# classs Serializer
class VisitSerializer < ActiveModel::Serializer
  attributes :id,:medical_unit_id,:patient_id,:user,:visit_number,:created_at,:updated_at,:amount_paid,
             :reason,:reason_note,:referred_to,:ref_department,:ref_department_note,:mode_of_conveyance,
             :mode_of_conveyance_note,:current_ref_department,:current_referred_to, :ambulance_number, :driver_name, :medical_unit_title , :department_id,:patient
  # has_one :patient
  has_many :patient_diagnoses
  has_many :vitals
  has_many :notes
  has_many :orders
  has_many :patient_histories
  has_one :department
  
  def medical_unit_title
  	medical_unit = MedicalUnit.find_by_id(object.medical_unit_id)
  	medical_unit.title if medical_unit.present? 
  end
end
