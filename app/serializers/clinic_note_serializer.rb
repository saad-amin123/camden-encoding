class ClinicNoteSerializer < NoteSerializer
  attributes :chief_complaint,:disposition,:med_history,:surgical_history,:physical_exam,:instructions,:plan
end