class NoteSerializer < ActiveModel::Serializer
  attributes :id,:visit_id,:medical_unit_id,:device_id,:department,:user_id,:type,:created_at,:updated_at
  has_many :patient_diagnoses
end
