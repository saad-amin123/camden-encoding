# classs Serializer
class ItemSerializer < ActiveModel::Serializer
  attributes :id,
             :medical_unit_id,     
             :user_id,        
             :item_name,     
             :item_code,     
             :item_type,    
             :main_group,    
             :uom,          
             :upc,           
             :price,         
             :sub_group,    
             :strength,
             :product_name,
             :pack,
             :unit,
             :pack_size,
             :created_at,     
             :updated_at,    
             :status       
     has_one :generic_item
     has_one :department
     has_one :item_stock
end