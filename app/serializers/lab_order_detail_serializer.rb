class LabOrderDetailSerializer < ActiveModel::Serializer
  attributes :id, :order_id, :test, :detail, :sample_collected,
            :instructions, :note, :status,:order_generator_id,:order_generator, :created_at,:updated_at
  has_many :lab_test_details
  def order_generator
  	user = User.where(id: object.order_generator_id) 
  end
end