#= require active_admin/base
ready = ->
  $('.medical-unit-select').parent().addClass('hide')
  $('.user-select').parent().addClass('hide') 
  $(".polyselect").on "change", ->
    $('.polyform').hide()
    $('#' + $(@).val() + '_poly').show()
    temp = ($(@).val());
    if temp == "MedicalUnit"
      $('.medical-unit-select').parent().removeClass('hide')
      $('.medical-unit-select').removeAttr('disabled');
      $('.user-select').parent().addClass('hide')
      $('.user-select').attr('disabled','disabled')  
    else if temp == "User"
      $('.medical-unit-select').parent().addClass('hide')
      $('.medical-unit-select').attr('disabled','disabled')
      $('.user-select').parent().removeClass('hide')
      $('.user-select').removeAttr('disabled')  
  $('.polyform').first().parent('form').on "submit", (e) ->
    $('.polyform').each (index, element) =>
      $e = $(element)
      if $e.css('display') != 'block'
        $e.remove()

    $(document).ready(ready)
    $(document).on('page:load', ready)
$(document).ready ->
  $('#userID').trigger 'change'
  $('#userIDWard').trigger 'change'
  $('#medicalUnitIDofLab').trigger 'change'
  return
$(document).on("change", "#userID", (e) ->
  user_id = $('#userID').val();
  if user_id != ""
    $.ajax
      type: "GET"
      url: "/activeadmindepartment"
      dataType: "html"
      data: { id: user_id }
      success: (data) ->
        $("#depID").prop('disabled', false);
        $("#depID").html(data)
      error: (e) ->
        console.log e
)

$(document).on("change", "#userIDWard", (e) ->
  user_id = $('#userIDWard').val();
  if user_id != ""
    $.ajax
      type: "GET"
      url: "/activeadminward"
      dataType: "html"
      data: { id: user_id }
      success: (data) ->
        $("#wardID").prop('disabled', false);
        $("#wardID").html(data)
      error: (e) ->
        console.log e
)
$(document).on("change", "#medicalUnitId", (e) ->
  medical_unit_id = $('#medicalUnitId').val();
  if medical_unit_id != ""
    $.ajax
      type: "GET"
      url: "/activeadminwardinmedicalunit"
      dataType: "html"
      data: { id: medical_unit_id }
      success: (data) ->
        $("#wardID").prop('disabled', false);
        $("#wardID").html(data)
        $( "#wardID" ).trigger( "change" );
      error: (e) ->
        console.log e
)
$(document).on("change", "#wardID", (e) ->
  ward_id = $('#wardID').val();
  if ward_id != ""
    $.ajax
      type: "GET"
      url: "/activeadminbayinward"
      dataType: "html"
      data: { id: ward_id }
      success: (data) ->
        $("#bayID").prop('disabled', false);
        $("#bayID").html(data)
      error: (e) ->
        console.log e
)
$(document).on("change", "#medicalUnitIDofLab", (e) ->
  medicalUnitIDofLab = $('#medicalUnitIDofLab').val();
  if medicalUnitIDofLab != ""
    $.ajax
      type: "GET"
      url: "/activeadminlabinvestigationsinmedicalunit"
      dataType: "html"
      data: { id: medicalUnitIDofLab }
      success: (data) ->
        $("#labInvestigationID").prop('disabled', false);
        $("#labInvestigationID").html(data)
      error: (e) ->
        console.log e
)

$(document).on("change", "#muId", (e) ->
  mu_id = $('#muId').val();
  if mu_id != ""
    $.ajax
      type: "GET"
      url: "/activeadminlabinvestigationsinmedicalunit"
      dataType: "html"
      data: { id: mu_id }
      success: (data) ->
        $("#liId").prop('disabled', false);
        $("#liId").html(data)
      error: (e) ->
        console.log e
)
