# == Schema Information
#
# Table name: preferences
#
#  id         :integer          not null, primary key
#  department :string
#  category   :string
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Preference < ActiveRecord::Base
	belongs_to :user
	validates :category, presence: true
	validates :department, presence: true
end
