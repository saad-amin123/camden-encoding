# == Schema Information
#
# Table name: vaccinations
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  patient_id      :integer
#  visit_id        :integer
#  medical_unit_id :integer
#  name            :string
#  given_on        :date
#  follow_up       :date
#  instruction     :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Vaccination < ActiveRecord::Base
  belongs_to :user
  belongs_to :patient
  belongs_to :visit
  belongs_to :medical_unit
end
