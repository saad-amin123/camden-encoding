# == Schema Information
#
# Table name: machines
#
#  id                     :integer          not null, primary key
#  machine_code           :string
#  machine_name           :string
#  medical_unit_id        :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  lab_assessment_id      :integer
#  machine_parameter_code :string
#

class Machine < ActiveRecord::Base
  belongs_to :medical_unit
  belongs_to :lab_assessment

  # validates :machine_code, uniqueness: true
  # validates :medical_unit_id, uniqueness: { scope: :machine_code,
  #                                           message: "Machine code is already taken!" }
  validates :medical_unit_id, presence: true
  validates :machine_name, presence: true
end
