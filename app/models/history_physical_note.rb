# == Schema Information
#
# Table name: notes
#
#  id                     :integer          not null, primary key
#  visit_id               :integer
#  department             :string
#  chief_complaint        :text
#  med_history            :text
#  surgical_history       :text
#  physical_exam          :text
#  instructions           :text
#  plan                   :text
#  type                   :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  user_id                :integer
#  device_id              :integer
#  medical_unit_id        :integer
#  subjective             :text
#  operation_date         :date
#  procedure_indications  :text
#  surgeon_assistant_note :text
#  procedure_description  :text
#  anesthesia             :text
#  complications          :text
#  specimens              :text
#  blood_loss             :string
#  disposition            :string
#  admission_date         :date
#  discharge_date         :date
#  free_text              :text
#  hospital_course        :text
#  follow_up              :text
#

class HistoryPhysicalNote < Note
end
