# == Schema Information
#
# Table name: orders
#
#  id                        :integer          not null, primary key
#  patient_id                :integer
#  doctor_id                 :integer
#  status                    :string
#  medical_unit_id           :integer
#  device_id                 :integer
#  updated_by                :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  department                :string
#  category                  :string
#  order_type                :string           default("Medicine")
#  visit_id                  :integer
#  cancel_reason             :string
#  cancel_reason_details     :string
#  store_id                  :integer
#  department_id             :integer
#  case_number               :string
#  external_medicine         :string
#  barcode_url               :string
#  lab_order_sequence_number :string
#

class Order < ActiveRecord::Base
  after_save :create_order_logs
  after_create :generate_barcode

  STATUS = %w(Open Pending Closed Cancel Expired Incomplete Completed)
  ORDER_TYPE = %w(Admission Radiology Consultation Nursing Procedure Medicine Lab Medication Supply)
  
  has_many :order_items, :dependent => :destroy
  has_many :consultation_order_details, :dependent => :destroy
  has_many :admission_order_details, :dependent => :destroy
  has_one :radiology_order_detail, :dependent => :destroy
  has_one :procedure_order_detail, :dependent => :destroy
  has_one :nursing_order_detail, :dependent => :destroy
  # has_one :lab_order_detail, :dependent => :destroy
  has_many :lab_order_details, :dependent => :destroy
  has_many :add_admissions
  has_many :lab_test_details
  has_many :order_logs
  has_many :line_items

  belongs_to :medical_unit
  belongs_to :device
  belongs_to :patient
  belongs_to :visit
  belongs_to :doctor, class_name: 'User' , foreign_key: 'doctor_id'
  has_one :invoice, as: :assignable
  
  validates :medical_unit, presence: true
  validates :device, presence: true
  validates :status, presence: true
  validates :doctor, presence: true
  validates :order_type, presence: true
  validates :status,
            inclusion: { in: %w(Open Pending Closed Cancel Expired Incomplete Completed Verified Unverified Not_Verified Collected open pending closed cancel expired incomplete completed verified unverified not_verified collected) }
  validates :order_type,
            inclusion: { in: %w(Admission Radiology Consultation Nursing Procedure Medicine Lab Medication) }
  paginates_per 20
  scope :open_orders, -> { where.not(status: 'Closed') }

  accepts_nested_attributes_for :order_items, :allow_destroy => true,
                                :reject_if => :all_blank
  accepts_nested_attributes_for :admission_order_details, :allow_destroy => true,
                                :reject_if => :all_blank
  accepts_nested_attributes_for :consultation_order_details, :allow_destroy => true,
                                :reject_if => :all_blank
  accepts_nested_attributes_for :lab_order_details, :allow_destroy => true,
                                :reject_if => :all_blank
  accepts_nested_attributes_for :invoice, :allow_destroy => true,
                                :reject_if => :all_blank
  def self.get_orders(params,medical_unit,patient)
    result = if params[:doctor_id].present?
      medical_unit.orders.where('created_at > ? AND status =? AND doctor_id=? AND patient_id IS NULL', DateTime.now-168.hours, params[:status], params[:doctor_id])
    elsif params[:order_type] != 'all' && params[:status] != 'all'
      medical_unit.orders.where('created_at > ? AND status =? AND patient_id=? AND order_type = ? ', DateTime.now-168.hours, params[:status], patient.id, params[:order_type])
    elsif params[:order_type] != 'all' && params[:status] == 'all'
      medical_unit.orders.where('created_at > ? AND patient_id=? AND order_type = ? ', DateTime.now-168.hours, patient.id, params[:order_type])
    elsif params[:status] == 'all'
      medical_unit.orders.open_orders.where('created_at > ? AND patient_id=?', DateTime.now-168.hours, patient.id)
    else
      medical_unit.orders.where('created_at > ? AND status =? AND patient_id=? ', DateTime.now-168.hours, params[:status], patient.id)
    end      
    result = result.page(params[:page]) if params[:page] 
    return result   
  end

  def self.find_by_scope(medical_unit, start_date, end_date, field, query, order_type, current_user,status)
    dept_ids = current_user.departments.first.sub_departments.pluck(:id)
    dept_ids << current_user.departments.first.id
    if field == "date_range"
      @orders = Order.joins(:lab_order_details).includes(:patient).where('medical_unit_id = ?  AND orders.Created_At >= ? AND orders.Created_at <= ? AND order_type=? AND lab_order_details.status in (?) AND lab_order_details.department_id IN (?) AND lab_order_details.lab_investigation_id IS NOT NULL', medical_unit, start_date, end_date, order_type, status,dept_ids).uniq
    else
      # patient_ids = []
      # patient_ids = Patient.where("lower(#{field}) = ?", query.downcase).pluck(:id).uniq
      patient_id = Patient.where('mrn = ?', "#{query}").pluck(:id)
      @orders = Order.joins(:lab_order_details).where("medical_unit_id = ? AND patient_id in (?) AND order_type =?  AND lab_order_details.status in (?) AND lab_order_details.lab_investigation_id IS NOT NULL AND lab_order_details.department_id IN (?)", medical_unit, patient_id, order_type, status, dept_ids).uniq
    end
    return @orders
  end

  def self.order_status(order)
    if order.lab_order_details.present?
      array2 = ["Expired", "Cancel", "Verified"]
      if (order.lab_order_details.pluck(:status)- array2).length == 0
        order.update_attributes(status: 'Completed')
      elsif order.lab_order_details.pluck(:status).uniq.length==1
        order.update_attributes(status: order.lab_order_details.pluck(:status).uniq.first)
      else
        order.update_attributes(status: 'Incomplete')
      end
    end
  end

  def self.avg_patient_LOS_in_a_dept(medical_unit_ids=nil, start_date, end_date)
    result = nil
    array = []
    unless medical_unit_ids != ['All']
      result1 = Order.joins(:visit).where('visits.Created_At >= ? AND visits.Created_at <=? AND order_type =? AND status =?', start_date, end_date, 'Medicine', 'Closed').map{|v| [(v.updated_at.to_i-v.visit.created_at.to_i).divmod(60)[0]]}
    else
      result1 = Order.joins(:visit).where('medical_unit_id in (?) AND visits.Created_At >= ? AND visits.Created_at <=? AND order_type =? AND status =?', medical_unit_ids, start_date, end_date, 'Medicine', 'Closed').map{|v| [(v.updated_at.to_i-v.visit.created_at.to_i).divmod(60)[0]]}
    end
    result = result1.collect {|ind| ind[0]}
    return array << result.inject(0){|sum,x| sum + x }/result1.count if result.present?
  end

  def self.avg_patient_LOS_in_a_given_dept(medical_unit_ids=nil,order_type,role_id, start_date, end_date)
    result = nil
    array = []
    unless medical_unit_ids != ['All']
      visit_ids = Order.where('Created_At >= ? AND Created_at <=? AND order_type =?', start_date, end_date, order_type).pluck(:visit_id)
      checkout_time = VisitUser.joins(:user).where('role_id = ? AND visit_id in (?) AND is_checkout =?', role_id,visit_ids, true).pluck(:created_at)
      checkin_time = [VisitUser.joins(:user).where('role_id = ? AND visit_id in (?) AND is_checkout =?', role_id,visit_ids, false).pluck(:created_at).last]
    else
      visit_ids = Order.where('medical_unit_id in (?) AND Created_At >= ? AND Created_at <=? AND order_type =?', medical_unit_ids, start_date, end_date, order_type).pluck(:visit_id)
      checkout_time = VisitUser.joins(:user).where('role_id = ? AND visit_id in (?) AND is_checkout =?', role_id,visit_ids, true).pluck(:created_at)
      checkin_time = [VisitUser.joins(:user).where('role_id = ? AND visit_id in (?) AND is_checkout =?', role_id,visit_ids, false).pluck(:created_at).last]
    end
    result = (checkin_time.zip(checkout_time).map { |x, y| ((y - x)/60).to_i }) if checkout_time.length > 0
    return array << result.inject(0){|sum,x| sum + x }/result.count  if result.present?
  end

  # def self.medicine_order(medical_unit_ids=nil,order_type,sub_category, start_date, end_date)
  #   result = nil
  #   unless medical_unit_ids != ['All']
  #     order_ids = Order.where('status =? AND order_type = ? AND Created_At >= ? AND Created_at <=?',
  #                              'Closed', order_type, start_date, end_date).pluck(:id)
  #   else
  #     order_ids = Order.where('status =? AND medical_unit_id in (?) AND order_type = ? AND Created_At >= ? AND Created_at <=?',
  #                              'Closed', medical_unit_ids, order_type, start_date, end_date).pluck(:id)
  #   end
  #   if order_type == "Medicine"
  #     result = OrderItem.where('order_id in (?)', order_ids).pluck('brand_drug_id')
  #   end
  #   return result
  # end  

  def self.generate_range(start_date, end_date)
    (start_date.to_date..end_date.to_date).map{|date| date.strftime("%d-%b-%y")}
  end

  def self.generate_order_stats(from, to, medical_unit_ids=nil, order_type, status)
    result = nil
    unless medical_unit_ids != ['All']
      result = Order.where(updated_at: from .. to, order_type: order_type, status: status).order('DATE(created_at) DESC')
    else
      result = Order.where(updated_at: from .. to, medical_unit_id: medical_unit_ids, order_type: order_type, status: status).order('DATE(created_at) DESC')
    end
    result = result.group_by { |v| v.updated_at.to_date.strftime("%Y-%m-%d") }
    return build_series(result, from , to)
  end

  def self.generate_nonapplicable_order_stats(from, to, medical_unit_ids=nil, order_type)
    result = nil
    unless medical_unit_ids != ['All']
      result = Order.where("updated_at >= ? and updated_at <= ? and order_type = ? and  (status= ? or status = ?)", from, to, order_type, "Expired", "Cancel").order('DATE(created_at) DESC')
      total_orders = Order.where(updated_at: from .. to, order_type: order_type).order('DATE(created_at) DESC')
    else
      result = Order.where("updated_at >= ? and updated_at <= ? and medical_unit_id in (?) and order_type = ? and  (status= ? or status = ?)", from, to, medical_unit_ids, order_type, "Expired", "Cancel").order('DATE(created_at) DESC')
      total_orders = Order.where(updated_at: from .. to, medical_unit_id: medical_unit_ids, order_type: order_type).order('DATE(created_at) DESC')
    end
    result = result.group_by { |v| v.updated_at.to_date.strftime("%Y-%m-%d") }
    total_orders = total_orders.group_by { |o| o.updated_at.to_date.strftime("%Y-%m-%d") }
    return build_series(result, from , to, total_orders)
  end

  def self.generate_ordered_patient_stats(from, to, medical_unit_ids=nil, order_type, status)
    result = nil
    unless medical_unit_ids != ['All']
      result = Order.where(updated_at: from .. to, order_type: order_type, status: status).order('DATE(created_at) DESC') 
    else
      result = Order.where(updated_at: from .. to, medical_unit_id: medical_unit_ids, order_type: order_type, status: status).order('DATE(created_at) DESC')
    end
    result = result.map{|h| [h.patient_id, h.updated_at.strftime("%Y-%m-%d")] }.uniq
    result = result.group_by { |v| v[1].to_date.strftime("%Y-%m-%d") }
    return build_series(result, from , to)
  end

  def self.total_order(current_user,order_type,medical_unit_ids, start_date, end_date)
    result = nil
    unless medical_unit_ids != ['All']
      result = Order.where('order_type = ?',order_type)
    else
      result = Order.where('order_type = ? AND medical_unit_id in (?)',order_type,medical_unit_ids)
    end
      return result
  end

  def self.top_ten_given_ordered(medical_unit_ids=nil,order_type,sub_category, start_date, end_date)
    result = nil
    unless medical_unit_ids != ['All']
      if order_type == "Medicine"
        hh = OrderItem.where('Created_At >= ? AND Created_at <=?',start_date, end_date).group("brand_drug_id").count
        # hh = OrderItem.group("brand_drug_id").count if sub_category == 'Top 10 medicines ordered' || sub_category == 'Top 10 medicines prescribed overall' || sub_category == 'Top requested medicine'
      elsif order_type == "Lab"
        hh = LabOrderDetail.where('Created_At >= ? AND Created_at <=?',start_date, end_date).group('test').count
        # hh = LabOrderDetail.group('test').count if sub_category == 'Top 10 investigations requested overall' || sub_category == 'Top 10 investigations ordered'
      end
      hh = Order.where('Created_At >= ? AND Created_at <=? AND order_type = ?', start_date, end_date,order_type).group('department').count if sub_category == 'Top 10 investigations requested OPD wise'
      return Hash[*hh.sort_by { |k,v| -v }[0].flatten] if sub_category == 'Top requested medicine' && hh.present?
      result = Hash[*hh.sort_by { |k,v| -v }[0..9].flatten]
    else
      order_ids = Order.where('medical_unit_id in (?) AND order_type = ? AND Created_At >= ? AND Created_at <=?',
                               medical_unit_ids, order_type, start_date, end_date).pluck(:id)
      # order_ids = Order.where('medical_unit_id in (?) AND order_type = ?',
      #                          medical_unit_ids, order_type).pluck(:id) if sub_category == 'Top 10 investigations requested overall' || sub_category == 'Top 10 investigations ordered' || sub_category == 'Top 10 medicines ordered' || sub_category == 'Top 10 medicines prescribed overall'
      hh = Order.where('medical_unit_id in (?) AND order_type = ?',
                               medical_unit_ids, order_type).group('department').count if sub_category == 'Top 10 investigations requested OPD wise'
      if order_type == "Medicine"
        hh = OrderItem.where('order_id in (?)', order_ids).group('brand_drug_id').count
      elsif order_type == "Lab"  && sub_category != 'Top 10 investigations requested OPD wise'
        hh = LabOrderDetail.where('order_id in (?)', order_ids).group('test').count
      end
      return Hash[*hh.sort_by { |k,v| -v }[0].flatten] if sub_category == 'Top requested medicine' && hh.present?
      result = Hash[*hh.sort_by { |k,v| -v }[0..9].flatten] if hh.present?
    end
    return result
  end

  def create_order_logs
    lod = lab_order_details.first
    status_users = {"Open" => lod.order_generator_id, "Collected" => lod.sample_collector_id, "Unverified" => lod.lab_technician_id, "Incomplete" => lod.sample_collector_id, "Completed" => lod.pathologist_id, "Not_Verified" => lod.pathologist_id}
    if self.status_changed?
      OrderLog.create(order_id: self.id, status: status, updated_by: doctor_name(status_users[status]))
    end
  end

  def doctor_name(doctor_id)
    doc = User.find(doctor_id) if doctor_id.present?
    return doc.full_name
  end

  def generate_case_number
    self.case_number =  Date.today.strftime("%Y") + '-' + Date.today.strftime("%m") + '-' + Date.today.strftime("%d")+ '-' + (Order.where('order_type = ? AND Created_At >= ?', 'Lab', Date.current).count+1).to_s
  end

  private

  def self.build_series(result, from , to, total_orders=nil)
    unless total_orders
      [
        
          {
              name: 'No. of Labs Performed',
              data: extract_info(result, from, to, total_orders)

          }
      ]
    else
      [
        
          {
              name: 'Percentage of labs not performed ',
              data: extract_info(result, from, to, total_orders)

          }
      ]
    end
  end

  def self.extract_info(result, from , to, total_orders)
    array = []
    (from..to).each do |date|
      array1 = nil
      array2 = nil
      key = date.strftime("%Y-%m-%d")
      if result[key]
        array << result[key].count unless total_orders
        array1 = result[key].count.to_f if total_orders
        array2 = total_orders[key].count if total_orders
        array << (array1/array2) * 100 if total_orders
      else
        array << 0
      end
    end
    array
  end

  def generate_barcode
    cdate = created_at.strftime("%Y%m%d")
    barcode = Barby::Code128.new(self.lab_order_sequence_number[7..12])
    id = self.id.to_s
    begin
      Dir.mkdir(Rails.root.join("tmp/uploader/")) unless Dir.exist?(Rails.root.join("tmp/uploader/"))
      full_path = Rails.root.join("tmp/uploader/","barcode_#{id}.png")
      File.open(full_path, 'w') { |f| f.write barcode.to_png }
      s3_storage = S3StoreService.new(full_path)
      s3_storage.store("barcode")
      barcode_url = s3_storage.url

      FileUtils.rm_rf Dir.glob("#{Rails.root}/tmp/uploader/*")
      update_attributes(barcode_url: barcode_url)
    rescue
      puts 'unable to create directoerhy'
    end
  end

end
