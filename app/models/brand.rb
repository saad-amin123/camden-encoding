# == Schema Information
#
# Table name: brands
#
#  id         :integer          not null, primary key
#  company_id :integer
#  name       :string
#  category   :string
#  created_at :datetime
#  updated_at :datetime
#

# class documentation
class Brand < ActiveRecord::Base
  belongs_to :company
  has_many :brand_drugs
  has_many :drugs, through: :brand_drugs
  acts_as_copy_target   # To enable the copy commands of postgres-copy gem.
  validates :name, presence: true
  validates :category, presence: true
end