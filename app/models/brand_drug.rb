# == Schema Information
#
# Table name: brand_drugs
#
#  id           :integer          not null, primary key
#  name         :string
#  category     :string
#  form         :string
#  dumb         :integer
#  packing      :string
#  trade_price  :float
#  retail_price :float
#  mg           :string
#  drug_id      :integer
#  brand_id     :integer
#  created_at   :datetime
#  updated_at   :datetime
#

# class documentation
class BrandDrug < ActiveRecord::Base
  belongs_to :brand
  belongs_to :drug
  acts_as_copy_target   # To enable the copy commands of postgres-copy gem.
  validates :name, presence: true
  validates :category, presence: true
  validates :brand, presence: true
  paginates_per 20

  def alternatives(current_medical_unit,page_no,store_id)
    drug_id = self.drug_id
    form = self.form
    brand_drugs_ids = BrandDrug.where(drug_id: drug_id,form: form).where.not(id: self.id).pluck(:id)
    available_brand_drugs_ids = current_medical_unit.inventories.where("brand_drug_id IN (?) AND quantity > ? AND store_id = ? AND department_id IS NULL", brand_drugs_ids, 0, store_id).pluck(:brand_drug_id)
    return BrandDrug.where(id: available_brand_drugs_ids).page(page_no)
  end
end
