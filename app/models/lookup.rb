# == Schema Information
#
# Table name: lookups
#
#  id         :integer          not null, primary key
#  category   :string
#  key        :string
#  value      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  parent_id  :integer
#

# class documentation
class Lookup < ActiveRecord::Base
  CATEGORY = ['visit-reason', 'visiter-referred-to',
              'mode-of-conveyance', 'education', 'relationship',
              'complaint_category', 'complaint_role', 'Procedure',
              'Lab', 'Historical Notes', 'Radiology_Imaging_type',
              'Radiology_Area', 'Radiology_other_options',
              'Radiology_other_options','Discipline']
  belongs_to :parent, class_name: 'Lookup', foreign_key: 'parent_id'
  has_many :lookups

  scope :visit_reason, -> { where(category: 'visit-reason') }
  scope :visiter_referred_to, -> { where(category: 'visiter-referred-to') }
  scope :mode_of_conveyance, -> { where(category: 'mode-of-conveyance') }
  scope :education, -> { where(category: 'education') }
  scope :relationship, -> { where(category: 'relationship') }
  scope :complaint_category, -> { where(category: 'complaint_category') }
  scope :complaint_role, -> { where(category: 'complaint_role') }
  scope :Procedure, -> { where(category: 'Procedure') }
  scope :Lab, -> { where(category: 'Lab') }
  scope :Historical_notes, -> { where(category: 'Historical Notes') }
  scope :Radiology_Imaging_type, -> { where(category: 'Radiology_Imaging_type') }
  scope :Radiology_Area, -> { where(category: 'Radiology_Area') }
  scope :Radiology_other_options, -> { where(category: 'Radiology_other_options') }
  scope :Radiology_other_options, -> { where(category: 'Radiology_other_options') }

  validates :category, presence: true
  validates :key, presence: true
  validates :value, presence: true

  def children
    Lookup.where(parent_id: self.id)
  end
end
