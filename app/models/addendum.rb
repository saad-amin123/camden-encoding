# == Schema Information
#
# Table name: addendums
#
#  id              :integer          not null, primary key
#  historical_note :text
#  note_id         :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Addendum < ActiveRecord::Base
  belongs_to :note
  validates :historical_note, presence: true
end
