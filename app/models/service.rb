# == Schema Information
#
# Table name: services
#
#  id              :integer          not null, primary key
#  category        :string
#  title           :string
#  charges         :string
#  medical_unit_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  currency        :string
#

class Service < ActiveRecord::Base
  belongs_to :medical_unit
end
