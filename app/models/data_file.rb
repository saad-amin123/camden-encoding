# == Schema Information
#
# Table name: data_files
#
#  id              :integer          not null, primary key
#  medical_unit_id :integer
#  user_id         :integer
#  file_url        :string
#  status          :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class DataFile < ActiveRecord::Base

  belongs_to :user
  belongs_to :medical_unit

  def download
    @bucket = aws_initialize 
    file_name = self.file_url.to_s.split('/').last if file_url
    # Get the file object
    if file_name
      file_obj = @bucket.object("csv_data/#{file_name}")
      # Save it on disk
      begin
        Dir.mkdir(Rails.root.join("tmp/csv_files/")) unless Dir.exist?(Rails.root.join("tmp/csv_files/"))
        file_obj.get(response_target: "#{Rails.root.join("tmp/csv_files")}/#{file_name}")
        return true
      rescue
        puts 'unable to create directory'
        return false
      end
    end
  end 

  private
  
  def aws_initialize
    @s3 = Aws::S3::Resource.new(region: 'us-east-1')
    return @s3.bucket("ranecs-production")
  end
end
