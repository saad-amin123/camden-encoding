# == Schema Information
#
# Table name: bays
#
#  id              :integer          not null, primary key
#  medical_unit_id :integer
#  department_id   :integer
#  parent_id       :integer
#  title           :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  ward_id         :integer
#  beds_count      :integer          default("0")
#  deleted_at      :datetime
#  bay_code        :string
#

# class documentation
class Bay < ActiveRecord::Base
	acts_as_paranoid
	belongs_to :ward, counter_cache: :bays_count
	has_many :beds
	has_many :bed_allocations
	belongs_to :medical_unit, counter_cache: :bays_count

	#validates :medical_unit_id, presence: true
	# validates :department_id, presence: true
	validates :ward_id, presence: true
end
