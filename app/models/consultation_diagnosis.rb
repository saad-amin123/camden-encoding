# == Schema Information
#
# Table name: patient_diagnoses
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  name         :string
#  code         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  note_id      :integer
#  type         :string
#  visit_id     :integer
#  patient_id   :integer
#  instructions :text
#  plan         :text
#

class ConsultationDiagnosis < PatientDiagnosis
end
