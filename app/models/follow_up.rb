# == Schema Information
#
# Table name: follow_ups
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  patient_id      :integer
#  visit_id        :integer
#  medical_unit_id :integer
#  follow_up       :date
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class FollowUp < ActiveRecord::Base
  belongs_to :user
  belongs_to :patient
  belongs_to :visit
  belongs_to :medical_unit
end
