# == Schema Information
#
# Table name: visit_histories
#
#  id              :integer          not null, primary key
#  referred_to     :string
#  ref_department  :string
#  medical_unit_id :integer
#  referred_by     :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  visit_id        :integer
#

class VisitHistory < ActiveRecord::Base
  belongs_to :visit
  validates :ref_department, presence: true
  validates :referred_to, presence: true
end
