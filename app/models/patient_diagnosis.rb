# == Schema Information
#
# Table name: patient_diagnoses
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  name         :string
#  code         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  note_id      :integer
#  type         :string
#  visit_id     :integer
#  patient_id   :integer
#  instructions :text
#  plan         :text
#

class PatientDiagnosis < ActiveRecord::Base
  belongs_to :visit
  belongs_to :user
  belongs_to :note
  belongs_to :patient

  # validates :code, presence: true
  validates :visit, presence: true

  def self.top_five_patient_diagnoses_list(medical_unit_ids=nil, start_date, end_date)
    result = nil
    unless medical_unit_ids != ['All']
      h = PatientDiagnosis.where('Created_At >= ? AND Created_at <=?', start_date, end_date).group("name").count
      result = Hash[*h.sort_by { |k,v| -v }[0..4].flatten].keys
    else
    	patient_ids = Visit.where('medical_unit_id in (?) AND Created_At >= ? AND Created_at <=?', medical_unit_ids, start_date, end_date).pluck(:patient_id).uniq
    	h = PatientDiagnosis.where('patient_id in (?)', patient_ids).group("name").count
      result = Hash[*h.sort_by { |k,v| -v }[0..4].flatten].keys
    end
    return result
  end

  def self.given_diagnosis_patients_list(medical_unit_ids=nil,diagnosis)
    result = nil
    unless medical_unit_ids != ['All']
      result = Patient.joins(:patient_diagnoses).where("patient_diagnoses.name = ?", diagnosis).uniq
    else
    	patient_ids = Visit.where(medical_unit_id: medical_unit_ids).pluck(:patient_id).uniq
      result = Patient.joins(:patient_diagnoses).where('patient_diagnoses.patient_id in (?) AND patient_diagnoses.name = ?',patient_ids,diagnosis).uniq
    end
    return result
  end
end
