# == Schema Information
#
# Table name: allergies
#
#  id              :integer          not null, primary key
#  patient_id      :integer
#  user_id         :integer
#  medical_unit_id :integer
#  visit_id        :integer
#  allergy_type    :string
#  value           :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Allergy < ActiveRecord::Base

  ALLERGY_TYPE = %w(Drug Non\ Drug)

  scope :Drug, -> { where(allergy_type: 'Drug') }
  scope :NON_Drug, -> { where(allergy_type: 'Non\ Drug') }

  belongs_to :patient
  belongs_to :user
  belongs_to :medical_unit
  belongs_to :visit

  validates :value, presence: true
  validates :allergy_type,
            inclusion: { in: %w(Drug Non\ Drug) }

  def self.top_five_allergies_list(medical_unit_ids=nil, start_date, end_date)
    result = nil
    unless medical_unit_ids != ['All']
      h = Allergy.where('Created_At >= ? AND Created_at <=?', start_date, end_date).group("value").count
      result = Hash[*h.sort_by { |k,v| -v }[0..4].flatten].keys
    else
      h = Allergy.where('medical_unit_id in (?) AND Created_At >= ? AND Created_at <=?', medical_unit_ids, start_date, end_date).uniq.group("value").count
      result = Hash[*h.sort_by { |k,v| -v }[0..4].flatten].keys
    end
    return result
  end

  def self.given_allergy_patients_list(medical_unit_ids=nil,allergy,sub_category)
    result = nil
    unless medical_unit_ids != ['All']
      result = Patient.joins(:allergies)
      result = result.where("allergies.value = ?", allergy).uniq if sub_category == 'Patient list allergy wise'
    else
      result = Patient.joins(:allergies)
      result = result.where('medical_unit_id in (?) AND allergies.value = ?',medical_unit_ids,allergy).uniq if sub_category == 'Patient list allergy wise'
    end
    return result
  end
end
