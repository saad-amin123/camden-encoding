# == Schema Information
#
# Table name: invoice_items
#
#  id              :integer          not null, primary key
#  invoice_id      :integer
#  assignable_id   :integer
#  assignable_type :string
#  invoice_type    :string
#  description     :string
#  amount          :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class InvoiceItem < ActiveRecord::Base

  ASSIGNABLE_TYPES = %w(OrderItem LabOrderDetail)

  belongs_to :invoice
  belongs_to :assignable, polymorphic: true

  accepts_nested_attributes_for :assignable
end
