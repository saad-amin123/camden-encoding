# == Schema Information
#
# Table name: favorite_complaints
#
#  id           :integer          not null, primary key
#  complaint_id :integer
#  user_id      :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class FavoriteComplaint < ActiveRecord::Base
	after_save :updated_complaint
	after_destroy :updated_complaint

  belongs_to :complaint
  belongs_to :user

  validates :complaint_id, presence: true

  private

  def updated_complaint
    self.complaint.update_attributes!(updated_at: Time.now)
  end
end
