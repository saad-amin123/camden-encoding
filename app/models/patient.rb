# == Schema Information
#
# Table name: patients
#
#  id                    :integer          not null, primary key
#  registered_by         :integer
#  registered_at         :integer
#  mrn                   :string
#  first_name            :string
#  last_name             :string
#  middle_name           :string
#  gender                :string
#  education             :string
#  birth_day             :integer
#  birth_month           :integer
#  birth_year            :integer
#  patient_nic           :string
#  patient_passport      :string
#  unidentify_patient    :boolean          default("false")
#  guardian_relationship :string
#  guardian_first_name   :string
#  guardian_last_name    :string
#  guardian_middle_name  :string
#  guardian_nic          :string
#  guardian_passport     :string
#  unidentify_guardian   :boolean          default("false")
#  state                 :string
#  city                  :string
#  near_by_city          :string
#  address1              :string
#  address2              :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  phone1                :string
#  phone1_type           :string
#  phone2                :string
#  phone2_type           :string
#  phone3                :string
#  phone3_type           :string
#  marital_status        :string
#  blood_group           :string
#  hiv                   :string
#  hepatitis_b_antigens  :string
#  hepatitis_c           :string
#  age                   :string
#  father_name           :string
#  unidentify_gender     :boolean          default("false")
#  guardian_state        :string
#  guardian_city         :string
#  guardian_near_by_city :string
#  guardian_address      :string
#  guardian_phone_type   :string
#  guardian_phone        :string
#  barcode_url           :string
#  qrcode_url            :string
#  unidentify            :boolean          default("false")
#  external_patient_id   :string
#

class Patient < ActiveRecord::Base
  # before_create :generate_mrn
  include PublicActivity::Model
  tracked owner: Proc.new { |controller, model| controller && controller.current_user }, params:{ "changed_attributes"=> proc {|controller, model| model.changed_attributes}}

  after_create :initialize_immunization, :barcode_qrcode
  #after_update :tracked_attributes

  has_one :registeration_staff, class_name: 'User', foreign_key: 'registered_by'
  has_one :registeration_place, class_name: 'MedicalUnit',
                                foreign_key: 'registered_at'

  has_one :picture, as: :imageable, :dependent => :destroy
  has_many :contacts, :dependent => :destroy
  has_many :visits, :dependent => :destroy
  has_many :patient_diagnoses, :dependent => :destroy
  has_many :allergies, :dependent => :destroy
  has_many :vitals, :dependent => :destroy
  has_many :home_medications, :dependent => :destroy
  has_many :immunizations, :dependent => :destroy
  has_one :medical_history, :dependent => :destroy
  has_one :surgical_history, :dependent => :destroy
  has_many :medications, :dependent => :destroy
  has_many :hospital_medications, :dependent => :destroy
  has_many :vaccinations, :dependent => :destroy
  has_many :patient_past_medical_histories, :dependent => :destroy
  has_many :patient_family_medical_histories, :dependent => :destroy
  has_many :follow_ups, :dependent => :destroy
  has_many :lab_results
  has_many :item_transactions
  has_many :admissions
  has_many :bed_allocations
  has_many :discharge_patients
  has_many :orders
  has_many :investigation_notes
  has_many :medicine_requests
  has_many :ward_comments

  accepts_nested_attributes_for :contacts

  # accepts_nested_attributes_for :visits, :allow_destroy => true,
  #                               :reject_if => :all_blank

  validates :mrn, uniqueness: true
  validates :first_name, presence: true
  validates :city, presence: true
  # validates :last_name, presence: true
  validates :gender, presence: true
  validates_inclusion_of :birth_day, :in => 1..31, :message => "can only be number between 1 and 31", allow_nil: true, allow_blank: true
  validates_inclusion_of :birth_month, :in => 1..12, :message => "can only be number between 1 and 12", allow_nil: true, allow_blank: true
  validates_inclusion_of :birth_year, :in => 1900..3000, :message => "can only be number between 1900 and 3000", allow_nil: true, allow_blank: true
  # validates :patient_nic, uniqueness:{message: "has been already associated with some other Patient"}, allow_nil: true, allow_blank: true
  # validates :phone1, uniqueness:{message: "has been already associated with some other Patient"}, allow_nil: true, allow_blank: true

  def full_name
    "#{last_name}" != "null" ? "#{first_name} #{last_name}" : "#{first_name}"
  end

  def guardian_nic_format
    "#{guardian_nic[0,5]}-#{guardian_nic[5,7]}-#{guardian_nic[12, 1]}"
  end

  def patient_nic_format
    "#{patient_nic[0,5]}-#{patient_nic[5,7]}-#{patient_nic[12, 1]}"
  end

  def patient_phone1_format
    "#{phone1[0,4]}-#{phone1[4,3]}-#{phone1[7,4]}"
  end

  def patient_mrn_format
    "#{mrn[0,4]}-#{mrn[4,4]}-#{mrn[8, 4]}"
  end

  def gender_first_letter
    "#{gender.slice(0,1).capitalize}"
  end

  def check_patient_age_and_dob
    if self.age || (self.birth_day && self.birth_month && self.birth_year)
      return true
    end
    return false
  end


  def self.get_active_patients(category,department,current_user)
    Patient.find_by_sql("Select patients.* FROM patients INNER JOIN visits on visits.patient_id=patients.id 
                        WHERE visits.created_at = (SELECT MAX(visits.created_at) FROM visits WHERE visits.patient_id = patients.id)
                        AND visits.current_ref_department = '#{department}' AND visits.current_referred_to = '#{category}' AND patients.id IN 
                        (SELECT patient_id FROM doctor_patients WHERE doctor_id = #{current_user.id} AND active = 'true')")
  end

  def phone_in_format
    phone = phone1 || phone2 || phone3
    number = Phony.normalize(phone)
    number = "+92#{number}" unless number.starts_with?('+')
    number
  end

  def set_medical_history(history)
    if self.medical_history.present?
      self.medical_history.update_attributes(history: history)
    else
      self.create_medical_history(history: history)
    end 
  end

  def set_surgical_history(history)
    if self.surgical_history.present?
      self.surgical_history.update_attributes(history: history)
    else
      self.create_surgical_history(history: history)
    end
  end

  def barcode_qrcode
    cdate = created_at.strftime("%Y%m%d")
    barcode = Barby::Code128.new(mrn)
    # qrcode = RQRCode::QRCode.new(mrn)
    obj = self
    vcard = VCardio::VCard.new('3.0') do
      mrn obj.mrn
      name obj.full_name
      gender obj.gender
      dob obj.birth_day.to_s + "/" + obj.birth_month.to_s + "/" + obj.birth_year.to_s
      # tel '(555) 555-5556', type: 'WORK'
    end

    qrcode = RQRCode::QRCode.new(vcard.to_s)
    begin
      Dir.mkdir(Rails.root.join("tmp/uploader/")) unless Dir.exist?(Rails.root.join("tmp/uploader/"))
      full_path = Rails.root.join("tmp/uploader/","barcode_#{mrn}.png")
      File.open(full_path, 'w') { |f| f.write barcode.to_png }
      s3_storage = S3StoreService.new(full_path)
      s3_storage.store("barcode")
      barcode_url = s3_storage.url

      qrcode_path = Rails.root.join("tmp/uploader/","qrcode_#{mrn}.png")
      png = qrcode.as_png(
        resize_gte_to: false,
        resize_exactly_to: false,
        fill: 'white',
        color: 'black',
        size: 180,
        border_modules: 4,
        module_px_size: 6,
        file: qrcode_path
        )
      s3_storage = S3StoreService.new(qrcode_path)
      s3_storage.store("qrcode")
      qrcode_url = s3_storage.url
      FileUtils.rm_rf Dir.glob("#{Rails.root}/tmp/uploader/*")
      update_attributes(barcode_url: barcode_url, qrcode_url: qrcode_url)
    rescue
      puts 'unable to create directoerhy'
    end
  end

  def update_qrcode_to_vcard
    cdate = created_at.strftime("%Y%m%d")
    obj = self
    vcard = VCardio::VCard.new('3.0') do
      mrn obj.mrn
      name obj.full_name
      gender obj.gender
      dob obj.birth_day.to_s + "/" + obj.birth_month.to_s + "/" + obj.birth_year.to_s
      # tel '(555) 555-5556', type: 'WORK'
    end

    qrcode = RQRCode::QRCode.new(vcard.to_s)
    begin
      Dir.mkdir(Rails.root.join("tmp/uploader/")) unless Dir.exist?(Rails.root.join("tmp/uploader/"))

      qrcode_path = Rails.root.join("tmp/uploader/","qrcode_#{mrn}.png")
      png = qrcode.as_png(
        resize_gte_to: false,
        resize_exactly_to: false,
        fill: 'white',
        color: 'black',
        size: 180,
        border_modules: 4,
        module_px_size: 6,
        file: qrcode_path
        )
      s3_storage = S3StoreService.new(qrcode_path)
      s3_storage.store("qrcode")
      qrcode_url = s3_storage.url
      FileUtils.rm_rf Dir.glob("#{Rails.root}/tmp/uploader/*")
      update_attributes(qrcode_url: qrcode_url)
    rescue
      puts 'unable to create directoerhy'
    end
  end

  def self.total_registered_patients_count_category_wise(medical_unit_ids=nil,sub_category_type, sub_category, start_date, end_date)
    result = nil
    unless medical_unit_ids != ['All']
       patient_ids = Visit.where('Created_At >= ? AND Created_at <=?', start_date, end_date).pluck(:patient_id).uniq
       @patients = Patient.where("id in (?)", patient_ids).order("#{sub_category_type} ASC")
      result = @patients.group(sub_category_type).count
    else
      patient_ids = Visit.where('Created_At >= ? AND Created_at <=? AND medical_unit_id in (?)', start_date, end_date, medical_unit_ids).pluck(:patient_id).uniq
      @patients = Patient.where("id in (?)", patient_ids).order("#{sub_category_type} ASC")
      result = @patients.group(sub_category_type).count
    end
    if sub_category_type == "DATE_TRUNC('month', created_at)"
      result = result.transform_keys { |key| key.strftime("%B") }
    end
    return result,@patients
  end

  def self.total_registered_patients(medical_unit_ids=nil, sub_category, current_user, start_date, end_date)
    result = nil
    if medical_unit_ids == ['All'] && sub_category != 'Patients Registered By FDO'
      patient_ids = Visit.where('Created_At >= ? AND Created_at <=?', start_date, end_date).pluck(:patient_id)
      result = Patient.where("id in (?)", patient_ids).uniq
    elsif medical_unit_ids != ['All'] && sub_category == 'Patients Registered By FDO'
      patient_ids = Visit.where('user_id = ? AND medical_unit_id in (?) AND Created_At >= ? AND Created_at <=?', current_user.id, medical_unit_ids, start_date, end_date).pluck(:patient_id)
      result = Patient.where("id in (?)", patient_ids).uniq
    elsif medical_unit_ids == ['All'] && sub_category == 'Patients Registered By FDO'
      patient_ids = Visit.where('Created_At >= ? AND Created_at <=?', start_date, end_date).pluck(:patient_id)
      result = Patient.where("id in (?)", patient_ids).uniq
    else
      patient_ids = Visit.where('medical_unit_id in (?) AND Created_At >= ? AND Created_at <=?', medical_unit_ids, start_date, end_date).pluck(:patient_id)
      result = Patient.where("id in (?)", patient_ids).uniq
    end
    return result
  end

  def self.patient_csv_data(start_date, end_date)
    query = 'select patients.first_name, patients.last_name, patients.father_name,
    patients.mrn, patients.age, patients.gender, patients.city, patients.near_by_city,
    patients.guardian_first_name, patients.guardian_nic, patients.address1, patients.phone1,
    CASE WHEN visits.current_referred_to IS NULL THEN visits.referred_to ELSE visits.current_referred_to END,
    visits.created_at, users.first_name AS user_first_name, users.last_name AS user_last_name
    FROM patients 
    INNER JOIN visits ON patients.id=visits.patient_id 
    INNER JOIN users on users.id = visits.user_id;'
    csv_data = ActiveRecord::Base.connection.execute(query)

    # visits = Visit.joins(:patient, :users).where('visits.Created_At >= ? AND visits.Created_at <= ?',start_date, end_date).select('patients.*, visits.*, users.*')
  end

  def self.patient_csv_data_with_date(start_date, end_date, medical_unit_id)
    start_date = Date.today.beginning_of_month.strftime("%Y-%m-%d") if start_date.blank?
    end_date = Date.today.strftime("%Y-%m-%d") if end_date.blank?

    q = "SELECT
        patients.first_name,
        patients.last_name,
        patients.father_name,
        patients.mrn,
        patients.patient_nic,
        patients.age,
        patients.gender,
        patients.city,
        patients.near_by_city,
        patients.state,
        patients.guardian_first_name,
        patients.guardian_last_name,
        patients.guardian_nic,
        patients.guardian_state,
        patients.guardian_city,
        patients.guardian_near_by_city,
        patients.guardian_address,
        patients.guardian_phone,
        patients.created_at::timestamp::date AS patient_created_at_date,
        patients.created_at::timestamp::time AS patient_created_at_time,
        patients.address1,
        patients.phone1,
        patients.phone2,
        patients.unidentify AS unknown,
        CASE WHEN visits.current_referred_to IS NULL THEN visits.referred_to ELSE visits.current_referred_to END AS referred_to ,
        visits.created_at::timestamp::date AS created_at_date,
        visits.created_at::timestamp::time AS created_at_time,
        users.first_name AS user_first_name,
        users.last_name AS user_last_name,
        (CASE WHEN referred_to = 'Outpatient' THEN departments.title ELSE referred_to end) As opd_department
    FROM patients
    INNER JOIN visits ON patients.id=visits.patient_id
    left outer JOIN departments ON departments.id = visits.department_id
    INNER JOIN users ON users.id = visits.user_id"
       q += " where visits.medical_unit_id = #{medical_unit_id} AND Date(visits.created_at + interval '5 hour') >= '#{start_date}' AND Date(visits.created_at + interval '5 hour') <= '#{end_date}' order by visits.created_at ASC;"
       csv_data = ActiveRecord::Base.connection.execute(q)
  end

  def self.create_csv(csv_data)
    # require "csv"
    csv = CSV.generate(  ) do |csv|
      csv << ['first_name',
        'last_name',
        'father_name',
        'mrn',
        'patient_nic',
        'age',
        'gender',
        'city',
        'near_by_city',
        'state',
        'address1',
        'phone1',
        'phone2',
        'unknown',
        'patient_created_at_date',
        'patient_created_at_time',
        'guardian_first_name',
        'guardian_last_name',
        'guardian_nic',
        'guardian_state',
        'guardian_city',
        'guardian_near_by_city',
        'guardian_address',
        'guardian_phone',
        'created_at_date',
        'created_at_time',
        'referred_to',
        'data_entry_operator',
        'department']
      csv_data.each do |record|
        if record['unknown'] == 'f'
          record['unknown'] = 'false'
        else
          record['unknown'] = 'true'
        end
        csv << [
                record['first_name'].to_s.gsub(";",""),
                record['last_name'].to_s.gsub(";",""),
                record['father_name'].to_s.gsub(";",""),
                record['mrn'].to_s.gsub(";",""),
                record['patient_nic'].to_s.gsub(";",""),
                record['age'].to_s.gsub(";",""),
                record['gender'].to_s.gsub(";",""),
                record['city'].to_s.gsub(";",""),
                record['near_by_city'].to_s.gsub(";",""),
                record['state'].to_s.gsub(";",""),
                record['address1'].to_s.gsub(";",""),
                record['phone1'].to_s.gsub(";",""),
                record['phone2'].to_s.gsub(";",""),
                record['unknown'],
                Patient.set_date_format(record['patient_created_at_date']),
                record['patient_created_at_time'].split('.')[0],
                record['guardian_first_name'].to_s.gsub(";",""),
                record['guardian_last_name'].to_s.gsub(";",""),
                record['guardian_nic'].to_s.gsub(";",""),
                record['guardian_state'].to_s.gsub(";",""),
                record['guardian_city'].to_s.gsub(";",""),
                record['guardian_near_by_city'].to_s.gsub(";",""),
                record['guardian_address'].to_s.gsub(";",""),
                record['guardian_phone'].to_s.gsub(";",""),
                Patient.set_date_format(record['created_at_date']),
                record['created_at_time'].split('.')[0],
                record['referred_to'].to_s.gsub(";",""),
                record['user_first_name'].to_s.gsub(";","") + ' ' + record['user_last_name'].to_s.gsub(";",""),
                record['opd_department'].to_s.gsub(";",""),
              ]
      end      
    end
  end

  def self.set_date_format(date)
    date.present? ? date.to_date.strftime("%d-%m-%Y") : date
  end

  def self.find_by_scope(medical_unit, scope, start_date, end_date, field, query)
    patient_ids = []
    if ( start_date && end_date)
      patient_ids = Visit.where('medical_unit_id = ?  AND Created_At >= ? AND Created_at <= ?',medical_unit, start_date, end_date).map(&:patient_id)
    else
      patient_ids = Visit.where(medical_unit_id: medical_unit, current_referred_to: scope).map(&:patient_id) 
    end
    if field == "date_range"
      @patients = Patient.where("id in (?)", patient_ids).uniq
      # return Patient.where("id in (?)", patient_ids).uniq
    else
      @patients =  Patient.where("id in (?) AND lower(#{field}) = ?", patient_ids, query.downcase).uniq
    end
    @patients_arr = []
    @patients.each_with_index do |x, index|
      if [x.visits.order("created_at desc").first.current_referred_to].any? {|referred_to| scope.include?(referred_to) }
        @patients_arr << x
      end
    end
    return @patients_arr
  end

  def self.total_number_patients_referred_to(medical_unit_ids=nil, referred_to, start_date, end_date)
    result = nil
    unless medical_unit_ids != ['All']
      patient_ids = Visit.where('Created_At >= ? AND Created_at <= ? AND current_referred_to = ?', start_date, end_date, referred_to).map(&:patient_id)
    else
      patient_ids = Visit.where('medical_unit_id in (?)  AND Created_At >= ? AND Created_at <= ? AND current_referred_to = ?', medical_unit_ids, start_date, end_date, referred_to).map(&:patient_id)
    end
    result = Patient.where("id in (?)", patient_ids).uniq
    return result
  end

  # def self.patient_summary(medical_unit_ids=nil, start_date, end_date)
  #   result = nil
  #   @visit_arr = []
  #   @patient_histories = []
  #   @patients_arr = []
  #   @vitals_arr = []
  #   patient_ids = Visit.where('Created_At >= ? AND Created_at <=?', start_date, end_date).pluck(:patient_id).uniq
  #   @patients = Patient.where("id in (?)", patient_ids)
  #   unless medical_unit_ids != ['All']
  #     @patients.each_with_index do |x|
  #       @patients_arr << x
  #       @visit = x.visits.order("created_at desc").first
  #       @patient_histories << @visit.patient_histories if @visit.present?
  #       @vitals_arr << @visit.vitals.last if @visit.present?
  #       @visit_arr << @visit
  #     end
  #   end
  #   return @patients_arr,@patient_histories,@vitals_arr,@visit_arr
  # end

  def self.patient_summary(medical_unit_ids=nil, start_date, end_date)
    @patients_arr = []
    @patients_arr = Visit.where('Created_At >= ? AND Created_at <=?', start_date, end_date).includes(:patient, :vitals, :patient_diagnoses)
    return @patients_arr
  end

  # def self.generate_range(start_date, end_date)
  #   (start_date.to_date..end_date.to_date).map{|date| date.strftime("%d-%b-%y")}
  # end

  # def self.generate_patient_statistics(from, to, medical_unit_ids=nil)
  #   result = nil
  #   unless medical_unit_ids.present?
  #     result = Patient.where(created_at: from .. to).order('DATE(created_at) DESC')
  #   else
  #     result = Patient.where(created_at: from .. to, registered_by: medical_unit_ids).order('DATE(created_at) DESC')
  #   end
  #   result = result.group_by { |v| v.created_at.to_date.strftime("%Y-%m-%d") }
  #   return build_series(result, from , to)
  # end
  
  # private

  # def self.build_series(result, from , to)
  #   [
      
  #       {
  #           name: 'No. of Returing & New Patients',
  #           data: extract_info(result, from, to)

  #       }
  #   ]
  # end

  # def self.extract_info(result, from , to, filter=nil)
  #   array = []
  #   (from..to).each do |date|
  #     key = date.strftime("%Y-%m-%d")
  #     if result[key]
  #       array << result[key].count unless filter
  #       array << result[key].count {|a| a.referred_to == filter} if filter
  #     else
  #       array << 0
  #     end
  #   end
  #   array
  # end

  def generate_mrn(medical_unit)
    loop do
      self.mrn = (sprintf '%03d', medical_unit.region.id.to_s.first(3).to_i)+(sprintf '%03d',medical_unit.id.to_s.first(3).to_i)+rand.to_s[2..7]
      break unless Patient.find_by_mrn(mrn)
    end
  end

  def generate_new_mrn(medical_unit)
    loop do
      # self.mrn = (sprintf '%02d', medical_unit.region.id.to_s.first(2).to_i)+(sprintf '%02d',medical_unit.id.to_s.first(2).to_i)+rand.to_s[2..9]
      self.mrn =  medical_unit.code.to_s + rand.to_s[2..9]
      break unless Patient.find_by_mrn(mrn)
    end
  end

  def birthday
    if birth_year && birth_month && birth_day
      birthday = Date.new(birth_year,birth_month,birth_day)
    end
  end
  
  def initialize_immunization
    if birthday.present?
      immunizations.create!(age: 'Birth',vaccine:'BCG', dose:'', due: birthday)
      immunizations.create!(age: 'Birth',vaccine:'OPV-0', dose:'', due: birthday)
      immunizations.create!(age: 'Birth',vaccine:'Hepatitis B', dose:'', due: birthday+548)
      immunizations.create!(age: '6 Weeks',vaccine:'OPV-1', dose:'', due: birthday+42)
      immunizations.create!(age: '6 Weeks',vaccine:'Penta-1', dose:'', due: birthday+42)
      immunizations.create!(age: '6 Weeks',vaccine:'PCV-10-1', dose:'', due: birthday+42)
      immunizations.create!(age: '10 Weeks',vaccine:'OPV-2', dose:'', due: birthday+70)
      immunizations.create!(age: '10 Weeks',vaccine:'Penta-2', dose:'', due: birthday+70)
      immunizations.create!(age: '10 Weeks',vaccine:'PCV-10-2', dose:'', due: birthday+70)
      immunizations.create!(age: '14 Weeks',vaccine:'OPV-3', dose:'', due: birthday+98)
      immunizations.create!(age: '14 Weeks',vaccine:'Penta-3', dose:'', due: birthday+98)
      immunizations.create!(age: '14 Weeks',vaccine:'PCV-10-3', dose:'', due: birthday+98)
      immunizations.create!(age: '14 Weeks',vaccine:'IPV', dose:'', due: birthday+98)
      immunizations.create!(age: '9 Months',vaccine:'Measles-1', dose:'', due: birthday+274)
      immunizations.create!(age: '15 Months',vaccine:'Measles-2', dose:'', due: birthday+457)
    end
  end

  def self.fetch_patient_date_from_csv(row)
    row = {'first_name'=> row['first_name'] ,'gender'=> row['gender'] , 'birth_day'=> row['birth_day'] , 'birth_month'=> row['birth_month'], 'birth_year'=> row['birth_year'], 'state'=> row['state'], 'city'=> row['city'], 'near_by_city'=> row['near_by_city'], 'phone1'=> row['phone1'], 'age'=> row['age'],
           'last_name'=> row['last_name'], 'patient_nic'=> row['patient_nic'],'guardian_relationship'=> row['guardian_relationship'],'guardian_first_name'=> row['guardian_first_name'],'guardian_last_name'=> row['guardian_last_name'],'guardian_nic'=> row['guardian_nic'],'phone1_type'=> row['phone1_type'],'father_name'=> row['father_name'],'barcode_url'=> row['barcode_url'], 'qrcode_url'=> row['qrcode_url'], 'external_patient_id' => row['patient_id']}
  end

  private

  def tracked_attributes
    changed_attributes
    # activities.last.update_attributes(parameters: changed_attributes) if activities.last
  end
end
