# == Schema Information
#
# Table name: icd_codes
#
#  id         :integer          not null, primary key
#  code       :string
#  name       :string
#  created_at :datetime
#  updated_at :datetime
#

class IcdCode < ActiveRecord::Base

  acts_as_copy_target  
  validates :code, presence: true
  validates :name, presence: true
end
