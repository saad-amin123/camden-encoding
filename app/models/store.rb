# == Schema Information
#
# Table name: stores
#
#  id              :integer          not null, primary key
#  name            :string
#  medical_unit_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  parent_id       :integer
#  department_id   :integer
#

class Store < ActiveRecord::Base
  belongs_to :medical_unit
  belongs_to :department
  has_and_belongs_to_many :users
  belongs_to :parent, class_name: "Store", foreign_key: "parent_id"
  has_many :sub_stores, class_name: "Store", foreign_key: "parent_id"
  validates :name, presence: true
  has_many :medicine_requests
end
