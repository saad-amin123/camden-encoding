# == Schema Information
#
# Table name: lab_order_sequences
#
#  id              :integer          not null, primary key
#  medical_unit_id :integer
#  lab_sequence    :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class LabOrderSequence < ActiveRecord::Base

  belongs_to :medical_unit

  validates_length_of :lab_sequence, :is => 4

  def self.get_lab_sequence(lab_sequence)
    return lab_sequence.to_i + 1
    # return format('%04d',incremented)
    # Rails.cache.fetch([self, "lab_sequence_count"]) {$redis.incr lab_sequence}
  end
end
