# == Schema Information
#
# Table name: order_logs
#
#  id                  :integer          not null, primary key
#  order_id            :integer
#  lab_order_detail_id :integer
#  status              :string
#  updated_by          :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class OrderLog < ActiveRecord::Base
  belongs_to :order
  validates :status, presence: true
end
