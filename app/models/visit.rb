# == Schema Information
#
# Table name: visits
#
#  id                      :integer          not null, primary key
#  medical_unit_id         :integer
#  patient_id              :integer
#  user_id                 :integer
#  visit_number            :string
#  reason                  :string
#  reason_note             :string
#  ref_department          :string
#  ref_department_note     :string
#  mode_of_conveyance      :string
#  mode_of_conveyance_note :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  referred_to             :string
#  amount_paid             :float
#  current_ref_department  :string
#  current_referred_to     :string
#  is_active               :boolean          default("true")
#  ambulance_number        :string
#  driver_name             :string
#  mlc                     :boolean          default("false")
#  policeman_name          :string
#  belt_number             :string
#  police_station_number   :string
#  department_id           :integer
#  doctor_id               :integer
#  external_visit_number   :string
#

# class documentation
class Visit < ActiveRecord::Base
  include PublicActivity::Model
  #tracked owner: Proc.new { |controller, model| controller && controller.current_user }, params:{ "changed_attributes"=> proc {|controller, model| model.changed_attributes}}
  
  after_create :generate_visit_number
  after_create :set_current_location
  after_create :update_patient_age

  belongs_to :patient
  belongs_to :medical_unit
  belongs_to :created_by, class_name: 'User', foreign_key: 'user_id'
  has_many :visit_histories, :dependent => :destroy
  has_many :patient_diagnoses, :dependent => :destroy
  has_many :notes, :dependent => :destroy
  has_many :patient_histories
  has_many :orders
  has_many :vaccinations, :dependent => :destroy
  has_many :visit_users
  has_many :users, through: :visit_users
  has_many :pictures, as: :imageable
  has_many :vitals
  has_many :line_items
  belongs_to :department
  has_many :admissions
  has_many :bed_allocations
  has_many :investigation_notes
  has_many :ward_comments
  belongs_to :user
  # default_scope {where(is_active: true) }

  # validates :reason, presence: true

  def self.new_visit(patient,params)
    patient.visits.new(referred_to: params[:referred_to], current_referred_to: params[:referred_to],
                      mode_of_conveyance: params[:mode_of_conveyance],ambulance_number: params[:ambulance_number],
                      driver_name: params[:driver_name],mlc: params[:mlc],policeman_name: params[:policeman_name],
                      belt_number: params[:belt_number],police_station_number: params[:police_station_number])
  end

  def update_patient_age
    check_date_of_birth(patient)
    if patient.birth_year && patient.birth_month && patient.birth_day
      dob = DateTime.new(patient.birth_year, patient.birth_month, patient.birth_day)
      age = Time.now.to_f - dob.to_f
      age_to_update = ''
      if age > 2.year.to_f
        age_to_update = (age/1.year.to_f).round(1).to_s + " Year"
      elsif age < 2.year.to_i && age > 30.days.to_f
        # age_to_update = (age/1.month.to_f).round(1).to_s + " Month"
        age_to_update = ((age/1.year.to_f)*(12)).round(1).to_s + " Month"
      elsif age < 30.days.to_f && age > 1.day.to_i
        age_to_update = (age/1.day.to_i).round.to_s + " Day"
        # age_to_update = "less than 1 Day" if (age/1.day.to_i).round.to_s == "0"
      else (age/1.day.to_i).round.to_s == "0"
        age_to_update = "less than 1 Day"
      end
      self.patient.update_attributes(age: age_to_update)
    end
  end

  def check_date_of_birth(patient)
    if !(patient.birth_day && patient.birth_month && patient.birth_year) && patient.age
      patient.birth_year = Date.today.years_ago(patient.age.to_i).year
      patient.birth_month = Date.today.years_ago(patient.age.to_i).month
      patient.birth_day = Date.today.years_ago(patient.age.to_i).day
    end
  end


  def self.generate_statistics(from, to, medical_unit_ids)
    result = nil
    unless medical_unit_ids != ['All']
      result = Visit.where(created_at: from .. to).order('DATE(created_at) DESC')
    else
      result = Visit.where(created_at: from .. to, medical_unit_id: medical_unit_ids).order('DATE(created_at) DESC')
    end

    result = result.group_by { |v| v.created_at.to_date.strftime("%Y-%m-%d") }
    return build_series(result, from , to)
  end

  def self.generate_patient_wait_stats(from, to, medical_unit_ids=nil)
    result = nil
    unless medical_unit_ids != ['All']
      result1 =  Visit.joins(:patient_histories).where(patient_histories: {created_at: from .. to}).map{|v| [(v.patient_histories.first.created_at.to_i-v.created_at.to_i).divmod(60)[0], v.created_at]}.uniq
    else
      result1 =  Visit.joins(:patient_histories).where(patient_histories: {created_at: from .. to, medical_unit_id: medical_unit_ids}).map{|v| [(v.patient_histories.first.created_at.to_i-v.created_at.to_i).divmod(60)[0], v.created_at]}.uniq
    end
    result1 = result1.group_by { |v| v[1].to_date.strftime("%Y-%m-%d") }
    return build_series(result=nil, from , to,result1)
  end

  def self.total_visit_count_category_wise(medical_unit_ids=nil,sub_category_type,sub_category, start_date, end_date)
    result = nil
    unless medical_unit_ids != ['All']
      result = Visit.where('Created_At >= ? AND Created_at <=?', start_date, end_date).group(sub_category_type).count
    else
      result = Visit.where('medical_unit_id in (?) AND Created_At >= ? AND Created_at <=?', medical_unit_ids ,start_date, end_date).group(sub_category_type).count
    end
    result = Hash[*result.sort_by { |k,v| -v }[0].flatten] if sub_category == 'Most busiest OPD' && result.present?
    return result
  end

  def self.total_by_ref_dep(medical_unit_ids, start_date, end_date)
    Visit.joins(:patient).where('medical_unit_id in (?) AND visits.Created_At >= ? AND visits.Created_at <=? AND referred_to = ?' ,medical_unit_ids ,start_date, end_date, "Inpatient").group(:current_ref_department).uniq(:patient_id).order('count_id desc').count
  end

  def self.total_by_gender(medical_unit_ids, start_date, end_date)
    @hash = Visit.joins(:patient).where('medical_unit_id in (?) AND visits.Created_At >= ? AND visits.Created_at <=?', medical_unit_ids ,start_date, end_date).select('visits.patient_id').uniq('visits.patient_id').group('patients.gender').count
  end

  def self.total_by_city(medical_unit_ids, start_date, end_date)
    Visit.joins(:patient).where('medical_unit_id in (?) AND visits.Created_At >= ? AND visits.Created_At <=? AND near_by_city is not null', medical_unit_ids ,start_date, end_date).select('visits.patient_id').uniq('visits.patient_id').group('patients.near_by_city').order('count_visits_patient_id  desc').limit(6).count
  end

  def self.total_by_users(medical_unit_ids, start_date, end_date)
    @hash1 = Visit.joins(:created_by).where('medical_unit_id in (?) AND visits.Created_At >= ? AND visits.Created_At <=? AND referred_to =?', medical_unit_ids ,start_date, end_date, 'Emergency').group('users.first_name||users.last_name').order('count_all DESC').count
    @hash2 = Visit.joins(:created_by).where('medical_unit_id in (?) AND visits.Created_At >= ? AND visits.Created_At <=? AND referred_to =?', medical_unit_ids ,start_date, end_date, 'Outpatient').group('users.first_name||users.last_name').order('count_all DESC').count
    return @hash1, @hash2
  end

  def self.most_busiest_hours(medical_unit_ids, start_date, end_date)
    @hash1 = {"12AM to 8AM" => 0, "8AM to 10AM" => 0,"10AM to 12PM" => 0,"12PM to 2PM" => 0,"2PM to 6PM" => 0,"6PM to 12AM" => 0}
    time_array = Visit.where('medical_unit_id in (?) AND visits.Created_At >= ? AND visits.Created_at <=?', medical_unit_ids ,start_date, end_date).group('created_at').count.keys.map{|time| time.strftime("%T")}
    time_array.each do |time| # server time 5 hours back
        if time.to_i >= 19 ||  time.to_i < 3
          @hash1['12AM to 8AM'] += 1
        elsif time.to_i >= 3 &&  time.to_i < 5
          @hash1['8AM to 10AM'] += 1
        elsif time.to_i >= 5 &&  time.to_i < 7
          @hash1['10AM to 12PM'] += 1
        elsif time.to_i >= 7 &&  time.to_i < 9
          @hash1['12PM to 2PM'] += 1
        elsif time.to_i >= 9 &&  time.to_i < 13
          @hash1['2PM to 6PM'] += 1
        elsif time.to_i >= 13 &&  time.to_i < 19
          @hash1['6PM to 12AM'] += 1
      end
    end
    return @hash1
  end

  def self.total_by_ref_to(medical_unit_ids, start_date, end_date)
    list= ["Emergency", "Inpatient", "Outpatient"]
    result = Visit.where('medical_unit_id in (?) AND visits.Created_At >= ? AND visits.Created_At <=? AND referred_to in (?)', medical_unit_ids ,start_date, end_date,list).group(:referred_to).count
    result["total_visit"] ||= 0
    list.each do |item|
      result[item] ||= 0
      result["total_visit"]  = result["total_visit"]  + result[item]
    end
    return result.sort.to_h
  end

  def self.total_returning(medical_unit_ids, start_date, end_date)
    #h = Visit.joins(:patient).where('medical_unit_id in (?) AND visits.Created_At >= ? AND visits.Created_At <=? AND patients.created_at::Date = visits.created_at::Date AND visits.created_at >= ?', medical_unit_ids ,start_date, end_date, Time.zone.now.beginning_of_day  ).group('visits.created_at::Date').count.values.sum
    @visits =  Visit.where('medical_unit_id in (?) AND Created_At >= ? AND Created_at <=? ', medical_unit_ids ,start_date, end_date)
    ids = @visits.map(&:patient_id).uniq
    new_patients = Patient.where("id in (?) AND Created_At >= ? AND Created_at <=? ", ids, start_date, end_date).count
    returning = @visits.select(:patient_id).group(:patient_id).having("count(*) > 1").length
    result = {}
    result[:total_patients] = ids.count
    result[:new_patients] = new_patients
    result[:returning] =  returning
    return result

  end

  def self.last_30_days_stats(medical_unit_ids)
    from = 30.days.ago.to_date
    to = Time.zone.now.beginning_of_day.to_date
    result = nil
    # result = Visit.where(created_at: from .. to, medical_unit_id: medical_unit_ids).order('DATE(created_at) DESC')
    result = Visit.where(created_at: from .. to, medical_unit_id: medical_unit_ids).order('DATE(created_at) ASC').group('DATE(created_at::Date)').count
    # result = result.group_by { |v| v.created_at.to_date.strftime("%Y-%m-%d") }
    # return build_series(result, from , to-2)
    hash1 = {}
    array = []
    hash1[:data] = result.values
    array << hash1
    dates = result.keys.map{|date| date.strftime("%d-%b-%y")}
    return array, from , to-2, dates
  end

  def self.total_visit_count_ref_to(medical_unit_ids,sub_category_type,sub_category, start_date, end_date, ref_to)
    result = nil
    @ref_to_hash = {"Emergency" => 0, "Inpatient" => 0, "Outpatient" => 0}
    @ref_to_hash1 = {"New" => 0, "Old" => 0}
      result = Visit.where('medical_unit_id in (?) AND Created_At >= ? AND Created_at <=? AND current_referred_to in  (?)', medical_unit_ids,start_date.to_date, end_date.to_date, ref_to).group(sub_category_type).count.sort.to_h
    result.each_with_index do |(k, v), i|
      @ref_to_hash.each_with_index do |(k, v), index|
        if result.keys[i] == @ref_to_hash.keys[index]
          @ref_to_hash[@ref_to_hash.keys[index]] =  result.values[i].to_i
        end
      end
    end
    return @ref_to_hash
  end
  def self.total_patient_count(medical_unit_ids,sub_category_type,sub_category, start_date, end_date, ref_to)
    @ref_to_hash1 = {"New" => 0, "Returning" => 0,"Total"=> 0}
    patients =  Visit.where('medical_unit_id in (?) AND Created_At >= ? AND Created_at <=? ', medical_unit_ids ,start_date.to_date, end_date.to_date).map(&:patient_id).uniq
    @patients = Patient.where("id in (?)", patients)
    @patients_arr = []
    @patients.each_with_index do |x, index|
      if x.visits.order("created_at desc").first.created_at.strftime("%d-%b-%y") == x.created_at.strftime("%d-%b-%y")
        @patients_arr << x
      end
    end
    # return @patients_arr
    # result = Patient.where('id in (?) AND Created_At >= ? AND Created_At <=?', patients,start_date.to_date,end_date.to_date).count
    # result1 = Patient.where('id in (?) AND Created_At <?', patients,start_date.to_date).count
    @ref_to_hash1['Total'] = @patients.count
    @ref_to_hash1['New'] =  @patients_arr.count
    @ref_to_hash1['Returning'] =  @patients.count - @patients_arr.count
    return @ref_to_hash1
  end

  def self.total_patient_age(medical_unit_ids,sub_category_type,sub_category, start_date, end_date, ref_to)
    @ref_to_hash1 = {"New Born" => 0, "1M to 6M" => 0,"6M to 12M" => 0,"1Y to 3Y" => 0,"3Y to 7Y" => 0,">7Y" => 0}
    patients =  Visit.where('medical_unit_id in (?) AND Created_At >= ? AND Created_at <=? AND current_referred_to in  (?)', medical_unit_ids ,start_date.to_date, Time.zone.now.end_of_day,ref_to).map(&:patient_id)
    patients = patients.uniq
    result = Patient.where('id in (?)',patients)
    start_date = start_date.to_date
    result.each do |p|
      date = Date.parse("#{p.birth_year}-#{p.birth_month}-#{p.birth_day}")
        if (start_date.year - date.year)  >=   7
          @ref_to_hash1['>7Y'] += 1
        elsif (start_date.year - date.year)  >=   3  && (start_date.year - date.year)  <  7
          @ref_to_hash1['3Y to 7Y'] += 1
        elsif (start_date.year - date.year)  >= 1 && (start_date.year - date.year)  < 3 && (start_date.month >= date.month)
          @ref_to_hash1['1Y to 3Y'] += 1
        elsif (start_date.to_date.month - date.month)  >=   6 && (start_date.to_date.month - date.month)  <   12 && (start_date.to_date.year - date.year)  <  1
          @ref_to_hash1['6M to 12M'] += 1
        elsif (start_date.to_date.month - date.month)  >=   1 && (start_date.to_date.month - date.month)  <   6
          @ref_to_hash1['1M to 6M'] += 1
        elsif(start_date.to_date.day - date.day)  <   30
          @ref_to_hash1['New Born'] += 1
      end
    end
    patients =  Visit.where('medical_unit_id in (?) AND Created_At >= ? AND Created_at <=? AND current_referred_to in  (?)', medical_unit_ids ,start_date.to_date, Time.zone.now.end_of_day,ref_to).map(&:patient_id)
    patients = patients.uniq
    result1 = Patient.where('id in (?)',patients)
    result1.each do |p|
      date = Date.parse("#{p.birth_year}-#{p.birth_month}-#{p.birth_day}")
         if (start_date.year - date.year)  >=   7
          @ref_to_hash1['>7Y'] += 1
        elsif (start_date.year - date.year)  >=   3  && (start_date.year - date.year)  <  7
          @ref_to_hash1['3Y to 7Y'] += 1
        elsif (start_date.year - date.year)  >= 1 && (start_date.year - date.year)  < 3 && (start_date.month >= date.month)
          @ref_to_hash1['1Y to 3Y'] += 1
        elsif (start_date.to_date.month - date.month)  >=   6 && (start_date.to_date.month - date.month)  <   12 && (start_date.to_date.year - date.year)  <  1
          @ref_to_hash1['6M to 12M'] += 1
        elsif (start_date.to_date.month - date.month)  >=   1 && (start_date.to_date.month - date.month)  <   6
          @ref_to_hash1['1M to 6M'] += 1
        elsif(start_date.to_date.day - date.day)  <   30
          @ref_to_hash1['New Born'] += 1
      end
    end
  return @ref_to_hash1
  end

  def self.top_ten_city_list(medical_unit_ids, start_date, end_date,ref_to)
    result = nil
    patients =  Visit.where('medical_unit_id in (?) AND Created_At >= ? AND Created_at <=?', medical_unit_ids ,start_date.to_date, end_date.to_date).map(&:patient_id)
    patients = patients.uniq
    patients_count = Patient.where('id in (?)',patients).group("city").count
    result = Hash[*patients_count.sort_by { |k,v| -v }[0..4].flatten]
    if result.present?
      count = patients_count.values.sum(:+) - result.values.sum(:+)
      result ["Others"] = count      
    end
    return result
  end

  def self.user_performance_by_dept(medical_unit_ids,sub_category_type,sub_category, start_date, end_date, ref_to)
    result = nil
    @ref_to_hash1 = []
    @ref_to_hash2 = []
    @ref_to_hash3 = []
      result1 = Visit.where('Created_At >= ? AND Created_at <=? AND current_referred_to in  (?)', start_date.to_date, end_date.to_date, ref_to[0]).group('user_id').count.sort.to_h
      result2 = Visit.where('Created_At >= ? AND Created_at <=? AND current_referred_to in  (?)', start_date.to_date, end_date.to_date, ref_to[1]).group('user_id').count.sort.to_h
      result3 = Visit.where('Created_At >= ? AND Created_at <=? AND current_referred_to in  (?)', start_date.to_date, end_date.to_date, ref_to[2]).group('user_id').count.sort.to_h
    users1 = User.where(id: result1.keys)
    users2 = User.where(id: result2.keys)
    users3 = User.where(id: result3.keys)
    users1.each do |user|
      @ref_to_hash1 << {name:  user.full_name, y: result1[user.id]} 
    end
    users2.each do |user|
      @ref_to_hash2 << {name:  user.full_name, y: result2[user.id]} 
    end
    users3.each do |user|
      @ref_to_hash3 << {name:  user.full_name, y: result3[user.id]} 
    end
    patients = Visit.where('medical_unit_id in (?) AND Created_At >= ? AND Created_at <=?  AND current_referred_to in  (?)', medical_unit_ids ,start_date.to_date, Time.zone.now.end_of_day, ref_to).map(&:patient_id)
    patients = patients.uniq
    @gender_hash = {"male" => 0, "female" => 0, "unidentified" => 0, "transgender" => 0}
    @patients = Patient.where('id in (?) AND Created_At >= ?', patients,start_date.to_date).order("gender")
    result4 = @patients.group('gender').count
    result4.each_with_index do |(k, v), i|
      @gender_hash.each_with_index do |(k, v), index|
        if result4.keys[i] == @gender_hash.keys[index]
          @gender_hash[@gender_hash.keys[index]] =  result4.values[i].to_i
        end
      end
    end
    return @ref_to_hash1, @ref_to_hash2, @ref_to_hash3,@gender_hash
  end

  def self.user_performance_by_dept_thirty_days(medical_unit_ids,sub_category_type,sub_category, start_date, end_date, ref_to)
    result = nil
    @ref_to_hash1 = []
    @ref_to_hash2 = []
    @ref_to_hash3 = []
      result1 = Visit.where('Created_At >= ? AND Created_at <=? AND current_referred_to in  (?)', start_date.to_date - 30.day, end_date.to_date, ref_to[0]).group('user_id').count.sort.to_h
      result2 = Visit.where('Created_At >= ? AND Created_at <=? AND current_referred_to in  (?)', start_date.to_date - 30.day, end_date.to_date, ref_to[1]).group('user_id').count.sort.to_h
      result3 = Visit.where('Created_At >= ? AND Created_at <=? AND current_referred_to in  (?)', start_date.to_date - 30.day, end_date.to_date, ref_to[2]).group('user_id').count.sort.to_h
    users1 = User.where(id: result1.keys)
    users2 = User.where(id: result2.keys)
    users3 = User.where(id: result3.keys)
    users1.each do |user|
      @ref_to_hash1 << {name:  user.full_name, y: result1[user.id]} 
    end
    users2.each do |user|
      @ref_to_hash2 << {name:  user.full_name, y: result2[user.id]} 
    end
    users3.each do |user|
      @ref_to_hash3 << {name:  user.full_name, y: result3[user.id]} 
    end
    patients = Visit.where('medical_unit_id in (?) AND Created_At >= ? AND Created_at <=?  AND current_referred_to in  (?)', medical_unit_ids ,start_date.to_date, Time.zone.now.end_of_day, ref_to).map(&:patient_id)
    patients = patients.uniq
    @gender_hash = {"male" => 0, "female" => 0, "unidentified" => 0, "transgender" => 0}
    @patients = Patient.where('id in (?) AND Created_At >= ?', patients,start_date.to_date).order("gender")
    result4 = @patients.group('gender').count
    result4.each_with_index do |(k, v), i|
      @gender_hash.each_with_index do |(k, v), index|
        if result4.keys[i] == @gender_hash.keys[index]
          @gender_hash[@gender_hash.keys[index]] =  result4.values[i].to_i
        end
      end
    end
    return @ref_to_hash1, @ref_to_hash2, @ref_to_hash3,@gender_hash
  end

  def self.generate_range(start_date, end_date)
    (start_date.to_date..end_date.to_date).map{|date| date.strftime("%d-%b-%y")}
  end

  def category_dept
    "#{current_referred_to}-#{current_ref_department}"
  end

  def order_logs(order_type)
    Order.where(visit_id: self.id,order_type: order_type)
  end

  def self.fetch_visits_from_csv(row)
    Visit.new({'ref_department' => row['ref_department'], 'referred_to' => self.check_referred_to(row['referred_to'], row['current_referred_to']), 'department_id'=> row['department_id'], 'doctor_id'=> row['doctor_id'],
               'ambulance_number'=> row['ambulance_number'], 'driver_name'=> row['driver_name'], 'mlc'=> row['mlc'], 'policeman_name'=> row['policeman_name'], 'belt_number'=> row['belt_number'], 'police_station_number'=> row['police_station_number'], 'external_visit_number' => row['visit_id']})
  end

  def self.check_referred_to(referred_to, current_referred_to)
    if referred_to == "OPD" || referred_to == "opd" || referred_to == 'Opd' || current_referred_to == "OPD" || current_referred_to == "opd" || current_referred_to == 'Opd' || current_referred_to == 'Outpatient'
      referred_to = 'Outpatient'
    elsif referred_to == "ER" || referred_to == 'er' || referred_to == 'Er'  || current_referred_to == "ER" || current_referred_to == "er" || current_referred_to == 'Er' || current_referred_to == 'Emergency'
      referred_to = 'Emergency'
    else
      referred_to 
    end
    return referred_to
  end

  private

  def self.build_series(result, from , to, result1=nil)
    if result
      [
        {
            name: 'No. of patients registered',
            data: extract_info(result, from, to)

        }, {
            name: 'No. of new patients',
            data: extract_info(result, from, to, filter=result)

        }
        # , {
        #     name: 'Inpatient',
        #     data: extract_info(result, from, to, 'Inpatient')

        # }, {
        #     name: 'OutPatient',
        #     data: extract_info(result, from, to, 'OutPatient')
        # }
      ]
    elsif result1
      [ 
        {
            name: 'Avg. Patient wait (from registration to dr. order)',
            data: extract_info(result1, from, to)

        }
      ]
    end
  end

  def self.extract_info(result, from , to, filter=nil)
    array = []
    (from..to).each do |date|
      key = date.strftime("%Y-%m-%d")
      if result[key]
        r = result[key].collect {|ind| ind[0]} if result[key].collect {|ind| ind[0]}[0].present?
        array << r.inject(0){|sum,x| sum + x }/result[key].count if result[key].collect {|ind| ind[0]}[0].present?
        # result[key].map{|r| r.notes.present? ? (r.notes.first.created_at - r.created_at).divmod(60)[0] : (r.created_at - r.created_at)}
        array << result[key].count if (!filter && !result[key].collect {|ind| ind[0]}[0].present?)
        array << result[key].map(&:patient_id).uniq.count if filter
        puts array
        # array << result[key].count {|a| a.referred_to == filter} if filter
      else
        array << 0
      end
    end
    array
  end

  def set_current_location
    update_attributes(current_referred_to: self.referred_to, current_ref_department: self.ref_department)
  end

  def generate_visit_number
    visit = nil
    if Time.zone.at(Visit.where(medical_unit_id: medical_unit.id, referred_to: self.referred_to).last(2).first.created_at).to_date == Time.zone.now.to_date
      # visit = Visit.where('visit_number is not null and referred_to  = ? AND medical_unit_id = ?  AND created_at >= ?', self.referred_to, medical_unit.id, Time.zone.now.beginning_of_day).order('id desc').limit(1).first
      visit = Visit.where('visit_number is not null and referred_to  = ? AND medical_unit_id = ?', self.referred_to, medical_unit.id).order('id desc').limit(1).first
    end
    number = 1
    if visit
      number = (visit.visit_number.split('-')[3].to_i) + 1
    end 
    update_attributes(visit_number: "#{Time.now.year}-#{Time.now.month}-#{Time.now.day}-#{number}") # rubocop:disable Metrics/LineLength
  end
end
