# == Schema Information
#
# Table name: radiology_order_details
#
#  id              :integer          not null, primary key
#  order_id        :integer
#  imaging_type    :string
#  area            :string
#  contrast_option :string
#  other_option    :string
#  routine         :boolean          default("false")
#  urgent          :boolean          default("false")
#  left            :boolean          default("false")
#  right           :boolean          default("false")
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  instructions    :text
#  note            :text
#  result          :string
#

class RadiologyOrderDetail < ActiveRecord::Base
  belongs_to :order

  validates :imaging_type, presence: true
  validates :area, presence: true
  validates :order, presence: true
  has_many :pictures, as: :imageable, :dependent => :destroy
end
