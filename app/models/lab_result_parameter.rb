# == Schema Information
#
# Table name: lab_result_parameters
#
#  id              :integer          not null, primary key
#  parameter_title :string
#  result1         :string
#  result2         :string
#  uom             :string
#  range_title     :string
#  lab_result_id   :integer
#  out_of_range    :boolean
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class LabResultParameter < ActiveRecord::Base
  attr_accessor :profile_name

  belongs_to :lab_result

  def is_out_of_range?(range)
    return true if range.blank?
    range_arr = range.gsub(' ','').split('-')
    result1.to_i < range_arr[0].to_i || result1.to_i > range_arr[1].to_i
  end
end
