# == Schema Information
#
# Table name: permissions
#
#  id         :integer          not null, primary key
#  role_id    :integer
#  activity   :string
#  access     :boolean          default("true")
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# class documentation
class Permission < ActiveRecord::Base
end
