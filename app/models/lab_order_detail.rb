# == Schema Information
#
# Table name: lab_order_details
#
#  id                   :integer          not null, primary key
#  order_id             :integer
#  test                 :string
#  detail               :string
#  sample_collected     :boolean
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  instructions         :string
#  note                 :text
#  status               :string           default("Open")
#  lab_investigation_id :integer
#  department_id        :integer
#  order_generator_id   :integer
#  sample_collector_id  :integer
#  lab_technician_id    :integer
#  pathologist_id       :integer
#

class LabOrderDetail < ActiveRecord::Base
  # after_create :generate_lab_test_details
  # before_save :set_status_on_sample_collected

  STATUS = %w(Open Pending Closed Cancel Expired Verified Unverified Lost Not_Verified Collected )

  belongs_to :order
  has_many :lab_test_details, :dependent => :destroy
  validates :test, presence: true
  has_many :pictures, as: :imageable, :dependent => :destroy
  has_one :invoice_item, as: :assignable
  belongs_to :lab_investigation
  has_one :lab_result
  belongs_to :department
  before_save :set_department

  accepts_nested_attributes_for :invoice_item, :allow_destroy => true,
                                :reject_if => :all_blank

  validates :status,
            inclusion: { in: %w(Open Pending Closed Cancel Expired Verified Unverified Lost Not_Verified Collected open pending closed cancel expired verified unverified lost not_verified collected) }

  # def set_status_on_sample_collected
  #   lab_sample_collected = self.sample_collected
  #   if lab_sample_collected
  #     order = Order.find_by(id: self.order.id)
  #     order.update_attributes(status: 'Pending')
  #   end
  # end

  private

  def generate_lab_test_details
    ls = LabStudy.where(name: test).first
    StudyTemplate.where(lab_study_id: ls.id).each do |st|
      LabTestDetail.create!(lab_order_detail_id: self.id, caption: st.caption, range: st.range , unit: st.unit, test_label: st.test_label,  order_id: self.order.id )
    end
  end

  def set_department
    self[:department_id] = self.lab_investigation.department_id
  end
end
