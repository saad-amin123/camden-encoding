# == Schema Information
#
# Table name: histories
#
#  id         :integer          not null, primary key
#  patient_id :integer
#  history    :text
#  type       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SurgicalHistory < History
end
