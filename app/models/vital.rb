# == Schema Information
#
# Table name: vitals
#
#  id              :integer          not null, primary key
#  patient_id      :integer
#  user_id         :integer
#  medical_unit_id :integer
#  visit_id        :integer
#  weight          :float
#  height          :float
#  temperature     :float
#  bp_systolic     :float
#  bp_diastolic    :float
#  pulse           :float
#  resp_rate       :float
#  o2_saturation   :float
#  head_circ       :float
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Vital < ActiveRecord::Base

  belongs_to :patient
  belongs_to :user
  belongs_to :medical_unit
  belongs_to :visit

  validates_inclusion_of :weight, :in => 0..999, :message => "can only be number between 1 and 999", allow_nil: true, allow_blank: true
  validates_inclusion_of :height, :in => 0..96, :message => "can only be number between 1 and 96", allow_nil: true, allow_blank: true
  validates_inclusion_of :temperature, :in => 95..107, :message => "can only be number between 95 and 107", allow_nil: true, allow_blank: true
  validates_inclusion_of :bp_systolic, :in => 50..210, :message => "can only be number between 50 and 210", allow_nil: true, allow_blank: true
  validates_inclusion_of :bp_diastolic, :in => 33..120, :message => "can only be number between 33 and 120", allow_nil: true, allow_blank: true
  validates_inclusion_of :pulse, :in => 40..140, :message => "can only be number between 40 and 140", allow_nil: true, allow_blank: true
  validates_inclusion_of :resp_rate, :in => 12..70, :message => "can only be number between 12 and 70", allow_nil: true, allow_blank: true
  validates_inclusion_of :o2_saturation, :in => 0..100, :message => "can only be number between 0 and 100", allow_nil: true, allow_blank: true
  validates_inclusion_of :head_circ, :in => 16..56, :message => "can only be number between 16 and 56", allow_nil: true, allow_blank: true

  def self.generate_vital_statistics(from, to, patient_ids)
    result = Vital.where(created_at: from .. to, patient_id: patient_ids).order('DATE(created_at) DESC')
    result = result.group_by { |v| v.created_at.to_date.strftime("%Y-%m-%d") }
    return build_series(result, from , to)
  end

  private

  def self.build_series(result, from , to)
    {  
     'Weight':[  
        {  
          name: 'Weight',
          value: extract_info(result, from, to, 'weight'),
        }        
     ],

     'Height':[  
        {  
          name: 'Height',
          value: extract_info(result, from, to, 'height')
        }
     ],

     'BP_Systolic':[  
        {  
          name: 'BP Systolic',
          value: extract_info(result, from, to, 'bp_systolic'),
        }
     ],

     'BP_Diastolic':[
        {  
          name: 'BP Diastolic',
          value: extract_info(result, from, to, 'bp_diastolic')
        }
     ],

     'Temperature':[  
        {  
          name: 'Temperature',
          value: extract_info(result, from, to, 'temperature'),
        }
     ],
     
     'o2_saturation':[  
        {  
          name: 'O2 Saturation',
          value: extract_info(result, from, to, 'o2_saturation'),
        }
     ],

     'Pulse':[  
        {  
          name: 'Pulse',
          value: extract_info(result, from, to, 'pulse'),
        }
     ],

     'Resp_rate':[
        {  
          name: 'Respiratory Rate',
          value: extract_info(result, from, to, 'resp_rate')
        }
     ],

     'head_circ':[  
        {  
          name: 'Head Circumference',
          value: extract_info(result, from, to, 'head_circ'),
        }
     ]
    }
  end

  def self.extract_info(result, from , to, filter=nil)
    array = []
    average_wieght = []
    (from..to).each do |date|
      key = date.strftime("%Y-%m-%d")
      if result[key]
        array = array + result[key].map{|a| a[filter].present? ? a[filter] : 0} if filter
        # array << result[key][0][filter] if filter
      else
        array << 0
      end
    end
    if result.present?
      new_array = array.reject{|a| a == 0 }
      if new_array.length > 0
        filter = new_array.sum / new_array.length 
        array  = array.map{|a| a == 0 ? filter : a} 
      else
        array  = array.map{|a| a == nil ? 0 : a} 
      end
    end
    array
  end

end
