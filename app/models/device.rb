# == Schema Information
#
# Table name: devices
#
#  id               :integer          not null, primary key
#  uuid             :string
#  assignable_id    :integer
#  assignable_type  :string
#  role_id          :integer
#  operating_system :string
#  os_version       :string
#  status           :string
#  latitude         :float
#  longitude        :float
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

# class documentation
class Device < ActiveRecord::Base
  STATUS = %w(active inactive lost)
  ASSIGNABLE_TYPES = %w(MedicalUnit User)

  scope :active, -> { where(status: 'active') }
  scope :inactive, -> { where(status: 'inactive') }
  scope :lost, -> { where(status: 'lost') }

  belongs_to :assignable, polymorphic: true
  belongs_to :role
  has_many :complaints
  has_many :feedbacks

  accepts_nested_attributes_for :assignable

  validates :uuid, presence: true
  validates :role, presence: true
  validates :assignable_type, presence: true
  validates :assignable_id, presence: true
  validates :operating_system, presence: true
  validates :os_version, presence: true
  validates :status, presence: true

  validates_associated :assignable
  validates_associated :role
end
