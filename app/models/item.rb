# == Schema Information
#
# Table name: items
#
#  id              :integer          not null, primary key
#  generic_item_id :integer
#  medical_unit_id :integer
#  department_id   :integer
#  user_id         :integer
#  item_name       :string
#  item_code       :string
#  item_type       :string
#  main_group      :string
#  uom             :string
#  upc             :string
#  price           :string
#  sub_group       :string
#  strength        :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  status          :string           default("Open")
#  product_name    :string
#  pack            :float            default("1.0")
#  unit            :float            default("1.0")
#  pack_size       :float            default("1.0")
#

class Item < ActiveRecord::Base
  before_create :generate_item_code

  belongs_to :generic_item
  belongs_to :medical_unit
  belongs_to :department
  belongs_to :store
  belongs_to :user

  has_one :item_stock

  # validates :item_code, uniqueness: true
  validates :product_name, uniqueness: true

  def generate_item_code
    loop do
      self.item_code = (self.generic_item.name.first.capitalize + self.item_type.first.capitalize + rand.to_s[2..4])
      break unless Item.where(item_code: item_code).first
    end
  end
end
