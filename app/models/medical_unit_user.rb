# == Schema Information
#
# Table name: medical_unit_users
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  medical_unit_id :integer
#  role_id         :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

# class documentation
class MedicalUnitUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :medical_unit
  belongs_to :role
end
