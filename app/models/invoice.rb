# == Schema Information
#
# Table name: invoices
#
#  id                  :integer          not null, primary key
#  user_id             :integer
#  medical_unit_id     :integer
#  patient_id          :integer
#  visit_id            :integer
#  assignable_id       :integer
#  assignable_type     :string
#  total_amount        :string
#  discount_percentage :string
#  discount_value      :string
#  total_discount      :string
#  payable             :string
#  paid                :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class Invoice < ActiveRecord::Base

  ASSIGNABLE_TYPES = %w(Order)

  belongs_to :user
  belongs_to :medical_unit
  belongs_to :patient
  belongs_to :visit
  belongs_to :assignable, polymorphic: true
  has_many   :invoice_items, :dependent => :destroy

  accepts_nested_attributes_for :assignable
end
