# == Schema Information
#
# Table name: lab_investigation_prices
#
#  id                   :integer          not null, primary key
#  medical_unit_id      :integer
#  lab_investigation_id :integer
#  department_name      :string
#  price                :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class LabInvestigationPrice < ActiveRecord::Base
	belongs_to :lab_investigation
end
