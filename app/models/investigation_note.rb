# == Schema Information
#
# Table name: investigation_notes
#
#  id                 :integer          not null, primary key
#  medical_unit_id    :integer
#  patient_id         :integer
#  visit_id           :integer
#  admission_id       :integer
#  user_id            :integer
#  note               :string
#  investigation_type :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class InvestigationNote < ActiveRecord::Base

	belongs_to :medical_unit
	belongs_to :patient
	belongs_to :visit
	belongs_to :admission
	belongs_to :user
end
