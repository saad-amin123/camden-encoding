# == Schema Information
#
# Table name: bed_allocations
#
#  id                      :integer          not null, primary key
#  medical_unit_id         :integer
#  patient_id              :integer
#  admission_id            :integer
#  ward_id                 :integer
#  bay_id                  :integer
#  bed_id                  :integer
#  status                  :string           default("Free")
#  patient_last_updated_by :integer
#  user_id                 :integer
#  visit_id                :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  comment                 :string
#

class BedAllocation < ActiveRecord::Base
	include PublicActivity::Model
  	tracked owner: Proc.new { |controller, model| controller && controller.current_user }
	
	belongs_to :medical_unit
	belongs_to :patient
	belongs_to :admission
	belongs_to :ward
	belongs_to :bay
	belongs_to :bed
	belongs_to :user
	belongs_to :visit
	has_many :ward_comments

	def self.update_status(bed)
		@bed = BedAllocation.where("ward_id =? AND bed_id =? AND status =?", bed.ward_id, bed.id , 'Occupied')
		if @bed.count == 1
			bed.update_attributes(status: 'Free')
		end
	end

	def update_medicine_request_status
		medicine_requests = MedicineRequest.where(status: "Open",patient_id: self.patient_id)
		medicine_requests.update_all(status: "Canceled")
	end
end
