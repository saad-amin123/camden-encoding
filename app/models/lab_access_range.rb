# == Schema Information
#
# Table name: lab_access_ranges
#
#  id                   :integer          not null, primary key
#  gender               :string
#  age_start            :integer
#  age_end              :integer
#  date_unit            :string
#  start_range          :float
#  end_range            :float
#  range_title          :string
#  status               :string
#  heading              :string
#  lab_assessment_id    :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  medical_unit_id      :integer
#  lab_investigation_id :integer
#

class LabAccessRange < ActiveRecord::Base
  belongs_to :lab_assessment
  belongs_to :medical_unit
  belongs_to :lab_investigation
  before_save :set_age_start_age_end, :set_date_unit

  def set_date_unit
    self[:date_unit] = "Day"
  end

  def set_age_start_age_end
    if date_unit.present?
      if "years".include?(date_unit.downcase)
        self[:age_start] = age_start.to_i * 365
        self[:age_end] = age_end.to_i * 365
      elsif "months".include?(date_unit.downcase)
        self[:age_start] = age_start.to_f * (365.0/12.0)
        self[:age_end] = age_end.to_f * (365.0/12.0)
      else
        self[:age_start] = age_start.to_i
        self[:age_end] = age_end.to_i
      end
    end
  end
end
