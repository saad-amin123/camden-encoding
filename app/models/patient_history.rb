# == Schema Information
#
# Table name: patient_histories
#
#  id               :integer          not null, primary key
#  visit_id         :integer
#  patient_id       :integer
#  medical_unit_id  :integer
#  visit_reason     :string
#  chief_complaint  :string
#  diagnosis        :text
#  notes            :string
#  medication_order :text
#  disposition      :text
#  allergies        :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  doctor_name      :string
#  user_id          :integer
#

class PatientHistory < ActiveRecord::Base
  belongs_to :user
  belongs_to :visit

  def self.generate_dr_performance_stats(from, to, medical_unit_ids)
  	result = nil
    unless medical_unit_ids != ['All']
    	result = PatientHistory.select(:doctor_name).where(created_at: from .. to).group(:doctor_name, 'DATE(created_at)').having('COUNT("patient_histories"."doctor_name") >= 5').count
    else
    	result = PatientHistory.select(:doctor_name).where(created_at: from .. to, medical_unit_id: medical_unit_ids).group(:doctor_name, 'DATE(created_at)').having('COUNT("patient_histories"."doctor_name") >= 5').count
    end
    hash = calculate_doctors_count(result)
    return build_series(hash, from, to)
  end

  def self.generate_no_of_patient_per_dr_stats(from, to, medical_unit_ids)
  	result = nil
    unless medical_unit_ids != ['All']
    	result1 = PatientHistory.all.where(created_at: from .. to).map{|p| [p.patient_id, p.created_at.strftime("%Y-%m-%d"), p.doctor_name]}
    else
    	result1 = PatientHistory.all.where(created_at: from .. to, medical_unit_id: medical_unit_ids).map{|p| [p.patient_id, p.created_at.strftime("%Y-%m-%d"), p.doctor_name]}
    end
    result1 = result1.group_by { |v| v[1].to_date.strftime("%Y-%m-%d") }
    return build_series(result=nil, from , to, result1)
  end

  private

  def self.build_series(result, from , to, result1=nil)
    if result
      [ 
        {
          name: 'No. of Doctors (at least 5 patients/day)',
          data: extract_info(result, from, to,result1)
        }
      ]
    elsif result1
      [ 
        {
          name: 'No. of Patients per Doctor',
          data: extract_info(result, from, to,result1)
        }
      ]
    end
      
  end

  def self.calculate_doctors_count(result)
    hash = {}
    result.keys.each do |k|
      key = k[1].strftime("%Y-%m-%d")
      hash[key] = hash[key].present? ? hash[key]+1 : 1
    end
    hash
  end

  def self.extract_info(result, from , to, result1)
    array = []
    (from..to).each do |date|
    	array1 = nil
      array2 = nil
      key = date.strftime("%Y-%m-%d")
      if (result && result[key]) or (result1 && result1[key])
        array << result[key] unless result1
        array1 = result1[key].count.to_f if result1
        array2 = result1[key].collect {|ind| ind[2]}.uniq.count if result1
        array << (array1/array2) if result1
      else
        array << 0
      end
    end
    array
  end
end
