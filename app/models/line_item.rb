# == Schema Information
#
# Table name: line_items
#
#  id               :integer          not null, primary key
#  order_id         :integer
#  brand_drug_id    :integer
#  quantity         :integer
#  category         :string
#  expiry           :date
#  medical_unit_id  :integer
#  device_id        :integer
#  inventory_id     :integer
#  order_type       :string
#  status           :string
#  description      :text
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  user_id          :integer
#  reason           :string
#  order_item_id    :integer
#  transaction_type :string
#  store_id         :integer
#  department_id    :integer
#  batch_id         :integer
#  visit_id         :integer
#

class LineItem < ActiveRecord::Base
  belongs_to :inventory
  belongs_to :brand_drug
  belongs_to :medical_unit
  belongs_to :device
  belongs_to :user
  belongs_to :store
  belongs_to :department
  belongs_to :batch
  belongs_to :visit
  validates :inventory, presence: true
  validates :category, presence: true
  validates :medical_unit, presence: true
  validates :brand_drug, presence: true
  validates :quantity, presence: true
  validates :transaction_type, presence: true
  validates :device, presence: true
  validates :user, presence: true
  CATEGORY = %w(medicine supply)
  TRANSACTION_TYPE = %w(add issue update)
  after_create :update_inventory

  def update_inventory
    inventory = self.inventory
    batch = self.batch
    if self.transaction_type == "add"
      inventory.quantity += self.quantity
      batch.quantity += self.quantity
      batch.save
    elsif self.transaction_type == "update"
      inventory.quantity -= batch.quantity
      inventory.quantity += self.quantity 
      batch.quantity = self.quantity
      batch.save
    else # self.transaction_type == "issue"
      if inventory.quantity > self.quantity
        inventory.quantity -= self.quantity
        order_item = OrderItem.find(self.order_item_id)
        order_item.issued_brand_drug_id = self.brand_drug_id
        order_item.save
        order = order_item.order
        order.update_attributes(status: "Closed")
      end      
    end
    inventory.device_id = self.device_id
    inventory.updated_at = self.updated_at
    inventory.save
    
  end

  def self.medicine_order(medical_unit_ids=nil,order_type,sub_category, start_date, end_date)
    result = nil
    @quantity_arr = []
    @result1 = []
    unless medical_unit_ids != ['All']
      result = LineItem.where('transaction_type = ? AND Created_At >= ? AND Created_at <=?', "issue",start_date, end_date).pluck(:brand_drug_id, :quantity ).group_by(&:first).map{ |k,a| [k,a.map(&:last)] }
    else
      result = LineItem.where('transaction_type = ? AND Created_At >= ? AND Created_at <=? AND medical_unit_id in (?)', "issue",start_date, end_date, medical_unit_ids).pluck(:brand_drug_id, :quantity ).group_by(&:first).map{ |k,a| [k,a.map(&:last)] }
    end
    result.each do |r|
      @quantity_arr << r[1].inject(0){|sum,x| sum + x }
      @result1 <<  BrandDrug.where(id: r[0]).first
    end
    return @result1, @quantity_arr
  end  
end
