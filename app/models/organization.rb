# == Schema Information
#
# Table name: organizations
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  medical_unit_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Organization < ActiveRecord::Base
  belongs_to :user
  belongs_to :medical_unit
end
