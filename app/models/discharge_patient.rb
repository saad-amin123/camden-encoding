# == Schema Information
#
# Table name: discharge_patients
#
#  id              :integer          not null, primary key
#  medical_unit_id :integer
#  patient_id      :integer
#  bed_id          :integer
#  admission_id    :integer
#  treatment_plan  :string
#  medications     :string
#  follow_up       :string
#  discharge_type  :string
#  comments        :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  visit_id        :integer
#

class DischargePatient < ActiveRecord::Base
	belongs_to :medical_unit
	belongs_to :patient
	belongs_to :visit
	belongs_to :bed
	belongs_to :admission
	after_create :update_admisssion_status
	after_create :update_medicine_request_status

	def update_admisssion_status
		admission = Admission.find_by_id(self.admission_id)
		bedAllocation = admission.bed_allocations.last
		admission.update_attributes(status: "Discharged")
		bedAllocation.update_attributes(status: "Discharged")
		@bed = BedAllocation.where("bed_id =? AND status =?", bedAllocation.bed.id , 'Occupied')
		if @bed.count == 0
		  bedAllocation.bed.update_attributes(status: 'Free')
		end
	end
	def update_medicine_request_status
		medicine_requests = MedicineRequest.where(status: "Open",patient_id: self.patient_id)
		medicine_requests.update_all(status: "Canceled")
	end
end
