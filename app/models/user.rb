# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  role_id                :integer
#  username               :string           default(""), not null
#  first_name             :string
#  last_name              :string
#  title                  :string
#  phone                  :string
#  nic                    :string
#  dob                    :date
#  father_name            :string
#  father_nic             :string
#  mother_name            :string
#  mother_nic             :string
#  address1               :string
#  authentication_token   :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default("0"), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  failed_attempts        :integer          default("0"), not null
#  unlock_token           :string
#  locked_at              :datetime
#  address2               :string
#  department_id          :integer
#

# class documentation
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  include TokenAuthenticatable
  before_save :ensure_authentication_token

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # belongs_to :role
  has_one :device, as: :assignable
  
  has_many :complaints
  has_many :feedbacks
  has_many :complaint_watchers
  has_many :favorite_complaints
  has_many :medical_unit_users
  has_many :medical_units, through: :medical_unit_users
  has_many :roles, through: :medical_unit_users
  has_many :pictures, as: :imageable
  has_many :allergies
  has_many :vitals
  has_many :home_medications
  has_many :radiologies
  has_one :preference
  has_many :immunizations
  has_many :rosters
  has_many :leave # leave plural is also leave
  has_many :vaccinations, :dependent => :destroy
  has_many :follow_ups, :dependent => :destroy
  has_many :visit_users
  has_many :visits, through: :visit_users
  has_many :organizations
  # belongs_to :department
  has_many :user_departments
  has_many :departments, through: :user_departments
  has_many :user_wards
  has_many :wards, through: :user_wards

  has_many :orders, class_name: 'Order', foreign_key: 'doctor_id'
  has_and_belongs_to_many :stores
  has_many :admissions
  has_many :bed_allocations
  has_many :investigation_notes
  has_many :ward_comments
  has_many :visits

  validates :username, presence: true, uniqueness: { case_sensitive: false }
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :phone, presence: true
  validates :nic, presence: true
  validates :dob, presence: true
  validates :email, presence: true

  scope :logged_in_users, -> { where.not(authentication_token: 'NULL') }

  def title
    "User: #{first_name} #{last_name}"
  end

  def skip_confirmation!
    self.confirmed_at = Time.now
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def phone_in_format
    number = Phony.normalize(phone)
    number = "+92#{number}" unless number.starts_with?('+92')
    number
  end

  def set_preference(category,department)
    if self.preference.present?
      self.preference.update_attributes(category: category,department: department)
    else
      self.create_preference(category: category,department: department)
    end
    self.preference
  end
end
