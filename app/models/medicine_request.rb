# == Schema Information
#
# Table name: medicine_requests
#
#  id              :integer          not null, primary key
#  medical_unit_id :integer
#  patient_id      :integer
#  visit_id        :integer
#  admission_id    :integer
#  prescribed_by   :integer
#  requested_by    :integer
#  status          :string
#  item_id         :integer
#  request_id      :string
#  prescribed_qty  :float
#  product_name    :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  store_id        :integer
#

class MedicineRequest < ActiveRecord::Base
  belongs_to :medical_unit
  belongs_to :patient
  belongs_to :visit
  belongs_to :admission
  belongs_to :item
  belongs_to :store

  # def generate_request_number
  #   self.request_id =  Date.today.strftime("%Y") + '-' + Date.today.strftime("%m") + '-' + Date.today.strftime("%d")+ '-' + (MedicineRequest.where('Created_At >= ?', Date.current).count+1).to_s
  # end
end
