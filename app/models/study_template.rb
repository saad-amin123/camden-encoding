# == Schema Information
#
# Table name: study_templates
#
#  id           :integer          not null, primary key
#  lab_study_id :integer
#  caption      :string
#  range        :string
#  unit         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  test_label   :string
#

class StudyTemplate < ActiveRecord::Base
  belongs_to :lab_study

  validates :caption, presence: true
end
