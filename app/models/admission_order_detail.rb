# == Schema Information
#
# Table name: admission_order_details
#
#  id             :integer          not null, primary key
#  order_id       :integer
#  category       :string
#  department     :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  admission_type :string
#  instructions   :string
#

class AdmissionOrderDetail < ActiveRecord::Base
  TYPE = %w(new disposition)
  
  belongs_to :order
  validates :department, presence: true
  validates :category, presence: true
  validates :admission_type, presence: true
  validates :admission_type,
            inclusion: { in: %w(new disposition) }
end
