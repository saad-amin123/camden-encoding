# == Schema Information
#
# Table name: contacts
#
#  id         :integer          not null, primary key
#  patient_id :integer
#  number     :string
#  category   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# class documentation
class Contact < ActiveRecord::Base
  belongs_to :patient
end
