# class documention
class Api::V1::RadiologyOrderDetailsController < ApplicationController
  before_action :verify_authenticity_token, only: [:update]
  before_action :verify_device, only: [:update]
  before_action :verify_order, only: [:update]

  respond_to :json

  def update
    radiology_order_detail = @order.radiology_order_detail
    if radiology_order_detail.update_attributes(radiology_order_detail_params)
      render status: :ok, json: radiology_order_detail
    else
      render status: :unprocessable_entity,
             json: { message: radiology_order_detail.errors.full_messages }
    end
  end

  private

  def radiology_order_detail_params
    params.require(:radiology_order_detail).permit(:note, :result)
  end
end
