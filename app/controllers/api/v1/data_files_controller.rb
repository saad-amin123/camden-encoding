class  Api::V1::DataFilesController < ApplicationController
  before_action :verify_authenticity_token, :check_file_exist, :check_file_format,  only: [:upload]

  def upload
    @current_medUnit = @current_user.medical_units.first
    s3_storage = S3StoreService.new(params[:file].tempfile.path)
    s3_storage.csv_store("csv_data", @current_medUnit)
    @file_data = DataFile.new(file_url: s3_storage.url, status:"uploaded")
    @file_data.user = @current_user
    @file_data.medical_unit = @current_medUnit
    if @file_data.save!
      render status: :ok, json: { message: "File uploaded successfully!" }
    else
      render status: :not_found, json: { message: "File upload failed!" }
    end
  end

  def check_file_exist
    if !params[:file].present?
      render status: :not_found, json: { message: "No file chosen. Please select a CSV file first!" }
    end
  end

  def check_file_format
    extention = params[:file].tempfile.path.split('.').last
    if extention != 'csv'
      render status: :not_found, json: { message: "Not a CSV file. Please select CSV file!" }
    end
  end

end
