# class documention
class Api::V1::RostersController < ApplicationController
  before_action :verify_authenticity_token, only: [:create,:index]
  before_action :verify_device, only: [:create]
  before_action :fetch_medical_unit, only: [:create,:index]
  before_action :fetch_department, only: [:create,:index]

  respond_to :json

  def create
    @roster = current_user.rosters.new(roster_params)
    @roster.map {|p| p.user = @current_user;p.medical_unit_id = @current_medical_unit.id;p.department_id = @current_department.id} 
    if  @roster.map {|p| p.save }
      render status: :ok, json: @roster
    else
      render status: :unprocessable_entity,
             json: { message: @roster.errors.full_messages }
    end
  end

  def index
    if params[:shift] == 'All'
      @rosters = Roster.where('date >=  (?) AND date::date <= (?) AND department_id = (?)',params[:start_date],params[:end_date],params[:department_id])
    elsif params[:shift] != 'All'
      @rosters = Roster.where('date >=  (?) AND date::date <= (?) AND department_id = (?) AND shift = (?)',params[:start_date],params[:end_date],params[:department_id],params[:shift])
    end
    if @rosters.present?
      render status: :ok, json: @rosters     
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  private

  def roster_params
    params.require(:roster).map { |m| m.permit(:doctors, :shift, :date)}
  end
end
