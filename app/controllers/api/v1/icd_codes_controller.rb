# class documention
class Api::V1::IcdCodesController < ApplicationController
  before_action :verify_authenticity_token, only: [:search]
  before_action :verify_device, only: [:search]
  respond_to :json

  def search
    icds = IcdCode.arel_table
    possible_icds = IcdCode.where(icds[:name].matches("#{params[:query]}%"))
    if possible_icds.present?
      render status: :ok, json: possible_icds
    else
      render status: :not_found, json: { message: "Record not found" }
    end
  end
end