# class documention
class Api::V1::HomeMedicationsController < ApplicationController
  before_action :verify_authenticity_token
  before_action :verify_device, only: [:create,:last_medication]
  before_action :fetch_patient
  before_action :verify_device_medical_unit, only: [:create]
  before_action :verify_device_role_medical_unit, only: [:create] if Rails.env != "test"
  before_action :fetch_visit, only: [:create]
  before_action :fetch_medical_unit, only: [:create,:last_medication]
  # before_action :verify_brand_drug, only: [:create]

  serialization_scope :fetch_medical_unit
  serialization_scope :current_user
  respond_to :json

  def create
    begin
      medications = []
      params.require(:medication).each do |med|
        medication = @patient.medications.new(med.permit(:brand_drug_id, :dosage, :unit, :route,
                                  :frequency, :usage, :instruction, :type))
        medication.user = @current_user
        medication.visit = @visit
        # medication.brand_drug = @brand_drug
        medication.medical_unit = @current_medical_unit

        if medication.save
          medications << medication
        end
      end
      render status: :ok, json: medications
    rescue Exception => e
      render status: :unprocessable_entity, json: {message: "#{e.message}"}
    end
  end

  def index
    @home_medications = @patient.home_medications
    if @home_medications.present?
      render status: :ok, json: @home_medications
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  def destroy
    @home_medication = @patient.home_medications.find_by_id(params[:id])
    if @home_medication.present?
      @home_medication.destroy
       render status: :ok, json: { message: 'Successfully deleted' }
    else
      render status: :unprocessable_entity, json: { message: 'Invalid Request' }
    end
  end

  def last_medication
    last_med = HospitalMedication.where(patient_id: @patient.id,medical_unit_id: @current_medical_unit.id,brand_drug_id: params[:brand_drug_id]).last
    if last_med.present?
      render status: :ok, json: last_med
    else
      render status: :not_found, json: { message: "Record not found" }
    end
  end

  private

  def medication_params
    params.require(:medication).permit(:brand_drug_id, :dosage, :unit, :route,
                                  :frequency, :usage, :instruction, :type)
  end
end
