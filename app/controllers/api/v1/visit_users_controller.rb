# class documention
class Api::V1::VisitUsersController < ApplicationController
  before_action :verify_authenticity_token, only: [:create,:index,:update]
  before_action :verify_device, only: [:create,:update]
  before_action :fetch_visit, only: [:create,:index]
  before_action :verify_device_role_medical_unit, only: [:create] if Rails.env != "test"

  respond_to :json

  def create
    @visit_user = @visit.visit_users.new(visit_user_params)
    @visit_user.user = @current_user
    @visit_user.visit = @visit
    if @visit_user.save
      render status: :ok, json: @visit_user
    else
      render status: :unprocessable_entity,
             json: { message: @visit_user.errors.full_messages }
    end
  end

  def index
    visit_user = VisitUser.where(visit_id: params[:visit_id], user_id: params[:user_id]).last
    if visit_user.present?
      render status: :ok, json: visit_user
    else
      render status: :not_found, json: { message: "Record not found"}
    end
  end

  def update
    @visit_user = @visit.visit_users.find_by_id(params[:id])
    if @visit_user.update_attributes(visit_user_params)
      render status: :ok, json: @visit_user
    else
      render status: :unprocessable_entity, json: { message: @visit_user.errors.full_messages }
    end
  end

  private

  def visit_user_params
    params.require(:visit_user).permit(:is_checkout)
  end
end
