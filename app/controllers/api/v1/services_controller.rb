# class documention
class Api::V1::ServicesController < ApplicationController
  skip_before_filter :verify_authenticity_token

  def index
    @services = Service.where('category = ?', params[:category]) if params[:category] # rubocop:disable Metrics/LineLength
    @services = Service.all if params[:category] == 'all'
    if @services.present?
      render status: :ok, json: @services
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end
end
