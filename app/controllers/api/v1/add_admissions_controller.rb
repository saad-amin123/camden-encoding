# class documention
class Api::V1::AddAdmissionsController < ApplicationController
  before_action :verify_authenticity_token, only: [:create, :index]
  before_action :verify_device, only: [:create]
  before_action :verify_order, only: [:create, :index]
  # before_action :verify_add_admission, only: [:index]

  serialization_scope :fetch_medical_unit
  serialization_scope :current_user
  respond_to :json

  def create
    add_admission = @order.add_admissions.new(add_admission_params)
    if add_admission.save
      render status: :ok, json: add_admission
    else
      render status: :unprocessable_entity,
             json: { message: add_admission.errors.full_messages }
    end
  end

  def index
    add_admissions = @order.add_admissions
    if add_admissions.present?
      render status: :ok, json: add_admissions
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  private

  def add_admission_params
    params.require(:add_admission).permit(:category, :department, :floor_number,
                                          :bed_number, :bay_number)
  end
end
