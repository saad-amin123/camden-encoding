# class documention
class Api::V1::PatientHistoriesController < ApplicationController
  before_action :verify_authenticity_token, only: [:create, :index]
  before_action :verify_device, only: [:create]
  before_action :fetch_patient, only: [:create]
  before_action :verify_device_role_medical_unit, only: [:create] if Rails.env != "test"
  before_action :fetch_visit, only: [:create]
  before_action :fetch_medical_unit, only: [:create]
  respond_to :json

  def create
    @patient_history = PatientHistory.new(patient_history_params)
    @patient_history.patient_id = @patient.id
    @patient_history.visit_id = @visit.id
    @patient_history.user = @current_user
    @patient_history.medical_unit_id = @current_medical_unit.id
    if @patient_history.save
      render status: :ok, json: @patient_history
    else
      render status: :unprocessable_entity,
             json: { message: @patient_history.errors.full_messages }
    end
  end

  def index
    @patient_history = PatientHistory.where('created_at >= ? AND created_at::date <= ? AND patient_id = ?', params[:start_date], params[:end_date], params[:patient_id])
    if @patient_history.present?
      render status: :ok, json: @patient_history
    else
      render status: :not_found, json: { message: "Record not found"}
    end
  end

  private

  def patient_history_params
    params.require(:history).permit(:visit_reason, :chief_complaint, :diagnosis,
                                    :notes, :medication_order, :disposition,
                                    :allergies,:doctor_name)
  end
end

