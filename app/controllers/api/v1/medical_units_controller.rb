class Api::V1::MedicalUnitsController < ApplicationController
  before_action :verify_authenticity_token, only: [:index, :user_assigned_medical_units]
  def index
    @medical_units = params[:region_id].present? ? MedicalUnit.where(region_id: params[:region_id]).order('title ASC') : MedicalUnit.all
    if @medical_units.present?
      render status: :ok, json: @medical_units
    else
      render status: :not_found, json: { message: 'Medical unit not found' }
    end
  end

  def user_assigned_medical_units
    @medical_unit_ids_in_hash = MedicalUnit.fetch_assigned_units(@current_user)
    if @medical_unit_ids_in_hash.present?
      render json: { medical_units: @medical_unit_ids_in_hash }
    end
  end
end
