# class documention
class Api::V1::CallRequestsController < ApplicationController
  before_action :verify_authenticity_token, only: [:create, :index]
  before_action :verify_device, only: [:create]
  respond_to :json

  def create
    @call_request = CallRequest.new(call_request_params)
    @call_request.request_from = @current_user.id
    if @call_request.save
      render status: :ok, json: @call_request
    else
      render status: :unprocessable_entity,
             json: { message: @visit.errors.full_messages }
    end
  end

  def index
    @call_requests = CallRequest.where('request_to = ?',
                                       params[:request_to]).first if params[:request_to]
    requests = @call_requests
    @call_requests.destroy if @call_requests
    if requests.present?
      render status: :ok, json:{call_requests: requests.as_json(:only => [:code])} 
    else
      render status: :not_found, json: { message: 'Call Request not found' }
    end
  end

  private

  def call_request_params
    params.require(:call_request).permit(:request_from, :request_to, :code)
  end
end
