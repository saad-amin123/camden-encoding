# class documention
class Api::V1::LabTestDetailsController < ApplicationController
  before_action :verify_authenticity_token, only: [:update]
  before_action :verify_device, only: [:update]

  respond_to :json

  def update
    array = []
    begin
      params.require(:lab_test_details).each do |item|
        @lab_test_detail = LabTestDetail.find_by_id(item[:id])
        @lab_test_detail.update_attributes(value: item[:value],
                                           comments: item[:comments])
        array << @lab_test_detail
      end
      render status: :ok, json: array
    rescue Exception => e
      render status: :unprocessable_entity, json: {message: "#{e.message}"}
    end
  end

  private

  def lab_test_detail_params
    params.require(:lab_test_details).permit(:value, :comments)
  end
end
