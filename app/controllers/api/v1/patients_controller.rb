require 'barby'
require 'barby/barcode/code_128'
require 'barby/outputter/html_outputter'
require 'barby/outputter/png_outputter'
require 'barby/outputter/cairo_outputter'
# class documention
class Api::V1::PatientsController < ApplicationController
  # skip_before_filter  :verify_authenticity_token
  before_action :verify_authenticity_token, only: [:index, :create, :update, :show, :toggle_active, :favorite_patients]
  before_action :verify_device, only: [:create, :update]
  before_action :fetch_medical_unit, only: [:create, :update]
  before_action :verify_device_role_medical_unit, only: [:create, :update] if Rails.env != "test"
  before_action :fetch_patient, only: [:toggle_active]
  serialization_scope :current_user
  respond_to :json

  # def index # rubocop:disable Metrics/MethodLength
  #   @patients = []
  #   hash = {}
  #   hash[:fdo] = ['fdo','Nurse','MO','Dr', 'Lab_fdo', 'Outpatient', 'Emergency','Inpatient']
  #   hash[:Nurse] = ['Nurse','MO','Dr', 'Lab_fdo', 'Outpatient', 'Emergency','Inpatient']
  #   hash[:MO] = [ 'MO','Dr', 'Lab_fdo', 'Outpatient', 'Emergency','Inpatient']
  #   hash[:Dr] = ['Dr', 'Lab_fdo', 'Outpatient','Emergency','Inpatient']
  #   hash[:Lab_fdo] = ['Lab_fdo', 'Outpatient','Emergency','Inpatient']
  #   hash[:CH_OPD_fdo] = ['Outpatient','Emergency','Inpatient']
    
  #   @patients = Patient.find_by_scope(current_medical_unit.id, hash[params[:ref_to].to_sym], from_date, to_date, params[:type],  params[:query])
    
  #   if @patients.present?
  #     render status: :ok, json: @patients
  #   else
  #     render status: :not_found, json: { message: 'Patient not found' }
  #   end
  # end

  def create
    @patient = Patient.new(patient_params)
    @patient.registered_by = @current_user.id
    # @patient.registered_at = @current_device.assignable_id
    @patient.registered_at = @current_medical_unit
    @patient.generate_mrn(@current_medical_unit)
    if @patient.save
      # full_path = "/home/rails/api-pitb/public/barcode_#{@patient.mrn}.png"
      # barcode = Barby::Code128.new(@patient.mrn)
      # File.open(full_path, 'w') { |f| f.write barcode.to_png }
      # @visit = @patient.visits.new(referred_to: "fdo")
      # @visit.created_by = @current_user
      # @visit.medical_unit = @current_medical_unit
      # @visit.save
      render status: :ok, json: @patient
    else
      render status: :unprocessable_entity, json: { message: @patient.errors.full_messages }
    end
  end

  def update
    @patient = Patient.find_by_id(params[:id])
    if @patient.update_attributes(patient_update_params)
      render status: :ok, json: @patient
    else
      render status: :unprocessable_entity, json: { message: @patient.errors.full_messages }
    end
  end

  def show
    patient = Patient.where(mrn: params[:id])
    if patient.present?
      render status: :ok, json: patient.first
    else
      render status: :not_found, json: { message: 'Patient not found' }
    end
  end

  def toggle_active
    doctor_patient = DoctorPatient.where(doctor_id: current_user.id,patient_id: @patient.id).first_or_create
    doctor_patient.active = params[:active]
    if doctor_patient.save
      render status: :ok, json: doctor_patient
    else
      render status: :unprocessable_entity, json: { message: doctor_patient.errors.full_messages }
    end
  end

  def favorite_patients
    active_patients = Patient.get_active_patients(params[:category],params[:department],current_user)
    if active_patients.present?
      render status: :ok, json: active_patients
    else
      render status: :not_found, json: { message: 'Active patients not found'}
    end
  end

  private

  def patient_params
    params.require(:patient).permit(:first_name, :last_name, :middle_name,
                                  :gender, :education, :birth_day,
                                  :birth_month, :birth_year, :patient_nic,
                                  :patient_passport, :unidentify_patient,
                                  :guardian_relationship,
                                  :guardian_first_name, :guardian_last_name,
                                  :guardian_middle_name, :guardian_nic,
                                  :guardian_passport, :unidentify_guardian,
                                  :marital_status, :state, :city, :near_by_city,
                                  :address1, :address2, :phone1, :phone1_type,
                                  :phone2, :phone2_type, :phone3, :phone3_type,
                                  :father_name)     
  end
  def patient_update_params # rubocop:disable Metrics/MethodLength
    params.require(:patient).permit(:first_name, :last_name, :middle_name,
                                    :gender, :education, :birth_day,
                                    :birth_month, :birth_year, :patient_nic,
                                    :patient_passport, :unidentify_patient,
                                    :guardian_relationship,
                                    :guardian_first_name, :guardian_last_name,
                                    :guardian_middle_name, :guardian_nic,
                                    :guardian_passport, :unidentify_guardian,
                                    :marital_status, :state, :city, :near_by_city,
                                    :address1, :address2, :phone1, :phone1_type,
                                    :phone2, :phone2_type, :phone3, :phone3_type,
                                    :blood_group, :hiv, :hepatitis_b_antigens,
                                    :hepatitis_c,:father_name, :unidentify, :age
                                   )
  end
end
