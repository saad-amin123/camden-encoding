# class documention
class Api::V1::PatientPastMedicalHistoriesController < ApplicationController
  before_action :verify_authenticity_token, only: [:create, :index, :update]
  before_action :verify_device, only: [:create, :update]
  before_action :fetch_patient, only: [:create, :update, :index]
  before_action :verify_device_medical_unit, only: [:create, :update]
  before_action :verify_device_role_medical_unit, only: [:create, :update] if Rails.env != "test"
  before_action :fetch_medical_unit, only: [:create, :update]
  before_action :fetch_visit, only: [:create, :update]

  # serialization_scope :fetch_medical_unit
  # serialization_scope :current_user
  respond_to :json

  def create
    @pat_med_history = @patient.patient_past_medical_histories.new(patient_past_medical_history_params)
    @pat_med_history.user = @current_user
    @pat_med_history.medical_unit = @current_medical_unit
    @pat_med_history.visit = @visit
    if @pat_med_history.save
      render status: :ok, json: @pat_med_history
    else
      render status: :unprocessable_entity,
             json: { message: @pat_med_history.errors.full_messages }
    end
  end

  def index
    @pat_med_histories = @patient.patient_past_medical_histories
    if @pat_med_histories.present?
      render status: :ok, json: @pat_med_histories
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  private

  def patient_past_medical_history_params
    params.require(:patient_past_medical_history).permit(:med_name, :note)
  end
end
