# class documention
class Api::V1::VitalsController < ApplicationController
  before_action :verify_authenticity_token, only: [:create, :index, :destroy]
  before_action :verify_device, only: [:create]
  before_action :fetch_patient, only: [:create, :index, :destroy]
  before_action :verify_device_medical_unit, only: [:create]
  before_action :verify_device_role_medical_unit, only: [:create] if Rails.env != "test"
  # before_action :fetch_visit, only: [:create]
  before_action :fetch_medical_unit, only: [:create]

  serialization_scope :fetch_medical_unit
  serialization_scope :current_user
  respond_to :json

  def index
    @vitals = @patient.vitals
    if @vitals.present?
      render status: :ok, json: @vitals
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  def create
    @vital = @patient.vitals.new(vital_params)
    @visit = Visit.find_by_id(params[:visit_id])
    if @visit.present?
      @vital.visit = @visit
    else
      @vital.visit = @patient.visits.last
    end
    @vital.user = @current_user
    @vital.medical_unit = @current_medical_unit
    if @vital.save
      render status: :ok, json: @vital
    else
      render status: :unprocessable_entity,
             json: { message: @vital.errors.full_messages }
    end
  end

  def destroy
    @vital = @patient.vitals.find_by_id(params[:id])
    if @vital.present?
      @vital.destroy
       render status: :ok, json: { message: 'Successfully deleted' }
    else
      render status: :unprocessable_entity, json: { message: 'Invalid Request' }
    end
  end

  private

  def vital_params
    params.require(:vital).permit(:weight, :height, :temperature,
                                      :bp_systolic, :bp_diastolic,
                                      :pulse, :resp_rate, :o2_saturation,
                                      :head_circ)
  end
end
