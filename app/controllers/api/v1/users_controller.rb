# class documention
class Api::V1::UsersController < ApplicationController
  before_filter  :verify_authenticity_token, only: [:set_preference,:delete_preference,:hospital_doctors, :logged_in_doctors,:index,:update, :change_password]
  before_action :fetch_medical_unit, only: [:hospital_doctors,:update]
  before_action :verify_device, only: [:hospital_doctors,:update]
  respond_to :json

  def forgot_username
    user = User.find_by(phone: params[:phone], dob: params[:dob])
    if user.present?
      render status: :ok, json: user
    else
      render status: :not_found, json: { message: 'Record not match' }
    end
  end

  def forgot_password # rubocop:disable Metrics/MethodLength
    user = User.find_by(phone: params[:phone],
                        nic: params[:nic], dob: params[:dob])
    # @twilio_sms_client = Twilio::REST::Client.new(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN)
    if user.present?
      user.password = (0...8).map { ('a'..'z').to_a[rand(26)] }.join
      user.save
      # Send SMS to User
      # begin
      #   @twilio_sms_client.messages.create(from: TWILIO_NUMBER,
      #                                      to: user.phone_in_format, body: "your new password: #{user.password}")
      # rescue Exception => e
      # end
      render status: :ok,
             json: { message: 'Message has been sent to your Mobile number ' }
    else
      render status: :not_found, json: { message: 'Record not match' }
    end
  end

  def set_preference
    department = params[:user][:department]
    category = params[:user][:category]
    default = current_user.set_preference(category,department)
    if default.present?
      render status: :ok, json: default
    else
      render status: :unprocessable_entity, json: { message: default.errors.full_messages }
    end
  end

  def delete_preference
    if current_user.preference
      if current_user.preference.destroy
        render status: :ok, json: { message: "User Preference deleted"}
      else
        render status: :unprocessable_entity, json: { message: current_user.preference.errors.full_messages }
      end
    else
      render status: :not_found, json: { message: 'Record not match' }
    end
  end

  def hospital_doctors
    doctors = []
    doctor_role = Role.find_by_default_screen("DrHomeActivity")
    doctors = @current_medical_unit.users.where(role_id: doctor_role.id) if doctor_role.present?
    # doctors = doctors.where("first_name ilike (?)", '#{params[:query]}%')
    if params[:query].present?
      users = User.arel_table
      doctors = doctors.where(users[:first_name].matches("#{params[:query]}%"))
    end
    if doctors.present?
      render status: :ok, json: doctors
    else
      render status: :not_found, json: { message: 'Doctor not found' }
    end
  end

  def user_with_role
    role = Role.find_by(title: params[:role])
    if role.present?
      users = MedicalUnit.find(params[:medical_unit_id]).users.where(role_id: role.id)
      if users.present?
        render status: :ok, json: users
      else
        render status: :not_found, json: { message: 'No user found with this user role' }
      end
    else
      render status: :not_found, json: { message: 'No Role found with this user role' }
    end
  end
  def logged_in_doctors
    doctor_role = Role.find_by_default_screen("DrHomeActivity")
    login_doctors = User.logged_in_users.where("role_id = ? AND id != (?)", doctor_role.id, current_user.id) if doctor_role.present?
    if login_doctors.present?
      render status: :ok, json: login_doctors
    else
      render status: :not_found, json: { message: 'no logged in Doctor found' }
    end
  end

  def update
    @user = User.find_by_id(params[:id])
    if @user.update_attributes(user_params)
      render status: :ok, json: @user
    else
      render status: :unprocessable_entity, json: { message: @user.errors.full_messages }
    end
  end

  def show
    @user = User.find_by_id(params[:id])
    if @user.present?
      render status: :ok, json: @user
    else
      render status: :not_found, json: { message: 'User not found' }
    end
  end
  # byebug user.valid_password?(params[:user][:password])
  #   if @current_user.password == params[:user][:current_password]
  #     if params[:user][:new_password] == params[:user][:confirm_password]
  #       User.find_by_id(params[:user][:id]).update_attributes(password: 45454545)
  def change_password
    # puts 'I am debugger'
    # byebug
    user = User.find_by_id(params[:user][:id])
    if user.valid_password? params[:user][:current_password]
      if params[:user][:new_password] == params[:user][:confirm_password]
        user.update_attributes(password: params[:user][:new_password])
        render status: :ok,
               json: { message: 'Password has been changed successfully' }
      else
        render status: :not_found, json: { message: 'Confirmation Password not match' }
      end
    else
      render status: :not_found, json: { message: 'Password entered does not match with your current password' }
    end
  end
      
  # def change_password
  #   if @current_user.password == params[:user][:current_password]
  #     if params[:user][:new_password] == params[:user][:confirm_password]
  #       @current_user.password = params[:user][:new_password]
  #       @current_user.save
  #       render status: :ok,
  #              json: { message: 'Password has been changed successfully' }
  #     else
  #       render status: :not_found, json: { message: 'Confirmation Password not match' }
  #     end
  #   else
  #     render status: :not_found, json: { message: 'user not found' }
  #   end
  # end

  private

  def user_params # rubocop:disable Metrics/MethodLength
    params.require(:user).permit(:first_name, :last_name, :username, :title,
                                 :phone, :nic, :dob, :father_name, :father_nic,
                                 :mother_name, :mother_nic,:address1, :email,
                                 :encrypted_password, :reset_password_token,
                                 :reset_password_sent_at,:address2
                                )
  end
end
