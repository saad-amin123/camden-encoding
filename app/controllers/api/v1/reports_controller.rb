require 'jwt'

class Api::V1::ReportsController < ApplicationController
  before_action :verify_authenticity_token

  def generate_visits_stats
    statistics = Visit.generate_statistics(from_date, to_date, medical_unit_ids)
    date_range = Visit.generate_range(from_date, to_date)
    render json: { from: from_date, to: to_date, data: statistics, range: date_range }
  end

  # def generate_new_patient_stats
  #   statistics = Patient.generate_patient_statistics(from_date, to_date, medical_unit_ids)
  #   date_range = Patient.generate_range(from_date, to_date)
  #   render json: { from: from_date, to: to_date, data: statistics, range: date_range }
  # end

  def generate_order_performance_stats
    statistics = Order.generate_order_stats(from_date, to_date, medical_unit_ids, params[:order_type], params[:status])
    date_range = Order.generate_range(from_date, to_date)
    render json: { from: from_date, to: to_date, data: statistics, range: date_range }
  end

  def generate_not_applicable_order_stats
    statistics = Order.generate_nonapplicable_order_stats(from_date, to_date, medical_unit_ids, params[:order_type])
    date_range = Order.generate_range(from_date, to_date)
    render json: { from: from_date, to: to_date, data: statistics, range: date_range }
  end

  def generate_order_for_patients_stats
    statistics = Order.generate_ordered_patient_stats(from_date, to_date, medical_unit_ids, params[:order_type], params[:status])
    date_range = Order.generate_range(from_date, to_date)
    render json: { from: from_date, to: to_date, data: statistics, range: date_range }
  end

  def generate_patient_waiting_time_stats
    statistics = Visit.generate_patient_wait_stats(from_date, to_date, medical_unit_ids)
    date_range = Visit.generate_range(from_date, to_date)
    render json: { from: from_date, to: to_date, data: statistics, range: date_range }
  end

  def generate_staff_performance_stats
    statistics = MedicalUnit.performance_statistics(from_date, to_date, medical_unit_ids)
    render json: { from: from_date, to: to_date, data: statistics }
  end

  def generate_doctor_performance_stats
    statistics = PatientHistory.generate_dr_performance_stats(from_date, to_date, medical_unit_ids)
    render json: { from: from_date, to: to_date, data: statistics}
  end

  def generate_no_of_patient_per_doctor_stats
    statistics = PatientHistory.generate_no_of_patient_per_dr_stats(from_date, to_date, medical_unit_ids)
    render json: { from: from_date, to: to_date, data: statistics}
  end

  def generate_inventories_stats
    statistics = Visit.generate_statistics(from_date, to_date, medical_unit_ids)
    render json: { from: from_date, to: to_date, data: statistics }
  end

   def generate_inventory_stats
    statistics = Inventory.generate_medicine_statistics(from_date, to_date, medical_unit_ids)
    render json: { from: from_date, to: to_date, data: statistics }
  end

  def generate_vitals_stats
    statistics = Vital.generate_vital_statistics(from_date, to_date, params[:patient_ids])
    render json: { from: from_date, to: to_date, data: statistics }
  end

  def generate_total_registered_patients_count
    statistics = Patient.total_registered_patients(medical_unit_ids, params[:sub_category],  @current_user, from_date, to_date)
    render json: { data: { patients: statistics, data: [statistics.count],  query: [Date.current.strftime('%a %d %b %Y')], title: params[:sub_category] } }
  end

  def generate_total_registered_patients_count_category_wise
    statistics = Patient.total_registered_patients_count_category_wise(medical_unit_ids,params[:sub_category_type],params[:sub_category], from_date, to_date)
    if (params[:sub_category] == 'List of patients registered') || (params[:sub_category] == 'List of patients')
      render json: { patients: statistics[1] }
    else
      render json: { data: { patients: statistics[1],  data: statistics[0].values,  query: statistics[0].keys, title: params[:sub_category] } }
    end
  end

  def generate_total_number_patients_referred_to
    statistics = Patient.total_number_patients_referred_to(medical_unit_ids, params[:sub_category_type], from_date, to_date)
    render json: { data: { patients: statistics, data: [statistics.count],  query: [ params[:sub_category_type] ], title: params[:sub_category] } }
  end

  def generate_top_five_allergies_list
    allergies = Allergy.top_five_allergies_list(medical_unit_ids, from_date, to_date)
    render json: { data: allergies }
  end

  def generate_given_allergy_patients_list
    @patients = Allergy.given_allergy_patients_list(medical_unit_ids, params[:categoryType], params[:sub_category])
    render json: { patients: @patients }
  end

  def generate_top_five_patient_diagnosis_list
    diagnoses = PatientDiagnosis.top_five_patient_diagnoses_list(medical_unit_ids, from_date, to_date)
    render json: { data: diagnoses }
  end

  def generate_given_diagnosis_patients_list
    @patients = PatientDiagnosis.given_diagnosis_patients_list(medical_unit_ids, params[:categoryType])
    render json: { patients: @patients }
  end

  def generate_top_ten_given_ordered
    statistics = Order.top_ten_given_ordered(medical_unit_ids,params[:sub_category_type],params[:sub_category], from_date, to_date)
    keys = statistics.keys if statistics.present?
    statistics = statistics.values if keys.present?
    keys = BrandDrug.where('id in (?)', keys).pluck(:name) if params[:sub_category_type] == 'Medicine' && keys.present?
    render json: { data: { data: statistics,  query: keys, title: params[:sub_category] } }
  end

  def generate_total_order_count
    statistics = Order.total_order(@current_user,params[:sub_category_type],medical_unit_ids, from_date, to_date)
    render json: { data: { orders: statistics, data: [statistics.count],  query: [Date.current.strftime('%a %d %b %Y')], title: params[:sub_category] } }
  end

  def generate_total_number_of_patients_seen
    statistics = VisitUser.total_number_of_patients_seen(medical_unit_ids,params[:user_id], from_date, to_date)
    render json: { data: { patients: statistics, data: [statistics.count],  query: [Date.current.strftime('%a %d %b %Y')], title: params[:sub_category] } }
  end

  def generate_total_visit_count_category_wise
    statistics = Visit.total_visit_count_category_wise(medical_unit_ids,params[:sub_category_type],params[:sub_category], from_date, to_date)
    render json: { data: { data: statistics.values,  query: statistics.keys, title: params[:sub_category] } }
  end

  def generate_total_visit_count_ref_to
    @medical_units = MedicalUnit.fetch_assigned_units(@current_user)
    if (params[:medical_unit_ids].include? ",") || params[:medical_unit_ids] == 'undefined'
      medical_unit_ids = @medical_units.collect {|ind| ind[0]}
    else
      medical_unit_ids = params[:medical_unit_ids]
    end
    from = from_date.midnight
    to = to_date.midnight
    statistics2 = Visit.total_by_gender(medical_unit_ids,from,to)
    total_patient_hash = statistics2.values
    statistics7 = Visit.total_by_city(medical_unit_ids,from,to)
    statistics4 = Visit.total_by_users(medical_unit_ids,from,to)
    statistics1 = Visit.total_by_ref_to(medical_unit_ids,from,to)
    statistics5 = Visit.last_30_days_stats(medical_unit_ids)
    statistics6 = Visit.total_returning(medical_unit_ids,from,to)
    statistics8 = Visit.total_by_ref_dep(medical_unit_ids,from,to)
    statistics9 = ItemTransaction.top_ten_issued_medicine(medical_unit_ids,from,to)
    generic_names = Item.joins(:generic_item).where('items.id in (?)', statistics9.keys).order("items.id DESC").pluck(:name)
    statistics10 = Visit.most_busiest_hours(medical_unit_ids,from,to)
    date_range = Visit.generate_range(from_date=30.days.ago, to_date=Time.zone.now.beginning_of_day)
    render json: { from: params[:date], to: to_date,data: { data: statistics1.values, data2: statistics2, data5: statistics5[0],dates: statistics5[3],data7: statistics7.values,range: date_range,query: statistics1.keys, query2: statistics2,query7: statistics7.keys,total_patient_hash: total_patient_hash, title: params[:sub_category],statistics6: statistics6, data4: statistics4, ref_dept: statistics8, top_ten_issued_med_count: statistics9.values, top_ten_issued_med_names: generic_names, busy_hours: statistics10,  medical_units: @medical_units} }
  end

  # def generate_total_visit_count_ref_to
  #   params[:medical_unit_ids] = params[:medical_unit_ids].split(/,/) if params[:medical_unit_ids].include? ","
  #   ref_to = ['Emergency','Inpatient', 'Outpatient']
  #   statistics1 = Visit.total_visit_count_ref_to(params[:medical_unit_ids],params[:sub_category_type],params[:sub_category], params[:date], params[:date1], ref_to)
  #   statistics2 = Visit.user_performance_by_dept(params[:medical_unit_ids],params[:sub_category_type],params[:sub_category], params[:date], params[:date1], ref_to)
  #   statistics3 = Visit.total_patient_count(params[:medical_unit_ids],params[:sub_category_type],params[:sub_category], params[:date],params[:date1], ref_to)
  #   statistics4 = Visit.user_performance_by_dept_thirty_days(params[:medical_unit_ids],params[:sub_category_type],params[:sub_category], params[:date], params[:date1], ref_to)
  #   statistics5 = Visit.generate_statistics(from_date, to_date,params[:medical_unit_ids])
  #   statistics6 = Visit.total_patient_age(params[:medical_unit_ids],params[:sub_category_type],params[:sub_category], params[:date], params[:date1], ref_to)
  #   statistics7 = Visit.top_ten_city_list(params[:medical_unit_ids],params[:date], params[:date1],ref_to)
  #   date_range = Visit.generate_range(from_date, params[:date1])
  #   # medical_unit_ids_in_hash = MedicalUnit.fetch_assigned_units(@current_user)
  #   render json: { from: params[:date], to: to_date,data: { data: statistics1.values, data2: statistics2,data3: statistics3.values, data4: statistics4,data5: statistics5,data6: statistics6.values,data7: statistics7.values,range: date_range,query: statistics1.keys, query2: statistics2,query3: statistics3.keys,query4: statistics4,query6: statistics6.keys,query7: statistics7.keys,title: params[:sub_category] } }
  # end

  def generate_avg_patient_LOS_in_a_dept
    statistics = Order.avg_patient_LOS_in_a_dept(medical_unit_ids, from_date, to_date)
    render json: {data: { data: statistics, query: "Minutes", title: params[:sub_category] } }
  end

  def generate_avg_patient_LOS_in_a_given_dept
    statistics = Order.avg_patient_LOS_in_a_given_dept(medical_unit_ids,params[:sub_category_type],params[:role_id], from_date, to_date)
    render json: {data: { data: statistics, query: "Minutes", title: params[:sub_category] } }
  end

  def generate_total_medicine_list
    statistics = Inventory.total_medicine_list(medical_unit_ids)
    render json: { medicines: statistics[0], medicine_quantity: statistics[1] }
  end

  def generate_issued_medicine
    statistics = LineItem.medicine_order(medical_unit_ids, params[:sub_category_type], params[:sub_category], from_date, to_date)
    render json: { medicines: statistics[0], medicine_quantity: statistics[1] }
  end

  # def generate_patient_summary
  #   statistics = Patient.patient_summary(medical_unit_ids, from_date, to_date)
  #   render json: { data: { patient_summary_report: statistics, title: params[:sub_category] } }
  # end

  def generate_patient_summary
    statistics = Patient.patient_summary(medical_unit_ids, from_date, to_date)
    render json: statistics
  end

  def get_dashboard_url

    @medical_units = MedicalUnit.fetch_assigned_units(@current_user)
    @clinics = MedicalUnit.fetch_assigned_clinics(@current_user)
    if (params[:medical_unit_ids].include? ",") || params[:medical_unit_ids] == 'undefined'
      if @medical_units.present?
        if @medical_units.count > 1
          medical_unit_ids = 65
        else
          medical_unit_ids = @medical_units.first.first
        end
      end
    else
      medical_unit_ids = params[:medical_unit_ids]
    end

    metabase_site_url = "http://192.241.176.125:3000"
    metabase_secret_key = "7facb6c6d33ae35a94169e669736b0421102d97786ed45080089092ed152f38f"

    if params[:type] == 'live'
      if @medical_units.count > 1
        medical_unit_ids = 0
      else
        medical_unit_ids = @medical_units.first.first
      end
      payload = {
        :resource => {:dashboard => 1},
        :params => {
            "id" => medical_unit_ids
        }
        }
    elsif params[:type] == 'history'
      payload = {
        resource: { dashboard: 5 },
        params: {
            "start_date" => params[:start_date],
            "end_date" => params[:end_date]
        }
      };
    elsif params[:type] == 'stats'
      # medical_unit_ids = @medical_units.first.first
      payload = {
          :resource => {:dashboard => 37},
          :params => {
              "id" => medical_unit_ids,
              "start_date" => params[:start_date],
              "end_date" => params[:end_date]
          }
      }
    elsif params[:type] == 'clinics'
      if @clinics.present?
        @medical_units = @clinics
        medical_unit_ids = 88
        payload = {
            :resource => {:dashboard => 36},
            :params => {
                "start_date" => params[:start_date],
                "id" => medical_unit_ids,
                "end_date" => params[:end_date]
            }
        }
      end
    elsif params[:type] == 'diseases'
      payload = {
          :resource => {:dashboard => 38},
          :params => {
              "start_date" => params[:start_date],
              "medical_unit_id" => medical_unit_ids,
              "end_date" => params[:end_date]
          }
      }
    end
    
    token = JWT.encode payload, metabase_secret_key
    iframe_url = metabase_site_url + "/embed/dashboard/" + token + "#theme=night&bordered=false&titled=false"
    puts iframe_url
    render json: {url: iframe_url, medical_units: @medical_units}
  end

end
