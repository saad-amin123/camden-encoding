# class documention
class Api::V1::RegionsController < ApplicationController
  skip_before_filter :verify_authenticity_token
  respond_to :json

  def index
    @regions = Region.where('category = ?',
                            params[:category]).order('name ASC') if params[:category]
    @regions = Region.where('parent_id = ?',
                            params[:region_id]).order('name ASC') if params[:region_id]
    @regions = Region.all.order('name ASC') if params[:category] == 'all' || params[:region_id] == 'all' # rubocop:disable Metrics/LineLength
    if @regions.present?
      @regions = @regions
      render status: :ok, json: @regions
    else
      render status: :not_found, json: { message: 'Region not found' }
    end
  end
end
