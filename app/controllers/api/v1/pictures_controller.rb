# class documention
class Api::V1::PicturesController < ApplicationController
  before_action :verify_authenticity_token, only: [:create]
  before_action :verify_device, only: [:create]
  before_action :fetch_visit, only: [:create]
  before_action :verify_device_role_medical_unit, only: [:create] if Rails.env != "test"

  def create
    if params[:picture][:imageable_id].present?
      @picture = Picture.new(picture_params)
    else
      @picture = @visit.pictures.new(file: params[:picture])
    end
    @picture.user = @current_user
    @picture.visit = @visit
    if @picture.save
      render status: :ok, json: @picture
    else
      render status: :unprocessable_entity,
             json: { message: @picture.errors.full_messages }
    end
  end

  private

  def picture_params
    params.require(:picture).permit(:file,:imageable_id,:imageable_type)
  end
end
