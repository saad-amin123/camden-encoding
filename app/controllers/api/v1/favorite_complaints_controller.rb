# class documention
class Api::V1::FavoriteComplaintsController < ApplicationController
  before_action :verify_authenticity_token, only: [:add, :remove]
  before_action :verify_device, only: [:add]
  before_action :fetch_complaint, only: [:add, :remove]
  before_action :verify_device_role_medical_unit, only: [:add] if Rails.env != "test"
  respond_to :json

  def add
    @favorite_complaints = FavoriteComplaint.new(user_id: current_user.id, complaint_id: @complaint.id)
    @favorite_complaints.user = @current_user
    if @favorite_complaints.save
      render status: :ok, json: @favorite_complaints
    else
      render status: :unprocessable_entity,
             json: { message: @favorite_complaints.errors.full_messages }
    end
  end

  def remove
    @favorite_complaints = @current_user.favorite_complaints.where(complaint_id: params[:complaint_id])
    if @favorite_complaints.present?
      @favorite_complaints.destroy_all
      render status: :ok, json: @complaint.reload
    else
      render status: :unprocessable_entity, json: { message: 'Invalid Request' }
    end
  end
end
