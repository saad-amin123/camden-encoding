# class documention
class Api::V1::InventoriesController < ApplicationController
  before_action :verify_authenticity_token, only: [:create,:search,:index,:update_inventories]
  before_action :verify_device, only: [:create,:search,:index,:update_inventories]
  before_action :fetch_medical_unit, only: [:create,:search,:index,:update_inventories]
  before_action :verify_device_role_medical_unit, only: [:create] if Rails.env != "test"
  # before_action :verify_store, only: [:search]
  serialization_scope :current_inventory
  respond_to :json
  def create
    begin
      inventories = []
      store_id = params[:store_id] || nil
      department_id = params[:department_id] || nil
      params.require(:line_items).each do |line_item|
        brand_drug_id = line_item[:brand_drug_id]
        if line_item[:transaction_type] == "issue"
          inventory = Inventory.where(medical_unit_id: params[:medical_unit_id], brand_drug_id: brand_drug_id, store_id: store_id, department_id: nil).first_or_create
        else
          inventory = Inventory.where(medical_unit_id: params[:medical_unit_id], brand_drug_id: brand_drug_id, store_id: store_id, department_id: department_id).first_or_create
        end
        lineitem = inventory.line_items.build(line_item.permit!)
        lineitem.user_id = @current_user.id
        lineitem.device_id = @current_device.id
        lineitem.medical_unit_id = @current_medical_unit.id
        lineitem.save
        inventories << inventory
  
      end
      render status: :ok, json: inventories      
    rescue Exception => e
      render status: :unprocessable_entity, json: {message: "#{e.message}"}
    end
  end
  def update_inventories
    begin
      inventories = []
      params.require(:line_items).each do |line_item|
        inventory = Inventory.find_by_id(line_item[:inventory_id])
        inventory.quantity -= line_item[:usage]
        inventory.save 
        inventories << inventory
      end
      render status: :ok, json: inventories      
    rescue Exception => e
      render status: :unprocessable_entity, json: {message: "#{e.message}"}
    end
  end
  def search
    store_id = params[:store_id] || nil
    department_id = params[:department_id] || nil
    inventory = @current_medical_unit.inventories.where(brand_drug_id: params[:brand_drug_id],store_id: store_id,department_id: department_id).first
    if inventory.present?
      render status: :ok, json: inventory
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end
  def index
    department_id = params[:department_id] || nil
    inventories = @current_medical_unit.inventories.where('department_id = ? AND quantity > ?', department_id, 0)
    if inventories.present?
      render status: :ok, json: inventories
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  private

  def line_item_params
    params.permit!
  end
end

