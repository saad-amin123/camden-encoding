# class documention
class Api::V1::OrderItemsController < ApplicationController
  before_action :verify_authenticity_token, only: [:create,:update,:destroy]
  serialization_scope :fetch_medical_unit
  before_action :order, only: [:create,:update,:destroy]
  before_action :order_item, only: [:update,:destroy]
  respond_to :json

  def create
    order_item = order.order_items.new(order_item_params)
    if order_item.save
      render status: :ok, json: order     
    else
      render status: :unprocessable_entity, json: { message: order_item.errors.full_messages }
    end
  end

  def update
    if order_item.update_attributes(order_item_params)
      render status: :ok, json: order     
    else
      render status: :unprocessable_entity, json: { message: order_item.errors.full_messages }
    end
  end

  def destroy
    if order_item.destroy
      render status: :ok, json: { message: 'OrderItem destroyed successfully' }      
    else
      render status: :unprocessable_entity, json: { message: order_item.errors.full_messages }
    end
  end

  private

  def order
    Order.find(params[:order_id])
  end
  
  def order_item
    order.order_items.find(params[:id])
  end

  def order_item_params
    params.require(:order_item).permit(:order_id,:brand_drug_id,:quantity_prescribed,
            :frequency,:frequency_unit,:issued_brand_drug_id,:notes,:duration,
            :duration_unit,:type)    
  end
end

