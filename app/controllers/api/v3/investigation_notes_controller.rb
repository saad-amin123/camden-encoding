class Api::V3::InvestigationNotesController < ApplicationController
  before_action :verify_authenticity_token, only: [:create,:index]
  before_action :fetch_medical_unit, only: [:create,:index]
  before_action :fetch_patient, only: [:create,:index]
  # before_action :fetch_visit, only: [:create]
  #before_action :fetch_admission, only: [:create]
  
  respond_to :json

  def create
      @investigation_note = InvestigationNote.new(investigation_note_params)
      @investigation_note.medical_unit = @current_medical_unit
      @investigation_note.patient = @patient
      @admission = Admission.find_by_id(params[:admission_id])
      @investigation_note.visit_id = @admission.present? ? @admission.visit_id : @patient.visits.last.id
      @investigation_note.admission = @admission
      @investigation_note.user = @current_user
      if @investigation_note.save
        render status: :ok, json: @investigation_note
      else
        render status: :unprocessable_entity, json: { message: @investigation_note.errors.full_messages }
      end
  end

  def investigation_note_params
    params.require(:investigation_note).permit(:note,:investigation_type)
  end

  def index
  	@investigation_note_user_wise = InvestigationNote.includes(:user).where(patient_id: @patient.id)
  	  if @investigation_note_user_wise.present?
        render status: :ok, json: @investigation_note_user_wise
      else
        render status: :not_found, json: { message: 'Record not found' }
      end
  end

end
