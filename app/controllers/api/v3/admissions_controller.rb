class Api::V3::AdmissionsController < ApplicationController
  before_action :verify_authenticity_token, only: [:create, :index]
  before_action :fetch_medical_unit, only: [:create, :index]
  # before_action :fetch_visit, only: [:create]
  before_action :fetch_ward, only: [:create, :index]
  before_action :fetch_patient, only: [:create]
  # before_action :fetch_department, only: [:create]

  respond_to :json

  def create
    if @patient.admissions.last.present? && @patient.admissions.last.status == 'Admitted'
      render status: :unprocessable_entity, json: { message: 'You cannot admit a patient who is already admitted. Patient with MRN: <b>'+@patient.mrn+'</b> is already admitted in Ward: <b>'+@patient.admissions.last.ward.title+'</b>' }
    else
      @admission = Admission.new(admission_params)
      @admission.generate_admission_number
      @admission.user = @current_user
      @admission.patient = @patient
      @admission.medical_unit = @current_medical_unit
      @admission.ward = @ward
      # @admission.visit_id = params[:visit_id] if params[:visit_id]
      @admission.visit = @patient.visits.last if @patient.visits.present?
      @admission.referred_from = params[:department_id] if params[:department_id]
      @admission.doctor_id = params[:doctor_id]
      if @admission.save
        render status: :ok, json: @admission
      else
        render status: :unprocessable_entity, json: { message: @admission.errors.full_messages }
      end
    end
  end
  def admission_params
    params.require(:admission).permit(:attendent_name,:attendent_mobile,:attendent_address,
                                      :attendent_nic, :attendent_relationship,:status)
  end

end
