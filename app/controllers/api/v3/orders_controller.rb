# class documention
class Api::V3::OrdersController < ApplicationController
    require 'time'
  before_action :verify_authenticity_token, only: [:update_lab_test_status,:fetch_total_lab_tests]
  before_action :verify_device, only: [:update_lab_test_status]
  # before_action :fetch_medical_unit, only: [:update_lab_test_status]
  respond_to :json

  def index
    @orders = patient_ids = []
    patient_ids =  Patient.where("mrn = ?", params[:mrn]).pluck(:id).uniq
    @orders = Order.where('patient_id in (?) AND case_number =? AND order_type =?', patient_ids, params[:case_no], params[:order_type])
    if @orders.present?
      render status: :ok, json: @orders
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  def update_lab_test_status
    dept_ids = @current_user.departments.first.sub_departments.pluck(:id)
    dept_ids << @current_user.departments.first.id
    order = Order.joins(:lab_order_details).where('orders.id =? AND medical_unit_id = ? AND lab_order_details.lab_investigation_id IS NOT NULL AND lab_order_details.department_id IN (?)', params[:order_id], params[:medical_unit_id],dept_ids).uniq.first
    if params[:status] == 'Collected'
      # order.update_attributes(status: 'Collected')
      order.lab_order_details.where(department_id: dept_ids).update_all(sample_collected: true, status: params[:status], sample_collector_id: current_user.id)
    elsif params[:status] == 'Incomplete'
      # order.update_attributes(status: 'Collected')
      # order.lab_order_details.where(department_id: dept_ids).update_all(sample_collected: true, status: params[:status], sample_collector_id: current_user.id)
      lod = order.lab_order_details.where(department_id: dept_ids)
      lod.each_with_index do |item, index|
        if item.status == 'Open'
          lod[index].update_attributes(sample_collected: true, status: 'Collected', sample_collector_id: current_user.id)
        end
      end
    elsif params[:lab_order_detail_ids].present?
      params[:lab_order_detail_ids].each do |lab_order|
        LabOrderDetail.find_by_id(lab_order).update_attributes(sample_collected: true, status: params[:status], pathologist_id: current_user.id)
      end
    end
    Order.order_status(order)
    if order.present?
      render status: :ok, json: order.lab_order_details.where(department_id: dept_ids)    
    else
      render status: :unprocessable_entity,
             json: { message: @complaint.errors.full_messages }
    end
  end
  
  def get_order_by_mrn
    patient_ids = []
    patient_ids =  Patient.where("mrn = ?", params[:mrn]).pluck(:id).uniq
    orders = Order.where('patient_id in (?) AND order_type =?', patient_ids, params[:order_type])
    if orders.present?
      render status: :ok, json: { order: {patient: orders.first.patient, lab_order_detail: orders.map{|o| o.lab_order_details}}}
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  def generate_order_pdf_attachment
    @order = Order.find_by_id(params[:id])
    @lab_order_details = LabOrderDetail.where(id: params[:lab_order_detail_id].split(','))
    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => "generate_order_pdf_attachment", :layout => 'pdf.html.haml', :disposition => 'attachment'
      end
    end
  end

  def generate_order_pdf
    @order = Order.find_by_id(params[:id])
    @lab_order_details = LabOrderDetail.where(id: params[:lab_order_detail_id].split(','))
    # @patient_age = age(@order.patient.birth_year, @order.patient.birth_month,
    #                @order.patient.birth_day).split[0...4].join(' ')
    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => "generate_order_pdf", :layout => 'pdf.html.haml'
      end
    end
  end

  def lab_payment_receipt
    @order = Order.find_by_id(params[:id])
    @patient_age = age(@order.patient.birth_year, @order.patient.birth_month,
                   @order.patient.birth_day).split[0...4].join(' ')
    pdf = render :pdf => "generate_order_pdf", :layout => 'pdf.html.haml'
    save_path = Rails.root.join('public','filename.pdf')
    File.open(save_path, 'wb') do |file|
      file << pdf
    end
    system("lpr", "public/filename.pdf")
  end

  def fetch_total_lab_tests
    lab_count = $redis.get('all_lab_tests_count')
    if lab_count.present?
      render status: :ok, json: lab_count
    else
      render status: :not_found, json: { message: "Record not found" }
    end
  end

  def lab_result_slip
    @count = 1
    @users_prepared = []
    @users_verified = []

    @duplicate = params[:print_type].present?
    if params[:id] == 'undefined'
      @lab_order_details = LabOrderDetail.where(id: params[:lab_order_detail_id].split(','))
      # @order_log = OrderLog.where(order_id: @lab_order_details.first.order_id, status: "Unverified").first
      @lab_order_details.each do |lab_order_detail|
        @users_prepared <<  User.find_by_id(lab_order_detail.lab_technician_id)
        @users_verified <<  User.find_by_id(lab_order_detail.pathologist_id)
      end
    else
      @order = Order.find_by_id(params[:id])
    end

    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Lab Report" , page_size: 'A4', 
        margin: {top: '83mm', bottom: '25mm', left: '8mm', right: '8mm'},
        header: {
                  content: render_to_string(template: 'api/v3/orders/header.pdf.haml'),
                   spacing: 2
                },
       footer: {
                  content: render_to_string(template: 'api/v3/orders/footer.pdf.haml'),
                  #:right => '[page] of [topage]'
                }
      end
    end
  end

  # def age(y,m,d)
  #   now = Time.now.utc.to_date
  #   now.year - y - ((now.month > m || (now.month == m && now.day >= d)) ? 0 : 1)
  # end
  
  def humanize secs
    [ 
      [60, :seconds], 
      [60, :minutes], 
      [24, :hours], 
      [365, :days], 
      [150, :years]
    ].map do |count, name|
      if secs > 0
        secs, n = secs.divmod(count)
        "#{n.to_i} #{name}"
      end
    end.compact.reverse.join(' ')
  end
  def age(y,m,d)
    distance = Time.new - Time.parse(y.to_s+"-"+m.to_s+"-"+d.to_s)
    humanize(distance)
  end
end

