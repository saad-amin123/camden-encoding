class Api::V3::LabResultsController < ApplicationController
  include ApplicationHelper
  before_action :verify_authenticity_token
  before_action :verify_patient, only: [:result_parameters, :create, :update_result_parameters]
  before_action :check_lab_result, :check_machine_code, :check_order, :check_patient, only: [:machine_result_parameters]
  # before_action :set_lab_investigation, only: [:create]


  def result_parameters
    @lab_results = []
    params[:lab_order_detail_ids].split(',').each_with_index do |lab_order_detail_id, index|
      results = LabResult.where(
        lab_order_detail_id: lab_order_detail_id, 
        patient_id: @patient.id, 
        medical_unit_id: params[:medical_unit_id]
      )
      @lab_results << results.first.lab_result_parameters if results.present? 
      # @lab_results.first[index].profile_name = 'gdgdkgdgd'
    end
    if @lab_results.present?
      render status: :ok, json: @lab_results
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  def create
    @lab_result = []
    params[:lab_order_details].each_with_index do |lab_order_detail, index|
      @lab_investigation = LabInvestigation.find_by(profile_name: lab_order_detail[:test])
      @lab_result << @patient.lab_results.create(
        medical_unit_id: params[:medical_unit_id], 
        lab_investigation_id: @lab_investigation.id,
        lab_order_detail_id: lab_order_detail[:id]
      )
      params[:lab_results].each do |result_parameter|
        if result_parameter[:profile_name] == @lab_investigation.profile_name
          lrp = @lab_result[index].lab_result_parameters.new
          lrp.parameter_title = result_parameter[:parameter_title]
          lrp.result1         = result_parameter[:result1]
          lrp.result2         = result_parameter[:result2]
          lrp.uom             = result_parameter[:uom]
          lrp.range_title     = result_parameter[:range_title]
          lrp.out_of_range    = lrp.is_out_of_range?(result_parameter[:range_title])
          lrp.save!
        end
      end
      @lab_result[index].lab_order_detail.update_attributes(status: 'Unverified', lab_technician_id: current_user.id)
    end
    Order.order_status(@lab_result.first.lab_order_detail.order)
    if @lab_result
      render status: :ok, json: @lab_result
    else
      render status: :unprocessable_entity, json: { message: @lab_result.errors.full_messages }
    end
  end
  # def update_result_parameters
  #   @lab_result = []
  #   params[:lab_order_details].each_with_index do |lab_order_detail, index|
  #     @lab_investigation = LabInvestigation.find_by(profile_name: lab_order_detail[:test])
  #     @lab_result << LabResult.where(
  #       medical_unit_id: params[:medical_unit_id], 
  #       lab_investigation_id: @lab_investigation.id,
  #       lab_order_detail_id: lab_order_detail[:id]
  #     ) 
  #     params[:lab_results].each do |result_parameter|
  #       if result_parameter[:profile_name] == @lab_investigation.profile_name
  #         LabResultParameter.find_by_id(result_parameter[:id]).update_attributes(result1: result_parameter[:result1])
  #       end
  #     end

  #     @lab_result[index].lab_order_detail.update_attributes(status: 'Unverified')
  #   end
  #   if @lab_result
  #     render status: :ok, json: @lab_result
  #   else
  #     render status: :unprocessable_entity, json: { message: @lab_result.errors.full_messages }
  #   end
  # end
  def update_result_parameters
    dept_ids = @current_user.departments.first.sub_departments.pluck(:id)
    dept_ids << @current_user.departments.first.id
    params[:lab_results].each do |result_parameter|
      @lab_result = LabResult.find_by_id(result_parameter[:lab_result_id])
      LabResultParameter.find_by_id(result_parameter[:id]).update_attributes(result1: result_parameter[:result1])
      # @lab_result.lab_order_detail.update_attributes(status: 'Unverified')
    end
    if @current_user.roles.pluck(:title).include? "Lab"
      LabOrderDetail.where(order_id: @lab_result.lab_order_detail.order.id, department_id: dept_ids).update_all(status: 'Unverified')
    elsif @current_user.roles.pluck(:title).include? "Pathologist"
      LabOrderDetail.where(id: params[:lab_ord_ids], department_id: dept_ids).update_all(status: 'Verified')
    end
    Order.order_status(@lab_result.lab_order_detail.order)
    if @lab_result
      render status: :ok, json: @lab_result
    else
      render status: :unprocessable_entity, json: { message: @lab_result.errors.full_messages }
    end
  end

  def split_result(result)
    result_parameter = result.tr('^A-Za-z0-9-.', '')
    if result_parameter.blank?
      render status: :unprocessable_entity, json: { message: "Wrong format in result parameters" }
    else
      return result_parameter
    end
  end

  def machine_result_parameters
    @lab_result = []
    machine_code = params[:machine_code]
    @lab_assessment_data = []
    @data = []
    patient_age = @patient.age.split(' ')[0].to_i * 365
    @order.lab_order_details.each_with_index do |lab_order_detail, index|
      if lab_order_detail.status != 'Unverified'
        lab_order_detail.update_attributes(sample_collected: true, sample_collector_id: current_user.id)
        @assessments = lab_order_detail.lab_investigation.lab_assessments if lab_order_detail.lab_investigation.present?
        if @assessments.present?
          @lab_result << @patient.lab_results.create(
              medical_unit_id:  @medical_unit_id,
              lab_investigation_id: lab_order_detail.lab_investigation.id,
              lab_order_detail_id: lab_order_detail[:id]
          )
          begin
            @lab_results_param.to_s.split(',').each do |result|
              @assessments.each do |assessment|
                @machine = Machine.find_by(lab_assessment_id: assessment.id, medical_unit_id: @medical_unit_id, machine_code: params[:machine_code])
                if @machine.present?
                  if assessment.lab_access_ranges.present?
                    lab_access_range = assessment.lab_access_ranges.find_by('medical_unit_id =? AND gender =? AND age_start <=? AND age_end >=?', @medical_unit_id, @patient.gender, patient_age,patient_age)
                  else
                    lab_access_range.range_title = ''
                  end
                  if @machine.machine_parameter_code == split_result(result.split('=>')[0])
                    lrp = @lab_result.first.lab_result_parameters.new
                    lrp.parameter_title = assessment.title
                    lrp.result1         = split_result(result.split('=>')[1])
                    lrp.uom             = assessment.uom
                    lrp.range_title     = lab_access_range.range_title
                    lrp.save!
                  end
                end
              end
            end
          rescue Exception => e
            render status: :unprocessable_entity, json: { message: "Wrong format in result parameters" }
          end
          @lab_result.first.lab_order_detail.update_attributes(status: 'Unverified', lab_technician_id: current_user.id)
        end
      end
    end
    
    if @lab_result.present?
      Order.order_status(@lab_result.first.lab_order_detail.order)
      render status: :ok, json: { message: "Result saved successfully!" }
    else
      render status: :unprocessable_entity, json: { message: "You have already added result of current order" }
    end
  end

  def check_lab_result
    begin
      @lab_results_param = eval(params[:lab_results])
      if @lab_results_param.blank?
        render status: :not_found, json: { message: 'Lab result not found' }
      end
    rescue Exception => e
      render status: :unprocessable_entity, json: { message: "Wrong format in result parameters" }
    end
  end

  def check_order
    order_id = params[:order_id]
    @date =  create_lab_order_sequence(order_id)
    @lab_order_sequence_number = format('%03d',@medical_unit_id).to_s + @date + order_id[2..5]
    @order = Order.where(lab_order_sequence_number: @lab_order_sequence_number).first
    if @order.blank?
      render status: :not_found, json: { message: 'Enter a valid order id' }
    end
  end

  def check_machine_code
    machine_info = Machine.find_by(machine_code: params[:machine_code])
    if machine_info.blank?
      render status: :not_found, json: { message: 'Invalid machine code' }
    else
      @medical_unit_id = machine_info.medical_unit_id
    end
  end

  def check_patient
    @patient = Patient.find(@order.patient_id)
    if @patient.blank?
      render status: :not_found, json: { message: 'Patient not found' }
    end
  end

  private

  def set_lab_investigation
    @lab_investigation = LabInvestigation.find_by(profile_name: params[:profile_name])
  end
end