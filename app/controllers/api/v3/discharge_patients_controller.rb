class Api::V3::DischargePatientsController < ApplicationController
  # before_action :verify_authenticity_token, only: [:create, :index, :update, :get_patients_by_type,:patient_activity, :csv_data, :unknown_patients]
  before_action :verify_authenticity_token
  before_action :fetch_medical_unit
  before_action :fetch_patient
  before_action :fetch_bed
  before_action :fetch_admission

  respond_to :json

  def create
    @discharge_patient = DischargePatient.new(discharge_patients_params)
    @discharge_patient.medical_unit = @current_medical_unit
    @discharge_patient.patient = @patient
    @discharge_patient.bed = @bed
    @discharge_patient.visit_id = @admission.visit_id
    @discharge_patient.admission = @admission

    if @discharge_patient.save
      render status: :ok, json: @discharge_patient
    else
      render status: :unprocessable_entity, json: { message: @discharge_patient.errors.full_messages }
    end
  end

  def discharge_patients_params
    params.require(:discharge_patient).permit(:treatment_plan,:medications,
                                      :follow_up,:discharge_type,:comments)
  end

end