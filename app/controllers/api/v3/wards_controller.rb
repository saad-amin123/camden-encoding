class Api::V3::WardsController < ApplicationController
  # before_action :verify_authenticity_token, only: [:create, :index, :update, :get_patients_by_type,:patient_activity, :csv_data, :unknown_patients]
  before_action :verify_authenticity_token, only: [:index, :get_hospital_wards,:fetchWardById]
  before_action :fetch_medical_unit, only: [:index, :get_hospital_wards,:fetchWardById]
  
  respond_to :json

  def index
    wards = @current_user.wards.order(:title)
    if wards.present?
      render status: :ok, json: wards
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  def get_hospital_wards
    wards = Ward.where(medical_unit_id: params[:medical_unit_id]).order(:title)
    if wards.present?
      render status: :ok, json: wards
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end
  def fetchWardById
    wards = Ward.where(id: params[:ward_id]).order(:title)
    if wards.present?
      render status: :ok, json: wards
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end
end