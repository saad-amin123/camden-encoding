# class documention
class Api::V3::ItemStocksController < ApplicationController
  before_action :verify_authenticity_token, only: [:create, :index, :fetch_items_Dept_wise, :issue_item, :get_stock_item_from_store, :department_issue_item]
  before_action :verify_device, only: [:create, :index, :get_stock_item_from_store]
  before_action :fetch_medical_unit, only: [:create, :index, :fetch_items_Dept_wise, :issue_item, :get_stock_item_from_store, :department_issue_item]
  before_action :fetch_department, only: [:create]
  before_action :verify_store, only: [:create, :issue_item, :department_issue_item, :fetch_item_by_store, :get_stock_item_from_store, :fetch_exiting_stock_in_store]
  before_action :verify_item, only: [:create, :get_stock_item_from_store]
  before_action :fetch_patient, only: [:issue_item]
  before_action :verify_device_role_medical_unit, only: [:create] if Rails.env != "test"

  respond_to :json

  def create
    @item_stock = verify_item_present_in_stock_or_not
    if @item_stock.nil?
      @item_stock = ItemStock.new(item_stock_params)
      @item_stock.item = @item
      @item_stock.user = @current_user
      @item_stock.medical_unit = @current_medical_unit
      @item_stock.department = @store.department
      @item_stock.store = @store
      if @item_stock.save
        @item_stock.create_item_transactions(@item_stock, params[:item_stock], @patient=nil, params[:transaction_type], @current_user, receipt_number=params[:receipt_number], doctor_id=nil,on_hand_quantity=nil )
        render status: :ok, json: @item_stock
      else
        render status: :unprocessable_entity, json: { message: @item_stock.errors.full_messages }
      end
    else
      update
    end
  end

  def update
    if @item_stock.present?
      on_hand_quantity = @item_stock.quantity_in_hand
      @item_stock.update_attributes(item_stock_params)
      @item_stock.create_item_transactions(@item_stock, params[:item_stock], @patient=nil, params[:transaction_type], @current_user, receipt_number = params[:receipt_number], doctor_id=nil,on_hand_quantity )
      render status: :ok, json: @item_stock
    else
      render status: :unprocessable_entity, json: { message: @item_stock.errors.full_messages }
    end
  end

  def issue_item
    params[:item_stock].each do |item_stock|
      if item_stock[:id]
        @item_stock = ItemStock.find(item_stock[:id])
      elsif @patient.medicine_requests && item_stock[:id].nil?
        @patient.medicine_requests.where("status = 'Open' AND product_name =?", item_stock[:title]).first.update_attributes(status: "Not_Issued")
      end
      if @item_stock
        on_hand_quantity = @item_stock.quantity_in_hand
        if item_stock[:quantity].to_f >= 0
          update_quantity_current = @item_stock.update_quantity(item_stock[:quantity])
          @item_stock.update_attributes(quantity_in_hand: update_quantity_current.to_s)
          @item_stock.create_item_transactions(@item_stock, item_stock, @patient, params[:transaction_type], @current_user, params[:receipt_number], params[:doctor_id],on_hand_quantity)
          @medicine = @patient.medicine_requests.where(item_id: @item_stock.item_id, status: "Open")
          if @medicine.present?
            @medicine.first.update_attributes(status: "Issued")
          end
        end
      end
    end
    render status: :ok, json: @item_stock
  end

  def department_issue_item
    params[:item_stock].each do |item_stock|
      @item_stock = ItemStock.find(item_stock[:id])
      on_hand_quantity = @item_stock.quantity_in_hand
      if item_stock[:quantity].to_f > 0
        update_quantity_current = @item_stock.update_quantity(item_stock[:quantity])
        @item_stock.update_attributes(quantity_in_hand: update_quantity_current.to_s)
        @item_stock.create_department_item_transactions(@item_stock, item_stock, params[:transaction_type], @current_user, params[:receipt_number], params[:doctor_id],on_hand_quantity, params[:transaction] )
      end
    end
    render status: :ok, json: @item_stock
  end

  def get_stock_item_from_store
    @item_stock =  ItemStock.where("item_id = ? AND store_id =? AND item_stocks.department_id =?", params[:item_id],params[:store_id], @store.department_id)
    if @item_stock.present?
      render status: :ok, json: @item_stock
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  def fetch_item_by_store
    @item_stock =  ItemStock.joins(:item).where("product_name ILIKE ? AND status =? AND store_id =? AND item_stocks.department_id =?", "#{params[:query]}%",'Open', params[:store_id], @store.department_id)
    if @item_stock.present?
      render status: :ok, json: @item_stock
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  def fetch_items_by_store
    @item_stock = []
    params[:query].split(',').each do |query|
      @item_stock <<  ItemStock.joins(:item).where("product_name ILIKE ? AND store_id =?", "#{query}%", params[:store_id])
    end
    if @item_stock.present?
      render status: :ok, json: { item_stock: @item_stock }
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  def fetch_exiting_stock_in_store
    @item_stock =  ItemStock.joins(:item).where("quantity_in_hand != '0.0' AND product_name ILIKE ? AND status =? AND store_id =? AND item_stocks.department_id =?", "#{params[:query]}%",'Open', params[:store_id], @store.department_id)
    if @item_stock.present?
      render status: :ok, json: @item_stock
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end


  private

  def verify_item_present_in_stock_or_not
    @item_stock = ItemStock.where(item_id: params[:item_stock][:item_id], store_id: params[:store_id],
                                  department_id: params[:department_id]).first
  end

  def item_stock_params
    params.require(:item_stock).permit(:quantity_in_hand)
  end

end

