class Api::V3::BaysController < ApplicationController
  # before_action :verify_authenticity_token, only: [:create, :index, :update, :get_patients_by_type,:patient_activity, :csv_data, :unknown_patients]
  before_action :verify_authenticity_token
  before_action :fetch_medical_unit
  before_action :fetch_ward
  respond_to :json

  def index
    if @ward.present?
      bays = @ward.bays.order(:title)
      if bays.present?
        render status: :ok, json: bays
      else
        render status: :not_found, json: { message: 'No Bays in this ward' }  
      end
    else
      render status: :not_found, json: { message: 'No Wards assigned to this user' }
    end
  end
end