class Api::V3::ReportsController < ApplicationController
  before_action :verify_authenticity_token
  before_action :set_hospital, :set_store
  respond_to :json

  def medicine_list
    report_service = PharmacyReportService.new(
      @current_user.id, @store.try(:id), @hospital.try(:id))
    csv = report_service.medicine_list
    send_data csv.encode('Windows-1251'), type: 'text/csv; charset=windows-1251; header=present', disposition: "attachment; filename=medicine_list.csv"
  end

  def new_medicine_list
    report_service = Reports::Pharmacy.new(params, @hospital)
    csv = report_service.list
    send_data csv.encode('Windows-1251'), type: 'text/csv; charset=windows-1251; header=present', disposition: "attachment; filename=list.csv"
  end

  def prescriptions
    report_service = PharmacyReportService.new(
      @current_user.id, @store.try(:id), @hospital.try(:id), params[:start_date], params[:end_date])
    csv = report_service.prescriptions
    send_data csv.encode('Windows-1251'), type: 'text/csv; charset=windows-1251; header=present', disposition: "attachment; filename=prescriptions.csv"
  end

  def new_prescriptions
    report_service = Reports::Pharmacy.new(params, @hospital)
    csv = report_service.transaction_report
    send_data csv.encode('Windows-1251'), type: 'text/csv; charset=windows-1251; header=present', disposition: "attachment; filename=transaction_report.csv"
  end

  def issue_stock
    report_service = PharmacyReportService.new(
      @current_user.id, @store.try(:id), @hospital.try(:id), params[:start_date], params[:end_date])
    csv = report_service.issue_stock
    send_data csv.encode('Windows-1251'), type: 'text/csv; charset=windows-1251; header=present', disposition: "attachment; filename=issue_stock.csv"
  end

  def current_stock
    report_service = PharmacyReportService.new(
      @current_user.id, @store.try(:id), @hospital.try(:id))
    csv = report_service.current_stock
    send_data csv.encode('Windows-1251'), type: 'text/csv; charset=windows-1251; header=present', disposition: "attachment; filename=current_stock.csv"
  end

  def stock_balance
    report_service = PharmacyReportService.new(
      @current_user.id, @store.try(:id), @hospital.try(:id), params[:start_date], params[:end_date])
    csv = report_service.stock_balance
    send_data csv.encode('Windows-1251'), type: 'text/csv; charset=windows-1251; header=present', disposition: "attachment; filename=stock_balance.csv"
  end

  def new_stock_balance
    report_service = Reports::Pharmacy.new(params, @hospital)
    csv = report_service.stock_report
    send_data csv.encode('Windows-1251'), type: 'text/csv; charset=windows-1251; header=present', disposition: "attachment; filename=stock_report.csv"
  end

  def user_patients
    report_service = PatientsReportService.new(
      @current_user.id, @store.try(:id), @hospital.try(:id), 
      params[:start_date], params[:end_date]
    )
    csv = report_service.user_patients_report
    send_data csv.encode('Windows-1251'), type: 'text/csv; charset=windows-1251; header=present', disposition: "attachment; filename=user_patients.csv"
  end

  def patient_registration
    report_service = Reports::Registration.new(params, @hospital)
    csv = report_service.transaction_report
    send_data csv.encode("utf-8"), type: 'text/csv; header=present', disposition: "attachment; filename=patient_registrations.csv"
  end

  def lab_report
    report_service = Reports::Lab.new(params, @hospital)
    csv = report_service.lab_report
    send_data csv.encode('Windows-1251'), type: 'text/csv; charset=windows-1251; header=present', disposition: "attachment; filename=lab_report.csv"
  end

  private

  def set_store
    @store = @current_user.stores.first
  end

  def set_hospital
    @hospital = @current_user.medical_units.first
  end
end
