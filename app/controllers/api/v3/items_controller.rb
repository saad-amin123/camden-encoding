# class documention
class Api::V3::ItemsController < ApplicationController
  before_action :verify_authenticity_token, only: [:create, :index, :fetch_items_Dept_wise, :fetch_total_medicine_dispensed]
  before_action :verify_device, only: [:create, :index]
  before_action :fetch_medical_unit, only: [:create, :index, :fetch_items_Dept_wise]
  # before_action :fetch_department, only: [:create]
  before_action :verify_device_role_medical_unit, only: [:create] if Rails.env != "test"

  respond_to :json

  def index
    @item =  Item.where("generic_item_id = ? AND item_code =? AND status =?", params[:item_id],params[:item_code], params[:status])
    if @item.present?
      render status: :ok, json: @item
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  def create
    @item = Item.new(item_params)
    @item.generic_item_id = params[:item][:generic_id] 
    @item.user = @current_user
    @item.medical_unit = @current_medical_unit
    # @item.department = @department
    if @item.save
      render status: :ok, json: @item
    else
      render status: :unprocessable_entity, json: { message: @item.errors.full_messages }
    end
  end

  def fetch_items_by_dept
    @items =  Item.where("department_id = ? AND status =?", params[:department_id], params[:status])
    if @items.present?
      render status: :ok, json: @items
    else
      render status: :not_found, json: { message: 'Record not found' }
    end
  end

  def search
    items = Item.arel_table
    possible_items = Item.where(items[:product_name].matches("#{params[:query]}%")).where(items[:status].eq("Open"))
    if possible_items.present?
      render status: :ok, json: possible_items
    else
      render status: :not_found, json: { message: "Record not found" }
    end
  end

  def fetch_total_medicine_dispensed
    lab_count =  $redis.get('all_medicines_count')
    if lab_count.present?
      render status: :ok, json: lab_count
    else
      render status: :not_found, json: { message: "Record not found" }
    end
  end

  private

  def item_params
    params.require(:item).permit(:item_name, :item_code, :item_type, :main_group, :uom,
                                 :upc, :price, :sub_group, :strength, :quantity_in_hand, :product_name, :status,
                                 :pack, :unit, :pack_size)
  end

end

