# every 2.day, :at => '12:01 am' do
#   rake "update_to_inactive"
# end

# every 15.minutes do # Many shortcuts available: :hour, :day, :month, :year, :reboot
#   rake 'update_lab_qrcode_to_vcard'
# end

# every 10.minutes do # Many shortcuts available: :hour, :day, :month, :year, :reboot
#   rake 'data:import_patients_csv_from_s3'
# end

# every 15.minutes do # Many shortcuts available: :hour, :day, :month, :year, :reboot
#   rake 'data:import_patients_data_from_csv'
# end

every 10.minutes do
  # rake "reset_lab_sequence"
  rake 'data:set_emr_count'
  rake 'data:set_total_today_visit_count'
  rake 'data:set_opd_count'
  rake 'data:set_all_visit_count'
  rake 'data:set_all_medicines_count'
  rake 'data:set_all_patients_count'
  rake 'data:set_all_lab_tests_count'

end

every 54.minutes do
  rake 'data:refresh_mat_views'
end

every 1.day, :at => '07:01 am' do
  rake "data:update_history_tables"
end

