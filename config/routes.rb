Rails.application.routes.draw do

  devise_for :users
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  root 'home#index'

  match '*any' => 'application#options', :via => [:options]
  match '/activeadmindepartment',to: 'home#get_departments', via: 'get'
  match '/activeadminward',to: 'home#get_wards', via: 'get'
  match '/activeadminwardinmedicalunit',to: 'home#get_wards_in_medical_unit', via: 'get'
  match '/activeadminbayinward',to: 'home#get_bays_in_ward', via: 'get'
  match '/activeadminlabinvestigationsinmedicalunit',to: 'home#get_labInvestigations_in_medicalUnit', via: 'get'
  
  namespace :api, constraints: { format: 'json' } do
    namespace :v3 do
      resources :lab_tests do
        collection do
          get 'parameters_ranges'
          get 'get_test_paramsters'
        end
      end
      resources :departments, only:[:index] do
        collection do
          get 'fetch_doctors'
        end
      end
      resources :users do
        collection do
          get 'user_default_screen_wise'
        end
      end
      resources :admissions, only:[ :create]
      resources :patient_diagnoses, only:[ :create, :index]
      resources :medicine_requests, only:[ :create, :index]
      resources :ward_comments, only:[:index]
      resources :bed_allocations do
        collection do
          put 'update_ward'
          get 'fetch_bed_allocations_ward_wise'
          get 'fetch_beds_ward_wise'
          get 'fetch_allocated_beds_ward_wise'
        end
      end
      resources :wards do
        collection do
          get 'get_hospital_wards'
          get 'fetchWardById'
        end
      end
      resources :bays, only: [:index]
      resources :discharge_patients, only: [:create]
      resources :investigation_notes, only: [:create, :index]
      resources :beds do
        collection do
          get 'get_count'
        end
      end
      resources :lab_results do
        collection do
          get 'result_parameters'
          put 'update_result_parameters'
          get 'machine_result_parameters'
        end
      end
      resources :reports, only: [:item_stock] do
        collection do
          get 'medicine_list'
          get 'prescriptions'
          get 'issue_stock'
          get 'current_stock'
          get 'stock_balance'
          get 'user_patients'
          get 'patient_registration'
          get 'new_medicine_list'
          get 'new_prescriptions'
          get 'new_stock_balance'
          get "lab_report"
        end
      end
      resources :orders do
        member do
          get 'generate_order_pdf', format: 'pdf'
          get 'lab_payment_receipt', format: 'pdf'
          get 'generate_order_pdf_attachment', format: 'pdf'
          get 'lab_result_slip', format: 'pdf'
        end
        collection do
           put 'update_lab_test_status'
           get 'get_order_by_mrn'
           get 'fetch_total_lab_tests'
           # get 'print'
        end
      end
      resources :patients do
        member do
          get 'generate_prescription_slip_pdf', format: 'pdf'
          get 'print'
          # get 'emergency_slip'
          get 'adult_emergency_slip', format: 'pdf'
          get 'adult_opd_slip', format: 'pdf'
          get 'admission_slip', format: 'pdf'
          get 'print_health_card', format: 'pdf'
          get 'print_wrist_band', format: 'pdf'
          get 'non_adult_emr_slip', format: 'pdf'
          get 'prescription_slip', format: 'pdf'
        end
        collection do
           get 'get_patients_by_type'
           get 'patient_activity'
           get 'csv_data'
           get 'unknown_patients'
           get 'get_patient_by_medicnes'
           get 'fetch_admitted_patients'
           get 'fetch_opd_patients'
           get 'fetch_all_patients'
           get 'fetch_all_visits'
           get 'fetch_all_visits_by_today'
           get 'today_opd'
           get 'today_emergency'
        end
      end
      resources :items do
        collection do
           get 'fetch_items_by_dept'
           get 'search'
           get 'fetch_total_medicine_dispensed'
        end
      end
      resources :item_stocks do
        collection do
           put 'issue_item'
           put 'department_issue_item'
           get 'get_stock_item_from_store'
           get 'fetch_item_by_store'
           get 'fetch_items_by_store'
           get 'fetch_exiting_stock_in_store'
        end
      end
      resources :generic_items do
        collection do
           get 'search'
        end
      end
    end
    namespace :v2 do
      resources :orders do
        resources :lab_order_details
      end
    end
    namespace :v1 do
      devise_scope :user do
        # post 'registrations' => 'registrations#create', :as => 'register'
        post 'sessions' => 'sessions#create', :as => 'login'
        delete 'sessions' => 'sessions#destroy', :as => 'logout'
        # get 'recover_password' => 'registrations#recover_password', :as => 'recover_password'
      end
      resources :regions
      resources :services
      resources :call_requests
      resources :medical_units do
        collection do
          get :user_assigned_medical_units
        end
      end
      resources :departments, only:[:index]
      resources :visit_users
      resources :reports do
        collection do
          get :generate_visits_stats
          get :generate_staff_performance_stats
          get :generate_inventories_stats
          get :generate_vitals_stats
          get :generate_new_patient_stats
          get :generate_order_performance_stats
          get :generate_patient_waiting_time_stats
          get :generate_order_for_patients_stats
          get :generate_not_applicable_order_stats
          get :generate_doctor_performance_stats
          get :generate_no_of_patient_per_doctor_stats
          get :generate_inventory_stats
          get :generate_total_registered_patients_count_category_wise
          get :generate_total_registered_patients_count
          get :generate_total_number_patients_referred_to
          get :generate_top_five_allergies_list
          get :generate_given_allergy_patients_list
          get :generate_top_five_patient_diagnosis_list
          get :generate_given_diagnosis_patients_list
          get :generate_top_ten_given_ordered
          get :generate_total_number_of_patients_seen
          get :generate_total_visit_count_category_wise
          get :generate_avg_patient_LOS_in_a_dept
          get :generate_avg_patient_LOS_in_a_given_dept
          get :generate_total_medicine_list
          get :generate_total_order_count
          get :generate_issued_medicine
          get :generate_patient_summary
          get :generate_total_visit_count_ref_to
          get :get_dashboard_url
        end
      end

      resources :users do
        collection do
          post 'forgot_username'
          post 'forgot_password'
          post 'set_preference'
          delete 'delete_preference'
          get 'hospital_doctors'
          get 'user_with_role'
          get 'logged_in_doctors'
          put 'edit_user'
          post 'change_password'
        end
      end
      resources :icd_codes do
        collection do
          get 'search'
        end
      end
      resources :brands do
        collection do
          get 'search'
        end
      end
      resources :drugs do
        collection do
          get 'search'
        end
      end
      resources :brand_drugs do
        collection do
          get 'search'
          get 'alternative'
        end
      end
      resources :inventories do
        collection do
          get 'search'
          post 'update_inventories'
        end
      end
      resources :line_items do
        collection do
          get 'all_batches'
          put 'update_batches'
        end
      end
      resources :pictures
      resources :devices
      resources :lookups
      resources :patient_histories
      resources :rosters
      resources :leaves
      resources :orders do
        collection do
          post 'radiology_order'
          get 'order_logs'
          post 'nursing_order'
          post 'procedure_order'
          post 'lab_order'
          get 'search_admission_patients'
          get 'search_consultation_patients'
          get 'lab_history'
          get 'lab_history_for_patient'
          get 'radiology_history'
          get 'orders_history'
          get 'order_barcode', format: 'pdf'
        end   
        resources :order_items
        resources :add_admissions
        resources :lab_order_details
        resources :radiology_order_details
        resources :order_logs
      end
      resources :lab_test_details
      resources :patients do
        collection do
          put 'toggle_active'
          get 'favorite_patients'
        end
        resources :visits
        resources :allergies
        resources :vitals
        resources :home_medications do
          collection do
            get 'last_medication'
          end
        end
        resources :immunizations
        resources :vaccinations
        resources :patient_past_medical_histories
        resources :patient_family_medical_histories
        resources :follow_ups
        resources :patient_diagnoses do
          collection do
            get 'logs'
          end
        end
      end
      resources :complaints do
        resources :feedbacks
        resources :favorite_complaints, only: [:add, :remove] do
          collection do
            post 'add'
            delete 'remove'
          end
        end
      end
      resources :notes do
        collection do
          get 'previous_note'
          get 'logs'
        end
      end

      resources :data_files do
        collection do
          post 'upload'
        end
      end

    end
  end
end
