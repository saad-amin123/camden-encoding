# == Schema Information
#
# Table name: patients
#
#  id                    :integer          not null, primary key
#  registered_by         :integer
#  registered_at         :integer
#  mrn                   :string
#  first_name            :string
#  last_name             :string
#  middle_name           :string
#  gender                :string
#  education             :string
#  birth_day             :integer
#  birth_month           :integer
#  birth_year            :integer
#  patient_nic           :string
#  patient_passport      :string
#  unidentify_patient    :boolean          default("false")
#  guardian_relationship :string
#  guardian_first_name   :string
#  guardian_last_name    :string
#  guardian_middle_name  :string
#  guardian_nic          :string
#  guardian_passport     :string
#  unidentify_guardian   :boolean          default("false")
#  state                 :string
#  city                  :string
#  near_by_city          :string
#  address1              :string
#  address2              :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  phone1                :string
#  phone1_type           :string
#  phone2                :string
#  phone2_type           :string
#  phone3                :string
#  phone3_type           :string
#  marital_status        :string
#  blood_group           :string
#  hiv                   :string
#  hepatitis_b_antigens  :string
#  hepatitis_c           :string
#  age                   :string
#  father_name           :string
#  unidentify_gender     :boolean          default("false")
#  guardian_state        :string
#  guardian_city         :string
#  guardian_near_by_city :string
#  guardian_address      :string
#  guardian_phone_type   :string
#  guardian_phone        :string
#  barcode_url           :string
#  qrcode_url            :string
#  unidentify            :boolean          default("false")
#  external_patient_id   :string
#

require 'test_helper'

class PatientTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
